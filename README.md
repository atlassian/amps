# Welcome to the Atlassian AMPS Source!

## Quick Links

* [How to Release][2]
* [Issues][1]
* [How to Change Default Product/Data Version][3]
* [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AMPS)

## Using AMPS and the Plugin SDK

Please consult our [Getting Started](https://developer.atlassian.com/display/DOCS/Getting+Started) documentation to learn how to build Atlassian Plugins. We also have a [quick-start tutorial](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) for those who want to get stuck straight in to it.

## Reporting a Problem

If you're having problems with AMPS, please go to [Atlassian Developer Ecosystem Community](https://developer.atlassian.com/community/), our Q&A community site for the Atlassian Ecosystem, and ask for help. To report a bug, use the [AMPS Jira Project][1]. If you're not sure if you've hit a bug, or just a usage problem, try the community site first!

## Contributing

This project is licensed under the [Apache 2.0 Software License](https://bitbucket.org/atlassian/amps/src/master/LICENSE). External contributors are welcome, but you must agree to the terms of the [Atlassian Contributor License Agreement](https://developer.atlassian.com/platform/open-source/contributor-license-agreement/) and then sign and submit a copy of the agreement to Atlassian.

### Rules of Engagement

If you want to submit bug fixes or improvements to AMPS, please assist us by adhering to these simple guidelines:

1. Do not commit directly to the primary branch. Please use a 'feature branch' from the main branch and then create a Pull Request for your changes to be accepted. The Server Ecosystem Engineering team will review and merge your work.
2. Create a Jira Issue in the [AMPS Jira Project][1] for any changes and prefix your commit messages with the relevant issue key.
3. You are responsible for testing and verifying your own changes. Yes, AMPS is a complex beast that must run on a variety of platforms against a variety of Atlassian products and must remain very backwards compatible and this means making changes can be difficult - please be diligent in this and help make life better for everyone who has to work on this project! :)

#### Releasing new minor version

1. Contact ecosystem team on [#dc-core-ecosystem-team](https://atlassian.enterprise.slack.com/archives/C060NK3V3M4) and get an approval from the team member
2. Make sure that previous minor branch is synced to master (new release is cut from master branch)
3. Make sure that changelog reflects the changes that are going to be released
4. Run https://ecosystem-bamboo.internal.atlassian.com/browse/AMPS-AMPSCREATERELEASEBRANCH with build variable `release.createBranch.version` set to the next version following the pattern `<major>.<minor>.x`
5. Create a Bamboo plan for a new release branch: [example](https://bitbucket.org/atlassian/build-specs/pull-requests/1515)
6. Update release documentation: [example](https://bitbucket.org/atlassian-developers/atlassian-sdk-docs/pull-requests/256/diff)


Atlassian developers, please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe).


[1]: https://ecosystem.atlassian.net/browse/AMPS
[2]: https://extranet.atlassian.com/pages/viewpage.action?pageId=2130217184
[3]: https://extranet.atlassian.com/display/DEVNET/HOWTO%3A+Change+the+default+product+version+generated+by+AMPS

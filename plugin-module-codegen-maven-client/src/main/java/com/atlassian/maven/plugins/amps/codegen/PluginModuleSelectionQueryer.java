package com.atlassian.maven.plugins.amps.codegen;

import java.util.Map;

import org.codehaus.plexus.components.interactivity.PrompterException;

import com.atlassian.plugins.codegen.modules.PluginModuleCreator;

/** @since 3.6 */
public interface PluginModuleSelectionQueryer {
    PluginModuleCreator selectModule(Map<Class, PluginModuleCreator> map) throws PrompterException;

    boolean addAnotherModule() throws PrompterException;
}

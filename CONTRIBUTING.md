# Contributing

## Do you want to add a new feature / bugfix? Follow these steps:

1. Create a ticket in the public [JIRA](https://ecosystem.atlassian.net/jira/software/c/projects/AMPS/issues?jql=project%20%3D%20%22AMPS%22%20ORDER%20BY%20created%20DESC) project. The ticket's title will be used for crafting release notes later. Make the status of the ticket **"In Progress"**
2. Notify DC Core Ecosystem team on [#dc-core-ecosystem-team](https://atlassian.enterprise.slack.com/archives/C060NK3V3M4) to keep us in the loop.
3. Create a feature / bugfix branch from the master branch. Do not commit directly to the master branch. Branch names are important as not following name convention will cause bamboo to not start check build on your PR.
- **feature/JIRA_TICKET/...** - for feature branches
- **issue/JIRA_TICKET/...** - for issue branches
- **bugfix/JIRA_TICKET/...** - for bugfix branches
- **renovate/...** - for renovate branches
4. Make the change you need to make.
5. Test your changes locally by running `mvn clean verify` in the root directory.
6. Rise a PR from your branch to master. In the PR description add a link to your ticket. Verify that the PR is passing the build (check the bamboo build status for failed tests, sometimes rerun of failed jobs helps).
7. Once it’s approved by at least one team member, merge it and mark your JIRA ticket status as **"Waiting for release"**.

## Release process
Every Wednesday we are releasing a new version of AMPS. The release process is as follows:
1. We check the [JIRA](https://ecosystem.atlassian.net/jira/software/c/projects/AMPS/issues?jql=project%20%3D%20%22AMPS%22%20ORDER%20BY%20created%20DESC) project for tickets marked as **"Waiting for release"**.
2. If there are any tickets:
    - we update the tickets with the "DONE" status
    - we create a new release from the master branch. The release version is the version from pom.xml.

### The world is on fire and we need to release a hotfix sooner?
If you need to release a hotfix sooner, please follow the steps:
1. Notify DC Core Ecosystem team on [#dc-core-ecosystem-team](https://atlassian.enterprise.slack.com/archives/C060NK3V3M4) to keep us in the loop.
2. Follow the steps from the **Release process** section but instead of waiting for Wednesday, release the hotfix as soon as possible.

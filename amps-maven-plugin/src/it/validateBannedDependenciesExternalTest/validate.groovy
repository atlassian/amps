def buildLogFile = new File(basedir, "build.log")
def lines = buildLogFile.readLines()

def bannedDependenciesCount = lines.count{it.startsWith("Found Banned Dependency")}
assert bannedDependenciesCount == 0: "Found one or more banned dependencies in the build log file, which was not expected"

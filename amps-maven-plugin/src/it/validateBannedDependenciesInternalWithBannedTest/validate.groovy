def buildLogFile = new File(basedir, "build.log")
def lines = buildLogFile.readLines()

def dependency = "org.slf4j:slf4j-api:jar:2.0.12"
assert lines.any{it.startsWith("Found Banned Dependency: ${dependency}")}: "Banned dependency ${dependency} has not been found in the build log file"

import groovy.json.JsonSlurper

def associationFile = new File("$basedir/target/classes/META-INF/fe-manifest-associations/fe-manifest-association.json")
assert associationFile.exists()

def jsonSlurper = new JsonSlurper()
def associationMapping = jsonSlurper.parse(associationFile)

def expectedPackageJsonPath = "META-INF/fe-module-manifests/src/package.json"
def expectedDeclarations = ["target/classes/META-INF/fe-manifest-associations/test-plugin-webpack.intermediary.json"]

assert associationMapping.keySet().size() == 4

[
    "index.js",
    "index-min.js",
    // webpack-webresource-plugin will produce more than just JavaScript files; even though they're not
    // checked for association mapping, we expect them to be passed along.
    "index-min.js.map",
    "index.js.i18n.properties"
].each { filename ->
    assert associationMapping[filename].manifest == expectedPackageJsonPath
    assert associationMapping[filename].declarations == expectedDeclarations
}

final File buildLog = new File("$basedir/build.log")
assert buildLog.text.contains("[WARNING] Some files exist in output but have not been declared in any manifest association") == false

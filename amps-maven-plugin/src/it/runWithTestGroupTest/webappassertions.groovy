import java.util.Properties

final File ampsFile = new File("${project.build.directory}/amps.properties")
assert ampsFile.exists(), "amps.properties doesn't exists at $ampsFile.absolutePath"

final Properties amps = new Properties();
ampsFile.withInputStream { amps.load(it) }

def getStatusCode(endpoint) {
    def url = new URL(endpoint)
    def response = url.openConnection()
    return response.getResponseCode()
}

def code1 = getStatusCode("http://localhost:${amps['http.product-1.port']}${amps['context.product-1.path']}")
assert code1 < 400, "Expected status code below 400 on home page of application"

def code2 = getStatusCode("http://localhost:${amps['http.product-2.port']}${amps['context.product-2.path']}")
assert code2 < 400, "Expected status code below 400 on home page of application"

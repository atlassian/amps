package it.com.atlassian.amps;

import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

class AuthorizedTestClientFactory implements TestClientFactory {
    // TODO: This should come from outside most likely as system properties
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";

    @Override
    public TestClient create() {
        CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(new BasicCookieStore()).build();
        String port = System.getProperty("http.port");
        String contextPath = System.getProperty("context.path");
        Credentials credentials =
                Credentials.builder().username(USERNAME).password(PASSWORD).rememberMe(true).build();

        return new AuthroizedTestClient(httpClient, port, contextPath, credentials);
    }
}

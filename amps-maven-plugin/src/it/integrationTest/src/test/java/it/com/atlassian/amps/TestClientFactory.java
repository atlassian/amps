package it.com.atlassian.amps;

interface TestClientFactory {
    TestClient create();
}

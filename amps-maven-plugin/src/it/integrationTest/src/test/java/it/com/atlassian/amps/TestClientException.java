package it.com.atlassian.amps;

import java.io.IOException;

class TestClientException extends IOException {
    TestClientException(String message) {
        super(message);
    }

    TestClientException(String message, Throwable cause) {
        super(message, cause);
    }
}

package it.com.atlassian.amps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

class AuthroizedTestClient implements TestClient {
    private final CloseableHttpClient httpClient;
    private final String port;
    private final String contextPath;
    private final Credentials credentials;

    AuthroizedTestClient(CloseableHttpClient httpClient, String port, String contextPath, Credentials credentials) {
        this.httpClient = httpClient;
        this.port = port;
        this.contextPath = contextPath;
        this.credentials = credentials;
    }

    @Override
    public CloseableHttpResponse get(String resourcePath) throws TestClientException {
        CloseableHttpResponse response = executeResourceRequest(resourcePath);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_FORBIDDEN) {
            return response;
        }

        // Some products require us to login to in order to access servlets
        // on the other hand, some products do not even have login page so first we try to access
        // and login only if the initial request fails with forbidden
        login();
        return executeResourceRequest(resourcePath);
    }

    @Override
    public void close() throws IOException {
        httpClient.close();
    }

    private CloseableHttpResponse executeResourceRequest(String resourcePath) throws TestClientException {
        try {
            return httpClient.execute(new HttpGet(createUrl(resourcePath)));
        } catch (IOException e) {
            String message = String.format("Authorized request to '%s' has failed", resourcePath);
            throw new TestClientException(message, e);
        }
    }

    private void login() throws TestClientException {
        int statusCode;

        try (CloseableHttpResponse response = httpClient.execute(createLoginRequest())) {
            statusCode = response.getStatusLine().getStatusCode();
        } catch (IOException e) {
            throw new TestClientException("Login request has failed", e);
        }

        if (statusCode != HttpStatus.SC_OK) {
            String message = String.format("Status code of response of the login request was %d", statusCode);
            throw new TestClientException(message);
        }
    }

    private String createUrl(String resourcePath) {
        return String.format("http://localhost:%s/%s/%s", port, contextPath, resourcePath);
    }

    private HttpUriRequest createLoginRequest() throws JsonProcessingException {
        HttpPost request = new HttpPost(createUrl("rest/security/login"));
        request.setHeader("X-Atlassian-Token", "no-check");

        ObjectMapper mapper = new ObjectMapper();
        StringEntity payload =
                new StringEntity(mapper.writeValueAsString(credentials), ContentType.APPLICATION_JSON);
        request.setEntity(payload);

        return request;
    }
}

package it.com.atlassian.amps;

import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.Closeable;

interface TestClient extends Closeable {
    CloseableHttpResponse get(String resourcePath) throws TestClientException;
}

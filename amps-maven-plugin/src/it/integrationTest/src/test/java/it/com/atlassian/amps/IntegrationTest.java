package it.com.atlassian.amps;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegrationTest {
    @Test
    public void anIntegrationTest() throws Exception {
        assertPresent("/plugins/servlet/it");
    }

    @Test
    public void anIntegrationTestForTestPlugin() throws Exception {
        assertPresent("/plugins/servlet/it-tests");
    }

    @Test
    public void libArtifactsAreDeployed() throws Exception {
        assertPresent("/plugins/servlet/dep");
    }

    private void assertPresent(String resourceUrl) throws Exception {
        TestClientFactory clientFactory = new AuthorizedTestClientFactory();

        int statusCode;
        try (TestClient client = clientFactory.create(); CloseableHttpResponse response = client.get(resourceUrl)) {
            statusCode = response.getStatusLine().getStatusCode();
        }

        assertEquals("Should have 200: " + resourceUrl, 200, statusCode);
    }
}

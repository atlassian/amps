package it.com.atlassian.amps;

import com.fasterxml.jackson.annotation.JsonProperty;

class Credentials {
    private final String username;
    private final String password;
    private final boolean rememberMe;

    @JsonProperty
    String username() {
        return username;
    }

    @JsonProperty
    String password() {
        return password;
    }

    @JsonProperty
    boolean rememberMe() {
        return rememberMe;
    }

    Credentials(String username, String password, boolean rememberMe) {
        this.username = username;
        this.password = password;
        this.rememberMe = rememberMe;
    }

    static Builder builder() {
        return new Builder();
    }

    static class Builder {
        private String username;
        private String password;
        private boolean rememberMe;

        Builder username(String username) {
            this.username = username;
            return this;
        }

        Builder password(String password) {
            this.password = password;
            return this;
        }

        Builder rememberMe(boolean rememberMe) {
            this.rememberMe = rememberMe;
            return this;
        }

        Credentials build() {
            return new Credentials(username, password, rememberMe);
        }
    }
}

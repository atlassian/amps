import java.util.Properties

final File ampsFile = new File("${project.build.directory}/amps.properties")

assert ampsFile.exists(), "amps.properties doesn't exists at ${ampsFile.absolutePath}"

final Properties amps = new Properties();
ampsFile.withInputStream { amps.load(it) }

// Check if the applicaion is running
def url = new URL("http://localhost:${amps['http.port']}${amps['context.path']}")
def response = url.openConnection()
assert response.getResponseCode() == 200, "Expected status code 200 on home page of application"

// Check debug port
Socket socket = null;
final def debugPort = Integer.valueOf(amps['debug.port'])
try
{
    socket = new Socket('localhost', debugPort);
}
catch (final IOException e)
{
    assert false, "We should have been able to open a socket to ${debugPort}!"
}
finally
{
    socket?.close();
}

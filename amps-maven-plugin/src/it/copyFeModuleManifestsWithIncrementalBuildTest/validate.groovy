import net.lingala.zip4j.core.ZipFile

final File buildLog = new File((String) basedir, "build.log")
assert buildLog.text.contains("[WARNING] File <$basedir/target/classes/META-INF/fe-module-manifests/package.json> already exists and is going to be overwritten.")

final def testJar = new File("$basedir/target/testjar.jar")
assert testJar.exists(), "Test jar should exist at $testJar.absolutePath"

// unzip the manifest
final ZipFile testJarCompressed = new ZipFile("$basedir/target/testjar.jar")
testJarCompressed.extractAll("$basedir/extract")

final def packageFile1 = new File("$basedir/extract/META-INF/fe-module-manifests/package.json")
assert packageFile1.exists()

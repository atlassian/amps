def product = binding.product
product = 'amps'.equals(product) ? 'refapp' : product

def integrationSurefireReportsDir = new File(basedir, "target/surefire-reports")
assert !integrationSurefireReportsDir.listFiles().any { it.name.contains('foo-tomcat85x') }, "Integration test should not run"

final File buildLog = new File(basedir, "build.log")
assert buildLog.text.contains("Test group 'foo' does not exist")

import net.lingala.zip4j.core.ZipFile

final def testJar = new File("$basedir/target/testjar.jar")
assert testJar.exists(), "Test jar should exist at $testJar.absolutePath"

// unzip the manifest
final ZipFile testJarCompressed = new ZipFile("$basedir/target/testjar.jar")
testJarCompressed.extractAll("$basedir/extract")

final def packageFile1 = new File("$basedir/extract/META-INF/fe-module-manifests/package.json")
assert !packageFile1.exists()


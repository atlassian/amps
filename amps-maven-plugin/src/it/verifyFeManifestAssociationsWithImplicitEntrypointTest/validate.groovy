final File buildLog = new File("$basedir/build.log")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleJar/target/moduleJar-testing.jar")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleWar/target/moduleWar-testing.war")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/modulePlugin/target/modulePlugin-testing.obr")

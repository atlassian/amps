final File buildLog = new File((String) basedir, "build.log")
assert buildLog.text.contains("Only [package.json, package-lock.json, yarn.lock] files are supported, not $basedir/src/main/resources/README.txt")

final def testJar = new File("$basedir/target/testjar.jar")
assert !testJar.exists()


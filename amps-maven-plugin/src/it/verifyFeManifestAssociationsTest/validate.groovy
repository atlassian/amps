final File buildLog = new File("$basedir/build.log")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp\n" +
        "[WARNING] No declarations provided for the module!\n" +
        "[ERROR] Undeclared files detected! First check that they are not left over from switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check them for third-party code (all 3rd-party code must be installed and bundled by using the package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\nThe undeclared files were : [\n" +
        "\tconsole/js/index.js\n" +
        "]")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp/WEB-INF/classes/moduleObr-testing.obr/moduleObr-testing.jar\n" +
        "[ERROR] Undeclared files detected! First check that they are not left over from switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check them for third-party code (all 3rd-party code must be installed and bundled by using the package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\nThe undeclared files were : [\n" +
        "\tindex-min.js,\n" +
        "\tindex2-min.js,\n" +
        "\tindex2.js\n" +
        "]")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp/WEB-INF/classes/bundled-plugins.zip/moduleJar-testing.jar\n" +
        "[WARNING] No declarations provided for the module!\n" +
        "[ERROR] Undeclared files detected! First check that they are not left over from switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check them for third-party code (all 3rd-party code must be installed and bundled by using the package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\nThe undeclared files were : [\n" +
        "\tindex.js\n" +
        "]")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp/WEB-INF/classes/moduleObr-testing.obr/moduleObr-testing.jar/META-INF/lib/moduleJar-testing.jar\n" +
        "[WARNING] No declarations provided for the module!\n" +
        "[ERROR] Undeclared files detected! First check that they are not left over from switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check them for third-party code (all 3rd-party code must be installed and bundled by using the package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\nThe undeclared files were : [\n" +
        "\tindex.js\n" +
        "]")

// There should info log about verifying directory but there should be no warnings or errors following it
assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp/WEB-INF/classes/bundled-plugins.zip/moduleJar2-testing.jar")
assert !buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp/WEB-INF/classes/bundled-plugins.zip/moduleJar2-testing.jar\n" +
        "[WARNING]")
assert !buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleTarGz/target/moduleTarGz-testing-test-distribution.tar.gz/webapp/WEB-INF/classes/bundled-plugins.zip/moduleJar2-testing.jar\n" +
        "[ERROR]")

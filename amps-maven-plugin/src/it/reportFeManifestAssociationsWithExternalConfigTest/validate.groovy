import groovy.json.JsonSlurper

def associationFile = new File("$basedir/target/classes/META-INF/fe-manifest-associations/fe-manifest-association.json")
assert associationFile.exists()

def jsonSlurper = new JsonSlurper()
def associationMapping = jsonSlurper.parse(associationFile)

assert associationMapping.keySet().size() == 1
assert associationMapping["file1.js"].manifest == "META-INF/fe-module-manifests/package.json"
assert associationMapping["file1.js"].declarations == ["fe-files-association.json"]

final File buildLog = new File("$basedir/build.log")
assert buildLog.text.contains("[WARNING] Some files exist in output but have not been declared in any manifest association") == false

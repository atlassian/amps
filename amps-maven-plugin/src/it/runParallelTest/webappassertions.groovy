import java.util.Properties

final File ampsFile = new File("${project.build.directory}/amps.properties")

assert ampsFile.exists(), "amps.properties doesn't exists at ${ampsFile.absolutePath}"

final Properties amps = new Properties();
ampsFile.withInputStream { amps.load(it) }

def url = new URL("http://localhost:${amps['http.port']}${amps['context.path']}")
def step = System.getProperty("step", "start")

// This script is supposed to be executed twice. The first time after `amps:run` and the second time after `amps:stop`
switch (step) {
    case "start":
        def response = url.openConnection()
        assert response.getResponseCode() == 200, "Expected status code 200 on home page of application"
        System.setProperty("step", "stop") // Next step will be stop
        break
    case "stop":
        def exception = null
        try {
            url.openConnection().getResponseCode()
        } catch (Exception e) {
            exception = e
        }
        assert exception != null, "Expected connection ${url} to fail after amps:stop"
        break
    default:
        assert false, "A wrong value was passed to it/run/webappassertions.groovy: -Dstep=${step}"
}

import groovy.json.JsonSlurper

final File buildLog = new File("$basedir/build.log")
assert buildLog.exists()

testDir = "$basedir/target/its/verifyFeManifestAssociationsWithExternalDepsContainingJsTest"

def expectedMessages = []
expectedMessages.add(
        "[INFO] Verifying frontend manifest associations for an artifact: $testDir/moduleB-unverified/target/moduleB-unverified-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleB-unverified/target/moduleB-unverified-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleB-unverified/target/moduleB-unverified-testing.jar/META-INF/lib/moduleA-testing.jar\n" +
        "[WARNING] No declarations provided for the module!\n" +
        "[ERROR] Undeclared files detected! First check that they are not left over from switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check them for third-party code (all 3rd-party code must be installed and bundled by using the package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\nThe undeclared files were : [\n" +
        "\tindex.js\n" +
        "]\n" +
        "[INFO] Verifying directory: $testDir/moduleB-unverified/target/moduleB-unverified-testing.jar/META-INF/lib/rhino-1.7.15.jar\n" +
        "[WARNING] No declarations provided for the module!\n" +
        "[ERROR] Undeclared files detected! First check that they are not left over from switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check them for third-party code (all 3rd-party code must be installed and bundled by using the package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\nThe undeclared files were : [\n" +
        "\torg/mozilla/javascript/tools/debugger/test.js\n" +
        "]")
expectedMessages.add(
        "[INFO] Verifying frontend manifest associations for an artifact: $testDir/moduleB-verified/target/moduleB-verified-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleB-verified/target/moduleB-verified-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleB-verified/target/moduleB-verified-testing.jar/META-INF/lib/moduleA-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleB-verified/target/moduleB-verified-testing.jar/META-INF/lib/rhino-1.7.15.jar\n"
)
expectedMessages.add(
        "[INFO] Verifying frontend manifest associations for an artifact: $testDir/moduleC/target/moduleC-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleC/target/moduleC-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleC/target/moduleC-testing.jar/META-INF/lib/moduleA-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleC/target/moduleC-testing.jar/META-INF/lib/rhino-1.7.15.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleC/target/moduleC-testing.jar/META-INF/lib/moduleB-verified-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleC/target/moduleC-testing.jar/META-INF/lib/moduleB-verified-testing.jar/META-INF/lib/moduleA-testing.jar\n" +
        "[INFO] Verifying directory: $testDir/moduleC/target/moduleC-testing.jar/META-INF/lib/moduleB-verified-testing.jar/META-INF/lib/rhino-1.7.15.jar\n" +
        "[INFO] ------------------------------------------------------------------------\n" +
        "[INFO] BUILD SUCCESS\n" +
        "[INFO] ------------------------------------------------------------------------\n")

for (String expectedMessage in expectedMessages) {
    println "Checking for message:\n######\n$expectedMessage\n######\n"
    assert buildLog.text.contains(expectedMessage)
}

def externalDependenciesFile = new File("$basedir/moduleB-verified/target/classes/META-INF/fe-manifest-associations/fe-external-dependencies-with-js-files.json")
assert externalDependenciesFile.exists()

def externalDependencies = new JsonSlurper().parseText(externalDependenciesFile.text)
assert externalDependencies.size() == 2
assert externalDependencies["META-INF/lib/moduleA-testing.jar"] == ["index.js"]
assert externalDependencies["META-INF/lib/rhino-1.7.15.jar"] == ["org/mozilla/javascript/tools/debugger/test.js"]

import java.util.Properties

final File ampsFile = new File("${project.build.directory}/amps.properties")

assert ampsFile.exists(), "amps.properties doesn't exists at $ampsFile.absolutePath"

final Properties amps = new Properties()
ampsFile.withInputStream { amps.load(it) }

// Check if the applicaion is running
def url = new URL("http://localhost:${amps['http.port']}${amps['context.path']}")
def response = url.openConnection()
assert response.getResponseCode() == 200, "Expected status code 200 on home page of application"

import java.util.Properties
import groovy.json.JsonSlurper

final File ampsFile = new File("${project.build.directory}/amps.properties")
assert ampsFile.exists(), "amps.properties doesn't exists at $ampsFile.absolutePath"

final Properties amps = new Properties();
ampsFile.withInputStream { amps.load(it) }

def url = new URL("http://localhost:${amps['http.port']}${amps['context.path']}/plugins/servlet/it")
def response = url.openConnection()

assert response.getResponseCode() == 200, "Expected status code 200"

def content = response.getInputStream().getText()
def data = new JsonSlurper().parseText(content)

assert data.containsKey("plugin.resource.directories")

final File buildLog = new File("$basedir/build.log")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleAtlassianPlugin/target/moduleAtlassianPlugin-testing.obr")

assert buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleAtlassianPluginNoObr/target/moduleAtlassianPluginNoObr-testing.jar")

assert !buildLog.text.contains("[INFO] Verifying directory: $basedir/moduleVendorPlugin/target/moduleVendorPlugin-testing.obr")


import java.nio.file.Files
import java.nio.file.Paths

def associationsFolderPath = Paths.get(basedir.toString(), 'moduleConsumer/target/classes/META-INF/fe-manifest-associations')
assert !Files.list(associationsFolderPath).findAny().isPresent()

def manifestsFolderPath = Paths.get(basedir.toString(), 'moduleConsumer/target/classes/META-INF/fe-module-manifests')
assert !Files.list(manifestsFolderPath).findAny().isPresent()
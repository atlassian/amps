def reportsDir = new File(basedir, "target/surefire-reports")
assert reportsDir.exists(), "Reports directory does not exist"

// Check that all files in reportsDir have "-anAwesomeSuffix". This suffix is defined in pom.xml file.
def files = reportsDir.listFiles().findAll{it.name.endsWith('.txt') || it.name.endsWith('.xml')}
def suffixedFiles = files.findAll{it.name.contains('-anAwesomeSuffix')}
assert !suffixedFiles.isEmpty(), "Integration tests reports do not have the expected suffix"
import net.lingala.zip4j.core.ZipFile

final def testJar = new File("$basedir/target/testjar.jar")
assert testJar.exists(), "Test jar should exist at $testJar.absolutePath"

// unzip the manifest
final ZipFile testJarCompressed = new ZipFile("$basedir/target/testjar.jar")
testJarCompressed.extractAll("$basedir/extract")

final def packageFile1 = new File("$basedir/extract/META-INF/fe-module-manifests/package.json")
assert packageFile1.exists()

// src/package.json is not included in plugin configuration - should not be present in artifact
final def packageFile2 = new File("$basedir/extract/META-INF/fe-module-manifests/src/package.json")
assert !packageFile2.exists()

final def packageLockFile = new File("$basedir/extract/META-INF/fe-module-manifests/src/package-lock.json")
assert packageLockFile.exists()


final def packageFile3 = new File("$basedir/extract/META-INF/fe-module-manifests/src/frontend/package.json")
assert packageFile3.exists()

final def yarnLockFile = new File("$basedir/extract/META-INF/fe-module-manifests/src/frontend/yarn.lock")
assert yarnLockFile.exists()


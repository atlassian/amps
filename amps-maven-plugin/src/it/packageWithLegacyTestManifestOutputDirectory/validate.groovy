def manifestFile = new File(basedir, "target/test-classes/META-INF/MANIFEST.MF")

assert manifestFile.exists()

def lines = manifestFile.readLines()

assert lines.any { it.equals('Export-Package: com.atlassian.bar;version="8.2.9"') }: "Does not include a package exported by production sources"

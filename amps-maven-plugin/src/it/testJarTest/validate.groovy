def product = binding.product
product = 'amps'.equals(product) ? 'refapp' : product

def springXmlFile = new File(basedir, "target/test-classes/META-INF/spring/wired-test-components.xml")
assert springXmlFile.exists(), "wired-test-components.xml file has not been created"

assert springXmlFile.text.contains('class="it.com.atlassian.amps.test.GreetingServiceTest"'): "wired-test-components.xml does not include definition of GreetingsServiceTest"

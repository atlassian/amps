package com.atlassian.amps.test.api;

public interface GreetingService {
    String getName();
}

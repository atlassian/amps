package com.atlassian.amps.test.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.amps.test.api.GreetingService;

import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService({ GreetingService.class })
@Named("greetingService")
public class GreetingServiceImpl implements GreetingService {
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @Inject
    public GreetingServiceImpl(final ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public String getName() {
        if (null != applicationProperties) {
            return "myComponent:" + applicationProperties.getDisplayName();
        }

        return "myComponent";
    }
}

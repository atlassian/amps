package it.com.atlassian.amps.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.amps.test.api.GreetingService;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class GreetingServiceTest {
    private final ApplicationProperties applicationProperties;
    private final GreetingService greetingService;

    public GreetingServiceTest(ApplicationProperties applicationProperties, GreetingService greetingService) {
        this.applicationProperties = applicationProperties;
        this.greetingService = greetingService;
    }

    @Test
    public void testMyName() {
        assertEquals("names do not match!", "myComponent:" + applicationProperties.getDisplayName(), greetingService.getName());
    }
}

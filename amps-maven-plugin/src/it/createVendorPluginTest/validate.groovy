import groovy.json.JsonSlurper

// This test only covers vendor plugin specific checks. General plugin creation checks are handled in it/createTest

final File projectDir = new File("$basedir/amps-it-create")
assert projectDir.exists(), "The project should have been created under $projectDir"

final File packageJsonFile = new File(projectDir, "package.json")
assert packageJsonFile.exists(), "The package json should be created at $packageJsonFile"

def packageJson = new JsonSlurper().parse(packageJsonFile)
assert packageJson.name == 'amps-it-create-vendor'
assert packageJson.author == null


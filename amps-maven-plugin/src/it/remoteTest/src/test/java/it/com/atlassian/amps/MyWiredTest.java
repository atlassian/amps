package it.com.atlassian.amps;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AtlassianPluginsTestRunner.class)
public class MyWiredTest {

    private final ApplicationProperties applicationProperties;

    public MyWiredTest(final ApplicationProperties applicationProperties) {
        this.applicationProperties = requireNonNull(applicationProperties);
    }

    @Test
    public void productShouldHaveNonBlankDisplayName() {
        // Invoke
        final String displayName = applicationProperties.getDisplayName();

        // Assert
        assertNotNull(displayName);
        assertTrue("Invalid display name '" + displayName + "'", displayName.trim().length() > 0);
        System.out.println(">>>>> " + getClass().getName() + " passed");
    }
}

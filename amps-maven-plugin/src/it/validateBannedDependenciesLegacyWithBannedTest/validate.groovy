def buildLogFile = new File(basedir, "build.log")
def lines = buildLogFile.readLines()

def dependency = "io.atlassian.fugue:fugue:jar:5.0.0"
assert lines.any{it.startsWith("Found Banned Dependency: ${dependency}")}: "Banned dependency ${dependency} has not been found in the build log file"

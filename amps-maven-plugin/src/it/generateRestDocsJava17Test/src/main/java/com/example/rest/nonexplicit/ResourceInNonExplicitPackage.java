package com.example.rest.nonexplicit;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * My REST resource in a package that's not explicitly mentioned in {@code atlassian-plugin.xml}.
 */
@Path("nonexplicit")
public class ResourceInNonExplicitPackage {

    /**
     * Says goodbye.
     *
     * @return a farewell
     */
    @GET
    @Path("message")
    public String goodbye() {
        return "Goodbye.";
    }
}

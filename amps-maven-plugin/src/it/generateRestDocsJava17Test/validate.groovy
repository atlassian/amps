RESOURCE_DOC_XML = "resourcedoc.xml"

/**
 * Returns the directory in which the RestDocsGenerator outputs the files it generates.
 */
File getDestinationDirectory() {
    //noinspection GrUnresolvedAccess - comes from invoker plugin
    new File(basedir, "target/classes")
}

/**
 * Asserts that the given files exist in the {@code target/classes} directory.
 *
 * @param filenames the filenames to check
 */
boolean fileExists(final String filename) {
    return new File(getDestinationDirectory(), filename).exists()
}

/**
 * Asserts that the given REST resource class names appear in resourcedoc.xml.
 *
 * @param classNames the class names to check for
 */
void assertResourceDocXmlContainsRestResources(final String... classNames) {
    def resourceDocXml = new File(getDestinationDirectory(), RESOURCE_DOC_XML).text
    for (final className in classNames) {
        if (!resourceDocXml.contains(className)) {
            throw new AssertionError("No REST resource called " + className)
        }
    }
}

assert !fileExists("application-doc.xml"): "application-doc.xml should not exist in Java 13+"
assert !fileExists("application-grammars.xml"): "application-grammars.xml should not exist in Java 13+"
assert !fileExists(RESOURCE_DOC_XML): "resourcedoc.xml should not exist in Java 13+"

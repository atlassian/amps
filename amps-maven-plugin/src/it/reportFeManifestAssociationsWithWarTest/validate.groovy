import groovy.json.JsonSlurper

def associationFile = new File("$basedir/target/testwar/WEB-INF/classes/META-INF/fe-manifest-associations/fe-manifest-association.json")
assert associationFile.exists()

def jsonSlurper = new JsonSlurper()
def associationMapping = jsonSlurper.parse(associationFile)

assert associationMapping.keySet().size() == 1
assert associationMapping["index.js"].manifest == "META-INF/fe-module-manifests/package.json"
assert associationMapping["index.js"].declarations == ["pom.xml"]

final File buildLog = new File("$basedir/build.log")
assert buildLog.text.find(/\[WARNING\] Cannot verify declared FE associations because the project specifies 'war' packaging./)

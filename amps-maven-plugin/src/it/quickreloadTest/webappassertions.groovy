import java.util.Properties

final File ampsFile = new File("${project.build.directory}/amps.properties")

assert ampsFile.exists(), "amps.properties doesn't exist at ${ampsFile.absolutePath}"

final Properties amps = new Properties();
ampsFile.withInputStream { amps.load(it) }

def url = new URL("http://localhost:${amps['http.port']}${amps['context.path']}/plugins/servlet/qr")
def response = url.openConnection()
assert response.getResponseCode() == 200, "Expected status 200 on QuickReload manage page"

package com.example.rest.explicit;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * My REST resource in a package that's explicitly mentioned in {@code atlassian-plugin.xml}.
 */
@Path("explicit")
public class ResourceInExplicitPackage {

    /**
     * Says hello.
     *
     * @return a greeting
     */
    @GET
    @Path("message")
    public String hello() {
        return "Hello.";
    }
}

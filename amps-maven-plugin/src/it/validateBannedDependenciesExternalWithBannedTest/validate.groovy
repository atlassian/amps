def buildLogFile = new File(basedir, "build.log")
def lines = buildLogFile.readLines()

def dependency = "com.fasterxml.jackson.core:jackson-core:jar:2.16.1"
assert lines.any{it.startsWith("Found Banned Dependency: ${dependency}")}: "Banned dependency ${dependency} has not been found in the build log file"

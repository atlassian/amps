def product = binding.product
product = 'amps'.equals(product) ? 'refapp' : product

def integrationSurefireReportsDir = new File(basedir, "target/surefire-reports")
assert integrationSurefireReportsDir.listFiles().any { it.name.contains('foo-tomcat85x') }, "\"Integration tests should have run and created test reports with suffix foo-tomcat85x"
assert integrationSurefireReportsDir.listFiles().any { it.name.contains('bar-tomcat85x') }, "\"Integration tests should have run and created test reports with suffix bar-tomcat85x"

assert new File(integrationSurefireReportsDir, 'it.com.atlassian.amps.foo.FooIntegrationTest-foo-tomcat85x.txt').exists(), "FooIntegrationTest-foo-tomcat85x.txt file should exist"
assert new File(integrationSurefireReportsDir, 'it.com.atlassian.amps.bar.BarIntegrationTest-bar-tomcat85x.txt').exists(), "BarIntegrationTest-bar-tomcat85x.txt file should exist"
assert !new File(integrationSurefireReportsDir, 'it.com.atlassian.amps.IntegrationTest.txt').exists(), "IntegrationTest.txt file should not exist"

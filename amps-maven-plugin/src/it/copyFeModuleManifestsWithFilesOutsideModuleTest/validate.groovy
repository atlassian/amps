import net.lingala.zip4j.core.ZipFile

final def testJar = new File("$basedir/submodule/target/testjar.jar")
assert testJar.exists(), "Test jar should exist at $testJar.absolutePath"

// unzip the manifest
final ZipFile testJarCompressed = new ZipFile("$basedir/submodule/target/testjar.jar")
testJarCompressed.extractAll("$basedir/extract")

// package.json from root directory
final def packageFile1 = new File("$basedir/extract/META-INF/fe-module-manifests/package.json")
assert packageFile1.exists()

// package.json from submodule directory
final def packageFile2 = new File("$basedir/extract/META-INF/fe-module-manifests/submodule/package.json")
assert packageFile2.exists()
def product = binding.product
product = 'amps'.equals(product) ? 'refapp' : product

def logFile = new File(basedir, "target/jira/home/log/atlassian-jira.log")
assert logFile.exists(), "Integration tests should have run and created test reports in $logFile"

logFile.eachLine {
    if (it.contains('Application Server')) {
        println it
        // The Tomcat version we expect here is the one specified by the amps.product.specific.container
        // property in the Jira POM, at the Jira version specified by the POM in _this_ directory
        assert it.contains('Apache Tomcat/8.5.42')
    }
}

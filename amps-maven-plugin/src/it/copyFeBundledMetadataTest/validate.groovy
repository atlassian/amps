def moduleABundledInModuleBManifest = new File(basedir, 'moduleB/target/classes/META-INF/fe-bundled-metadata/copy-fe-bundled-metadata-module-a-testing-jar/META-INF/fe-module-manifests/package.json')
assert !moduleABundledInModuleBManifest.exists()
def moduleABundledInModuleBAssociation = new File(basedir, 'moduleB/target/classes/META-INF/fe-bundled-metadata/copy-fe-bundled-metadata-module-a-testing-jar/META-INF/fe-manifest-associations/fe-manifest-association.json')
assert !moduleABundledInModuleBAssociation.exists()

def moduleABundledInModuleCManifest = new File(basedir, 'moduleC/target/classes/META-INF/fe-bundled-metadata/copy-fe-bundled-metadata-module-a-testing-jar/META-INF/fe-module-manifests/package.json')
assert moduleABundledInModuleCManifest.exists()
def moduleABundledInModuleCAssociation = new File(basedir, 'moduleC/target/classes/META-INF/fe-bundled-metadata/copy-fe-bundled-metadata-module-a-testing-jar/META-INF/fe-manifest-associations/fe-manifest-association.json')
assert moduleABundledInModuleCAssociation.exists()

def moduleBBundledInModuleCManifest = new File(basedir, 'moduleC/target/classes/META-INF/fe-bundled-metadata/copy-fe-bundled-metadata-module-b-testing-jar/META-INF/fe-module-manifests/package.json')
assert moduleBBundledInModuleCManifest.exists()
def moduleBBundledInModuleCAssociation = new File(basedir, 'moduleC/target/classes/META-INF/fe-bundled-metadata/copy-fe-bundled-metadata-module-b-testing-jar/META-INF/fe-manifest-associations/fe-manifest-association.json')
assert moduleBBundledInModuleCAssociation.exists()
package com.atlassian.maven.plugins.amps.util;

import static com.atlassian.maven.plugins.amps.util.FileUtils.copyDirectory;
import static com.atlassian.maven.plugins.amps.util.FileUtils.doesFileNameMatchArtifact;
import static com.atlassian.maven.plugins.amps.util.FileUtils.file;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SystemUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestFileUtils {
    @Rule
    public final TemporaryFolder tempDir = new TemporaryFolder();

    @Test
    public void testFile() {
        File parent = new File("bob");
        assertEquals(
                new File(parent, "jim").getAbsolutePath(), file(parent, "jim").getAbsolutePath());

        assertEquals(
                new File(new File(parent, "jim"), "sarah").getAbsolutePath(),
                file(parent, "jim", "sarah").getAbsolutePath());
    }

    @Test
    public void testDoesFileNameMatchArtifact() {
        assertTrue(doesFileNameMatchArtifact("sal-crowd-plugin-2.0.7.jar", "sal-crowd-plugin"));
        assertTrue(doesFileNameMatchArtifact("sal_crowd_plugin-2.0.7.jar", "sal_crowd_plugin"));
        assertTrue(doesFileNameMatchArtifact("sal-crowd-plugin-2.0.7-m01-123abcd.jar", "sal-crowd-plugin"));
        assertTrue(doesFileNameMatchArtifact("com.atlassian.crowd_sal-crowd-plugin-2.0.7.jar", "sal-crowd-plugin"));
        assertTrue(doesFileNameMatchArtifact("com.atlassian.crowd_sal_crowd_plugin-2.0.7.jar", "sal_crowd_plugin"));
        assertTrue(doesFileNameMatchArtifact(
                "com.atlassian.crowd_sal-crowd-plugin-2.0.7-m01-123abcd.jar", "sal-crowd-plugin"));
        assertFalse(doesFileNameMatchArtifact("sal-crowd-plugin-2.0.7.jar", "crowd-plugin"));
        assertFalse(doesFileNameMatchArtifact("sal_crowd_plugin-2.0.7.jar", "sal-crowd-plugin"));
        assertFalse(doesFileNameMatchArtifact("com-atlassian-crowd_sal_crowd_plugin-2.0.7.jar", "sal-crowd-plugin"));
    }

    @Test
    public void testCopyDirectory() throws IOException {
        File src = tempDir.newFolder("src");
        File dest = tempDir.newFolder("dst");
        try {
            File file = new File(src, "something");
            file.createNewFile();

            // Ignore the executable assert on Windows
            boolean executable = file.setExecutable(true);
            new File(src, "a/b").mkdirs();
            new File(src, "a/b/c").createNewFile();
            new File(src, "a/d").createNewFile();
            copyDirectory(src, dest, true);
            assertTrue(new File(dest, "a/b/c").exists());
            assertTrue(new File(dest, "a/d").exists());
            if (SystemUtils.IS_OS_WINDOWS) {
                assertEquals(executable, new File(dest, "a/d").canExecute());
            } else {
                assertFalse(new File(dest, "a/d").canExecute());
            }
            assertEquals(executable, new File(dest, file.getName()).canExecute());
        } finally {
            FileUtils.deleteDir(src);
            FileUtils.deleteDir(dest);
        }
    }

    @Test
    public void testReadFileToString() {
        // Invoke
        final String actualText = FileUtils.readFileToString("TestFileUtils.txt", getClass(), UTF_8);

        // Check
        final String expectedText = "\nThis file is for reading by TestFileUtils#testReadFileToString.\n"
                + "It has multiple lines, and leading/trailing whitespace.\n";
        assertThat(actualText, is(expectedText));
    }

    @Test
    public void testFindBaseDirectory_whenSiblingPaths_thenFindsCommonPath() {
        // given
        Set<String> paths = new HashSet<>(
                asList(file("/a/b/c/d").getAbsolutePath(), file("/a/b/c/e").getAbsolutePath()));

        // when
        final File commonDir = FileUtils.findBaseDirectory(paths);

        // then
        assertEquals(file("/a/b/c").getAbsolutePath(), commonDir.toString());
    }

    @Test
    public void testFindBaseDirectory_whenParentAndChildPaths_thenFindsCommonPath() {
        // given
        Set<String> paths = new HashSet<>(asList(
                file("/a/b/c").getAbsolutePath(),
                file("/a/b/c/d").getAbsolutePath(),
                file("/a/e").getAbsolutePath()));

        // when
        final File commonDir = FileUtils.findBaseDirectory(paths);

        // then
        assertEquals(file("/a").getAbsolutePath(), commonDir.toString());
    }

    @Test
    public void testFindBaseDirectory_whenSinglePath_thenFindsFileDirectory() {
        // given
        Set<String> paths = new HashSet<>(singleton(file("/a/b/c").getAbsolutePath()));

        // when
        final File commonDir = FileUtils.findBaseDirectory(paths);

        // then
        assertEquals(commonDir.toString(), file("/a/b").getAbsolutePath());
    }

    @Test(expected = RuntimeException.class)
    public void testFindBaseDirectory_whenPathsHaveNoCommonDir_thenThrowsException() {
        // given
        Set<String> paths;

        if (SystemUtils.IS_OS_WINDOWS) {
            paths = new HashSet<>(asList(
                    Paths.get("a:\\b\\c").toAbsolutePath().toString(),
                    Paths.get("b:\\c\\d").toAbsolutePath().toString(),
                    Paths.get("c:\\d\\e").toAbsolutePath().toString()));
        } else {
            paths = new HashSet<>(asList("/a/b/c", "/b/c/d", "c/d/e"));
        }

        // when
        FileUtils.findBaseDirectory(paths);

        // then throws RuntimeException
    }

    @Test
    public void testFindBaseDirectory_whenPathsDifferInCasing_thenFindIsCaseSensitive() {
        // given
        Set<String> paths = new HashSet<>(asList(
                Paths.get("/a/b/c").toAbsolutePath().toString(),
                Paths.get("/a/B/c").toAbsolutePath().toString()));

        // when
        final File commonDir = FileUtils.findBaseDirectory(paths);

        // then
        if (SystemUtils.IS_OS_WINDOWS) {
            assertEquals(file("/a/b").getAbsolutePath(), commonDir.toString());
        } else {
            assertEquals(file("/a").getAbsolutePath(), commonDir.toString());
        }
    }

    @Test
    public void testFindBaseDirectory_whenOnWindowsEnv_thenFindsAbsoluteCommonPath() {
        assumeTrue(SystemUtils.IS_OS_WINDOWS);

        // given
        Set<String> paths =
                new HashSet<>(asList(new File("C:\\a\\b").getAbsolutePath(), new File("C:\\a\\c").getAbsolutePath()));

        // when
        final File commonDir = FileUtils.findBaseDirectory(paths);

        // then
        assertEquals("C:\\a", commonDir.getAbsolutePath());
    }
}

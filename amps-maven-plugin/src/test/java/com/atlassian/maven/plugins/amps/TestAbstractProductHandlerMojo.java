package com.atlassian.maven.plugins.amps;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.atlassian.maven.plugins.amps.upm.signing.tools.AtlassianCertificate;
import com.atlassian.maven.plugins.amps.upm.signing.tools.UpmConfigWriter;

public class TestAbstractProductHandlerMojo {

    @Test
    public void testMakeProductsInheritDefaultConfiguration() throws Exception {
        SomeMojo mojo = new SomeMojo("foo");

        Product fooProd = new Product();
        fooProd.setInstanceId("foo");
        fooProd.setVersion("1.0");

        Product barProd = new Product();
        barProd.setInstanceId("bar");
        barProd.setVersion("2.0");

        Map<String, Product> prodMap = new HashMap<String, Product>();
        mojo.makeProductsInheritDefaultConfiguration(asList(fooProd, barProd), prodMap);
        assertEquals(2, prodMap.size());
        assertEquals("1.0", prodMap.get("foo").getVersion());
        assertEquals("/foo", prodMap.get("foo").getContextPath());
        assertEquals("2.0", prodMap.get("bar").getVersion());
        assertEquals("/foo", prodMap.get("bar").getContextPath());
    }

    @Test
    public void testMakeProductsInheritDefaultConfigurationDifferentInstanceIds() throws Exception {
        SomeMojo mojo = new SomeMojo("baz");

        Product fooProd = new Product();
        fooProd.setInstanceId("foo");
        fooProd.setVersion("1.0");

        Product barProd = new Product();
        barProd.setInstanceId("bar");
        barProd.setVersion("2.0");

        Map<String, Product> prodMap = new HashMap<String, Product>();
        mojo.makeProductsInheritDefaultConfiguration(asList(fooProd, barProd), prodMap);
        assertEquals(3, prodMap.size());
        assertEquals("1.0", prodMap.get("foo").getVersion());
        assertEquals("/foo", prodMap.get("foo").getContextPath());
        assertEquals("2.0", prodMap.get("bar").getVersion());
        assertEquals("/foo", prodMap.get("bar").getContextPath());
        assertEquals(null, prodMap.get("baz").getVersion());
        assertEquals("/foo", prodMap.get("baz").getContextPath());
    }

    @Test
    public void testMakeProductsInheritDefaultConfigurationNoProducts() throws Exception {
        SomeMojo mojo = new SomeMojo("foo");

        Map<String, Product> prodMap = new HashMap<String, Product>();
        mojo.makeProductsInheritDefaultConfiguration(Collections.<Product>emptyList(), prodMap);
        assertEquals(1, prodMap.size());
        assertEquals("/foo", prodMap.get("foo").getContextPath());
    }

    @Test
    public void testShouldNotCreateUpmConfiguration() throws Exception {
        Path testClasses = Paths.get(TestAbstractProductHandlerMojo.class
                .getClassLoader()
                .getResource("")
                .toURI());
        Path configPath = testClasses.resolve("upmconfig");
        UpmConfigWriter writer = new UpmConfigWriter(configPath);
        try {
            String configPathProperty = configPath.toFile().getAbsolutePath();
            CustomCertificateBuilder customCertificateBuilder = new CustomCertificateBuilder();

            customCertificateBuilder.setIssuer("issuer");
            customCertificateBuilder.setName("NAME");
            customCertificateBuilder.setStartOffset("-P1D");
            customCertificateBuilder.setDuration("P1Y");

            SomeMojo mojo = new SomeMojo(
                    "foo",
                    true,
                    configPathProperty,
                    AtlassianCertificatesSelector.ALL,
                    customCertificateBuilder,
                    false,
                    false);

            mojo.execute();
            Assertions.assertThat(mojo.systemPropertyVariables)
                    .containsEntry("atlassian.upm.configuration.directory", configPathProperty);
            Assertions.assertThat(mojo.systemPropertyVariables)
                    .containsEntry("atlassian.upm.config.loosecheck.allowed", "true");
            Assertions.assertThat(writer.getConfigPath()).doesNotExist();
        } finally {
            writer.deleteConfiguration();
        }
    }

    @Test
    public void testShouldCreateUpmConfiguration() throws Exception {
        Path testClasses = Paths.get(TestAbstractProductHandlerMojo.class
                .getClassLoader()
                .getResource("")
                .toURI());
        Path configPath = testClasses.resolve("upmconfig");
        UpmConfigWriter writer = new UpmConfigWriter(configPath);
        try {
            String configPathProperty = configPath.toFile().getAbsolutePath();
            CustomCertificateBuilder customCertificateBuilder = new CustomCertificateBuilder();

            customCertificateBuilder.setIssuer("issuer");
            customCertificateBuilder.setName("NAME");
            customCertificateBuilder.setStartOffset("-P1D");
            customCertificateBuilder.setDuration("P1Y");

            SomeMojo mojo = new SomeMojo(
                    "foo",
                    true,
                    configPathProperty,
                    AtlassianCertificatesSelector.ALL,
                    customCertificateBuilder,
                    false,
                    true);

            mojo.execute();
            Assertions.assertThat(mojo.systemPropertyVariables)
                    .containsEntry("atlassian.upm.configuration.directory", configPathProperty);
            Assertions.assertThat(mojo.systemPropertyVariables)
                    .containsEntry("atlassian.upm.config.loosecheck.allowed", "true");
            Assertions.assertThat(writer.getConfigPath().resolve("upm.properties"))
                    .isRegularFile();
            for (AtlassianCertificate cert : AtlassianCertificate.values()) {
                Assertions.assertThat(writer.getTruststorePath()).isDirectoryContaining("glob:**" + cert.getFileName());
            }
            Assertions.assertThat(writer.getTruststorePath()).isDirectoryContaining("glob:**NAME-cert.pem");
            Assertions.assertThat(writer.getKeystorePath()).isDirectoryContaining("glob:**NAME-pk.pem");
        } finally {
            writer.deleteConfiguration();
        }
    }

    @Test
    public void testShouldCreateUpmConfigurationWithDefaultPath() throws Exception {
        Path target = Paths.get(TestAbstractProductHandlerMojo.class
                        .getClassLoader()
                        .getResource("")
                        .toURI())
                .getParent();
        Path configPath = target.resolve("upmconfig");
        UpmConfigWriter writer = new UpmConfigWriter(configPath);
        try {
            SomeMojo mojo = new SomeMojo("foo", true, null, null, null, false, true);

            mojo.execute();
            String absolutePath =
                    mojo.targetDirectory.toPath().resolve("upmconfig").toString();

            Assertions.assertThat(mojo.systemPropertyVariables)
                    .containsEntry("atlassian.upm.configuration.directory", absolutePath);
            Assertions.assertThat(mojo.systemPropertyVariables)
                    .containsEntry("atlassian.upm.config.loosecheck.allowed", "true");
            Assertions.assertThat(writer.getConfigPath().resolve("upm.properties"))
                    .isRegularFile();
            Assertions.assertThat(writer.getTruststorePath()).isEmptyDirectory();
            Assertions.assertThat(writer.getKeystorePath()).isEmptyDirectory();
        } finally {
            writer.deleteConfiguration();
        }
    }

    public static class SomeMojo extends AbstractProductHandlerMojo {
        private final String defaultProductId;
        private final boolean enablePluginSignatureCheck;
        private final String upmConfigPath;
        private final AtlassianCertificatesSelector certificatesSelector;
        private final CustomCertificateBuilder customCertificateBuilder;
        private final boolean overwiteConfiguration;
        private final boolean startProduct;

        public SomeMojo(String defaultProductId) throws URISyntaxException {
            this(defaultProductId, false, null, null, null, false, false);
        }

        public SomeMojo(
                String defaultProductId,
                boolean enablePluginSignatureCheck,
                String upmConfigPath,
                AtlassianCertificatesSelector certificatesSelector,
                CustomCertificateBuilder customCertificateBuilder,
                boolean overwiteConfiguration,
                boolean startProduct)
                throws URISyntaxException {
            this.defaultProductId = defaultProductId;
            this.enablePluginSignatureCheck = enablePluginSignatureCheck;
            this.upmConfigPath = upmConfigPath;
            this.certificatesSelector = certificatesSelector;
            this.customCertificateBuilder = customCertificateBuilder;
            this.overwiteConfiguration = overwiteConfiguration;
            this.targetDirectory = Paths.get(
                            SomeMojo.class.getClassLoader().getResource("").toURI())
                    .getParent()
                    .toFile();
            contextPath = "/foo";
            this.startProduct = startProduct;
        }

        @Override
        protected String getDefaultProductId() throws MojoExecutionException {
            return defaultProductId;
        }

        @Override
        protected boolean isStartingProduct() {
            return startProduct;
        }

        @Override
        protected void doExecute() throws MojoExecutionException, MojoFailureException {}

        @Override
        protected MavenContext getMavenContext() {
            MavenProject project = mock(MavenProject.class);
            Build build = mock(Build.class);
            when(build.getTestOutputDirectory()).thenReturn(".");
            when(project.getBuild()).thenReturn(build);
            when(project.getBasedir()).thenReturn(new File("."));
            return new MavenContext(project, null, null, null, null);
        }

        @Override
        @Nonnull
        protected PluginInformation getPluginInformation() {
            return PluginInformation.fromArtifactId("maven-test-product-plugin", "Test SDK Version");
        }

        protected boolean isOverwriteUpmConfiguration() {
            return overwiteConfiguration;
        }

        protected boolean isEnablePluginSigningSet() {
            return enablePluginSignatureCheck;
        }

        protected boolean isUpmConfigFolderSet() {
            return upmConfigPath != null;
        }

        protected String getUpmConfigPathProperty() {
            return upmConfigPath;
        }

        protected AtlassianCertificatesSelector getAtlassianCertificatesSelector() {
            return certificatesSelector;
        }

        protected CustomCertificateBuilder getCustomCertificateBuilder() {
            return customCertificateBuilder;
        }

        protected boolean makeUpmConfigurationWritableOnShutdown() {
            return false;
        }
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import java.io.File;
import java.nio.file.Paths;

public class ResourceHelper {

    public static String getResourceAbsolutePath(String path) {
        String resourceName = "src/test/resources/com/atlassian/maven/plugins/amps/frontend/association/mapping";
        File resourceFile = new File(Paths.get(resourceName, path).toString());
        return resourceFile.getAbsolutePath();
    }
}

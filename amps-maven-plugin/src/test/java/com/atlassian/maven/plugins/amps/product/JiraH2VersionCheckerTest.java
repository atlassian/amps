package com.atlassian.maven.plugins.amps.product;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class JiraH2VersionCheckerTest {
    private final String version;
    private final boolean expected;

    public JiraH2VersionCheckerTest(String version, boolean expected) {
        this.version = version;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> jiraVersions() {
        return Arrays.asList(new Object[][] {
            {"8.20.18", false},
            {"9.4.15", false},
            {"9.15.0", false},
            {"9.99.99-SNAPSHOT", false},
            {"10.0.0-test-release-whatever", true},
            {"10.0.0-SNAPSHOT", true},
            {"10.0.0", true},
            {"10.0.1", true},
            {"10.15.1", true},
            {"11.0.1-SNAPSHOT", true},
            {"11.10.10", true}
        });
    }

    @Test
    public void testShouldBundleH2() {
        // when
        boolean actual = JiraH2VersionChecker.shouldBundleH2(version);

        // then
        assertEquals(expected, actual);
    }
}

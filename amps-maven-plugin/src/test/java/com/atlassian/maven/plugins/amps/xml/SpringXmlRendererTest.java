package com.atlassian.maven.plugins.amps.xml;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.io.IOUtils.readLines;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class SpringXmlRendererTest {
    private static final TransformerFactory TRANSFORMER_FACTORY = TransformerFactory.newInstance();
    private static final XmlMapper XML_MAPPER = new XmlMapper();
    private static final SpringXmlModel MODEL = SpringXmlModel.builder().build();

    @Test
    public void render_emptyModel_returnsEmptyXml() throws SpringXmlException, IOException {
        SpringXmlModel model = SpringXmlModel.builder().build();
        SpringXmlRenderer sut = create();
        StringWriter writer = new StringWriter();

        sut.render(model, writer);

        assertThat(writer.toString(), containsXml("empty.xml"));
    }

    @Test
    public void render_singleComponentNoProperties_returnsEmptyXml() throws SpringXmlException, IOException {
        SpringXmlComponent fooComponent = SpringXmlComponent.builder()
                .id("foo")
                .fullClassName("com.example.Foo")
                .build();

        SpringXmlModel model =
                SpringXmlModel.builder().addComponent(fooComponent).build();

        SpringXmlRenderer sut = create();
        StringWriter writer = new StringWriter();

        sut.render(model, writer);

        assertThat(writer.toString(), containsXml("single-no-properties.xml"));
    }

    @Test
    public void render_singleComponentWithProperties_returnsEmptyXml() throws SpringXmlException, IOException {
        SpringXmlComponent fooComponent = SpringXmlComponent.builder()
                .id("foo")
                .fullClassName("com.example.Foo")
                .putServiceProperty("author", "John Smith")
                .putServiceProperty("publisher", "ACME Inc.")
                .build();

        SpringXmlModel model =
                SpringXmlModel.builder().addComponent(fooComponent).build();

        SpringXmlRenderer sut = create();
        StringWriter writer = new StringWriter();

        sut.render(model, writer);

        assertThat(writer.toString(), containsXml("single-with-properties.xml"));
    }

    @Test
    public void render_multipleComponents_returnsEmptyXml() throws SpringXmlException, IOException {
        SpringXmlComponent fooComponent = SpringXmlComponent.builder()
                .id("foo")
                .fullClassName("com.example.Foo")
                .putServiceProperty("author", "John Smith")
                .putServiceProperty("publisher", "ACME Inc.")
                .build();

        SpringXmlComponent barComponent = SpringXmlComponent.builder()
                .id("bar")
                .fullClassName("com.example.Bar")
                .build();

        SpringXmlModel model = SpringXmlModel.builder()
                .addComponent(fooComponent)
                .addComponent(barComponent)
                .build();

        SpringXmlRenderer sut = create();
        StringWriter writer = new StringWriter();

        sut.render(model, writer);

        assertThat(writer.toString(), containsXml("multiple.xml"));
    }

    @Test
    public void constructor_transformerFactoryIsNull_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> new SpringXmlRendererImpl(null, XML_MAPPER));
        assertEquals("'transformerFactory' must not be null", exception.getMessage());
    }

    @Test
    public void constructor_xmlMapperIsNull_throwsIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class, () -> new SpringXmlRendererImpl(TRANSFORMER_FACTORY, null));
        assertEquals("'xmlMapper' must not be null", exception.getMessage());
    }

    @Test
    public void render_modelIsNull_throwsIllegalArgumentException() {
        SpringXmlRenderer sut = create();
        Writer writer = mock(Writer.class);

        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> sut.render(null, writer));
        assertEquals("'model' must not be null", exception.getMessage());
    }

    @Test
    public void render_writerIsNull_throwsIllegalArgumentException() {
        SpringXmlRenderer sut = create();

        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> sut.render(MODEL, null));
        assertEquals("'writer' must not be null", exception.getMessage());
    }

    @Test
    public void render_transformerFactoryThrows_throwsSpringXmlException() throws TransformerConfigurationException {
        TransformerFactory transformerFactory = mock(TransformerFactory.class);
        TransformerConfigurationException transformerFactoryException = mock(TransformerConfigurationException.class);
        doThrow(transformerFactoryException).when(transformerFactory).newTransformer(any(StreamSource.class));
        SpringXmlRenderer sut = new SpringXmlRendererImpl(transformerFactory, XML_MAPPER);

        SpringXmlException exception =
                assertThrows(SpringXmlException.class, () -> sut.render(MODEL, mock(Writer.class)));
        assertEquals(
                "Unable to process a template of the Spring XML document. Check the stylesheet",
                exception.getMessage());
        assertSame(transformerFactoryException, exception.getCause());
    }

    @Test
    public void render_xmlMapperThrows_throwsSpringXmlException() throws JsonProcessingException {
        XmlMapper xmlMapper = mock(XmlMapper.class);
        JsonProcessingException mapperException = mock(JsonProcessingException.class);
        doThrow(mapperException).when(xmlMapper).writeValueAsString(MODEL);

        SpringXmlRenderer sut = new SpringXmlRendererImpl(TRANSFORMER_FACTORY, xmlMapper);
        Writer writer = new StringWriter();
        SpringXmlException exception = assertThrows(SpringXmlException.class, () -> sut.render(MODEL, writer));
        assertEquals("Spring XML model serialization has failed", exception.getMessage());
        assertSame(mapperException, exception.getCause());
    }

    @Test
    public void render_transformerThrows_throwsSpringXmlException() throws TransformerException {
        TransformerFactory transformerFactory = mock(TransformerFactory.class);
        Transformer transformer = mock(Transformer.class);
        doReturn(transformer).when(transformerFactory).newTransformer(any(StreamSource.class));
        TransformerException transformerException = mock(TransformerException.class);
        doThrow(transformerException).when(transformer).transform(any(StreamSource.class), any(StreamResult.class));

        SpringXmlRenderer sut = new SpringXmlRendererImpl(transformerFactory, XML_MAPPER);

        SpringXmlException exception =
                assertThrows(SpringXmlException.class, () -> sut.render(MODEL, mock(Writer.class)));
        assertEquals("Application of the stylesheet to the model has failed", exception.getMessage());
        assertSame(transformerException, exception.getCause());
    }

    @Test
    public void render_writerThrows_throwsSpringXmlException() throws IOException {
        SpringXmlRenderer sut = create();
        Writer writer = mock(Writer.class);
        IOException writerException = mock(IOException.class);
        doThrow(writerException).when(writer).write(contains("<?xml"));

        SpringXmlException exception = assertThrows(SpringXmlException.class, () -> sut.render(MODEL, writer));
        assertEquals("Writing XML declaration has failed", exception.getMessage());
        assertSame(writerException, exception.getCause());
    }

    private static SpringXmlRenderer create() {
        return new SpringXmlRendererImpl(TRANSFORMER_FACTORY, XML_MAPPER);
    }

    private static Matcher<String> containsXml(String resourceName) {
        return new TextContentMatcher(
                String.join("\n", readLines(SpringXmlRendererTest.class.getResourceAsStream(resourceName), UTF_8)));
    }

    private static final class TextContentMatcher extends TypeSafeMatcher<String> {
        private final String expectedXml;
        private Diff diff;

        TextContentMatcher(String expectedXml) {
            this.expectedXml = expectedXml;
        }

        @Override
        public boolean matchesSafely(String actualXml) {
            diff = DiffBuilder.compare(expectedXml)
                    .ignoreWhitespace()
                    .withTest(actualXml)
                    .build();
            return !diff.hasDifferences();
        }

        @Override
        public void describeTo(Description description) {
            description.appendText(String.format("is equivalent to: `%s`", expectedXml));
        }

        @Override
        public void describeMismatchSafely(String actualXml, Description description) {
            description.appendText("\ndifference was: " + diff);
        }
    }
}

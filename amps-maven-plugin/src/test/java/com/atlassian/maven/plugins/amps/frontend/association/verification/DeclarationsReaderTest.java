package com.atlassian.maven.plugins.amps.frontend.association.verification;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThrows;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.DeclarationsReaderImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.NestedModuleDeclarationData;

public class DeclarationsReaderTest {

    private final TFile moduleRoot = new TFile(ResourceHelper.getResourceAbsolutePath("associations"));
    private DeclarationsReader underTest;

    @Before
    public void setup() {
        underTest = new DeclarationsReaderImpl();
    }

    @Test
    public void testReadDeclarations_whenEmptyList_shouldReturnEmptySet() throws MojoExecutionException {
        // given
        List<TFile> files = Collections.emptyList();

        // when
        Set<String> declarations = underTest.readRelativeDeclarations(files, moduleRoot);

        // then
        assertThat(declarations, hasSize(0));
    }

    @Test
    public void testReadDeclarations_whenSingleDeclaration_shouldReturnDeclarations() throws MojoExecutionException {
        // given
        List<TFile> files = Collections.singletonList(getResourceByPath("associations/fe-manifest-association.json"));

        // when
        Set<String> declarations = underTest.readRelativeDeclarations(files, moduleRoot);

        // then
        assertThat(declarations, hasSize(1));
    }

    @Test
    public void testReadDeclarations_whenMultipleDeclarations_shouldReturnMergedDeclarations()
            throws MojoExecutionException {
        // given
        List<TFile> files = Arrays.asList(
                getResourceByPath("associations/fe-manifest-association.json"),
                getResourceByPath("associations/fe-manifest-association2.json"));

        // when
        Set<String> declarations = underTest.readRelativeDeclarations(files, moduleRoot);

        // then
        assertThat(declarations, hasSize(2));
    }

    @Test
    public void testReadDeclarations_whenAssociationHasUnknownProperty_shouldThrowException() {
        // given
        List<TFile> files = Collections.singletonList(getResourceByPath("associations/malformed-association.json"));

        // when
        final ThrowingRunnable runnable = () -> underTest.readRelativeDeclarations(files, moduleRoot);

        // then
        MojoExecutionException e = assertThrows(MojoExecutionException.class, runnable);
        assertThat(
                e.getMessage(),
                allOf(
                        containsString("Error reading:"),
                        containsString("malformed-association.json"),
                        containsString("Unrecognized field \"js/less/less-rhino.js\"")));
    }

    @Test
    public void testReadDeclarations_whenFilesOfDifferentTypes_shouldReturnOnlyJsFiles() throws MojoExecutionException {
        // given
        List<TFile> files = Collections.singletonList(getResourceByPath("associations/all-files-associations.json"));

        // when
        Set<String> declarations = underTest.readRelativeDeclarations(files, moduleRoot);

        // then
        assertThat(declarations, hasSize(4));
        assertThat(declarations, hasItems("index.js", "index.cjs", "index.mjs", "index.ejs"));
    }

    @Test
    public void testReadNestedModule_whenPomWithGroupId_shouldReadDeclaration() throws MojoExecutionException {
        // given
        List<TFile> files = Collections.singletonList(getResourceByPath("nested-dependencies/single-declaration.json"));
        TFile moduleRoot = new TFile(ResourceHelper.getResourceAbsolutePath("nested-dependencies"));

        // when
        Map<TFile, NestedModuleDeclarationData> dataMapping =
                underTest.readNestedModuleDeclarationData(files, moduleRoot);

        // then
        TFile expectedArtifactFile = new TFile(getResourceByPath("nested-dependencies/lib/library-a"));
        NestedModuleDeclarationData data = dataMapping.get(expectedArtifactFile);
        assertThat(data.getDeclaredFiles(), hasItem("src/file.js"));
    }

    @Test
    public void testReadNestedModule_whenPomWithParent_shouldReadDeclaration() throws MojoExecutionException {
        // given
        List<TFile> files = Collections.singletonList(getResourceByPath("nested-dependencies/multi-declarations.json"));
        TFile moduleRoot = new TFile(ResourceHelper.getResourceAbsolutePath("nested-dependencies"));

        // when
        Map<TFile, NestedModuleDeclarationData> dataMapping =
                underTest.readNestedModuleDeclarationData(files, moduleRoot);

        // then
        assertThat(dataMapping.size(), equalTo(2));

        TFile expectedArtifactFile = new TFile(getResourceByPath("nested-dependencies/lib/library-b"));
        NestedModuleDeclarationData data = dataMapping.get(expectedArtifactFile);
        assertThat(data.getDeclaredFiles(), hasItems("src/different-file1.js", "src/different-file2.js"));
    }

    public TFile getResourceByPath(String path) {
        return new TFile(ResourceHelper.getResourceAbsolutePath(path));
    }
}

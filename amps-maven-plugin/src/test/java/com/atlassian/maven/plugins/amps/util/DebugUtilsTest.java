package com.atlassian.maven.plugins.amps.util;

import static com.atlassian.maven.plugins.amps.util.DebugUtils.getDebugPortProperties;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;

public class DebugUtilsTest {

    private static final int DEFAULT_DEBUG_PORT = 666;

    @Test
    public void setNodeDebugPorts_whenOneProductAndOneNodeAndPortNotSet_shouldSetDefaultPort() throws Exception {
        // Arrange
        final Product product = mock(Product.class);
        final Node node = mock(Node.class);
        when(product.getNodes()).thenReturn(singletonList(node));
        when(node.getJvmDebugPort()).thenReturn(0, DEFAULT_DEBUG_PORT, DEFAULT_DEBUG_PORT);

        // Act
        DebugUtils.setNodeDebugPorts(singletonList(product), DEFAULT_DEBUG_PORT);

        // Assert
        verify(node).setJvmDebugPort(DEFAULT_DEBUG_PORT);
    }

    @Test
    public void setNodeDebugPorts_whenOneProductAndOneNodeAndPortSet_shouldNotSetThePort() throws Exception {
        // Arrange
        final int userConfiguredDebugPort = DEFAULT_DEBUG_PORT + 1;
        final Product product = mockProduct("any", userConfiguredDebugPort);

        // Act
        DebugUtils.setNodeDebugPorts(singletonList(product), DEFAULT_DEBUG_PORT);

        // Assert
        final Node node = product.getNodes().get(0);
        verify(node, never()).setJvmDebugPort(anyInt());
    }

    @Test
    public void setNodeDebugPorts_whenOneProductAndTwoNodesAndPortsConflict_shouldThrowException() {
        // Arrange
        final int conflictingDebugPort = DEFAULT_DEBUG_PORT + 1;
        final Product product = mockProduct("foo", conflictingDebugPort, conflictingDebugPort);

        // Act
        final MojoExecutionException exception = assertThrows(
                MojoExecutionException.class,
                () -> DebugUtils.setNodeDebugPorts(singletonList(product), DEFAULT_DEBUG_PORT));

        // Assert
        assertThat(exception.getMessage(), is("Debug port 667 is used by these nodes: [foo node 0, foo node 1]"));
    }

    @Test
    public void setNodeDebugPorts_whenTwoProductsWithOneNodesAndPortsConflict_shouldThrowException() {
        // Arrange
        final int conflictingDebugPort = DEFAULT_DEBUG_PORT + 2;
        final Product product1 = mockProduct("foo", conflictingDebugPort);
        final Product product2 = mockProduct("bar", conflictingDebugPort);

        // Act
        final MojoExecutionException exception = assertThrows(
                MojoExecutionException.class,
                () -> DebugUtils.setNodeDebugPorts(asList(product1, product2), DEFAULT_DEBUG_PORT));

        // Assert
        assertThat(exception.getMessage(), is("Debug port 668 is used by these nodes: [foo node 0, bar node 0]"));
    }

    @Test
    public void setNodeDebugPorts_whenOneProductAndTwoNodesAndPortsNotSet_shouldSetDefaultPortAndOffsetByOne()
            throws Exception {
        // Arrange
        final Product product = mock(Product.class);
        final Node node1 = mock(Node.class);
        final Node node2 = mock(Node.class);
        when(product.getNodes()).thenReturn(asList(node1, node2));
        final int node1DebugPort = DEFAULT_DEBUG_PORT;
        final int node2DebugPort = node1DebugPort + 1;
        when(node1.getJvmDebugPort()).thenReturn(0, node1DebugPort, node1DebugPort);
        when(node2.getJvmDebugPort()).thenReturn(0, node2DebugPort, node2DebugPort);

        // Act
        DebugUtils.setNodeDebugPorts(singletonList(product), DEFAULT_DEBUG_PORT);

        // Assert
        verify(node1).setJvmDebugPort(node1DebugPort);
        verify(node2).setJvmDebugPort(node2DebugPort);
    }

    @Test
    public void setNodeDebugPorts_whenOneProductAndThreeNodesAndPortsNotSet_shouldSetDefaultPortAndOffsetByOneAndTwo()
            throws Exception {
        // Arrange
        final Product product = mock(Product.class);
        final Node node1 = mock(Node.class);
        final Node node2 = mock(Node.class);
        final Node node3 = mock(Node.class);
        when(product.getNodes()).thenReturn(asList(node1, node2, node3));
        final int node1DebugPort = DEFAULT_DEBUG_PORT;
        final int node2DebugPort = node1DebugPort + 1;
        final int node3DebugPort = node2DebugPort + 1;
        when(node1.getJvmDebugPort()).thenReturn(0, node1DebugPort, node1DebugPort);
        when(node2.getJvmDebugPort()).thenReturn(0, node2DebugPort, node2DebugPort);
        when(node2.getJvmDebugPort()).thenReturn(0, node3DebugPort, node3DebugPort);

        // Act
        DebugUtils.setNodeDebugPorts(singletonList(product), DEFAULT_DEBUG_PORT);

        // Assert
        verify(node1).setJvmDebugPort(node1DebugPort);
        verify(node2).setJvmDebugPort(node2DebugPort);
        verify(node3).setJvmDebugPort(node3DebugPort);
    }

    @Test
    public void setNodeDebugPorts_whenOneProductAndTwoNodesAndOnePortSet_shouldUseDefaultPortOnlyOnce()
            throws Exception {
        // Arrange
        final Product product = mock(Product.class);
        final Node node1 = mock(Node.class);
        final Node node2 = mock(Node.class);
        when(product.getNodes()).thenReturn(asList(node1, node2));
        final int node1DebugPort = DEFAULT_DEBUG_PORT;
        final int node2DebugPort = 42; // user-configured
        when(node1.getJvmDebugPort()).thenReturn(0, node1DebugPort, node1DebugPort);
        when(node2.getJvmDebugPort()).thenReturn(node2DebugPort);

        // Act
        DebugUtils.setNodeDebugPorts(singletonList(product), DEFAULT_DEBUG_PORT);

        // Assert
        verify(node1).setJvmDebugPort(node1DebugPort);
        verify(node2, never()).setJvmDebugPort(anyInt());
    }

    private static Product mockProduct(final String instanceId, final int... debugPorts) {
        final Product product = mock(Product.class);
        when(product.getInstanceId()).thenReturn(instanceId);
        final List<Node> nodes = new ArrayList<>();
        for (final int debugPort : debugPorts) {
            final Node node = mock(Node.class);
            when(node.getJvmDebugPort()).thenReturn(debugPort);
            nodes.add(node);
        }
        when(product.getNodes()).thenReturn(nodes);
        return product;
    }

    @Test
    public void getDebugPortProperties_whenNoProducts_shouldReturnEmptyMap() {
        // Act
        final Map<String, String> properties = getDebugPortProperties(emptyList());

        // Assert
        assertThat(properties, is(emptyMap()));
    }

    @Test
    public void getDebugPortProperties_whenOneProduct_shouldReturnExpectedEntries() {
        // Arrange
        final Product product = mockProduct("foo", 42);

        // Act
        final Map<String, String> properties = getDebugPortProperties(singletonList(product));

        // Assert
        assertThat(properties.size(), is(3));
        assertThat(properties, hasEntry("debug.port", "42"));
        assertThat(properties, hasEntry("debug.foo.port", "42"));
        assertThat(properties, hasEntry("debug.foo.port.0", "42"));
    }

    @Test
    public void getDebugPortProperties_whenTwoNodeProduct_shouldReturnExpectedEntries() {
        // Arrange
        final Product product = mockProduct("foo", 42, 43);

        // Act
        final Map<String, String> properties = getDebugPortProperties(singletonList(product));

        // Assert
        assertThat(properties.size(), is(4));
        assertThat(properties, hasEntry("debug.port", "42"));
        assertThat(properties, hasEntry("debug.foo.port", "42"));
        assertThat(properties, hasEntry("debug.foo.port.0", "42"));
        assertThat(properties, hasEntry("debug.foo.port.1", "43"));
    }

    @Test
    public void getDebugPortProperties_whenTwoSingleNodeProducts_shouldReturnExpectedEntries() {
        // Arrange
        final Product product1 = mockProduct("foo", 42);
        final Product product2 = mockProduct("bar", 43);

        // Act
        final Map<String, String> properties = getDebugPortProperties(asList(product1, product2));

        // Assert
        assertThat(properties.size(), is(5));
        assertThat(properties, hasEntry("debug.port", "42"));
        assertThat(properties, hasEntry("debug.foo.port", "42"));
        assertThat(properties, hasEntry("debug.foo.port.0", "42"));
        assertThat(properties, hasEntry("debug.bar.port", "43"));
        assertThat(properties, hasEntry("debug.bar.port.0", "43"));
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.verification.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.atlassian.maven.plugins.amps.frontend.association.verification.utils.ArtifactPathUtils;

public class NestedModuleDeclarationDataTest {

    @Test
    public void given_crossPlatformPathsDuringConstruction_then_getDeclaredFiles_should_returnNormalizedPaths() {
        Set<String> paths = new HashSet<>(Arrays.asList("/linux/path", "\\windows\\path", "/mixed\\path"));

        NestedModuleDeclarationData nestedModuleDeclarationData = new NestedModuleDeclarationData(paths);

        assert (nestedModuleDeclarationData.getDeclaredFiles())
                .equals(ArtifactPathUtils.normalizePathSeparators(paths));
    }
}

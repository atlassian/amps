package com.atlassian.maven.plugins.amps;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class ValidateBannedDependenciesExecutorTest {
    public final @Rule MockitoRule rule = MockitoJUnit.rule();

    private boolean skipBanningDependencies = false;
    private @Mock Set<String> banningExcludes;
    private String platformVersion;
    private boolean forceInternalPlugin;
    private boolean legacyValidateBannedDependencies;
    private @Mock MavenGoals mavenGoals;

    @Test
    public void test_skipBanningDependencies_returns() throws Exception {
        skipBanningDependencies = true;

        createSut().execute(mavenGoals);

        verifyNoInteractions(mavenGoals);
    }

    @Test
    public void test_regularPlatformVersionSpecified_callsGoal() throws Exception {
        platformVersion = "2.3.3";

        createSut().execute(mavenGoals);

        verify(mavenGoals, times(1))
                .validateBannedDependencies(
                        banningExcludes, platformVersion, forceInternalPlugin, legacyValidateBannedDependencies);
    }

    @Test
    public void test_noVersionSpecified_callsGoal() throws Exception {
        platformVersion = null;

        createSut().execute(mavenGoals);

        verify(mavenGoals, times(1))
                .validateBannedDependencies(
                        banningExcludes, "[0,)", forceInternalPlugin, legacyValidateBannedDependencies);
    }

    @Test
    public void test_worksOffline() {
        platformVersion = "2.3.3";

        try {
            doThrow(PlatformDependencyProvisioningException.class)
                    .when(mavenGoals)
                    .validateBannedDependencies(
                            banningExcludes, platformVersion, forceInternalPlugin, legacyValidateBannedDependencies);

            createSut().execute(mavenGoals);
        } catch (Exception e) {
            fail(e.getClass().getSimpleName() + " was thrown");
        }
    }

    private ValidateBannedDependenciesExecutor createSut() {
        return new ValidateBannedDependenciesExecutor(
                skipBanningDependencies,
                banningExcludes,
                platformVersion,
                forceInternalPlugin,
                legacyValidateBannedDependencies);
    }
}

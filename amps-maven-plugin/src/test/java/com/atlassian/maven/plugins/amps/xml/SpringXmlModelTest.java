package com.atlassian.maven.plugins.amps.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

public class SpringXmlModelTest {
    private static final SpringXmlComponent FOO_COMPONENT =
            componentBuilder().id("foo").build();
    private static final SpringXmlComponent BAR_COMPONENT =
            componentBuilder().id("bar").build();

    @Test
    public void getters_values_returnsValues() {
        SpringXmlModel sut = SpringXmlModel.builder()
                .addComponent(FOO_COMPONENT)
                .addComponent(BAR_COMPONENT)
                .build();

        assertEquals(Arrays.asList(FOO_COMPONENT, BAR_COMPONENT), sut.components());
    }

    @Test
    public void equals_null_returnsFalse() {
        SpringXmlModel sut = builder().build();

        assertFalse(sut.equals(null));
    }

    @Test
    public void equals_notInstanceOf_returnsFalse() {
        SpringXmlModel sut = builder().build();

        assertFalse(sut.equals(new Object()));
    }

    @Test
    public void equals_differentComponents_returnsFalse() {
        SpringXmlModel sut = builder().build();
        SpringXmlModel other = builder().addComponent(BAR_COMPONENT).build();

        assertFalse(sut.equals(other));
        assertNotEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_equal_returnsFalse() {
        SpringXmlModel sut = builder().build();
        SpringXmlModel other = builder().build();

        assertTrue(sut.equals(other));
        assertEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_same_returnsFalse() {
        SpringXmlModel sut = builder().build();

        assertTrue(sut.equals(sut));
    }

    @Test
    public void build_componentsIsNotSet_defaultsToEmptyList() {
        SpringXmlModel sut = SpringXmlModel.builder().build();

        assertEquals(Collections.emptyList(), sut.components());
    }

    @Test
    public void addComponent_null_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().addComponent(null));
        assertEquals("'component' must not be null", exception.getMessage());
    }

    private static SpringXmlComponent.Builder componentBuilder() {
        return SpringXmlComponent.builder().id("foo").fullClassName("com.example.Foo");
    }

    private static SpringXmlModel.Builder builder() {
        return SpringXmlModel.builder().addComponent(FOO_COMPONENT);
    }
}

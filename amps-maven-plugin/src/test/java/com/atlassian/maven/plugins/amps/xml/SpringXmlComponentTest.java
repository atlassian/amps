package com.atlassian.maven.plugins.amps.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class SpringXmlComponentTest {
    private static final String ID = "foo";
    private static final String FULL_CLASS_NAME = "com.example.Foo";
    private static final Map<String, String> SERVICE_PROPERTIES = new LinkedHashMap<>();

    static {
        SERVICE_PROPERTIES.put("author", "John Smith");
        SERVICE_PROPERTIES.put("published", "ACME Limited");
    }

    @Test
    public void getters_values_returnsValues() {
        SpringXmlComponent sut = SpringXmlComponent.builder()
                .id("bar")
                .fullClassName("com.example.Bar")
                .putServiceProperty("author", "Jane Doe")
                .putServiceProperty("location", "Forest")
                .build();

        assertEquals("bar", sut.id());
        assertEquals("com.example.Bar", sut.fullClassName());

        List<SpringXmlServiceProperty> expectedServiceProperties = Arrays.asList(
                SpringXmlServiceProperty.builder()
                        .key("author")
                        .value("Jane Doe")
                        .build(),
                SpringXmlServiceProperty.builder()
                        .key("location")
                        .value("Forest")
                        .build());
        assertEquals(expectedServiceProperties, sut.serviceProperties());
    }

    @Test
    public void equals_null_returnsFalse() {
        SpringXmlComponent sut = builder().build();

        assertFalse(sut.equals(null));
    }

    @Test
    public void equals_notInstanceOf_returnsFalse() {
        SpringXmlComponent sut = builder().build();

        assertFalse(sut.equals(new Object()));
    }

    @Test
    public void equals_differentId_returnsFalse() {
        SpringXmlComponent sut = builder().build();
        SpringXmlComponent other = builder().id("qux").build();

        assertFalse(sut.equals(other));
        assertNotEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_differentFullClassName_returnsFalse() {
        SpringXmlComponent sut = builder().build();
        SpringXmlComponent other = builder().fullClassName("com.example.Qux").build();

        assertFalse(sut.equals(other));
        assertNotEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_differentServiceProperties_returnsFalse() {
        SpringXmlComponent sut = builder().build();
        SpringXmlComponent other =
                builder().putServiceProperty("author", "Josh Smith").build();

        assertFalse(sut.equals(other));
        assertNotEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_equal_returnsTrue() {
        SpringXmlComponent sut = builder().build();
        SpringXmlComponent other = builder().build();

        assertTrue(sut.equals(other));
        assertEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_same_returnsTrue() {
        SpringXmlComponent sut = builder().build();

        assertTrue(sut.equals(sut));
    }

    @Test
    public void build_servicePropertiesIsNotSet_defaultsToEmptyList() {
        SpringXmlComponent sut = SpringXmlComponent.builder()
                .id(ID)
                .fullClassName(FULL_CLASS_NAME)
                .build();

        assertEquals(Collections.emptyList(), sut.serviceProperties());
    }

    @Test
    public void build_idIsNotSet_throwsIllegalStateException() {
        SpringXmlComponent.Builder builder = SpringXmlComponent.builder().fullClassName(FULL_CLASS_NAME);

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> builder.build());
        assertEquals("'id' property is mandatory", exception.getMessage());
    }

    @Test
    public void build_fullClassNameIsNotSet_throwsIllegalStateException() {
        SpringXmlComponent.Builder builder = SpringXmlComponent.builder().id(ID);

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> builder.build());
        assertEquals("'fullClassName' property is mandatory", exception.getMessage());
    }

    @Test
    public void setId_null_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().id(null));
        assertEquals("'id' must not be null", exception.getMessage());
    }

    @Test
    public void setId_blank_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().id(""));
        assertEquals("'id' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void setId_whitespace_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().id("\r\n"));
        assertEquals("'id' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void setFullClassName_null_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().fullClassName(null));
        assertEquals("'fullClassName' must not be null", exception.getMessage());
    }

    @Test
    public void setFullClassName_blank_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().fullClassName(""));
        assertEquals("'fullClassName' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void setFullClassName_whitespace_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().fullClassName("\r\n"));
        assertEquals("'fullClassName' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void putServiceProperty_keyIsNull_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().putServiceProperty(null, "John Smith"));
        assertEquals("'key' must not be null", exception.getMessage());
    }

    @Test
    public void putServiceProperty_keyIsBlank_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().putServiceProperty("", "John Smith"));
        assertEquals("'key' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void putServiceProperty_keyIsWhitespace_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().putServiceProperty("\t\r", "John Smith"));
        assertEquals("'key' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void putServiceProperty_valueIsNull_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().putServiceProperty("author", null));
        assertEquals("'value' must not be null", exception.getMessage());
    }

    private static SpringXmlComponent.Builder builder() {
        SpringXmlComponent.Builder builder =
                SpringXmlComponent.builder().id("bar").fullClassName("com.example.Bar");

        for (Map.Entry<String, String> entry : SERVICE_PROPERTIES.entrySet()) {
            builder.putServiceProperty(entry.getKey(), entry.getValue());
        }

        return builder;
    }
}

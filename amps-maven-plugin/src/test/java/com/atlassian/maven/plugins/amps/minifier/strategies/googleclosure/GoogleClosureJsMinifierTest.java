package com.atlassian.maven.plugins.amps.minifier.strategies.googleclosure;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.junit.Assert.assertThat;

import java.util.HashMap;

import org.apache.maven.plugin.logging.Log;
import org.junit.Test;
import org.mockito.Mock;
import com.google.javascript.jscomp.Compiler;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.Result;
import com.google.javascript.jscomp.SourceFile;

import com.atlassian.maven.plugins.amps.code.Sources;

public class GoogleClosureJsMinifierTest {
    @Mock
    Log log;

    @Mock
    Result result;

    @Test
    public void minification() {
        final HashMap<String, String> closureOptions = new HashMap<>();
        final String code = "var a = 1;"
                + "\n"
                + "\n"
                + "\nif (false) {"
                + "\n    console.log('whoops!');"
                + "\n}"
                + "\nvar b = 2;";
        Sources compiled = GoogleClosureJsMinifier.compile(code, closureOptions, log, true);
        assertThat(compiled.getContent(), equalTo("var a=1,b=2;"));
        assertThat(compiled.getSourceMapContent(), not(isEmptyString()));
    }

    @Test
    public void closureWarningForJSDocsEnabled() {
        final HashMap<String, String> closureOptions = new HashMap<>();
        Compiler compiler = new Compiler();

        final String code = "/**\n"
                + " The homepage view. Uses the {@link external:views/news} widget to render each news article.\n"
                + " @external views/index\n"
                + " @extends external:views/layout\n"
                + "**/"
                + "/** @abstract */"
                + "function Foo() {}"
                + "/** @return {number} */"
                + "Foo.prototype.prop = function() {};";
        CompilerOptions options = GoogleClosureJsMinifier.getOptions(closureOptions, log, true);
        SourceFile extern = SourceFile.fromCode("externs.js", "function alert(x) {}");

        // The dummy input name "input.js" is used here so that any warnings or
        // errors will cite line numbers in terms of input.js.
        SourceFile input = SourceFile.fromCode("input.js", code);

        result = compiler.compile(extern, input, options);
        assertThat(
                result.warnings.stream()
                        .filter(x -> "JSC_BAD_JSDOC_ANNOTATION: Parse error. {0}"
                                .contains(x.getType().toString()))
                        .findAny()
                        .orElse(null),
                is(notNullValue()));
        assertThat(
                result.warnings.stream()
                        .filter(x -> "JSC_MISPLACED_ANNOTATION: Misplaced {0} annotation. {1}"
                                .contains(x.getType().toString()))
                        .findAny()
                        .orElse(null),
                is(notNullValue()));
    }

    @Test
    public void closureWarningForJSDocsDisabled() {
        final HashMap<String, String> closureOptions = new HashMap<>();
        Compiler compiler = new Compiler();

        final String code = "/**\n"
                + " The homepage view. Uses the {@link external:views/news} widget to render each news article.\n"
                + " @external views/index\n"
                + " @extends external:views/layout\n"
                + "**/"
                + "/** @abstract */"
                + "function Foo() {}"
                + "/** @return {number} */"
                + "Foo.prototype.prop = function() {};";

        CompilerOptions options = GoogleClosureJsMinifier.getOptions(closureOptions, log, false);
        SourceFile extern = SourceFile.fromCode("externs.js", "function alert(x) {}");

        // The dummy input name "input.js" is used here so that any warnings or
        // errors will cite line numbers in terms of input.js.
        SourceFile input = SourceFile.fromCode("input.js", code);

        result = compiler.compile(extern, input, options);
        assertThat(result.warnings.size(), is(0));
    }

    @Test
    public void checkErrorsAreNotDisabled() {
        final HashMap<String, String> closureOptions = new HashMap<>();
        Compiler compiler = new Compiler();

        final String code = "lets bar = 'bar';";
        CompilerOptions options = GoogleClosureJsMinifier.getOptions(closureOptions, log, false);
        SourceFile extern = SourceFile.fromCode("externs.js", "function alert(x) {}");

        // The dummy input name "input.js" is used here so that any warnings or
        // errors will cite line numbers in terms of input.js.
        SourceFile input = SourceFile.fromCode("input.js", code);

        result = compiler.compile(extern, input, options);
        assertThat(
                result.errors.stream()
                        .filter(x -> "JSC_PARSE_ERROR: Parse error. {0}"
                                .contains(x.getType().toString()))
                        .findAny()
                        .orElse(null),
                is(notNullValue()));
    }
}

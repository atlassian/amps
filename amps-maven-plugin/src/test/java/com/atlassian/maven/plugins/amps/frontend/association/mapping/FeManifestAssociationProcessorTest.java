package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static com.atlassian.maven.plugins.amps.frontend.CopyFeModuleManifestsMojo.MODULE_MANIFESTS_OUTPUT_DIR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociation;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

public class FeManifestAssociationProcessorTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    @Mock
    DirectoryHelper directoryHelper;

    private FeManifestAssociationProcessor underTest;

    @Before
    public void setup() {
        underTest = new FeManifestAssociationProcessor(directoryHelper, new JsonParser());

        when(directoryHelper.getOutputDirectoryPath()).thenReturn(ResourceHelper.getResourceAbsolutePath("output"));
        when(directoryHelper.getOutputFileAbsolutePath(MODULE_MANIFESTS_OUTPUT_DIR))
                .thenReturn(ResourceHelper.getResourceAbsolutePath("output"));
        when(directoryHelper.getSourceDirectoryPath()).thenReturn(ResourceHelper.getResourceAbsolutePath(""));
        when(directoryHelper.getSourceFileAbsolutePath(anyString())).thenCallRealMethod();
    }

    @Test
    public void testTransformManifestAssociationsToFileDeclarations_whenEmptyInputList_shouldReturnEmpty()
            throws MojoExecutionException {
        // Arrange
        List<FeManifestAssociation> manifestAssociations = Collections.emptyList();

        // Act
        Map<String, FeOutputJsFileDeclaration> result =
                underTest.transformManifestAssociationsToFileDeclarations(manifestAssociations);

        // Assert
        assertEquals(Collections.emptyMap(), result);
    }

    @Test
    public void testTransformManifestAssociationsToFileDeclarations_whenDuplicateFileDeclarations_shouldReturnUnique()
            throws MojoExecutionException {
        // Arrange
        List<FeManifestAssociation> manifestAssociations = Arrays.asList(
                new FeManifestAssociation("test-package-with-output", Arrays.asList("file1"), "origin1"),
                new FeManifestAssociation("test-package-with-output", Arrays.asList("file1"), "origin1"));

        // Act
        Map<String, FeOutputJsFileDeclaration> result =
                underTest.transformManifestAssociationsToFileDeclarations(manifestAssociations);

        // Assert
        Map<String, FeOutputJsFileDeclaration> expected = new HashMap<>();
        putFileDeclaration(expected, "file1", Arrays.asList("origin1"), "manifests/package.json");

        assertEquals(expected, result);
    }

    @Test
    public void
            testTransformManifestAssociationsToFileDeclarations_whenFilesInMultipleDeclarations_shouldProduceSetOfDeclarations()
                    throws MojoExecutionException {
        // Arrange
        List<FeManifestAssociation> manifestAssociations = Arrays.asList(
                new FeManifestAssociation("test-package-with-output", Arrays.asList("file1", "file2"), "origin1"),
                new FeManifestAssociation("test-package-with-output", Arrays.asList("file2", "file3"), "origin2"),
                new FeManifestAssociation(
                        "test-package-with-output-2", Arrays.asList("file_solo1", "file_solo2"), "origin_solo"));

        // Act
        Map<String, FeOutputJsFileDeclaration> result =
                underTest.transformManifestAssociationsToFileDeclarations(manifestAssociations);

        // Assert
        Map<String, FeOutputJsFileDeclaration> expected = new HashMap<>();
        putFileDeclaration(expected, "file1", Arrays.asList("origin1"), "manifests/package.json");
        putFileDeclaration(expected, "file2", Arrays.asList("origin1", "origin2"), "manifests/package.json");
        putFileDeclaration(expected, "file3", Arrays.asList("origin2"), "manifests/package.json");
        putFileDeclaration(expected, "file_solo1", Arrays.asList("origin_solo"), "manifests/other/package.json");
        putFileDeclaration(expected, "file_solo2", Arrays.asList("origin_solo"), "manifests/other/package.json");

        assertThat(result, equalTo(expected));
    }

    @Test
    public void testGetPackageNamesMappedToOutputManifestPaths_whenAllManifestsAvailable_shouldUpdatePaths()
            throws MojoExecutionException {
        // given
        List<FeManifestAssociation> manifestAssociations = Collections.singletonList(
                new FeManifestAssociation("test-package-with-output", Collections.singletonList("file1"), "origin1"));

        // when
        Map<String, String> result = underTest.getPackageNamesMappedToOutputManifestPaths(manifestAssociations);

        // then
        assertThat(result.size(), equalTo(1));
        assertThat(result.get("test-package-with-output"), equalTo("manifests/package.json"));
    }

    @Test
    public void testGetPackageNamesMappedToOutputManifestPaths_whenAnyManifestMissing_shouldThrowException() {
        // given
        List<FeManifestAssociation> manifestAssociations = Collections.singletonList(new FeManifestAssociation(
                "package-with-no-output-mirror.json", Collections.singletonList("file1"), "origin1"));

        // when
        final ThrowingRunnable runnable =
                () -> underTest.getPackageNamesMappedToOutputManifestPaths(manifestAssociations);

        // then
        assertThrows(MojoExecutionException.class, runnable);
    }

    @Test
    public void testGetPackageNamesMappedToOutputManifestPaths_whenManifestDirNotExists_shouldThrowException() {
        // given
        when(directoryHelper.getOutputFileAbsolutePath(MODULE_MANIFESTS_OUTPUT_DIR))
                .thenCallRealMethod();
        List<FeManifestAssociation> manifestAssociations = Collections.singletonList(new FeManifestAssociation(
                "package-with-no-output-mirror.json", Collections.singletonList("file1"), "origin1"));

        // when
        final ThrowingRunnable runnable =
                () -> underTest.getPackageNamesMappedToOutputManifestPaths(manifestAssociations);

        // then
        MojoExecutionException exception = assertThrows(MojoExecutionException.class, runnable);
        assertThat(
                exception.getMessage(),
                equalTo(
                        "Directory with frontend module manifests (META-INF/fe-module-manifests) couldn't be found. Please make sure goal `copy-fe-module-manifests` is configured to include package.json and lock files in build output."));
    }

    private void putFileDeclaration(
            Map<String, FeOutputJsFileDeclaration> fileDeclarations,
            String filepath,
            List<String> declarations,
            String manifestPath) {
        fileDeclarations.put(
                filepath, new FeOutputJsFileDeclaration(filepath, new HashSet<>(declarations), manifestPath));
    }
}

package com.atlassian.maven.plugins.amps.util;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import org.junit.Test;

public class StreamUtilsTest {

    @Test
    public void stream_whenEnumerationIsNull_shouldReturnEmptyStream() {
        // Act
        final List<?> contents = StreamUtils.stream(null).collect(toList());

        // Assert
        assertThat(contents, is(empty()));
    }

    @Test
    public void stream_whenEnumerationIsEmpty_shouldReturnEmptyStream() {
        assertStream();
    }

    @Test
    public void stream_whenEnumerationIsNonEmpty_shouldReturnAllElements() {
        assertStream("foo", "bar");
    }

    @SafeVarargs
    private static <T> void assertStream(final T... elements) {
        // Arrange
        final Enumeration<T> enumeration = new Vector<>(asList(elements)).elements();

        // Act
        final List<T> contents = StreamUtils.stream(enumeration).collect(toList());

        // Assert
        assertThat(contents, is(asList(elements)));
    }
}

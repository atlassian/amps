package com.atlassian.maven.plugins.amps.analytics.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

public class UserAgentTest {

    private UserAgent userAgent;

    @Before
    public void setUp() {
        final Locale locale = Locale.CANADA_FRENCH; // final class => can't be mocked
        userAgent = new UserAgent(locale, "theOsName", "theOsVersion", "theProduct", "theProductVersion");
    }

    @Test
    public void getHeaderValue_whenInvoked_shouldReturnExpectedValue() {
        // Act
        final String headerValue = userAgent.getHeaderValue();

        // Assert
        assertThat(
                headerValue,
                is("theProduct/theProductVersion"
                        + "(compatible; theProduct theProductVersion; theOsName theOsVersion; fr-ca)"));
    }
}

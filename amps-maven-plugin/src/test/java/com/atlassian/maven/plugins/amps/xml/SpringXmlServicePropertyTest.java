package com.atlassian.maven.plugins.amps.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SpringXmlServicePropertyTest {
    private static final String KEY = "foo";
    private static final String VALUE = "bar";

    @Test
    public void getters_values_returnsValues() {
        SpringXmlServiceProperty sut =
                SpringXmlServiceProperty.builder().key("foo").value("bar").build();

        assertEquals("foo", sut.key());
        assertEquals("bar", sut.value());
    }

    @Test
    public void equals_null_returnsFalse() {
        SpringXmlServiceProperty sut = builder().build();

        assertFalse(sut.equals(null));
    }

    @Test
    public void equals_notInstanceOf_returnsFalse() {
        SpringXmlServiceProperty sut = builder().build();

        assertFalse(sut.equals(new Object()));
    }

    @Test
    public void equals_differentKey_returnsFalse() {
        SpringXmlServiceProperty sut = builder().build();
        SpringXmlServiceProperty other = builder().key("qux").build();

        assertFalse(sut.equals(other));
        assertNotEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_differentValue_returnsFalse() {
        SpringXmlServiceProperty sut = builder().build();
        SpringXmlServiceProperty other = builder().value("corge").build();

        assertFalse(sut.equals(other));
        assertNotEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_equal_returnsTrue() {
        SpringXmlServiceProperty sut = builder().build();
        SpringXmlServiceProperty other = builder().build();

        assertTrue(sut.equals(other));
        assertEquals(sut.hashCode(), other.hashCode());
    }

    @Test
    public void equals_same_returnsTrue() {
        SpringXmlServiceProperty sut = builder().build();

        assertTrue(sut.equals(sut));
    }

    @Test
    public void build_defaultKeyValue_valueIsBlank() {
        SpringXmlServiceProperty sut =
                SpringXmlServiceProperty.builder().key(KEY).build();

        assertEquals("", sut.value());
    }

    @Test
    public void build_keyIsNotSet_thowsIllegalStateException() {
        SpringXmlServiceProperty.Builder builder =
                SpringXmlServiceProperty.builder().value(VALUE);

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> builder.build());
        assertEquals("'key' property is mandatory", exception.getMessage());
    }

    @Test
    public void setKey_null_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().key(null));
        assertEquals("'key' must not be null", exception.getMessage());
    }

    @Test
    public void setKey_blank_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().key(""));
        assertEquals("'key' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void setKey_whitespace_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().key("\t"));
        assertEquals("'key' must not be blank or whitespace", exception.getMessage());
    }

    @Test
    public void setValue_null_throwsIllegalArgumentException() {
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () -> builder().value(null));
        assertEquals("'value' must not be null", exception.getMessage());
    }

    private static SpringXmlServiceProperty.Builder builder() {
        return SpringXmlServiceProperty.builder().key(KEY).value(VALUE);
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

public class FeDeclarationWriterTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    @Rule
    public final TemporaryFolder tempDir = new TemporaryFolder();

    @Mock
    DirectoryHelper directoryHelper;

    private FeDeclarationWriter underTest;

    private String outputDirectory;

    @Before
    public void setup() throws IOException {
        underTest = new FeDeclarationWriter(directoryHelper, new JsonParser());

        outputDirectory = tempDir.newFolder().getPath();
        when(directoryHelper.getOutputDirectoryPath()).thenReturn(outputDirectory);
        when(directoryHelper.getOutputFileAbsolutePath(anyString())).thenCallRealMethod();
    }

    @Test
    public void testWriteManifestAssociation_whenWriting_shouldCreateFile() throws MojoExecutionException {
        // given
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("file1.js", createJsFileDeclaration("file1.js"));

        // when
        underTest.writeManifestAssociation(outputJsFileDeclarations);

        // then
        File expectedOutputFile =
                new File(outputDirectory, "META-INF/fe-manifest-associations/fe-manifest-association.json");
        assertTrue(expectedOutputFile.exists());
    }

    @Test
    public void testWriteManifestAssociation_whenWritingMultipleTimes_shouldNotFail() throws MojoExecutionException {
        // given
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("file1.js", createJsFileDeclaration("file1.js"));

        // when
        underTest.writeManifestAssociation(outputJsFileDeclarations);
        underTest.writeManifestAssociation(outputJsFileDeclarations);

        // then OK
    }

    @Test
    public void testWriteExternalDependencies_shouldCreateDeclarationFile() throws MojoExecutionException {
        FeDependencyDeclaration declaration =
                new FeDependencyDeclaration("test/path", Collections.singletonList("test/file"));

        // when
        underTest.writeDependencyDeclarations(Collections.singletonList(declaration));

        // then
        File expectedOutputFile = new File(
                outputDirectory, "META-INF/fe-manifest-associations/fe-external-dependencies-with-js-files.json");
        assertTrue(expectedOutputFile.exists());
    }

    private FeOutputJsFileDeclaration createJsFileDeclaration(String filepath) {
        return new FeOutputJsFileDeclaration(
                filepath, new HashSet<>(Collections.singletonList("pom.xml")), "package.json");
    }
}

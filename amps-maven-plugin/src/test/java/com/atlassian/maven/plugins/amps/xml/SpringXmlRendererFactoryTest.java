package com.atlassian.maven.plugins.amps.xml;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

import org.junit.Test;

public class SpringXmlRendererFactoryTest {
    @Test
    public void create() {
        SpringXmlRendererFactory sut = new SpringXmlRendererFactory();
        SpringXmlRenderer renderer = sut.create();

        assertThat(renderer, instanceOf(SpringXmlRendererImpl.class));
    }
}

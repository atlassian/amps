package com.atlassian.maven.plugins.amps.pdk;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.PdkParams;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.atlassian.maven.plugins.amps.util.MojoExecutorWrapper;

public class InstallMojoTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private InstallMojo installMojo;

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MavenProject currentProject;

    @Mock
    private Log log;

    @Mock
    private RepositorySystem repositorySystem;

    @Mock
    private WebAppManager webAppManager;

    @Mock
    private MojoExecutorWrapper mojoExecutorWrapper;

    @Mock
    private ArtifactResolver artifactResolver;

    @Mock
    private MavenGoals mavenGoals;

    @Mock
    private Build build;

    private ArgumentCaptor<PdkParams> pdkParamsArgumentCaptor = ArgumentCaptor.forClass(PdkParams.class);

    @Before
    public void setUp() {
        when(mavenContext.getLog()).thenReturn(log);

        when(mavenContext.getBuildDirectory()).thenReturn("theBuildDirectory");
        when(currentProject.getGroupId()).thenReturn("groupid");
        when(currentProject.getArtifactId()).thenReturn("artifactid");
        when(currentProject.getBuild()).thenReturn(build);

        when(build.getFinalName()).thenReturn("theFinalName");
        when(mavenContext.getProject()).thenReturn(currentProject);
    }

    @Test
    public void shouldAlwaysUseJarFile() throws MojoExecutionException {
        doNothing().when(mavenGoals).installPlugin(pdkParamsArgumentCaptor.capture());

        when(currentProject.getPackaging()).thenReturn("atlassian-plugin");
        installMojo.installPlugin(false);
        when(currentProject.getPackaging()).thenReturn("jar");
        installMojo.installPlugin(false);
        when(currentProject.getPackaging()).thenReturn("obr");
        installMojo.installPlugin(false);

        List<PdkParams> params = pdkParamsArgumentCaptor.getAllValues();

        Assertions.assertThat(params)
                .extracting(PdkParams::getPluginFile)
                .isNotEmpty()
                .allMatch("theBuildDirectory/theFinalName.jar"::equals);
    }
}

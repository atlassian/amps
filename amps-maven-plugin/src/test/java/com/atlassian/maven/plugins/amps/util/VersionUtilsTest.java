package com.atlassian.maven.plugins.amps.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class VersionUtilsTest {

    @Test
    public void getVersion_whenPomPropertiesNotFound_shouldLiterallyReturnRelease() {
        // Act
        final String version = VersionUtils.getVersion();

        // Assert
        assertThat(version, is("RELEASE"));
    }
}

package com.atlassian.maven.plugins.amps;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.project.MavenProject;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.common.collect.ImmutableSet;

public class BannedArtifactsProviderTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    private static final Set<String> PUBLIC_ARTIFACT_IDS =
            ImmutableSet.of("platform-public-api", "platform-deprecated-public-api");
    private static final Set<String> ALL_ARTIFACT_IDS =
            ImmutableSet.of("platform-public-api", "platform-deprecated-public-api", "platform-internal-api");
    private static final String INTERNAL_PLUGIN_GROUP_ID = "com.atlassian.my-plugin";
    private static final String EXTERNAL_PLUGIN_GROUP_ID = "org.example.my-plugin";

    @Mock
    private MavenProject mavenProject;

    @InjectMocks
    private BannedArtifactsProviderImpl sut;

    @Test
    public void getBannedArtifact_internalPrefixedAndNotForced_returnsInternal() {
        setGroupId(INTERNAL_PLUGIN_GROUP_ID);

        assertEquals(ALL_ARTIFACT_IDS, sut.getBannedArtifacts(false));
    }

    @Test
    public void getBannedArtifact_internalPrefixedAndForced_returnsInternal() {
        assertEquals(ALL_ARTIFACT_IDS, sut.getBannedArtifacts(true));
    }

    @Test
    public void getBannedArtifact_externalPrefixedButForced_returnsInternal() {
        assertEquals(ALL_ARTIFACT_IDS, sut.getBannedArtifacts(true));
    }

    @Test
    public void getBannedArtifact_externalPrefixedAndNotForced_returnsExternal() {
        setGroupId(EXTERNAL_PLUGIN_GROUP_ID);

        assertEquals(PUBLIC_ARTIFACT_IDS, sut.getBannedArtifacts(false));
    }

    private void setGroupId(String groupId) {
        Artifact artifact = mock(Artifact.class);
        doReturn(groupId).when(artifact).getGroupId();
        doReturn(artifact).when(mavenProject).getArtifact();
    }
}

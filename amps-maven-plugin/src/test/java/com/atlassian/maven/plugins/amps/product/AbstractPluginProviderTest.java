package com.atlassian.maven.plugins.amps.product;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;

public class AbstractPluginProviderTest {

    private Product product = mock(Product.class);

    @Test
    public void ensureAllPluginsWillBeReturned() {
        List<ProductArtifact> plugins =
                singletonList(new ProductArtifact("com.atlassian.plugin", "plugin-api", "1.0.0"));
        when(product.getPluginArtifacts()).thenReturn(plugins);

        List<ProductArtifact> provide = new TestPluginProvider().provide(product, null);

        assertContains(plugins, provide);
    }

    @Test
    public void ensureSelfWillBeIgnored() {
        List<ProductArtifact> plugins =
                singletonList(new ProductArtifact("com.atlassian.plugin", "plugin-api", "1.0.0"));
        when(product.getPluginArtifacts()).thenReturn(plugins);

        List<ProductArtifact> provide =
                new TestPluginProvider().provide(product, new ProductArtifact("ignored", "ignored"));

        assertContains(plugins, provide);
    }

    @Test
    public void ensureSALArtifactsWillBeAdded() {
        List<ProductArtifact> plugins =
                singletonList(new ProductArtifact("com.atlassian.plugin", "plugin-api", "1.0.0"));

        String salVersion = "2.0.0";
        when(product.getPluginArtifacts()).thenReturn(plugins);
        when(product.getSalVersion()).thenReturn(salVersion);

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide = pluginProvider.provide(product, null);

        ArrayList<ProductArtifact> expected = new ArrayList<>(plugins);
        expected.add(new ProductArtifact("com.atlassian.sal", "sal-api", salVersion));
        expected.add(new ProductArtifact("com.atlassian.sal", "sal-bamboo-plugin", salVersion));

        assertContains(expected, provide);
    }

    @Test
    public void ensureUserDefinedSALVersionWillBeUsed() {
        List<ProductArtifact> plugins = singletonList(new ProductArtifact("com.atlassian.sal", "sal-api", "1.0.0"));

        String salVersion = "2.0.0";
        when(product.getPluginArtifacts()).thenReturn(plugins);
        when(product.getSalVersion()).thenReturn(salVersion);

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide = pluginProvider.provide(product, null);

        ArrayList<ProductArtifact> expected = new ArrayList<>(plugins);
        expected.add(new ProductArtifact("com.atlassian.sal", "sal-bamboo-plugin", salVersion));

        assertContains(expected, provide);
    }

    // addresses case described in https://ecosystem.atlassian.net/browse/AMPS-1609
    @Test
    public void ensureSelfSALWillBeRemoved() {
        List<ProductArtifact> plugins =
                singletonList(new ProductArtifact("com.atlassian.plugin", "plugin-api", "1.0.0"));

        String salVersion = "2.0.0";
        when(product.getPluginArtifacts()).thenReturn(plugins);
        when(product.getSalVersion()).thenReturn(salVersion);

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide =
                pluginProvider.provide(product, new ProductArtifact("com.atlassian.sal", "sal-api", salVersion));

        ArrayList<ProductArtifact> expected = new ArrayList<>(plugins);
        expected.add(new ProductArtifact("com.atlassian.sal", "sal-bamboo-plugin", salVersion));

        assertContains(expected, provide);
    }

    @Test
    public void shouldIncludeRestApiBrowserIfVersionOfDevtoolBoxIsLessThan4() {
        String toolboxVersion = "3.9.99";

        when(product.getPluginArtifacts()).thenReturn(new LinkedList<>());
        when(product.isEnableDevToolbox()).thenReturn(true);
        when(product.getDevToolboxVersion()).thenReturn(toolboxVersion);

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide = pluginProvider.provide(product, null);

        ArrayList<ProductArtifact> expected = new ArrayList<>();
        expected.add(new ProductArtifact("com.atlassian.devrel", "developer-toolbox-plugin", toolboxVersion));
        expected.add(new ProductArtifact("com.atlassian.labs", "rest-api-browser", "3.2.8"));

        assertContains(expected, provide);
    }

    @Test
    public void shouldNotIncludeWebConsoleIfDisabled() {

        when(product.getPluginArtifacts()).thenReturn(new LinkedList<>());
        when(product.isEnableWebConsole()).thenReturn(false);
        when(product.getWebConsoleVersion()).thenReturn("1.2.3");

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide = pluginProvider.provide(product, null);

        ArrayList<ProductArtifact> expected = new ArrayList<>();

        assertContains(expected, provide);
    }

    @Test
    public void shouldIncludeWebConsoleIfEnabled() {

        when(product.getPluginArtifacts()).thenReturn(new LinkedList<>());
        when(product.isEnableWebConsole()).thenReturn(true);
        when(product.getWebConsoleVersion()).thenReturn("1.2.3");

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide = pluginProvider.provide(product, null);

        ArrayList<ProductArtifact> expected = new ArrayList<>();
        expected.add(new ProductArtifact("org.apache.felix", "org.apache.felix.webconsole", "1.2.3"));
        expected.add(new ProductArtifact("org.apache.felix", "org.osgi.compendium", "1.2.0"));
        expected.add(new ProductArtifact("com.atlassian.labs.httpservice", "httpservice-bridge", "0.6.2"));

        assertContains(expected, provide);
    }

    @Test
    public void shouldUseJakartaMigratedRestApiBrowserIfVersionOfDevtoolBoxIsGreaterOrEqualTo4() {
        String toolboxVersion = "4.0.0";

        when(product.getPluginArtifacts()).thenReturn(new LinkedList<>());
        when(product.isEnableDevToolbox()).thenReturn(true);
        when(product.getDevToolboxVersion()).thenReturn(toolboxVersion);

        TestPluginProvider pluginProvider = new TestPluginProvider();
        List<ProductArtifact> provide = pluginProvider.provide(product, null);

        ArrayList<ProductArtifact> expected = new ArrayList<>();
        expected.add(new ProductArtifact("com.atlassian.devrel", "developer-toolbox-plugin", toolboxVersion));
        expected.add(new ProductArtifact("com.atlassian.labs", "rest-api-browser", "4.0.0-jakarta-m001"));

        assertContains(expected, provide);
    }

    private void assertContains(Collection<?> expected, Collection<?> actual) {
        for (Object plugin : expected) {
            assertTrue("Missing expected " + plugin, actual.contains(plugin));
        }

        for (Object provided : actual) {
            assertTrue("Found unexpected " + provided, expected.contains(provided));
        }
    }

    private static class TestPluginProvider extends AbstractPluginProvider {

        @Override
        protected Collection<ProductArtifact> getSalArtifacts(String salVersion) {
            return Arrays.asList(
                    new ProductArtifact("com.atlassian.sal", "sal-api", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-bamboo-plugin", salVersion));
        }
    }
}

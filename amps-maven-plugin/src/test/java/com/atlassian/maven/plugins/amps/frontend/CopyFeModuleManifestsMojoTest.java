package com.atlassian.maven.plugins.amps.frontend;

import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.commons.lang3.SystemUtils;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.maven.plugins.amps.MavenContext;

public class CopyFeModuleManifestsMojoTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    @Rule
    public final TemporaryFolder tempDir = new TemporaryFolder();

    private CopyFeModuleManifestsMojo underTest;

    @Mock
    Log logger;

    @Before
    public void setup() {
        underTest = new CopyFeModuleManifestsMojo();
    }

    @Test
    public void testDirectoryCreation_shouldNotFailIfItAlreadyExists() throws IOException, MojoExecutionException {
        // given
        final String outputDirectory = tempDir.newFolder().getPath();

        // when
        underTest.createOutputSubdirectory(outputDirectory);
        underTest.createOutputSubdirectory(outputDirectory);

        // then everything went all good
    }

    @Test
    public void testFileCopies_shouldStillWorkIfTheFileAlreadyExists() throws IOException, MojoExecutionException {
        final File source = tempDir.newFile();
        final File dest = tempDir.newFile();

        underTest.copyWithOverwrite(source, dest, logger);
        underTest.copyWithOverwrite(source, dest, logger);
    }

    @Test
    public void testHappyPath_shouldPassValidation() throws Throwable {
        // given
        final String filepath = getClass().getResource("package.json").getPath();

        // when
        final ThrowingRunnable runnable = () -> underTest.validateDeclaredFiles(singleton(filepath), logger);

        // then
        runnable.run();
        verifyNoInteractions(logger);
    }

    @Test
    public void testUnsupportedFilenames_shouldFailValidation() throws MojoExecutionException {
        // given
        final String filepath = "/very-unlikely-to-be-supported.json";

        // when
        final ThrowingRunnable runnable = () -> underTest.validateDeclaredFiles(singleton(filepath), logger);

        // then
        assertThrows(MojoExecutionException.class, runnable);
        verify(logger)
                .error("Only [package.json, package-lock.json, yarn.lock] files are supported, "
                        + "not /very-unlikely-to-be-supported.json");
    }

    @Test
    public void testMissingMavenVariableInFilepath_shouldFailValidation() {
        // given
        final String filepath = "${project.parent.basedir}/package.json";

        // when
        final ThrowingRunnable runnable = () -> underTest.validateDeclaredFiles(singleton(filepath), logger);

        // then
        assertThrows(MojoExecutionException.class, runnable);
        verify(logger).error("File doesn't exist: ${project.parent.basedir}/package.json");
    }

    @Test
    public void testGetCommonDirRelativePath_whenOnUnixEnv_shouldResolveRelativePath() throws MojoExecutionException {
        Assume.assumeFalse(SystemUtils.IS_OS_WINDOWS);

        // given
        final File baseDir = new File("/build/NEW-COREDAILYJDKLTST583-WINUT/components/bamboo-web-app");
        final String absolutePath = "/build/NEW-COREDAILYJDKLTST583-WINUT/components/bamboo-web-app/package.json";

        // when
        final String relativePath = underTest.getCommonDirRelativePath(baseDir, absolutePath);

        // then
        assertEquals("package.json", relativePath);
    }

    @Test
    public void testGetCommonDirRelativePath_whenOnWindowsEnv_shouldResolveRelativePath()
            throws MojoExecutionException {
        Assume.assumeTrue(SystemUtils.IS_OS_WINDOWS);

        // given
        final File baseDir = new File("C:\\build\\NEW-COREDAILYJDKLTST583-WINUT\\components\\bamboo-web-app");
        final String absolutePath =
                "C:\\build\\NEW-COREDAILYJDKLTST583-WINUT\\components\\bamboo-web-app\\package.json";

        // when
        final String relativePath = underTest.getCommonDirRelativePath(baseDir, absolutePath);

        // then
        assertEquals("package.json", relativePath);
    }

    @Test
    public void getOutputDirectory_WHEN_targetDirectoryIsSet_THEN_returnOutputDirectoryContainsGivenFolderInItsPath()
            throws NoSuchFieldException, IllegalAccessException {
        // given
        String givenTargetDirectory = "givenTargetDirectory";
        CopyFeModuleManifestsMojo sut = createSutWithTargetDirectory(givenTargetDirectory);

        // when
        String outputDirectory = sut.getOutputDirectory();

        // then
        assertEquals(givenTargetDirectory, outputDirectory);
    }

    @Test
    public void getOutputDirectory_WHEN_targetDirectoryIsNotSet_THEN_returnOutputDirectoryContainsMavenOutputDirectory()
            throws NoSuchFieldException, IllegalAccessException {
        // given
        String givenMavenOutputDirectory = "givenMavenOutputDirectory";
        CopyFeModuleManifestsMojo sut = createSutWithoutTargetDirectory(givenMavenOutputDirectory);

        // when
        String outputDirectory = sut.getOutputDirectory();

        // then
        assertEquals(givenMavenOutputDirectory, outputDirectory);
    }

    private CopyFeModuleManifestsMojo createSutWithTargetDirectory(String targetDirectory)
            throws NoSuchFieldException, IllegalAccessException {
        CopyFeModuleManifestsMojo sut = new CopyFeModuleManifestsMojo();

        Field targetDirectoryField = CopyFeModuleManifestsMojo.class.getDeclaredField("feSbomTargetDirectory");
        targetDirectoryField.setAccessible(true);

        targetDirectoryField.set(sut, targetDirectory);

        return sut;
    }

    private CopyFeModuleManifestsMojo createSutWithoutTargetDirectory(String mockedMavenOutputDirectory)
            throws NoSuchFieldException, IllegalAccessException {
        CopyFeModuleManifestsMojo sut = new CopyFeModuleManifestsMojo();

        MavenContext mavenContext = Mockito.mock(MavenContext.class);
        MavenProject mavenProject = Mockito.mock(MavenProject.class);
        Build build = Mockito.mock(Build.class);

        when(mavenContext.getProject()).thenReturn(mavenProject);
        when(mavenProject.getBuild()).thenReturn(build);
        when(build.getOutputDirectory()).thenReturn(mockedMavenOutputDirectory);

        sut.setMavenContext(mavenContext);

        return sut;
    }
}

package com.atlassian.maven.plugins.amps;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.version.Version;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class PlatformDependenciesProviderImplTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder artifactsDirectory = new TemporaryFolder();

    @Mock
    private RepositorySystem repositorySystem;

    @Mock
    private MavenProject mavenProject;

    @Mock
    private Build build;

    @InjectMocks
    private PlatformDependenciesProviderImpl sut;

    private final MavenXpp3Writer mavenWriter = new MavenXpp3Writer();

    @Before
    public void setUp() {
        doReturn(build).when(mavenProject).getBuild();
        doReturn(artifactsDirectory.getRoot().toString()).when(build).getDirectory();
    }

    @Test
    public void getDependencies_everythingGoesWell_returnsSortedList() throws Exception {
        String groupId = "com.example.foo";
        Collection<String> artifactIds = Arrays.asList(new String[] {"bar", "baz", "quux"});
        String version = "4.5.23";

        configureResolveVersionRangeSuccess(groupId, "bar", version, new String[] {"1.2.3"});
        configureResolveVersionRangeSuccess(groupId, "baz", version, new String[] {"5.2.2", "6.2.1"});
        configureResolveVersionRangeSuccess(groupId, "quux", version, new String[] {"8.0.0", "9.9.2"});

        File barPom = createPomFile(
                createDependency("com.example.nature", "flower"),
                createDependency("com.example.notebook", "page"),
                createDependency("com.sample.building", "bank"));
        File bazPom = createPomFile(createDependency("com.sample.building", "bank"));
        File quuxPom = createPomFile(
                createDependency("com.sample.sport", "football"),
                createDependency("com.example.sport", "swimming"),
                createDependency("org.example.it", "screen"));

        configureResolveArtifactSuccess(groupId, "bar", "1.2.3", barPom);
        configureResolveArtifactSuccess(groupId, "baz", "6.2.1", bazPom);
        configureResolveArtifactSuccess(groupId, "quux", "9.9.2", quuxPom);

        List<String> expected = Arrays.asList(new String[] {
            "com.example.nature:flower",
            "com.example.notebook:page",
            "com.example.sport:swimming",
            "com.sample.building:bank",
            "com.sample.sport:football",
            "org.example.it:screen"
        });

        assertEquals(expected, sut.getDependencies(groupId, artifactIds, version));
    }

    @Test
    public void getDependencies_versionResolutionFails_throws() throws Exception {
        String groupId = "com.example.foo";
        Collection<String> artifactIds = Arrays.asList(new String[] {"bar"});
        String version = "4.5.23";
        VersionRangeResolutionException innerException = mock(VersionRangeResolutionException.class);

        configureResolveVersionRangeFailure(innerException);

        PlatformDependencyProvisioningException exception = assertThrows(
                PlatformDependencyProvisioningException.class,
                () -> sut.getDependencies(groupId, artifactIds, version));

        assertEquals("Could not find artifact com.example.foo:bar for version range 4.5.23", exception.getMessage());
        assertEquals(innerException, exception.getCause());
    }

    @Test
    public void getDependencies_artifactResolutionFails_throws() throws Exception {
        String groupId = "com.example.foo";
        Collection<String> artifactIds = Arrays.asList(new String[] {"bar"});
        String version = "4.5.23";
        ArtifactResolutionException innerException = mock(ArtifactResolutionException.class);

        configureResolveVersionRangeSuccess(groupId, "bar", version, new String[] {"5.2.3"});
        configureResolveArtifactFailure(innerException);

        PlatformDependencyProvisioningException exception = assertThrows(
                PlatformDependencyProvisioningException.class,
                () -> sut.getDependencies(groupId, artifactIds, version));

        assertEquals("Could not load artifact com.example.foo:bar:5.2.3", exception.getMessage());
        assertEquals(innerException, exception.getCause());
    }

    @Test
    public void getDependencies_unparsablePom_throws() throws Exception {
        String groupId = "com.example.foo";
        Collection<String> artifactIds = Arrays.asList(new String[] {"bar", "baz", "quux"});
        String version = "4.5.23";

        configureResolveVersionRangeSuccess(groupId, "bar", version, new String[] {"5.2.3"});
        File barPom = createPomFileWithContent("<project");
        configureResolveArtifactSuccess(groupId, "bar", "5.2.3", barPom);

        PlatformDependencyProvisioningException exception = assertThrows(
                PlatformDependencyProvisioningException.class,
                () -> sut.getDependencies(groupId, artifactIds, version));

        String expectedMessage = String.format("File %s is not a valid XML or cannot be read", barPom.toString());
        assertEquals(expectedMessage, exception.getMessage());
        assertThat(exception.getCause(), instanceOf(IOException.class));
    }

    @Test
    public void getDependencies_unparsablePom_throws2() throws Exception {
        String groupId = "com.example.foo";
        Collection<String> artifactIds = Arrays.asList(new String[] {"bar", "baz", "quux"});
        String version = "4.5.23";

        configureResolveVersionRangeSuccess(groupId, "bar", version, new String[] {"5.2.3"});
        File barPom = createPomFileWithContent("<foo />");
        configureResolveArtifactSuccess(groupId, "bar", "5.2.3", barPom);

        PlatformDependencyProvisioningException exception = assertThrows(
                PlatformDependencyProvisioningException.class,
                () -> sut.getDependencies(groupId, artifactIds, version));

        String expectedMessage = String.format("File %s is not a valid POM", barPom.toString());
        assertEquals(expectedMessage, exception.getMessage());
        assertThat(exception.getCause(), instanceOf(XmlPullParserException.class));
    }

    private void configureResolveVersionRangeSuccess(
            String groupId, String artifactId, String inputVersion, String[] outputVersions)
            throws VersionRangeResolutionException {
        VersionRangeResult result = new VersionRangeResult(new VersionRangeRequest());
        result.setVersions(Arrays.stream(outputVersions)
                .map(versionString -> new FakeVersion(versionString))
                .collect(toList()));

        Artifact artifact = new DefaultArtifact(groupId, artifactId, "pom", inputVersion);

        doReturn(result)
                .when(repositorySystem)
                .resolveVersionRange(
                        any(RepositorySystemSession.class), argThat(new VersionRangeRequestArgumentMatcher(artifact)));
    }

    private void configureResolveVersionRangeFailure(Throwable innerException) throws VersionRangeResolutionException {
        doThrow(innerException)
                .when(repositorySystem)
                .resolveVersionRange(any(RepositorySystemSession.class), any(VersionRangeRequest.class));
    }

    private void configureResolveArtifactSuccess(String groupId, String artifactId, String version, File file)
            throws ArtifactResolutionException {
        ArtifactResult result = new ArtifactResult(new ArtifactRequest());
        Artifact artifact = new DefaultArtifact(groupId, artifactId, "pom", version);
        result.setArtifact(artifact.setFile(file));

        doReturn(result)
                .when(repositorySystem)
                .resolveArtifact(
                        any(RepositorySystemSession.class), argThat(new ArtifactRequestArgumentMatcher(artifact)));
    }

    private void configureResolveArtifactFailure(Throwable innerException) throws ArtifactResolutionException {
        doThrow(innerException)
                .when(repositorySystem)
                .resolveArtifact(any(RepositorySystemSession.class), any(ArtifactRequest.class));
    }

    private File createPomFile(Dependency... dependencies) throws Exception {
        Model model = new Model();
        model.setDependencies(Arrays.asList(dependencies));
        File file = artifactsDirectory.newFile();
        mavenWriter.write(new FileOutputStream(file), model);
        return file;
    }

    private File createPomFileWithContent(String content) throws Exception {
        File file = artifactsDirectory.newFile();
        Files.write(file.toPath(), content.getBytes(UTF_8));
        return file;
    }

    private Dependency createDependency(String groupId, String artifactId) {
        Dependency dependency = new Dependency();
        dependency.setGroupId(groupId);
        dependency.setArtifactId(artifactId);
        return dependency;
    }

    private static class VersionRangeRequestArgumentMatcher implements ArgumentMatcher<VersionRangeRequest> {
        private final Artifact artifact;

        VersionRangeRequestArgumentMatcher(Artifact artifact) {
            this.artifact = artifact;
        }

        @Override
        public boolean matches(VersionRangeRequest request) {
            return request.getArtifact().equals(artifact);
        }
    }

    private static class ArtifactRequestArgumentMatcher implements ArgumentMatcher<ArtifactRequest> {
        private final Artifact artifact;

        ArtifactRequestArgumentMatcher(Artifact artifact) {
            this.artifact = artifact;
        }

        @Override
        public boolean matches(ArtifactRequest request) {
            return request.getArtifact().equals(artifact);
        }
    }

    private static class FakeVersion implements Version {
        private final String versionString;

        FakeVersion(String versionString) {
            this.versionString = versionString;
        }

        @Override
        public String toString() {
            return versionString;
        }

        @Override
        public int compareTo(Version other) {
            return versionString.compareTo(other.toString());
        }
    }
}

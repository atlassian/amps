package com.atlassian.maven.plugins.amps;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.twdata.maven.mojoexecutor.MojoExecutor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

@RunWith(Parameterized.class)
public class BannedDependenciesTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    private static final String PLATFORM_VERSION = "33.2.1";
    private static final Set<String> ARTIFACT_IDS = ImmutableSet.of("foo-bar", "foo-qux", "baz");
    private static final String PLATFORM_GROUP_ID = "com.atlassian.platform.dependencies";
    private final boolean forceInternal;

    @Mock
    private PlatformDependenciesProvider platformDependenciesProvider;

    @Mock
    private LegacyBannedDependenciesProvider legacyBannedDependenciesProvider;

    @Mock
    private BannedArtifactsProvider bannedArtifactsProvider;

    @InjectMocks
    private BannedDependenciesImpl sut;

    @Parameters
    public static Collection<Boolean> parameters() {
        return ImmutableList.of(true, false);
    }

    public BannedDependenciesTest(boolean forceInternal) {
        this.forceInternal = forceInternal;
    }

    @Before
    public void setUp() {
        doReturn(ARTIFACT_IDS).when(bannedArtifactsProvider).getBannedArtifacts(forceInternal);
    }

    @Test
    public void getBannedElements_withAllowedDependenciesUsePlatform_filtersOutPlatformDependencies() throws Exception {
        Set<String> platformDependencies = ImmutableSet.of(
                "com.fasterxml.jackson.core:jackson-core",
                "com.google.guava:guava",
                "com.h2database:h2",
                "commons-io:commons-io",
                "org.springframework.boot:spring-boot-devtools",
                "org.springframework.boot:spring-boot-starter-actuator",
                "org.springframework.boot:spring-boot-starter-aop",
                "org.springframework.boot:spring-boot-starter-data-mongodb",
                "org.springframework.boot:spring-boot-starter-data-redis",
                "org.springframework.boot:spring-boot-starter-jdbc",
                "org.springframework.boot:spring-boot-starter-oauth2-client",
                "org.springframework.boot:spring-boot-starter-test",
                "org.springframework.boot:spring-boot-starter-thymeleaf",
                "org.springframework.boot:spring-boot-starter-validation");

        doReturn(platformDependencies)
                .when(platformDependenciesProvider)
                .getDependencies(PLATFORM_GROUP_ID, ARTIFACT_IDS, PLATFORM_VERSION);

        Set<String> banningExcludes = ImmutableSet.of(
                "org.springframework.boot:spring-boot-starter-aop",
                "org.springframework.boot:spring-boot-starter-data-mongodb",
                "org.springframework.boot:spring-boot-starter-data-redis",
                "org.springframework.boot:spring-boot-starter-jdbc",
                "org.springframework.boot:spring-boot-starter-mail");

        Set<String> expected = ImmutableSet.of(
                "com.fasterxml.jackson.core:jackson-core",
                "com.google.guava:guava",
                "com.h2database:h2",
                "commons-io:commons-io",
                "org.springframework.boot:spring-boot-devtools",
                "org.springframework.boot:spring-boot-starter-actuator",
                "org.springframework.boot:spring-boot-starter-oauth2-client",
                "org.springframework.boot:spring-boot-starter-test",
                "org.springframework.boot:spring-boot-starter-thymeleaf",
                "org.springframework.boot:spring-boot-starter-validation");

        Set<MojoExecutor.Element> bannedDependencies =
                sut.getBannedElements(banningExcludes, PLATFORM_VERSION, forceInternal, false);
        assertThat(bannedDependencies.size(), is(expected.size()));
    }

    @Test
    public void getBannedElements_withAllowedDependenciesUseLegacy_filtersOutPlatformDependencies() throws Exception {
        Set<String> legacyDependencies = ImmutableSet.of(
                "com.fasterxml.jackson.core:jackson-core",
                "com.google.guava:guava",
                "com.h2database:h2",
                "commons-io:commons-io",
                "org.springframework.boot:spring-boot-devtools",
                "org.springframework.boot:spring-boot-starter-actuator",
                "org.springframework.boot:spring-boot-starter-aop",
                "org.springframework.boot:spring-boot-starter-data-mongodb",
                "org.springframework.boot:spring-boot-starter-data-redis",
                "org.springframework.boot:spring-boot-starter-jdbc",
                "org.springframework.boot:spring-boot-starter-oauth2-client",
                "org.springframework.boot:spring-boot-starter-test",
                "org.springframework.boot:spring-boot-starter-thymeleaf",
                "org.springframework.boot:spring-boot-starter-validation");

        doReturn(legacyDependencies).when(legacyBannedDependenciesProvider).getDependencies();

        Set<String> banningExcludes = ImmutableSet.of(
                "org.springframework.boot:spring-boot-starter-aop",
                "org.springframework.boot:spring-boot-starter-data-mongodb",
                "org.springframework.boot:spring-boot-starter-data-redis",
                "org.springframework.boot:spring-boot-starter-jdbc",
                "org.springframework.boot:spring-boot-starter-mail");

        Set<String> expected = ImmutableSet.of(
                "com.fasterxml.jackson.core:jackson-core",
                "com.google.guava:guava",
                "com.h2database:h2",
                "commons-io:commons-io",
                "org.springframework.boot:spring-boot-devtools",
                "org.springframework.boot:spring-boot-starter-actuator",
                "org.springframework.boot:spring-boot-starter-oauth2-client",
                "org.springframework.boot:spring-boot-starter-test",
                "org.springframework.boot:spring-boot-starter-thymeleaf",
                "org.springframework.boot:spring-boot-starter-validation");

        Set<MojoExecutor.Element> bannedDependencies =
                sut.getBannedElements(banningExcludes, PLATFORM_VERSION, forceInternal, true);
        assertThat(bannedDependencies.size(), is(expected.size()));
    }
}

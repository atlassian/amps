package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper.WEB_RESOURCE_PLUGIN_CONFIGURATIONS_DIR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclarationParameter;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociation;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociationParameter;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

public class FeDeclarationReaderTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    private FeDeclarationReader underTest;

    @Mock
    DirectoryHelper directoryHelper;

    @Before
    public void setup() {
        underTest = new FeDeclarationReader(directoryHelper, new JsonParser());

        when(directoryHelper.getOutputDirectoryPath())
                .thenReturn(ResourceHelper.getResourceAbsolutePath("non-existing-path"));
        when(directoryHelper.getOutputFileAbsolutePath(anyString())).thenCallRealMethod();
        when(directoryHelper.getSourceDirectoryPath()).thenReturn(ResourceHelper.getResourceAbsolutePath(""));
        when(directoryHelper.getSourceFileAbsolutePath(anyString())).thenCallRealMethod();
        when(directoryHelper.getPathRelativeToSourceDirectory(anyString())).thenCallRealMethod();
    }

    @Test
    public void testGetManifestAssociations_whenEmptyConfig_shouldReturnEmptyArray() throws MojoExecutionException {
        // given

        // when
        List<FeManifestAssociation> associations = underTest.getManifestAssociations(null);

        // then
        assertThat(associations, hasSize(0));
    }

    @Test
    public void testGetManifestAssociations_shouldFilterOutNullEntries() throws MojoExecutionException {
        // given
        FeManifestAssociationParameter manualConfiguration = new FeManifestAssociationParameter();
        manualConfiguration.setPackageName("package.json");
        ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest fileDeclaration =
                new ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest();
        fileDeclaration.setOutputDirectoryFiles(Arrays.asList("file1.js", null));
        manualConfiguration.setFilesDeclaration(fileDeclaration);

        // when
        List<FeManifestAssociation> associations =
                underTest.getManifestAssociations(Collections.singletonList(manualConfiguration));

        // then
        assertThat(associations, hasSize(1));
        assertThat(associations.get(0).getFiles(), hasSize(1));
        assertThat(associations.get(0).getFiles(), hasItems("file1.js"));
    }

    @Test
    public void testGetManifestAssociations_shouldTrimFilePaths() throws MojoExecutionException {
        // given
        FeManifestAssociationParameter manualConfiguration = new FeManifestAssociationParameter();
        manualConfiguration.setPackageName("package.json");
        ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest fileDeclaration =
                new ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest();
        fileDeclaration.setOutputDirectoryFiles(Arrays.asList("             file1.js", "file2.js    "));
        manualConfiguration.setFilesDeclaration(fileDeclaration);

        // when
        List<FeManifestAssociation> associations =
                underTest.getManifestAssociations(Collections.singletonList(manualConfiguration));

        // then
        assertThat(associations, hasSize(1));
        assertThat(associations.get(0).getFiles(), hasSize(2));
        assertThat(associations.get(0).getFiles(), hasItems("file1.js"));
        assertThat(associations.get(0).getFiles(), hasItems("file2.js"));
    }

    @Test
    public void
            testGetManifestAssociations_whenHasDataFromAssociationAndWebpackConfiguration_shouldSuccessfullyMergeData()
                    throws MojoExecutionException {
        // given
        FeManifestAssociationParameter manualConfiguration = new FeManifestAssociationParameter();
        manualConfiguration.setPackageName("package.json");
        ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest fileDeclaration =
                new ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest();
        fileDeclaration.setOutputDirectoryFiles(Arrays.asList("file1.js", null, "file4.mjs"));
        manualConfiguration.setFilesDeclaration(fileDeclaration);

        String resourceDirectory = ResourceHelper.getResourceAbsolutePath("");
        when(directoryHelper.getOutputFileAbsolutePath(eq(WEB_RESOURCE_PLUGIN_CONFIGURATIONS_DIR)))
                .thenReturn(resourceDirectory);

        // when
        List<FeManifestAssociation> associations =
                underTest.getManifestAssociations(Collections.singletonList(manualConfiguration));

        // then
        assertThat(associations, hasSize(2));

        assertThat(associations.get(0).getPackageName(), equalTo("package.json"));
        assertThat(associations.get(0).getFiles(), hasItems("file1.js", "file4.mjs"));
        assertThat(associations.get(0).getDeclarationOrigin(), equalTo("pom.xml"));

        assertThat(associations.get(1).getPackageName(), equalTo("local-webpack-package"));
        assertThat(associations.get(1).getFiles(), hasItems("file1.js", "file2.ejs", "file3.cjs"));
        assertThat(associations.get(1).getDeclarationOrigin(), equalTo("sample-plugin-webpack.intermediary.json"));
    }

    @Test
    public void testGetManifestAssociations_whenHasDataFromViteConfiguration_shouldSuccessfullyProcess()
            throws MojoExecutionException {
        // given
        List<FeManifestAssociationParameter> manualAssociationDeclarations = Collections.emptyList();

        String resourceDirectory = ResourceHelper.getResourceAbsolutePath("with-vite");
        when(directoryHelper.getOutputFileAbsolutePath(eq(WEB_RESOURCE_PLUGIN_CONFIGURATIONS_DIR)))
                .thenReturn(resourceDirectory);

        // when
        List<FeManifestAssociation> associations = underTest.getManifestAssociations(manualAssociationDeclarations);

        // then
        assertThat(associations, hasSize(1));

        assertThat(associations.get(0).getPackageName(), equalTo("local-vite-package"));
        assertThat(associations.get(0).getFiles(), hasItems("file1.js"));
        assertThat(
                Paths.get(associations.get(0).getDeclarationOrigin()).toString(),
                equalTo(Paths.get("with-vite/sample-plugin-vite.intermediary.json")
                        .toString()));
    }

    @Test
    public void testGetManifestAssociationsFromExternalConfiguration_shouldParseJson() throws MojoExecutionException {
        // given
        String resourcePath = ResourceHelper.getResourceAbsolutePath("external-association.json");

        // when
        FeManifestAssociation association = underTest.getManifestAssociationsFromJsonConfiguration(resourcePath);

        // then
        assertThat(association.getPackageName(), equalTo("local-package"));
        assertThat(association.getFiles(), hasItems("file1.js", "file2.ejs", "file3.cjs"));
        assertThat(association.getDeclarationOrigin(), equalTo("external-association.json"));
    }

    @Test
    public void testGetManifestAssociationsFromExternalConfiguration_whenFileNotFound_shouldThrowException() {
        // given
        String nonExistentResourcePath = "non-existent-file.json";

        // then
        MojoExecutionException exception = assertThrows(
                MojoExecutionException.class,
                () -> underTest.getManifestAssociationsFromJsonConfiguration(nonExistentResourcePath));

        String expectedMessageStart = "External manifest association couldn't be parsed. Path:";
        String exceptionMessage = exception.getMessage();

        assertThat(exceptionMessage, startsWith(expectedMessageStart));
        assertThat(exceptionMessage, endsWith(nonExistentResourcePath));
    }

    @Test
    public void testGetManifestAssociationsFromWebpackConfiguration_shouldParseJson() throws MojoExecutionException {
        // given
        String resourceDirectory = ResourceHelper.getResourceAbsolutePath("");

        // when
        List<FeManifestAssociation> associations =
                underTest.getManifestAssociationsFromWebResourcePluginConfigurations(resourceDirectory);

        // then
        assertThat(associations, hasSize(1));
        assertThat(associations.get(0).getPackageName(), equalTo("local-webpack-package"));
        assertThat(associations.get(0).getFiles(), hasItems("file1.js", "file2.ejs", "file3.cjs"));
        assertThat(associations.get(0).getDeclarationOrigin(), equalTo("sample-plugin-webpack.intermediary.json"));
    }

    @Test
    public void testGetManifestAssociationsFromWebpackConfiguration_whenOutputDirNotExists_shouldNotFail()
            throws MojoExecutionException {
        // given
        String resourceDirectory = ResourceHelper.getResourceAbsolutePath("not-existing-folder");

        // when
        List<FeManifestAssociation> association =
                underTest.getManifestAssociationsFromWebResourcePluginConfigurations(resourceDirectory);

        // then
        assertThat(association, hasSize(0));
    }

    @Test
    public void testGetDependencyDeclarations_shouldReadDeclarationsFromConfigurationAndFile()
            throws MojoExecutionException {
        // given
        FeDependencyDeclarationParameter pomDependencyDeclaration = new FeDependencyDeclarationParameter();
        pomDependencyDeclaration.setManuallyVerifiedNonAtlassianArtifactPath("lib/artifact-1.2.3");
        pomDependencyDeclaration.setManuallyVerifiedEveryJsDoesntHaveExternalDependencies(
                Arrays.asList("file1.js", "   file2.js ", null));

        FeDependencyDeclarationParameter fileDependencyDeclaration = new FeDependencyDeclarationParameter();
        fileDependencyDeclaration.setNonAtlassianArtifactDeclaration(
                ResourceHelper.getResourceAbsolutePath("dependency-declaration/declaration.json"));

        // when
        List<FeDependencyDeclaration> dependencyDeclarations =
                underTest.getDependencyDeclarations(Arrays.asList(pomDependencyDeclaration, fileDependencyDeclaration));

        // then
        assertThat(dependencyDeclarations, hasSize(2));

        assertThat(dependencyDeclarations.get(0).getArtifactPath(), equalTo("lib/artifact-1.2.3"));
        assertThat(dependencyDeclarations.get(0).getDeclaredJsFiles(), hasSize(2));
        assertThat(dependencyDeclarations.get(0).getDeclaredJsFiles(), hasItems("file1.js", "file2.js"));

        assertThat(dependencyDeclarations.get(1).getArtifactPath(), equalTo("lib/library-a"));
        assertThat(dependencyDeclarations.get(1).getDeclaredJsFiles(), hasSize(1));
        assertThat(dependencyDeclarations.get(1).getDeclaredJsFiles(), hasItems("src/file.js"));
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.verification;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.aMapWithSize;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThrows;

import java.nio.file.Paths;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.ArtifactScannerImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ArtifactScanResults;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ModuleScanResults;
import com.atlassian.maven.plugins.amps.frontend.association.verification.utils.TrueZipUtils;

public class ArtifactScannerTest {
    private ArtifactScannerImpl underTest;

    @Before
    public void setUp() {
        TrueZipUtils.configureTrueZip();
        underTest = new ArtifactScannerImpl();
    }

    @After
    public void tearDown() throws MojoExecutionException {
        TrueZipUtils.unmountTrueZip();
    }

    @Test
    public void testScan_whenJarArtifact_shouldCollectResults() throws MojoExecutionException {
        // given
        TFile artifact = getResourceByPath("/examples/jar-like.zip");

        // when
        ArtifactScanResults artifactScanResults = underTest.scan(artifact);

        // then
        assertThat(artifactScanResults.getArchiveFiles(), is(empty()));
        ModuleScanResults moduleScanResults = artifactScanResults.getModules().get(artifact);
        assertThat(moduleScanResults.getAssociationFiles(), hasSize(1));
        assertThat(moduleScanResults.getJavascriptFiles(), hasSize(1));
    }

    @Test
    public void testScan_whenWarArtifact_shouldCollectResults() throws MojoExecutionException {
        // given
        TFile artifact = getResourceByPath("/examples/war-like.zip");

        // when
        ArtifactScanResults artifactScanResults = underTest.scan(artifact);

        // then
        assertThat(artifactScanResults.getArchiveFiles(), is(empty()));
        ModuleScanResults moduleScanResults = artifactScanResults.getModules().get(new TFile(artifact, "war-like"));
        assertThat(moduleScanResults.getAssociationFiles(), hasSize(1));
        assertThat(moduleScanResults.getJavascriptFiles(), hasSize(1));
    }

    @Test
    public void testScan_whenObrArtifact_shouldCollectResults() throws MojoExecutionException {
        // given
        TFile artifact = getResourceByPath("/examples/obr-like.zip");

        // when
        ArtifactScanResults artifactScanResults = underTest.scan(artifact);

        // then
        assertThat(artifactScanResults.getArchiveFiles(), hasItem(new TFile(artifact, "obr-like/jar-like.zip")));
        assertThat(artifactScanResults.getModules(), aMapWithSize(0));
    }

    @Test
    public void testScan_whenArtifactNotExist_shouldThrow() {
        // given
        TFile artifact = getResourceByPath("/examples/imaginary.zip");

        // then
        assertThrows(MojoExecutionException.class, () -> underTest.scan(artifact));
    }

    public TFile getResourceByPath(String path) {
        String resourceDirectory =
                "src/test/resources/com/atlassian/maven/plugins/amps/frontend/association/verification";
        return new TFile(Paths.get(resourceDirectory, path).toString());
    }
}

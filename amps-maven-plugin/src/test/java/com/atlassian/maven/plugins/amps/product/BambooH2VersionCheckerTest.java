package com.atlassian.maven.plugins.amps.product;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import aQute.bnd.version.MavenVersion;

@RunWith(Parameterized.class)
public class BambooH2VersionCheckerTest {
    private final String version;
    private final boolean outcome;

    public BambooH2VersionCheckerTest(String version, boolean outcome) {
        this.version = version;
        this.outcome = outcome;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> bambooVersions() {
        return Arrays.asList(new Object[][] {
            {"8.0.0", false},
            {"9.0.0", false},
            {"9.1.1", false},
            {"9.2.3", false},
            {"9.2.7", false},
            {"9.2.8", true},
            {"9.2.8-SNAPSHOT", true},
            {"9.2.8sometthing", true},
            {"9.2.8.3-444-random%", true},
            {"9.2.8.3.", true},
            {"9.3.0", false},
            {"9.3.4", false},
            {"9.3.5", false},
            {"9.3.6", true},
            {"9.4.0", false},
            {"9.4.1", false},
            {"9.4.2", true},
            {"9.5.5", true},
            {"10.0.0", true}
        });
    }

    @Test
    public void testShouldInstall() {
        assertEquals(BambooH2VersionChecker.shouldInstallH2(new MavenVersion(version)), outcome);
    }
}

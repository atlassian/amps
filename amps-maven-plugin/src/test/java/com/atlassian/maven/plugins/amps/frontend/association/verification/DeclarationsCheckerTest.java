package com.atlassian.maven.plugins.amps.frontend.association.verification;

import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.DeclarationsCheckerImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.FailurePreferences;

public class DeclarationsCheckerTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    TFile moduleRoot;

    @Mock
    Logger logger;

    @Mock
    FailurePreferences failurePreferences;

    DeclarationsCheckerImpl underTest;

    @Before
    public void setUp() {
        moduleRoot = new TFile("module root");

        try (MockedStatic<LoggerFactory> loggerFactory = Mockito.mockStatic(LoggerFactory.class)) {
            loggerFactory.when(() -> getLogger(anyString())).thenReturn(logger);
            underTest = new DeclarationsCheckerImpl();
        }
        underTest.setFailurePreferences(failurePreferences);
    }

    /**
     * If we find any declarations that do not match found files, it might mean that either module contents has changed
     * or that we verify against wrong root folder.
     */
    @Test
    public void testVerifyModule_whenDeclarationsNotMatchFoundFiles_shouldLogIssuesAndFail() {
        // given
        when(failurePreferences.isFailOnExtraDeclarations()).thenReturn(true);
        Set<String> declarations = singleton("/declared.js");
        Set<String> files = emptySet();

        // when
        underTest.verifyModule(moduleRoot, declarations, files);

        // then
        verify(logger, times(1)).error("Extra declarations detected: [\n\t/declared.js\n]");
        MojoExecutionException e = assertThrows(MojoExecutionException.class, () -> underTest.finalizeChecks());
        assertThat(
                e.getMessage(),
                is(
                        "There were extra file declarations found when verifying frontend manifest associations. See logs above for a complete list of problems."));
    }

    /**
     * If we find any files that are not present in declarations it might mean that either some files were not checked
     * by developers for unwanted dependencies or that we verify against the wrong root folder.
     */
    @Test
    public void testVerifyModule_whenFilesAreNotInDeclarations_shouldLogIssuesAndFail() {
        // given
        when(failurePreferences.isFailOnUndeclaredFiles()).thenReturn(true);
        Set<String> declarations = emptySet();
        Set<String> files = singleton("/undeclared.js");

        // when
        underTest.verifyModule(moduleRoot, declarations, files);

        // then
        verify(logger, times(1)).error(contains("The undeclared files were : [\n\t/undeclared.js\n]"));
        MojoExecutionException e = assertThrows(MojoExecutionException.class, () -> underTest.finalizeChecks());
        assertThat(
                e.getMessage(),
                is(
                        "There were undeclared files found when verifying frontend manifest associations. See logs above for a complete list of problems."));
    }

    @Test
    public void testVerifyModule_whenFilesUndeclaredAndFailureOff_shouldWarnOfIssuesAndNotFail()
            throws MojoExecutionException {
        // given
        when(failurePreferences.isFailOnUndeclaredFiles()).thenReturn(false);

        Set<String> declarations = emptySet();
        Set<String> files = singleton("/undeclared.js");

        // when
        underTest.verifyModule(moduleRoot, declarations, files);
        underTest.finalizeChecks();

        // then
        verify(logger, times(1)).warn(contains("The undeclared files were : [\n\t/undeclared.js\n]"));
    }

    @Test
    public void testVerifyModule_whenExtraDeclarationsAndFailureOff_shouldWarnOfIssuesAndNotFail()
            throws MojoExecutionException {
        // given
        when(failurePreferences.isFailOnUndeclaredFiles()).thenReturn(false);

        Set<String> declarations = emptySet();
        Set<String> files = singleton("/undeclared.js");

        // when
        underTest.verifyModule(moduleRoot, declarations, files);
        underTest.finalizeChecks();

        // then
        verify(logger, times(1)).warn(contains("The undeclared files were : [\n\t/undeclared.js\n]"));
    }
}

package com.atlassian.maven.plugins.amps;

import static com.atlassian.maven.plugins.amps.AbstractProductHandlerMojo.NO_TEST_GROUP;
import static com.atlassian.maven.plugins.amps.util.FileUtils.file;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.twdata.maven.mojoexecutor.MojoExecutor.ExecutionEnvironment;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.apache.maven.archetype.common.DefaultPomManager;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Build;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.common.collect.ImmutableMap;

import com.atlassian.maven.plugins.amps.util.MojoExecutorWrapper;

public class TestMavenGoals {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private Build build;

    @Mock
    private ExecutionEnvironment executionEnvironment;

    @Mock
    private MavenContext ctx;

    @Mock
    private MavenProject project;

    @Mock
    private MavenSession session;

    @Mock
    private Product product;

    @Mock
    private BannedDependencies bannedDependencies;

    @Mock
    private MojoExecutorWrapper mojoExecutorWrapper;

    private MavenGoals goals;

    @Before
    public void setUp() {
        when(ctx.getProject()).thenReturn(project);
        when(ctx.getLog()).thenReturn(new SystemStreamLog());
        when(ctx.getVersionOverrides()).thenReturn(new Properties());

        when(executionEnvironment.getMavenProject()).thenReturn(project);
        when(executionEnvironment.getMavenSession()).thenReturn(session);

        when(project.getBuild()).thenReturn(build);

        when(session.getCurrentProject()).thenReturn(project);

        goals = new MavenGoals(ctx, mojoExecutorWrapper, bannedDependencies);
    }

    @Test
    public void shouldCorrectlyGenerateMinimalManifest() throws Exception {
        when(build.getOutputDirectory()).thenReturn(temporaryFolder.getRoot().getAbsolutePath());

        final Map<String, String> attrs = ImmutableMap.of("Attribute-A", "aaa", "Attribute-B", "bbb");

        goals.generateMinimalManifest(attrs);

        final File mf = file(temporaryFolder.getRoot().getAbsolutePath(), "META-INF", "MANIFEST.MF");
        assertTrue(mf.exists());

        final Manifest m = new Manifest(new FileInputStream(mf));
        assertEquals("aaa", m.getMainAttributes().getValue("Attribute-A"));
        assertEquals("bbb", m.getMainAttributes().getValue("Attribute-B"));
        assertNull(m.getMainAttributes().getValue("Bundle-SymbolicName"));
    }

    @Test
    public void shouldCorrectlyProcessFilesWithCrlf() throws Exception {
        final DefaultPomManager pomManager = new DefaultPomManager();
        final URL originalPomPath = TestMavenGoals.class.getResource("originalPom.xml");

        File originalPomFile = new File(originalPomPath.toURI());
        File temp = new File(temporaryFolder.getRoot(), "tempOriginalPom.xml");

        FileUtils.copyFile(originalPomFile, temp);
        String originalPomXml = readFileToString(temp, UTF_8);

        assertThat("Not expected file!", originalPomXml, startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        assertThat("File contains \r\n before process correct CRLF", originalPomXml, containsString("\r\n"));

        goals.processCorrectCrlf(pomManager, temp);

        String processedPomXml = readFileToString(temp, UTF_8);
        assertThat(
                "Not expected file after process correct CRLF!",
                processedPomXml,
                startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        assertThat("File contains \n after process correct CRLF", processedPomXml, containsString("\n"));
        assertThat("File not contains \r\n after process correct CRLF", processedPomXml, not(containsString("\r\n")));
        temp.deleteOnExit();
    }

    @Test
    public void shouldGenerateReportsDirectory() {
        final File reportsDirectory =
                Paths.get(System.getProperty("user.home"), "targetDirectory").toFile();

        final String shortReportsDirectory = MavenGoals.getReportsDirectory(reportsDirectory);

        assertEquals(
                Paths.get(System.getProperty("user.home"), "targetDirectory", "surefire-reports")
                        .toString(),
                shortReportsDirectory);
    }

    @Test
    public void shouldGenerateEmptyReportNameSuffix() {
        final String reportNameSuffix = MavenGoals.getReportNameSuffix(NO_TEST_GROUP, null);

        assertEquals("", reportNameSuffix);
    }

    @Test
    public void shouldGenerateReportNameSuffix() {
        final String reportNameSuffix = MavenGoals.getReportNameSuffix("group1", "container1");

        assertEquals("group1-container1", reportNameSuffix);
    }

    @Test
    public void runIntegrationTests_shouldUseConfigurationReportSuffix_whenDefined() throws MojoExecutionException {
        when(ctx.getPlugin("org.apache.maven.plugins", "maven-failsafe-plugin")).thenReturn(mock(Plugin.class));
        when(ctx.getExecutionEnvironment()).thenReturn(executionEnvironment);
        doNothing().when(mojoExecutorWrapper).executeWithMergedConfig(any(), any(), any(), any());
        Map<String, Object> properties = spy(new HashMap<>());

        goals.runIntegrationTests(
                "build-test", emptyList(), emptyList(), properties, "category", "debug", "test-suffix");

        verify(properties).put("reportNameSuffix", "test-suffix");
    }
}

package com.atlassian.maven.plugins.amps.util;

import static com.atlassian.maven.plugins.amps.util.ProjectUtils.hasAtlassianGroupId;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ProjectUtilsTest {

    @Test
    public void hasAtlassianGroupId_whenStartsWithAtlassianPrefix_shouldReturnTrue() {
        assertTrue(hasAtlassianGroupId("com.atlassian.refapp"));
        assertTrue(hasAtlassianGroupId("io.atlassian.fugue"));
        assertTrue(hasAtlassianGroupId("net.java.dev.activeobjects.test"));
        assertTrue(hasAtlassianGroupId("com.riadalabs.jira"));
        assertTrue(hasAtlassianGroupId("io.riada.jira"));
        assertTrue(hasAtlassianGroupId("com.pyxis.greenhopper.hop"));
    }

    @Test
    public void hasAtlassianGroupId_whenIsExactlyAtlassianPrefix_shouldReturnTrue() {
        assertTrue(hasAtlassianGroupId("com.atlassian"));
        assertTrue(hasAtlassianGroupId("io.atlassian"));
        assertTrue(hasAtlassianGroupId("net.java.dev.activeobjects"));
        assertTrue(hasAtlassianGroupId("com.riadalabs"));
        assertTrue(hasAtlassianGroupId("io.riada"));
        assertTrue(hasAtlassianGroupId("com.thinktilt.proforma"));
        assertTrue(hasAtlassianGroupId("com.pyxis.greenhopper"));
    }

    @Test
    public void hasAtlassianGroupId_whenDoesntStartWithAtlassianPrefix_shouldReturnFalse() {
        assertFalse(hasAtlassianGroupId("com.atlassian-test"));
        assertFalse(hasAtlassianGroupId("io.atlassians"));
        assertFalse(hasAtlassianGroupId("something.else"));
    }
}

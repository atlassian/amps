package com.atlassian.maven.plugins.amps.frontend.association.mapping.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import org.apache.maven.model.Build;
import org.apache.maven.project.MavenProject;
import org.junit.Test;
import org.mockito.Mockito;

import com.atlassian.maven.plugins.amps.MavenContext;

public class DirectoryHelperTest {
    @Test
    public void getOutputDirectoryPath_WHEN_targetDirectoryProvided_THEN_targetDirectoryReturned() {

        // Given
        String givenTargetDirectory = "target";
        DirectoryHelper sut = new DirectoryHelper(null, givenTargetDirectory);

        // When
        String outputDirectory = sut.getOutputDirectoryPath();

        // Then
        assertEquals(givenTargetDirectory, outputDirectory);
    }

    @Test
    public void getOutputDirectoryPath_WHEN_targetDirectoryNotProvided_THEN_projectBuildOutputDirectoryReturned()
            throws NoSuchFieldException, IllegalAccessException {

        // Given
        String givenMavenOutputDirectory = "mavenOutputDirectory";
        String givenTargetDirectory = "";
        DirectoryHelper sut = createSutWithoutTargetDirectory(givenMavenOutputDirectory, givenTargetDirectory);

        // When
        String outputDirectory = sut.getOutputDirectoryPath();

        // Then
        assertEquals(givenMavenOutputDirectory, outputDirectory);
        assertNotEquals(givenTargetDirectory, outputDirectory);
    }

    private DirectoryHelper createSutWithoutTargetDirectory(String mockedMavenOutputDirectory, String targetDirectory)
            throws NoSuchFieldException, IllegalAccessException {
        MavenContext mavenContext = Mockito.mock(MavenContext.class);
        MavenProject mavenProject = Mockito.mock(MavenProject.class);
        Build build = Mockito.mock(Build.class);

        when(mavenContext.getProject()).thenReturn(mavenProject);
        when(mavenProject.getBuild()).thenReturn(build);
        when(build.getOutputDirectory()).thenReturn(mockedMavenOutputDirectory);

        DirectoryHelper sut = new DirectoryHelper(mavenContext, targetDirectory);

        return sut;
    }
}

package com.atlassian.maven.plugins.amps.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class JavaVersionProviderTest {

    private String javaVersionString;
    private final int expectedMajorVersion;

    public JavaVersionProviderTest(String javaVersionString, int expectedMajorVersion) {
        this.javaVersionString = javaVersionString;
        this.expectedMajorVersion = expectedMajorVersion;
    }

    @Test
    public void testJavaVersionExtraction() {
        JavaVersionProvider javaVersionProvider = new JavaVersionProvider(javaVersionString);
        assertEquals(expectedMajorVersion, javaVersionProvider.getMajor());
    }

    @Parameterized.Parameters
    public static Collection<Object[]> javaVersions() {
        return Arrays.asList(new Object[][] {
            {"1.8.0_292", 8},
            {"9", 9},
            {"9.0.0", 9},
            {"17.0", 17},
            {"18.02", 18},
        });
    }
}

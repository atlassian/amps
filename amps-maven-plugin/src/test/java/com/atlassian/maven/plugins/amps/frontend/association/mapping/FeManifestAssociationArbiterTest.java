package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociationsConfiguration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;

public class FeManifestAssociationArbiterTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    private FeManifestAssociationArbiter underTest;

    @Mock
    DirectoryHelper directoryHelper;

    @Mock
    FeManifestAssociationsConfiguration configuration;

    @Mock
    Log logger;

    @Before
    public void setup() {
        underTest = new FeManifestAssociationArbiter(directoryHelper, configuration, logger);

        String outputDirectory = ResourceHelper.getResourceAbsolutePath("output");
        when(directoryHelper.getOutputDirectoryPath()).thenReturn(outputDirectory);
        when(directoryHelper.getOutputFileAbsolutePath(anyString())).thenCallRealMethod();
    }

    @Test
    public void testVerifyAllDeclaredFilesExistInOutput_shouldPass_whenAllFilesExist() throws MojoExecutionException {
        // given
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("output1.js", createJsFileDeclaration("output1.js"));
        outputJsFileDeclarations.put("inner/output2.js", createJsFileDeclaration("inner/output2.js"));

        // when
        underTest.verifyAllDeclaredFilesExistInOutput(outputJsFileDeclarations);

        // then OK
    }

    @Test
    public void testVerifyAllDeclaredFilesExistInOutput_shouldThrow_whenFileNotExists() {
        // given
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("not-existing-file.js", createJsFileDeclaration("not-existing-file.js"));

        // when
        final ThrowingRunnable runnable = () -> underTest.verifyAllDeclaredFilesExistInOutput(outputJsFileDeclarations);

        // then
        assertThrows(MojoExecutionException.class, runnable);
    }

    @Test
    public void testVerifyAllOutputFilesAreDeclared_shouldNotWarn_whenAllFilesDeclared() throws MojoExecutionException {
        // given
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("output1.js", createJsFileDeclaration("output1.js"));
        outputJsFileDeclarations.put("inner/output2.js", createJsFileDeclaration("inner/output2.js"));

        // when
        underTest.verifyAllOutputFilesAreDeclared(outputJsFileDeclarations);

        // then
        verify(logger, never()).warn(anyString());
    }

    @Test
    public void testVerifyAllOutputFilesAreDeclared_shouldWarn_whenAnyFileNotDeclaredAnd()
            throws MojoExecutionException {
        // given
        when(configuration.getIgnoreSomeFilesNotDeclared()).thenReturn(true);
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("output1.js", createJsFileDeclaration("output1.js"));
        // missing declaration of output2.js

        // when
        underTest.verifyAllOutputFilesAreDeclared(outputJsFileDeclarations);

        // then
        verify(logger, times(1))
                .warn(
                        "Some files exist in output but have not been declared in any manifest association:\ninner/output2.js");
    }

    @Test
    public void testVerifyAllOutputFilesAreDeclared_shouldWarn_whenAnyFileNotDeclared() throws MojoExecutionException {
        // given
        when(configuration.getIgnoreSomeFilesNotDeclared()).thenReturn(true);
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("output1.js", createJsFileDeclaration("output1.js"));
        // missing declaration of output2.js

        // when
        underTest.verifyAllOutputFilesAreDeclared(outputJsFileDeclarations);

        // then
        verify(logger, times(1))
                .warn(
                        "Some files exist in output but have not been declared in any manifest association:\ninner/output2.js");
    }

    @Test
    public void testVerifyAllOutputFilesAreDeclared_shouldThrow_whenAnyFileNotDeclaredAndCheckNotIgnored()
            throws MojoExecutionException {
        // given
        when(configuration.getIgnoreSomeFilesNotDeclared()).thenReturn(false);
        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations = new HashMap<>();
        outputJsFileDeclarations.put("output1.js", createJsFileDeclaration("output1.js"));
        // missing declaration of output2.js

        // when
        MojoExecutionException executionException = assertThrows(
                MojoExecutionException.class,
                () -> underTest.verifyAllOutputFilesAreDeclared(outputJsFileDeclarations));

        // then
        assertThat(
                executionException.getMessage(),
                equalTo(
                        "Some files exist in output but have not been declared in any manifest association:\ninner/output2.js"));
    }

    private FeOutputJsFileDeclaration createJsFileDeclaration(String filepath) {
        return new FeOutputJsFileDeclaration(
                filepath, new HashSet<>(Collections.singletonList("pom.xml")), "package.json");
    }
}

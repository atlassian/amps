package com.atlassian.maven.plugins.amps.frontend.association.verification;

import static org.mockito.Mockito.times;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.ArtifactScannerImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.DeclarationsCheckerImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.DeclarationsReaderImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.FailurePreferences;

public class VerifyFeManifestAssociationsMojoTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule().silent();

    @Spy
    ArtifactScanner artifactScanner = new ArtifactScannerImpl();

    @Spy
    DeclarationsReader declarationsReader = new DeclarationsReaderImpl();

    @Spy
    DeclarationsChecker declarationsChecker = new DeclarationsCheckerImpl();

    @InjectMocks
    VerifyFeManifestAssociationsMojo underTest;

    @Test
    public void testVerify_whenPathsFromDifferentSystems_shouldProcessInCurrentSystemPathStyle()
            throws MojoExecutionException {
        // given
        FailurePreferences failurePreferences = new FailurePreferences(true, true);
        String artifactEntry = ResourceHelper.getResourceAbsolutePath("examples/unpacked");

        // when
        underTest.verifyManifestAssociations(artifactEntry, failurePreferences);

        // then
        // should not fail
    }

    @Test
    public void testVerify_whenNestedArchive_shouldProcessAllArchives() throws MojoExecutionException {
        // given
        FailurePreferences failurePreferences = new FailurePreferences(true, true);
        String artifactEntry = ResourceHelper.getResourceAbsolutePath("examples/nested.zip");

        // when
        underTest.verifyManifestAssociations(artifactEntry, failurePreferences);

        // then
        Mockito.verify(declarationsChecker, times(1))
                .verifyModule(
                        Mockito.eq(new TFile(
                                ResourceHelper.getResourceAbsolutePath("examples/nested.zip/libs/jar-like.zip"))),
                        Mockito.any(),
                        Mockito.any());
    }

    @Test
    public void testVerify_whenNestedArchive_shouldExpandExternalAssociationsToParentModules()
            throws MojoExecutionException {
        // given
        FailurePreferences failurePreferences = new FailurePreferences(true, true);
        String artifactEntry =
                ResourceHelper.getResourceAbsolutePath("examples/nested-multiple-levels/moduleC-testing.jar");

        // when
        underTest.verifyManifestAssociations(artifactEntry, failurePreferences);

        // then
        // should not fail
    }
}

package com.atlassian.maven.plugins.amps.product;

import aQute.bnd.version.MavenVersion;
import aQute.bnd.version.Version;

public class BambooH2VersionChecker {

    public static final Version LAST_92_VERSION_WITH_H_2 = new MavenVersion("9.2.7").getOSGiVersion();
    public static final Version LAST_93_VERSION_WITH_H_2 = new MavenVersion("9.3.5").getOSGiVersion();
    public static final Version LAST_94_VERSION_WITH_H_2 = new MavenVersion("9.4.1").getOSGiVersion();
    public static final int LAST_MAJOR_WITH_H_2 = 9;
    public static final int LAST_9_MINOR_WITH_H_2 = 4;

    static boolean shouldInstallH2(MavenVersion version) {

        // No idea why is this method called #getOSGiVersion. Has nothing to do with OSGi
        final Version bambooVersion = version.getOSGiVersion();

        int majorVersion = bambooVersion.getMajor();

        if (majorVersion < LAST_MAJOR_WITH_H_2) {
            return false;
        }

        if (majorVersion > LAST_MAJOR_WITH_H_2) {
            return true;
        }

        int minorVersion = bambooVersion.getMinor();

        if (minorVersion > LAST_9_MINOR_WITH_H_2) {
            return true;
        }

        switch (minorVersion) {
            case 0:
            case 1:
                return false;
            case 2:
                if (bambooVersion.compareTo(LAST_92_VERSION_WITH_H_2) > 0) {
                    return true;
                }
            case 3:
                if (bambooVersion.compareTo(LAST_93_VERSION_WITH_H_2) > 0) {
                    return true;
                }
            case LAST_9_MINOR_WITH_H_2:
                if (bambooVersion.compareTo(LAST_94_VERSION_WITH_H_2) > 0) {
                    return true;
                }
            default:
                return false;
        }
    }
}

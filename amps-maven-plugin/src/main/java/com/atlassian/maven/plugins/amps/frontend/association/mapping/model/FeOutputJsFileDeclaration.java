package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FeOutputJsFileDeclaration {
    @JsonIgnore
    private String filepath;

    private Set<String> declarations;
    private String manifest;

    public FeOutputJsFileDeclaration() {}

    public FeOutputJsFileDeclaration(String filepath, Set<String> declarations, String manifest) {
        this.filepath = filepath;
        this.declarations = declarations;
        this.manifest = manifest;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public Set<String> getDeclarations() {
        return declarations;
    }

    public void setDeclarations(Set<String> declarations) {
        this.declarations = declarations;
    }

    public String getManifest() {
        return manifest;
    }

    public void setManifest(String manifest) {
        this.manifest = manifest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FeOutputJsFileDeclaration that = (FeOutputJsFileDeclaration) o;

        return Objects.equals(filepath, that.filepath)
                && Objects.equals(declarations, that.declarations)
                && Objects.equals(manifest, that.manifest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filepath, declarations, manifest);
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.verification.model;

import java.util.Set;

import com.atlassian.maven.plugins.amps.frontend.association.verification.utils.ArtifactPathUtils;

public class NestedModuleDeclarationData {
    private final Set<String> declaredFiles;

    public NestedModuleDeclarationData(Set<String> declaredFiles) {
        this.declaredFiles = ArtifactPathUtils.normalizePathSeparators(declaredFiles);
    }

    public Set<String> getDeclaredFiles() {
        return declaredFiles;
    }
}

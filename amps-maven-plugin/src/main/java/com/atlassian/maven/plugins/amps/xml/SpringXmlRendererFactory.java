package com.atlassian.maven.plugins.amps.xml;

import javax.xml.transform.TransformerFactory;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/** Factory used to create instance of {@link SpringXmlRenderer}. */
public class SpringXmlRendererFactory {
    // NOTE: This factory should really be replaced with a IoC module
    // when one gets added to AMPS

    /**
     * Returns a new instance of {@link SpringXmlRenderer}.
     *
     * @return SpringXmlRenderer
     */
    public SpringXmlRenderer create() {
        return new SpringXmlRendererImpl(TransformerFactory.newInstance(), new XmlMapper());
    }
}

package com.atlassian.maven.plugins.amps;

import static com.atlassian.maven.plugins.amps.util.FileUtils.file;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/** JARs up the current Maven project, bundling {@code target/classes/META-INF/MANIFEST.MF}, if it exists. */
@Mojo(name = "jar")
public class JarWithManifestMojo extends AbstractAmpsMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        File mf = file(getMavenContext().getProject().getBuild().getOutputDirectory(), "META-INF", "MANIFEST.MF");
        getMavenGoals().jarWithOptionalManifest(mf.exists());
    }
}

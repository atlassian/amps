package com.atlassian.maven.plugins.amps.frontend.association.mapping.utils;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonParser {

    private final ObjectMapper objectMapper;

    public JsonParser() {
        objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    }

    public void writeFile(File file, Object content) throws IOException {
        objectMapper.writeValue(file, content);
    }

    public <T> T readFile(File file, Class<T> clazz) throws IOException {
        return objectMapper.readValue(file, clazz);
    }

    public <T> T readFile(File src, TypeReference<T> valueType) throws IOException {
        return objectMapper.readValue(src, valueType);
    }
}

package com.atlassian.maven.plugins.amps.database;

import org.apache.maven.plugin.logging.Log;

/** An MSSQL database that uses the official Microsoft JDBC driver. */
public class MssqlMicrosoft extends AbstractMssqlDatabase {

    public MssqlMicrosoft(final Log log) {
        super(log, "com.microsoft.sqlserver.jdbc.SQLServerDriver", "jdbc:sqlserver");
    }
}

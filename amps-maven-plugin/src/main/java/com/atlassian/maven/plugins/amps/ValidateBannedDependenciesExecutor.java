package com.atlassian.maven.plugins.amps;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.maven.plugin.MojoExecutionException;
import org.slf4j.Logger;

class ValidateBannedDependenciesExecutor {
    private static final Logger logger = getLogger(ValidateBannedDependenciesExecutor.class);

    private static final AtomicBoolean hasEncounteredFailure = new AtomicBoolean(false);

    private final boolean skipBanningDependencies;
    private final Set<String> banningExcludes;
    private final String platformVersion;
    private final boolean forceInternalPlugin;
    private final boolean legacyValidateBannedDependencies;

    ValidateBannedDependenciesExecutor(
            boolean skipBanningDependencies,
            Set<String> banningExcludes,
            String platformVersion,
            boolean forceInternalPlugin,
            boolean legacyValidateBannedDependencies) {
        this.skipBanningDependencies = skipBanningDependencies;
        this.banningExcludes = banningExcludes;
        this.platformVersion = platformVersion;
        this.forceInternalPlugin = forceInternalPlugin;
        this.legacyValidateBannedDependencies = legacyValidateBannedDependencies;
    }

    void execute(MavenGoals mavenGoals) throws MojoExecutionException {
        if (legacyValidateBannedDependencies) {
            logger.warn(
                    "Property 'legacyValidateBannedDependencies' is deprecated and will be removed in further versions");
        }

        if (skipBanningDependencies) {
            logger.warn("dependencies validation has been skipped");
            return;
        }

        if (hasEncounteredFailure.get()) {
            logger.warn("Skipping checking of banned dependencies because lookup has failed during this build");
            return;
        }

        try {
            mavenGoals.validateBannedDependencies(
                    banningExcludes, platformVersion(), forceInternalPlugin, legacyValidateBannedDependencies);
            logger.info("No banned dependencies found!");
        } catch (PlatformDependencyProvisioningException exception) {
            hasEncounteredFailure.set(true);
            logger.warn("Failed to check for banned dependencies, skipping validation");
            logger.debug("Failed to check for banned dependencies because", exception);
        }
    }

    private String platformVersion() throws MojoExecutionException {
        if (platformVersion != null) {
            return platformVersion;
        }

        return "[0,)";
    }
}

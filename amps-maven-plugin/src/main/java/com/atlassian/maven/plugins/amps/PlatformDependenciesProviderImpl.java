package com.atlassian.maven.plugins.amps;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.slf4j.LoggerFactory.getLogger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.slf4j.Logger;

class PlatformDependenciesProviderImpl implements PlatformDependenciesProvider {
    private static final Logger log = getLogger(PlatformDependenciesProviderImpl.class);

    private final MavenXpp3Reader mavenReader = new MavenXpp3Reader();
    private RepositorySystemSession repositorySystemSession;

    @Component
    private RepositorySystem repositorySystem;

    @Component
    private MavenProject mavenProject;

    @Override
    public Collection<String> getDependencies(
            final String groupId, final Collection<String> artifactIds, final String version)
            throws PlatformDependencyProvisioningException {
        List<Set<String>> dependencies = new ArrayList<>();
        for (String artifactId : artifactIds) {
            dependencies.add(readSingleArtifact(groupId, artifactId, version));
        }

        return dependencies.stream().flatMap(Set::stream).distinct().sorted().collect(toList());
    }

    private Set<String> readSingleArtifact(String groupId, String artifactId, String version)
            throws PlatformDependencyProvisioningException {
        Artifact artifact = loadArtifact(determineArtifact(groupId, artifactId, version));
        List<Dependency> dependencies = readPomDependencies(artifact);

        return dependencies.stream()
                .map(dependency -> format("%s:%s", dependency.getGroupId(), dependency.getArtifactId()))
                .collect(toSet());
    }

    private Artifact determineArtifact(String groupId, String artifactId, String version)
            throws PlatformDependencyProvisioningException {
        assert mavenProject != null : "mavenProject must not be null";
        assert repositorySystem != null : "repositorySystem must not be null";

        Artifact artifact = new DefaultArtifact(groupId, artifactId, "pom", version);
        VersionRangeRequest request = new VersionRangeRequest();
        request.setArtifact(artifact);
        request.setRepositories(mavenProject.getRemoteProjectRepositories());
        VersionRangeResult result;
        try {
            result = repositorySystem.resolveVersionRange(getSession(), request);
        } catch (VersionRangeResolutionException e) {
            throw new PlatformDependencyProvisioningException(
                    format("Could not find artifact %s:%s for version range %s", groupId, artifactId, version), e);
        }

        if (isNull(result.getHighestVersion())) {
            result.getExceptions()
                    .forEach(exception -> log.debug(
                            format(
                                    "Exception (might be multiple) while looking up version range %s for %s:%s",
                                    version, groupId, artifactId),
                            exception));
            throw new PlatformDependencyProvisioningException(
                    format("No highest version found for %s:%s for version range %s", groupId, artifactId, version));
        }

        return artifact.setVersion(result.getHighestVersion().toString());
    }

    private Artifact loadArtifact(Artifact artifact) throws PlatformDependencyProvisioningException {
        ArtifactRequest request = new ArtifactRequest(artifact, mavenProject.getRemoteProjectRepositories(), null);

        ArtifactResult result;
        try {
            result = this.repositorySystem.resolveArtifact(getSession(), request);
        } catch (ArtifactResolutionException e) {
            throw new PlatformDependencyProvisioningException(
                    format(
                            "Could not load artifact %s:%s:%s",
                            artifact.getGroupId(), artifact.getArtifactId(), artifact.getVersion()),
                    e);
        }

        return result.getArtifact();
    }

    private List<Dependency> readPomDependencies(Artifact artifact) throws PlatformDependencyProvisioningException {
        Model result;
        File pomFile = artifact.getFile();

        try (Reader fileReader = new FileReader(pomFile)) {
            result = mavenReader.read(fileReader);
        } catch (IOException e) {
            throw new PlatformDependencyProvisioningException(
                    format("File %s is not a valid XML or cannot be read", pomFile.getPath()), e);
        } catch (XmlPullParserException e) {
            throw new PlatformDependencyProvisioningException(
                    format("File %s is not a valid POM", pomFile.getPath()), e);
        }

        return result.getDependencies();
    }

    private RepositorySystemSession getSession() {
        if (repositorySystemSession == null) {
            Path localRepositoryPath = Paths.get(mavenProject.getBuild().getDirectory(), "local-repository");
            DefaultRepositorySystemSession session = new DefaultRepositorySystemSession();
            LocalRepository localRepository = new LocalRepository(localRepositoryPath.toString());

            repositorySystemSession = session.setLocalRepositoryManager(
                    repositorySystem.newLocalRepositoryManager(session, localRepository));
        }

        return repositorySystemSession;
    }
}

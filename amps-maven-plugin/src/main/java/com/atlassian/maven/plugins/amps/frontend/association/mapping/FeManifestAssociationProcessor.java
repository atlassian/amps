package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static com.atlassian.maven.plugins.amps.frontend.CopyFeModuleManifestsMojo.MODULE_MANIFESTS_OUTPUT_DIR;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.Nonnull;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.maven.plugin.MojoExecutionException;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociation;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.PackageJson;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

public class FeManifestAssociationProcessor {

    private static final String PACKAGE_JSON_REGEX = "package\\.json";

    private final DirectoryHelper directoryHelper;
    private final JsonParser jsonParser;

    public FeManifestAssociationProcessor(DirectoryHelper directoryHelper, JsonParser jsonParser) {
        this.directoryHelper = directoryHelper;
        this.jsonParser = jsonParser;
    }

    public Map<String, FeOutputJsFileDeclaration> transformManifestAssociationsToFileDeclarations(
            List<FeManifestAssociation> manifestAssociations) throws MojoExecutionException {
        Map<String, FeOutputJsFileDeclaration> declarationsMap = new TreeMap<>();

        Map<String, String> packageNameToManifestPath =
                getPackageNamesMappedToOutputManifestPaths(manifestAssociations);
        for (FeManifestAssociation association : manifestAssociations) {
            for (String file : association.getFiles()) {
                if (!declarationsMap.containsKey(file)) {
                    String manifestPath = packageNameToManifestPath.get(association.getPackageName());
                    Set<String> declarations = new HashSet<>();
                    declarations.add(association.getDeclarationOrigin());
                    FeOutputJsFileDeclaration declaration =
                            new FeOutputJsFileDeclaration(file, declarations, manifestPath);
                    declarationsMap.put(file, declaration);
                } else {
                    FeOutputJsFileDeclaration declaration = declarationsMap.get(file);
                    declaration.getDeclarations().add(association.getDeclarationOrigin());
                }
            }
        }

        return declarationsMap;
    }

    @VisibleForTesting
    @Nonnull
    protected Map<String, String> getPackageNamesMappedToOutputManifestPaths(
            List<FeManifestAssociation> manifestAssociations) throws MojoExecutionException {
        Map<String, String> packageNameToManifestPath = new HashMap<>();

        for (FeManifestAssociation manifestAssociation : manifestAssociations) {
            String packageName = manifestAssociation.getPackageName();
            String outputManifestPath = findOutputManifestByPackageName(packageName);
            packageNameToManifestPath.put(packageName, outputManifestPath);
        }

        return packageNameToManifestPath;
    }

    private String findOutputManifestByPackageName(String manifestPackageName) throws MojoExecutionException {
        File outputDirectory = new File(directoryHelper.getOutputDirectoryPath());
        File moduleManifestsDirectory = getModuleManifestsDirectory();

        Collection<File> files = FileUtils.listFiles(
                moduleManifestsDirectory, new RegexFileFilter(PACKAGE_JSON_REGEX), DirectoryFileFilter.DIRECTORY);

        for (File fileEntry : files) {
            String packageName = readPackageName(fileEntry);
            if (packageName.equals(manifestPackageName)) {
                return outputDirectory.toURI().relativize(fileEntry.toURI()).getPath();
            }
        }

        throw new MojoExecutionException(String.format(
                "Couldn't find a frontend manifest file with provided package name in output directory.\nPackage name: %s\nModule manifests directory: %s",
                manifestPackageName, moduleManifestsDirectory));
    }

    private File getModuleManifestsDirectory() throws MojoExecutionException {
        File directory = new File(directoryHelper.getOutputFileAbsolutePath(MODULE_MANIFESTS_OUTPUT_DIR));
        if (!directory.exists() && !directory.isDirectory()) {
            throw new MojoExecutionException(String.format(
                    "Directory with frontend module manifests (%s) couldn't be found. Please make sure goal `copy-fe-module-manifests` is configured to include package.json and lock files in build output.",
                    MODULE_MANIFESTS_OUTPUT_DIR));
        }
        return directory;
    }

    private String readPackageName(File file) throws MojoExecutionException {
        try {
            PackageJson packageJson = jsonParser.readFile(file, PackageJson.class);
            return packageJson.getName();
        } catch (IOException e) {
            throw new MojoExecutionException(String.format("Couldn't parse package.json: %s", file.getAbsolutePath()));
        }
    }
}

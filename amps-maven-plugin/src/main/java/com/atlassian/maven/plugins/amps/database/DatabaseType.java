package com.atlassian.maven.plugins.amps.database;

import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.MojoExecutionException;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import com.atlassian.maven.plugins.amps.DataSource;

/** A type of database that AMPS supports. */
@ParametersAreNonnullByDefault
public interface DatabaseType {

    /**
     * Returns the configuration for the <code>sql-maven-plugin</code> to drop and recreate this database.
     *
     * @param dataSource the datasource being operated upon
     * @return see description
     */
    @Nonnull
    Xpp3Dom getSqlMavenCreateConfiguration(DataSource dataSource) throws MojoExecutionException;

    /**
     * Returns the configuration for the <code>sql-maven-plugin</code> to import an SQL dump file into the given
     * datasource.
     *
     * @param dataSource the datasource being operated upon
     * @return see description
     */
    @Nonnull
    Xpp3Dom getSqlMavenFileImportConfiguration(DataSource dataSource);

    /**
     * Returns the dependencies necessary for the <code>sql-maven-plugin</code> to operate on the given datasource.
     *
     * @param dataSource the datasource being operated upon
     * @return see description
     */
    @Nonnull
    List<Dependency> getSqlMavenDependencies(DataSource dataSource);

    /**
     * Returns the configuration for the <code>exec-maven-plugin</code> to import data into the given datasource using a
     * vendor-specific tool.
     *
     * @param dataSource the datasource being operated upon
     * @return <code>null</code> if this database doesn't use such a tool (e.g. uses SQL statements)
     */
    @Nullable Xpp3Dom getExecMavenToolImportConfiguration(DataSource dataSource) throws MojoExecutionException;

    /**
     * Indicates whether this type of database uses a schema.
     *
     * @return see description
     * @since 8.3
     */
    boolean hasSchema();

    /**
     * Returns the name of this type of database, as understood by OfBiz, the persistence framework used by Jira.
     *
     * @return a non-blank name
     */
    @Nonnull
    String getOfBizName();

    /**
     * Indicates whether the given {@link DataSource} is of this type.
     *
     * @param dataSource the datasource to check
     * @return see description
     */
    boolean isTypeOf(DataSource dataSource);
}

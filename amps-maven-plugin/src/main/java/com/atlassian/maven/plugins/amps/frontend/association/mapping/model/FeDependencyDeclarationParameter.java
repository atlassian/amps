package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class FeDependencyDeclarationParameter {
    private String manuallyVerifiedNonAtlassianArtifactPath;

    private List<String> manuallyVerifiedEveryJsDoesntHaveExternalDependencies;

    @JsonIgnore
    private String nonAtlassianArtifactDeclaration;

    public String getManuallyVerifiedNonAtlassianArtifactPath() {
        return manuallyVerifiedNonAtlassianArtifactPath;
    }

    public void setManuallyVerifiedNonAtlassianArtifactPath(String manuallyVerifiedNonAtlassianArtifactPath) {
        this.manuallyVerifiedNonAtlassianArtifactPath = manuallyVerifiedNonAtlassianArtifactPath;
    }

    public List<String> getManuallyVerifiedEveryJsDoesntHaveExternalDependencies() {
        return manuallyVerifiedEveryJsDoesntHaveExternalDependencies;
    }

    public void setManuallyVerifiedEveryJsDoesntHaveExternalDependencies(
            List<String> manuallyVerifiedEveryJsDoesntHaveExternalDependencies) {
        this.manuallyVerifiedEveryJsDoesntHaveExternalDependencies =
                manuallyVerifiedEveryJsDoesntHaveExternalDependencies;
    }

    public String getNonAtlassianArtifactDeclaration() {
        return nonAtlassianArtifactDeclaration;
    }

    public void setNonAtlassianArtifactDeclaration(String nonAtlassianArtifactDeclaration) {
        this.nonAtlassianArtifactDeclaration = nonAtlassianArtifactDeclaration;
    }
}

package com.atlassian.maven.plugins.amps.database;

import static java.sql.DriverManager.getDriver;
import static java.util.Arrays.stream;
import static org.apache.commons.lang3.StringUtils.startsWith;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;

import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import com.atlassian.maven.plugins.amps.DataSource;

@ParametersAreNonnullByDefault
public class MySQL extends AbstractDatabase {

    private static final String DROP_DATABASE = "DROP DATABASE IF EXISTS `%s`;\n";
    private static final String DROP_USER = "GRANT USAGE ON *.* TO `%s`@localhost;\nDROP USER `%s`@localhost;\n";
    private static final String CREATE_DATABASE = "CREATE DATABASE `%s` CHARACTER SET utf8 COLLATE utf8_bin;\n";
    private static final String CREATE_USER = "CREATE USER `%s`@localhost IDENTIFIED BY '%s';\n";
    private static final String GRANT_PERMISSION = "GRANT ALL ON `%s`.* TO `%s`@localhost;\n";
    private static final String URL_PREFIX = "jdbc:mysql";

    public MySQL(final Log log) {
        super(log, false, "mysql", "com.mysql.jdbc.Driver", URL_PREFIX);
    }

    @Override
    protected String dropDatabase(final DataSource dataSource) throws MojoExecutionException {
        return String.format(DROP_DATABASE, getDatabaseName(dataSource));
    }

    @Override
    protected String dropUser(final DataSource dataSource) {
        return String.format(DROP_USER, dataSource.getUsername(), dataSource.getUsername());
    }

    @Override
    protected String createDatabase(final DataSource dataSource) throws MojoExecutionException {
        return String.format(CREATE_DATABASE, getDatabaseName(dataSource));
    }

    @Override
    protected String createUser(final DataSource dataSource) {
        return String.format(CREATE_USER, dataSource.getUsername(), dataSource.getPassword());
    }

    @Override
    protected String grantPermissionForUser(final DataSource dataSource) throws MojoExecutionException {
        return String.format(GRANT_PERMISSION, getDatabaseName(dataSource), dataSource.getUsername());
    }

    /**
     * Reference mysql 8.0 documentation 8.0 Driver/Datasource Class Names, URL Syntax and Configuration Properties for
     * Connector/J http://dev.mysql.com/doc/connector-j/en/connector-j-reference-configuration-properties.html sample
     * connection URL: jdbc:mysql://localhost:3306/sakila?profileSQL=true
     *
     * @return database name
     */
    @Override
    protected String getDatabaseName(final DataSource dataSource) throws MojoExecutionException {
        try {
            Class.forName(dataSource.getDriver());
        } catch (ClassNotFoundException e) {
            throw new MojoExecutionException("Could not load Mysql database library to classpath");
        }
        try {
            final String jdbcUrl = dataSource.getUrl();
            final DriverPropertyInfo[] driverProperties = getDriver(jdbcUrl).getPropertyInfo(jdbcUrl, null);
            if (driverProperties == null) {
                return null;
            }
            return stream(driverProperties)
                    .filter(property -> "DBNAME".equalsIgnoreCase(property.name))
                    .map(property -> property.value)
                    .findFirst()
                    .orElse(null);
        } catch (SQLException e) {
            throw new MojoExecutionException("");
        }
    }

    @Override
    @Nonnull
    public Xpp3Dom getSqlMavenCreateConfiguration(final DataSource dataSource) throws MojoExecutionException {
        final String sql = dropDatabase(dataSource)
                + dropUser(dataSource)
                + createDatabase(dataSource)
                + createUser(dataSource)
                + grantPermissionForUser(dataSource);
        log.info("MySQL initialization database sql: " + sql);
        Xpp3Dom pluginConfiguration = systemDatabaseConfiguration(dataSource);
        pluginConfiguration.addChild(element(name("sqlCommand"), sql).toDom());
        return pluginConfiguration;
    }

    @Override
    public boolean isTypeOf(final DataSource dataSource) {
        return super.isTypeOf(dataSource) || (startsWith(dataSource.getUrl(), URL_PREFIX) && usesNewDriver(dataSource));
    }

    private boolean usesNewDriver(final DataSource dataSource) {
        return "com.mysql.cj.jdbc.Driver".equals(dataSource.getDriver());
    }
}

package com.atlassian.maven.plugins.amps;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.createPluginModule;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.BAMBOO;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.CONFLUENCE;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.CROWD;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.FECRU;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.JIRA;
import static org.apache.commons.lang3.StringUtils.endsWith;

import java.io.File;
import java.util.List;
import java.util.function.BinaryOperator;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import com.atlassian.maven.plugins.amps.codegen.ConditionFactory;
import com.atlassian.maven.plugins.amps.codegen.ContextProviderFactory;
import com.atlassian.maven.plugins.amps.codegen.PluginModuleSelectionQueryer;
import com.atlassian.maven.plugins.amps.codegen.jira.ActionTypeFactory;
import com.atlassian.maven.plugins.amps.codegen.jira.CustomFieldSearcherFactory;
import com.atlassian.maven.plugins.amps.codegen.jira.CustomFieldTypeFactory;
import com.atlassian.maven.plugins.amps.codegen.prompter.PluginModulePrompter;
import com.atlassian.maven.plugins.amps.codegen.prompter.PluginModulePrompterFactory;
import com.atlassian.plugins.codegen.MavenProjectRewriter;
import com.atlassian.plugins.codegen.PluginProjectChangeset;
import com.atlassian.plugins.codegen.PluginXmlRewriter;
import com.atlassian.plugins.codegen.ProjectFilesRewriter;
import com.atlassian.plugins.codegen.modules.PluginModuleCreator;
import com.atlassian.plugins.codegen.modules.PluginModuleCreatorFactory;
import com.atlassian.plugins.codegen.modules.PluginModuleLocation;
import com.atlassian.plugins.codegen.modules.PluginModuleProperties;

/**
 * Adds a new plugin module to an existing plugin project.
 *
 * @since 3.6
 */
@Mojo(name = "create-plugin-module", requiresDependencyResolution = ResolutionScope.COMPILE)
public class PluginModuleGenerationMojo extends AbstractProductAwareMojo {

    private static final BinaryOperator<String> FIND_LAST = (first, second) -> second;

    @Component
    private PluginModuleSelectionQueryer pluginModuleSelectionQueryer;

    @Component
    private PluginModulePrompterFactory pluginModulePrompterFactory;

    @Component
    private PluginModuleCreatorFactory pluginModuleCreatorFactory;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        trackFirstRunIfNeeded();
        Log log = getLog();
        // can't figure out how to get plexus to fire a method after injection, so doing it here
        pluginModulePrompterFactory.setLog(log);
        try {
            pluginModulePrompterFactory.scanForPrompters();
        } catch (Exception e) {
            String message = "Error initializing Plugin Module Prompters";
            log.error(message);
            throw new MojoExecutionException(message);
        }

        String productId = getProductId();
        MavenProject project = getMavenContext().getProject();
        File javaDir = getJavaSourceRoot(project);
        File testDir = getJavaTestRoot(project);
        File resourcesDir = getResourcesRoot(project);

        initHelperFactories(productId, project);

        PluginModuleLocation moduleLocation = new PluginModuleLocation.Builder(javaDir)
                .resourcesDirectory(resourcesDir)
                .testDirectory(testDir)
                .templateDirectory(new File(resourcesDir, "templates"))
                .groupAndArtifactId(project.getGroupId(), project.getArtifactId())
                .build();

        if (!moduleLocation.getPluginXml().exists()) {
            String message =
                    "Couldn't find the atlassian-plugin.xml, please run this goal in an atlassian plugin project root.";
            log.error(message);
            throw new MojoExecutionException(message);
        }

        runGeneration(productId, project, moduleLocation);
    }

    private void runGeneration(String productId, MavenProject project, PluginModuleLocation moduleLocation)
            throws MojoExecutionException {
        try {
            final PluginModuleCreator creator = pluginModuleSelectionQueryer.selectModule(
                    pluginModuleCreatorFactory.getModuleCreatorsForProduct(productId));

            sendAnalyticsEvent(createPluginModule(getPluginInformation().getId(), creator.getModuleName()));

            final PluginModulePrompter<?> modulePrompter =
                    pluginModulePrompterFactory.getPrompterForCreatorClass(creator.getClass());
            if (modulePrompter == null) {
                final String message = "Couldn't find an input prompter for: "
                        + creator.getClass().getName();
                getLog().error(message);
                throw new MojoExecutionException(message);
            }

            modulePrompter.setDefaultBasePackage(project.getGroupId());
            modulePrompter.setPluginKey(project.getGroupId() + "." + project.getArtifactId());

            final PluginModuleProperties moduleProps = modulePrompter.getModulePropertiesFromInput(moduleLocation);
            moduleProps.setProductId(getGadgetCompatibleProductId(productId));

            final PluginProjectChangeset changeset = creator.createModule(moduleProps);

            getLog().info("Adding the following items to the project:");
            for (String desc : changeset.getChangeDescriptionsOrSummaries()) {
                getLog().info("  " + desc);
            }

            editPomIfNeeded(project, changeset);

            // apply changes to project files
            new ProjectFilesRewriter(moduleLocation).applyChanges(changeset);
            new PluginXmlRewriter(moduleLocation).applyChanges(changeset);

            if (pluginModuleSelectionQueryer.addAnotherModule()) {
                runGeneration(productId, project, moduleLocation);
            }

        } catch (Exception e) {
            throw new MojoExecutionException("Error creating plugin module", e);
        }
    }

    private void editPomIfNeeded(final MavenProject project, final PluginProjectChangeset changeset) {
        try {
            new MavenProjectRewriter(project.getFile()).applyChanges(changeset);
        } catch (final Exception e) {
            getLog().error("Unable to apply changes to POM: " + e);
        }
    }

    private String getGadgetCompatibleProductId(final String pid) {
        switch (pid) {
            case BAMBOO:
                return "Bamboo";
            case CONFLUENCE:
                return "Confluence";
            case CROWD:
                return "Crowd";
            case FECRU:
                return "FishEye";
            case JIRA:
                return "JIRA";
            default:
                return "Other";
        }
    }

    private File getJavaSourceRoot(MavenProject project) {
        return new File(project.getModel().getBuild().getSourceDirectory());
    }

    private File getJavaTestRoot(MavenProject project) {
        return new File(project.getModel().getBuild().getTestSourceDirectory());
    }

    private File getResourcesRoot(final MavenProject project) {
        final String pathToCheck = "src" + File.separator + "main" + File.separator + "resources";
        return project.getModel().getBuild().getResources().stream()
                .map(FileSet::getDirectory)
                .filter(dir -> endsWith(dir, pathToCheck))
                .reduce(FIND_LAST)
                .map(File::new)
                .orElse(null);
    }

    private void initHelperFactories(String productId, MavenProject project) throws MojoExecutionException {
        List<String> pluginClasspath;
        try {
            pluginClasspath = project.getCompileClasspathElements();
        } catch (DependencyResolutionRequiredException e) {
            throw new MojoExecutionException("Dependencies MUST be resolved", e);
        }

        try {
            ConditionFactory.locateAvailableConditions(productId, pluginClasspath);
        } catch (Exception e) {
            String message = "Error initializing Plugin Module Conditions";
            getLog().error(message);
            // keep going, doesn't matter
        }

        try {
            ContextProviderFactory.locateAvailableContextProviders(productId, pluginClasspath);
        } catch (Exception e) {
            String message = "Error initializing Plugin Module Context Providers";
            getLog().error(message);
            // keep going, doesn't matter
        }

        if (JIRA.equals(productId)) {
            try {
                ActionTypeFactory.locateAvailableActionTypes(pluginClasspath);
            } catch (Exception e) {
                String message = "Error initializing JIRA Action Types";
                getLog().error(message);
                // keep going, doesn't matter
            }

            try {
                CustomFieldTypeFactory.locateAvailableCustomFieldTypes(pluginClasspath);
            } catch (Exception e) {
                String message = "Error initializing JIRA Custom Field Types";
                getLog().error(message);
                // keep going, doesn't matter
            }

            try {
                CustomFieldSearcherFactory.locateAvailableCustomFieldSearchers(pluginClasspath);
            } catch (Exception e) {
                String message = "Error initializing JIRA Custom Field Searchers";
                getLog().error(message);
                // keep going, doesn't matter
            }
        }
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

public class ExternalFeManifestAssociationConfiguration {

    private String packageName;

    private ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest
            manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest
            getManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest() {
        return manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest;
    }

    public void setManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest(
            ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest
                    manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest) {
        this.manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest =
                manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest;
    }
}

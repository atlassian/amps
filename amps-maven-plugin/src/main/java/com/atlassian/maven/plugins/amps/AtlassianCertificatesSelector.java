package com.atlassian.maven.plugins.amps;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import com.atlassian.maven.plugins.amps.upm.signing.tools.AtlassianCertificate;

public enum AtlassianCertificatesSelector {
    ALL(Collections.unmodifiableSet(EnumSet.allOf(AtlassianCertificate.class))),
    PROD(Collections.unmodifiableSet(
            EnumSet.of(AtlassianCertificate.MARKETPLACE_ROOT_V1, AtlassianCertificate.MARKETPLACE_INTERMEDIATE_V1))),
    STAGING(Collections.unmodifiableSet(EnumSet.of(
            AtlassianCertificate.MARKETPLACE_STAGING_ROOT, AtlassianCertificate.MARKETPLACE_STAGING_INTERMEDIATE)));

    private final Set<AtlassianCertificate> certificates;

    AtlassianCertificatesSelector(Set<AtlassianCertificate> certificates) {
        this.certificates = certificates;
    }

    public Set<AtlassianCertificate> getCertificates() {
        return certificates;
    }
}

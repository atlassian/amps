package com.atlassian.maven.plugins.amps.database;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.File;
import java.sql.DatabaseMetaData;
import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.apache.maven.plugin.logging.Log;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.util.FileUtils;

@SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
@ParametersAreNonnullByDefault
public class Oracle12c extends AbstractOracleDatabase {

    /**
     * The string that the database product version needs to start with in order for this database type to be detected.
     *
     * @see DatabaseMetaData#getDatabaseProductVersion
     */
    public static final String ORACLE_12C_VERSION_PREFIX = "Oracle Database 12c";

    public Oracle12c(final Log log) {
        super(log, "oracle12c");
    }

    protected String getSqlToDropAndCreateUser(final DataSource dataSource) {
        if (oracleInStandaloneMode(dataSource)) {
            // Use the "classic mode" user management query; does not require SYSDBA privilege
            return new Oracle10g(log).getSqlToDropAndCreateUser(dataSource);
        }
        return getTenantedModeDropAndCreateUserQuery(dataSource);
    }

    /**
     * Returns the "drop and create user" query for use in CDB (tenanted) mode. Because this query requires the SYSDBA
     * privilege, the AMPS datasource must be configured with a <code>systemUsername</code> that has that privilege,
     * e.g. <code>SYS AS SYSDBA</code>.
     *
     * @return see above
     */
    private String getTenantedModeDropAndCreateUserQuery(final DataSource dataSource) {
        // Note that because Oracle tries to resolve this dump file path relative to its own
        // file system, the data import will only work if AMPS and Oracle are running on the
        // same machine (or the path is valid on both machines by virtue of file sharing).
        final String dumpFileDirectoryPath = (new File(dataSource.getDumpFilePath())).getParent();
        return FileUtils.readFileToString("oracle12c-template.sql", getClass(), UTF_8)
                .replace("v_data_pump_dir", dumpFileDirectoryPath)
                .replace("v_product_user", dataSource.getUsername())
                .replace("v_product_pwd", dataSource.getPassword());
    }

    /**
     * Indicates whether Oracle 12c is running in standalone mode, the non-tenanted mode in which 10g and 11g run.
     *
     * @return <code>false</code> if Oracle is running in single or multi tenant mode
     */
    private boolean oracleInStandaloneMode(final DataSource dataSource) {
        final JdbcOperations jdbcOperations = new JdbcTemplate(dataSource.getJdbcDataSource());
        final String isCdb = jdbcOperations.queryForObject("select cdb from v$database", String.class);
        return isCdb == null || isCdb.toLowerCase().startsWith("n");
    }

    @Override
    public boolean isTypeOf(final DataSource dataSource) {
        // Because this check is more specific than the superclass, this class should be checked for a match
        // before other implementations in the Oracle type hierarchy.
        return super.isTypeOf(dataSource) && dbProductVersionIsOracle12c(dataSource);
    }

    private static boolean dbProductVersionIsOracle12c(final DataSource dataSource) {
        return dataSource
                .<String>getJdbcMetaData(DatabaseMetaData::getDatabaseProductVersion)
                .filter(version -> version.startsWith(ORACLE_12C_VERSION_PREFIX))
                .isPresent();
    }
}

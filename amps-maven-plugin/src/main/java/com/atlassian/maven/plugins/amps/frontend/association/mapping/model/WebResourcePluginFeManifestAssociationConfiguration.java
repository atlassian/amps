package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.List;

public class WebResourcePluginFeManifestAssociationConfiguration {
    private String packageName;

    private List<String> outputDirectoryFiles;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public List<String> getOutputDirectoryFiles() {
        return outputDirectoryFiles;
    }

    public void setOutputDirectoryFiles(List<String> outputDirectoryFiles) {
        this.outputDirectoryFiles = outputDirectoryFiles;
    }
}

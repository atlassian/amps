package com.atlassian.maven.plugins.amps.frontend.association.verification;

import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.METAINF_LIB_DIRECTORY_NAME;
import static com.atlassian.maven.plugins.amps.osgi.OsgiHelper.canCreateObrArtifact;
import static com.atlassian.maven.plugins.amps.util.ProjectUtils.isAtlassianProject;
import static org.apache.maven.plugins.annotations.LifecyclePhase.VERIFY;

import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.ReportFeManifestAssociationsMojo;
import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.ArtifactScannerImpl;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ArtifactScanResults;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.FailurePreferences;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ModuleScanResults;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.NestedModuleDeclarationData;
import com.atlassian.maven.plugins.amps.frontend.association.verification.utils.ArtifactPathUtils;
import com.atlassian.maven.plugins.amps.frontend.association.verification.utils.TrueZipUtils;

/**
 * Verifies if all javascript files in a given artifact have corresponding manifest association created by
 * {@link ReportFeManifestAssociationsMojo}. It accepts all standard archive types:
 * {@link ArtifactScannerImpl#SUPPORTED_ARCHIVE_EXTENSIONS}. And also works with directories.
 *
 * @since 8.17.0
 * @since 9.0.6 Default phase set to `VERIFY`
 */
@Mojo(name = "verify-fe-manifest-associations", defaultPhase = VERIFY, requiresProject = false)
public class VerifyFeManifestAssociationsMojo extends AbstractMojo {

    /**
     * Path to input artifact or directory which will be verified
     *
     * @since 8.17.0
     */
    @Parameter(
            property = "verify.fe.manifest.associations.inputEntrypoint",
            alias = "verifyFeManifestAssociationsInputEntrypoint")
    private String inputEntrypoint;

    /**
     * Flag controlling whether to fail when undeclared files were found
     *
     * @since 8.17.0
     */
    @Parameter(
            property = "verify.fe.manifest.associations.failOnUndeclaredFiles",
            alias = "verifyFeManifestAssociationsFailOnUndeclaredFiles",
            defaultValue = "true")
    private Boolean failOnUndeclaredFiles;

    /**
     * Flag controlling whether to fail when any declarations not matching existing files were found
     *
     * @since 8.17.0
     */
    @Parameter(
            property = "verify.fe.manifest.associations.failOnExtraDeclarations",
            alias = "verifyFeManifestAssociationsFailOnExtraDeclarations",
            defaultValue = "true")
    private Boolean failOnExtraDeclarations;

    /**
     * Flag controlling whether to skip the execution of the mojo
     *
     * @since 9.0.5
     * @since 9.0.6 Change alias `skip` to `skipVerifyFeManifestAssociations` to avoid property name overloading
     */
    @Parameter(
            property = "verify.fe.manifest.associations.skip",
            alias = "skipVerifyFeManifestAssociations",
            defaultValue = "false")
    private boolean skipVerifyFeManifestAssociations;

    /** The current Maven project. */
    @Parameter(property = "project", readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${mojoExecution}", readonly = true, required = true)
    private MojoExecution mojoExecution;

    @Parameter
    private Map instructions = new HashMap();

    private final Logger logger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * If the goal is started from the build lifecycle it will have `default-<goalName>` execution id <a
     * href="https://maven.apache.org/guides/mini/guide-default-execution-ids.html">Maven documentation</a>
     */
    private static final String DEFAULT_LIFECYCLE_EXECUTION_ID = "default-verify-fe-manifest-associations";

    @Component
    private ArtifactScanner artifactScanner;

    @Component
    private DeclarationsReader declarationsReader;

    @Component
    private DeclarationsChecker declarationsChecker;

    public void execute() throws MojoExecutionException {
        if (skipVerifyFeManifestAssociations) {
            logger.info("Skipping execute frontend manifest associations verification");
            return;
        }
        if (isLifecycleExecution()) {
            if (isAtlassianProject(project)) {
                logger.info(
                        "Running the verification of all the javascript files having association with frontend manifest files (package.json and lock file)\n"
                                + "The execution was started from the default atlassian plugin build lifecycle.\n"
                                + "You can remove any explicit executions of `verify-fe-manifest-associations` goal from your configuration.");
            } else {
                return;
            }
        }

        setInputEntrypoint();
        logger.info("Verifying frontend manifest associations for an artifact: {}", inputEntrypoint);

        FailurePreferences failurePreferences = new FailurePreferences(failOnUndeclaredFiles, failOnExtraDeclarations);
        verifyManifestAssociations(inputEntrypoint, failurePreferences);
    }

    @VisibleForTesting
    protected void verifyManifestAssociations(String inputEntrypoint, FailurePreferences failurePreferences)
            throws MojoExecutionException {
        TrueZipUtils.configureTrueZip();
        declarationsChecker.setFailurePreferences(failurePreferences);

        try {
            scanEntrypoint(inputEntrypoint);
            declarationsChecker.finalizeChecks();
        } finally {
            TrueZipUtils.unmountTrueZip();
        }
    }

    private void scanEntrypoint(String inputEntrypoint) throws MojoExecutionException {
        ArrayList<MutablePair<TFile, ArtifactScanResults>> completeScanData = runRecursiveScan(inputEntrypoint);

        Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations =
                getNestedModuleDeclarations(completeScanData);
        nestedModuleDeclarations.putAll(getExpandedNestedModuleDeclarations(nestedModuleDeclarations));

        verifyAllModules(completeScanData, nestedModuleDeclarations);
    }

    private void verifyAllModules(
            ArrayList<MutablePair<TFile, ArtifactScanResults>> completeScanData,
            Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations)
            throws MojoExecutionException {
        for (MutablePair<TFile, ArtifactScanResults> artifactScanData : completeScanData) {
            ArtifactScanResults scanResults = artifactScanData.getRight();

            verifyModules(scanResults.getModules(), nestedModuleDeclarations);
        }
    }

    /**
     * Expand nested module declarations to parent modules so that if we have a nested module declaration for:
     * something.jar/META-INF/lib/something-else.jar/META-INF/lib/third-party.jar we will copy it under:
     * something.jar/META-INF/lib/third-party.jar so that the associations are available for the flattened out
     * dependency files created by the CopyBundledDependenciesMojo
     */
    private static Map<TFile, NestedModuleDeclarationData> getExpandedNestedModuleDeclarations(
            Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations) {
        Map<TFile, NestedModuleDeclarationData> expandedModuleDeclarations = new HashMap<>();

        for (Map.Entry<TFile, NestedModuleDeclarationData> nestedModuleData : nestedModuleDeclarations.entrySet()) {
            TFile modulePath = nestedModuleData.getKey();
            NestedModuleDeclarationData moduleData = nestedModuleData.getValue();

            final String nestedModuleName = modulePath.getName();

            while (true) {
                TFile parent = modulePath.getParentFile();
                if (parent == null) {
                    break; // We're out but unlikely to happen
                }

                if (!parent.getPath().endsWith(METAINF_LIB_DIRECTORY_NAME)) {
                    break; // We got to the toplevel module so we're done
                }

                if (!modulePath.getName().equals(nestedModuleName)) {
                    TFile modulePathInParent = new TFile(parent, nestedModuleName);
                    if (expandedModuleDeclarations.containsKey(modulePathInParent)) {
                        break; // We're already in
                    }

                    expandedModuleDeclarations.put(modulePathInParent, moduleData);
                }

                assert parent.getParentFile() != null;
                modulePath = parent.getParentFile().getParentFile();
                assert modulePath != null;
            }
        }

        return expandedModuleDeclarations;
    }

    private Map<TFile, NestedModuleDeclarationData> getNestedModuleDeclarations(
            ArrayList<MutablePair<TFile, ArtifactScanResults>> completeScanData) throws MojoExecutionException {
        Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations = new HashMap<>();

        for (MutablePair<TFile, ArtifactScanResults> artifactScanData : completeScanData) {
            ArtifactScanResults scanResults = artifactScanData.getRight();
            nestedModuleDeclarations.putAll(processNestedDeclarations(scanResults.getModules()));
        }

        return nestedModuleDeclarations;
    }

    private ArrayList<MutablePair<TFile, ArtifactScanResults>> runRecursiveScan(String inputEntrypoint)
            throws MojoExecutionException {
        ArrayList<MutablePair<TFile, ArtifactScanResults>> result = new ArrayList<>();

        Queue<TFile> queue = new ArrayDeque<>();
        TFile root = new TFile(inputEntrypoint);

        queue.add(root);
        while (!queue.isEmpty()) {
            TFile currentArtifact = queue.poll();

            ArtifactScanResults scanResults = artifactScanner.scan(currentArtifact);
            result.add(new MutablePair<>(currentArtifact, scanResults));

            queue.addAll(scanResults.getArchiveFiles());
        }

        return result;
    }

    private Map<TFile, NestedModuleDeclarationData> processNestedDeclarations(Map<TFile, ModuleScanResults> modules)
            throws MojoExecutionException {
        Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations = new HashMap<>();

        for (Map.Entry<TFile, ModuleScanResults> module : modules.entrySet()) {
            TFile moduleRoot = module.getKey();
            ModuleScanResults moduleScanResults = module.getValue();

            if (moduleScanResults.hasNestedDeclarationFiles()) {
                nestedModuleDeclarations.putAll(declarationsReader.readNestedModuleDeclarationData(
                        moduleScanResults.getNestedDeclarationFiles(), moduleRoot));
            }
        }

        return nestedModuleDeclarations;
    }

    private void verifyModules(
            Map<TFile, ModuleScanResults> modules, Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations)
            throws MojoExecutionException {
        for (Map.Entry<TFile, ModuleScanResults> module : modules.entrySet()) {
            TFile moduleRoot = module.getKey();
            ModuleScanResults moduleScanResults = module.getValue();

            Set<String> moduleRelativeJsFiles =
                    ArtifactPathUtils.getRelativePaths(moduleScanResults.getJavascriptFiles(), moduleRoot);

            if (nestedModuleDeclarations.containsKey(moduleRoot)) {
                NestedModuleDeclarationData nestedModuleData = nestedModuleDeclarations.get(moduleRoot);

                declarationsChecker.verifyExternalModule(moduleRoot, nestedModuleData, moduleRelativeJsFiles);
            } else {
                Set<String> moduleRelativeDeclarations = declarationsReader.readRelativeDeclarations(
                        moduleScanResults.getAssociationFiles(), moduleRoot);

                declarationsChecker.verifyModule(moduleRoot, moduleRelativeDeclarations, moduleRelativeJsFiles);
            }
        }
    }

    private void setInputEntrypoint() throws MojoExecutionException {
        if (inputEntrypoint != null) return;

        if (project != null) {
            String buildDir = project.getBuild().getDirectory();
            String buildFinalName = project.getBuild().getFinalName();
            String extension = getProjectArtifactExtensions(project);

            if (StringUtils.isNotBlank(extension)) {
                inputEntrypoint = Paths.get(buildDir, String.format("%s.%s", buildFinalName, extension))
                        .toString();
            } else {
                throw new MojoExecutionException(
                        "Verification entrypoint hasn't been provided and couldn't be derived from project configuration. "
                                + "Please configure `verifyFeManifestAssociationsInputEntrypoint` property or skip artifact verification with `skipVerifyFeManifestAssociations`");
            }
        } else {
            throw new MojoExecutionException(
                    "Verification entrypoint is required. Please provide `verifyFeManifestAssociationsInputEntrypoint` property"
                            + "with path to input artifact or directory which will be verified");
        }
    }

    private String getProjectArtifactExtensions(MavenProject project) {
        String packaging = project.getPackaging();
        String extension = null;
        switch (packaging) {
            case "atlassian-plugin":
                if (canCreateObrArtifact(instructions)) {
                    extension = "obr";
                } else {
                    extension = "jar";
                }
                break;
            case "jar":
            case "war":
                extension = packaging;
                break;
        }

        return extension;
    }

    private boolean isLifecycleExecution() {
        String executionId = mojoExecution.getExecutionId();
        return executionId.equals(DEFAULT_LIFECYCLE_EXECUTION_ID);
    }
}

package com.atlassian.maven.plugins.amps.util;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;

import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;

/**
 * Utility methods related to setting up products for debugging.
 *
 * @since 8.3
 */
public final class DebugUtils {

    /**
     * Sets the debug ports of each node of the given products.
     *
     * @param products the products whose nodes to assign debug ports
     * @param defaultPort the default debug port for the first node of the first product
     * @throws MojoExecutionException if there are any port conflicts
     */
    public static void setNodeDebugPorts(final List<Product> products, final int defaultPort)
            throws MojoExecutionException {
        int portOffset = 0;
        final Map<Integer, List<String>> nodeIdsByDebugPort = new LinkedHashMap<>(); // to detect duplicate ports
        for (final Product product : products) {
            final List<Node> nodes = product.getNodes();
            for (int nodeIndex = 0; nodeIndex < nodes.size(); nodeIndex++) {
                final Node node = nodes.get(nodeIndex);
                portOffset = setDebugPort(node, defaultPort, portOffset);
                final int debugPort = node.getJvmDebugPort();
                final List<String> nodeIds = nodeIdsByDebugPort.computeIfAbsent(debugPort, k -> new ArrayList<>());
                nodeIds.add(product.getInstanceId() + " node " + nodeIndex);
            }
        }
        checkForPortConflicts(nodeIdsByDebugPort);
    }

    private static void checkForPortConflicts(final Map<Integer, List<String>> nodeIdsByDebugPort)
            throws MojoExecutionException {
        final String error = nodeIdsByDebugPort.entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                .map(entry -> format("Debug port %d is used by these nodes: %s", entry.getKey(), entry.getValue()))
                .collect(joining("; "));
        if (isNotBlank(error)) {
            throw new MojoExecutionException(error);
        }
    }

    private static int setDebugPort(final Node node, final int defaultPort, final int portOffset) {
        if (node.getJvmDebugPort() == 0) {
            node.setJvmDebugPort(defaultPort + portOffset);
            return portOffset + 1;
        }
        return portOffset;
    }

    /**
     * Returns the debug-related properties for the given products.
     *
     * @param products the products
     * @return see description
     */
    public static Map<String, String> getDebugPortProperties(final Collection<Product> products) {
        final Map<String, String> properties = new LinkedHashMap<>();
        putFirstNodeDebugPortProperty(products, properties);
        putInstanceSpecificProperties(products, properties);
        return properties;
    }

    private static void putFirstNodeDebugPortProperty(
            final Collection<Product> products, final Map<String, String> properties) {
        products.stream()
                .flatMap(product -> product.getNodes().stream())
                .findFirst()
                .ifPresent(node -> {
                    final String debugPort = String.valueOf(node.getJvmDebugPort());
                    properties.put("debug.port", debugPort);
                });
    }

    private static void putInstanceSpecificProperties(
            final Collection<Product> products, final Map<String, String> properties) {
        for (final Product product : products) {
            final List<Node> nodes = product.getNodes();
            for (int i = 0; i < nodes.size(); i++) {
                final Node node = nodes.get(i);
                final String debugPort = String.valueOf(node.getJvmDebugPort());
                properties.put("debug." + product.getInstanceId() + ".port." + i, debugPort);
                if (i == 0) {
                    // For backward compatibility, also publish the first node's debug port as that of the product
                    properties.put("debug." + product.getInstanceId() + ".port", debugPort);
                }
            }
        }
    }

    private DebugUtils() {}
}

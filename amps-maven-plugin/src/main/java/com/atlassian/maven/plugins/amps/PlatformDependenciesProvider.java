package com.atlassian.maven.plugins.amps;

import java.util.Collection;

interface PlatformDependenciesProvider {
    /**
     * Returns a sorted collection of platform dependencies. The collection is constructed based on specified POM
     * artifacts. Each POMs dependency is included in the final list.
     *
     * <p>Version can be a range of versions like: [3.4.12,).
     *
     * @param groupId A groupId of the platform POM
     * @param artifactIds Collection of artifactIds inside which dependency elements are collected
     * @param version A version of the platform
     * @throws PlatformDependencyProvisioningException
     */
    Collection<String> getDependencies(final String groupId, final Collection<String> artifactIds, final String version)
            throws PlatformDependencyProvisioningException;
}

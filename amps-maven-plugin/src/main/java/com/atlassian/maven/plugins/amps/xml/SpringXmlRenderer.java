package com.atlassian.maven.plugins.amps.xml;

import java.io.Writer;

/** Writes an XML file with Spring and OSGI configuration based on the provided model. */
public interface SpringXmlRenderer {
    /**
     * Applies the provided `model` to the internal template and writes resulting XML into the `writer`.
     *
     * @param model a model which is applied to the template
     * @param writer a writer where output of the operation is written
     * @throws SpringXmlException if the process fails
     */
    void render(SpringXmlModel model, Writer writer) throws SpringXmlException;
}

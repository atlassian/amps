package com.atlassian.maven.plugins.amps.xml;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/** Model which can be used to render Spring XML file configuring beans and OSGi services. */
@JacksonXmlRootElement(localName = "model")
public final class SpringXmlModel {
    private final List<SpringXmlComponent> components;

    private SpringXmlModel(final List<SpringXmlComponent> components) {
        this.components = components;
    }

    @JacksonXmlProperty(localName = "component")
    @JacksonXmlElementWrapper(localName = "components")
    List<SpringXmlComponent> components() {
        return components;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        SpringXmlModel model = (SpringXmlModel) other;
        return Objects.equals(model.components, components);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(components);
    }

    /**
     * Creates a new builder for {@link SpringXmlModel}.
     *
     * @return {@link Builder}.
     */
    public static Builder builder() {
        return new Builder();
    }

    /** A builder for {@link SpringXmlModel}. */
    public static final class Builder {
        private List<SpringXmlComponent> components = new ArrayList<>();

        private Builder() {}

        /**
         * Adds a new component to the model.
         *
         * @param component a component to be added
         * @return {@link Builder} so chaining the methods is possible
         */
        public Builder addComponent(final SpringXmlComponent component) {
            if (component == null) {
                throw new IllegalArgumentException("'component' must not be null");
            }

            this.components.add(component);
            return this;
        }

        /**
         * Completes the creation process and returns a new model.
         *
         * @return {@link SpringXmlModel}
         */
        public SpringXmlModel build() {
            return new SpringXmlModel(unmodifiableList(components));
        }
    }
}

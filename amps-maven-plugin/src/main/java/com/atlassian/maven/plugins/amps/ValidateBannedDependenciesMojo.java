package com.atlassian.maven.plugins.amps;

import java.util.HashSet;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * Checks that the current project has no platform modules in {@code compile} scope, even transitively, so that it does
 * not bundle any such artifacts and thereby cause classloading problems at runtime.
 */
@Mojo(name = "validate-banned-dependencies", requiresDependencyResolution = ResolutionScope.COMPILE)
public class ValidateBannedDependenciesMojo extends AbstractAmpsMojo {
    @Parameter(property = "skipBanningDependencies", defaultValue = "false")
    private boolean skipBanningDependencies;

    @Parameter(property = "banningExcludes")
    private Set<String> banningExcludes = new HashSet<>();

    @Parameter(property = "platformVersion")
    private String platformVersion;

    @Parameter(property = "forceInternalPlugin", defaultValue = "false")
    private boolean forceInternalPlugin;

    @Deprecated
    @Parameter(property = "legacyValidateBannedDependencies", defaultValue = "false")
    private boolean legacyValidateBannedDependencies;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        ValidateBannedDependenciesExecutor executor = new ValidateBannedDependenciesExecutor(
                skipBanningDependencies,
                banningExcludes,
                platformVersion,
                forceInternalPlugin,
                legacyValidateBannedDependencies);
        executor.execute(getMavenGoals());
    }
}

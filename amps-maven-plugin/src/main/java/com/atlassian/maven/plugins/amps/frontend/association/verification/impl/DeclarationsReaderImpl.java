package com.atlassian.maven.plugins.amps.frontend.association.verification.impl;

import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.JAVASCRIPT_FILE_REGEX;

import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugin.MojoExecutionException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileReader;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.verification.DeclarationsReader;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.NestedModuleDeclarationData;
import com.atlassian.maven.plugins.amps.frontend.association.verification.utils.ArtifactPathUtils;

public class DeclarationsReaderImpl implements DeclarationsReader {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final MavenXpp3Reader mavenReader = new MavenXpp3Reader();

    @Override
    public Set<String> readRelativeDeclarations(Collection<TFile> associationFiles, TFile moduleRoot)
            throws MojoExecutionException {
        Set<TFile> declaredFiles = readDeclarations(associationFiles).stream()
                .map(relativePath -> new TFile(moduleRoot, relativePath))
                .collect(Collectors.toSet());

        return ArtifactPathUtils.getRelativePaths(declaredFiles, moduleRoot);
    }

    @Override
    public Map<TFile, NestedModuleDeclarationData> readNestedModuleDeclarationData(
            Collection<TFile> declarationFiles, TFile moduleRoot) throws MojoExecutionException {
        Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations = new HashMap<>();

        for (TFile declarationFile : declarationFiles) {
            Map<String, List<String>> artifactDeclarations = readDeclarationFile(declarationFile);

            addNestedModuleDeclarations(nestedModuleDeclarations, moduleRoot, artifactDeclarations);
        }

        return nestedModuleDeclarations;
    }

    private void addNestedModuleDeclarations(
            Map<TFile, NestedModuleDeclarationData> nestedModuleDeclarations,
            TFile moduleRoot,
            Map<String, List<String>> artifactDeclarations) {
        for (Map.Entry<String, List<String>> artifactDeclaration : artifactDeclarations.entrySet()) {
            String relativeArtifactPath = artifactDeclaration.getKey();
            Set<String> declaredFiles = new HashSet<>(artifactDeclaration.getValue());
            NestedModuleDeclarationData nestedModuleData = new NestedModuleDeclarationData(declaredFiles);

            TFile artifactFile = new TFile(moduleRoot, relativeArtifactPath);

            nestedModuleDeclarations.put(artifactFile, nestedModuleData);
        }
    }

    private Set<String> readDeclarations(Collection<TFile> associationFiles) throws MojoExecutionException {
        Set<String> declaredFiles = new HashSet<>();

        for (TFile file : associationFiles) {
            Map<String, FeOutputJsFileDeclaration> associations = readAssociationFile(file);
            Set<String> declarations = associations.keySet();
            Set<String> jsDeclarations = declarations.stream()
                    .filter(path -> JAVASCRIPT_FILE_REGEX.matcher(path).matches())
                    .collect(Collectors.toSet());
            declaredFiles.addAll(jsDeclarations);
        }

        return declaredFiles;
    }

    private Map<String, FeOutputJsFileDeclaration> readAssociationFile(TFile file) throws MojoExecutionException {
        try {
            Reader reader = new TFileReader(file);
            return objectMapper.readValue(reader, new TypeReference<Map<String, FeOutputJsFileDeclaration>>() {});
        } catch (IOException e) {
            throw new MojoExecutionException("Error reading:\n" + file + "\n" + e);
        }
    }

    private Map<String, List<String>> readDeclarationFile(TFile file) throws MojoExecutionException {
        try {
            Reader reader = new TFileReader(file);
            return objectMapper.readValue(reader, new TypeReference<Map<String, List<String>>>() {});
        } catch (IOException e) {
            throw new MojoExecutionException("Error reading:\n" + file + "\n" + e);
        }
    }
}

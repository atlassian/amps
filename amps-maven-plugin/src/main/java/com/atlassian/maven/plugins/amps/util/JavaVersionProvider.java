package com.atlassian.maven.plugins.amps.util;

import org.apache.commons.lang3.StringUtils;

public class JavaVersionProvider {

    private final int majorVersion;

    public JavaVersionProvider() {
        this.majorVersion = parseMajorVersion(System.getProperty("java.version"));
    }

    public JavaVersionProvider(String majorVersion) {
        this.majorVersion = parseMajorVersion(majorVersion);
    }

    private int parseMajorVersion(String version) {
        if (StringUtils.isBlank(version)) {
            throw new IllegalStateException("java.version system property is not set");
        }

        if (version.startsWith("1.")) {
            version = version.substring(2, 3);
        } else {
            int dot = version.indexOf(".");
            if (dot != -1) {
                version = version.substring(0, dot);
            }
        }
        return Integer.parseInt(version);
    }

    public int getMajor() {
        return majorVersion;
    }
}

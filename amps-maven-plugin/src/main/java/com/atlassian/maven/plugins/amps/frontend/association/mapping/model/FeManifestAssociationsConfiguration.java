package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

public class FeManifestAssociationsConfiguration {
    /**
     * At the moment we don't want to enforce verifying that all files are declared on global level, but we let
     * developers decide. ignoreSomeFilesNotDeclared: true = if not all js files are declared maven goal will print a
     * warning ignoreSomeFilesNotDeclared: false = if not all js files are declared maven goal will throw an exception
     */
    private Boolean ignoreSomeFilesNotDeclared = Boolean.TRUE;

    public Boolean getIgnoreSomeFilesNotDeclared() {
        return ignoreSomeFilesNotDeclared;
    }

    public void setIgnoreSomeFilesNotDeclared(Boolean ignoreSomeFilesNotDeclared) {
        this.ignoreSomeFilesNotDeclared = ignoreSomeFilesNotDeclared;
    }
}

package com.atlassian.maven.plugins.amps;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.function.BiFunction;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.maven.plugins.amps.upm.signing.tools.PrivateKeyAndCert;

public class CustomCertificateBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomCertificateBuilder.class);

    private String issuer;

    private String name;

    private String duration;

    private String startOffset;

    public String getStartOffset() {
        return startOffset;
    }

    public void setStartOffset(String startOffset) {
        this.startOffset = startOffset;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getName() {
        String notNull = name == null ? issuer : name;
        return notNull.replaceAll("\\s", "_");
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public PrivateKeyAndCert createPrivateKeyAndCertificate() {
        Duration d = safeParse(duration).orElse(Duration.ofDays(1));
        ZonedDateTime start = ZonedDateTime.now();
        if (startOffset != null) {
            String offset = startOffset;
            BiFunction<ZonedDateTime, Duration, ZonedDateTime> func = ZonedDateTime::plus;
            if (startOffset.startsWith("-")) {
                offset = startOffset.substring(1);
                func = ZonedDateTime::minus;
            }
            Duration offsetDuration = safeParse(offset).orElse(Duration.ZERO);
            start = func.apply(start, offsetDuration);
        }
        return PrivateKeyAndCert.generateSelfSignedX509Certificate(start, start.plus(d), issuer);
    }

    private Optional<Duration> safeParse(String duration) {
        if (duration == null || StringUtils.isBlank(duration)) {
            return Optional.empty();
        }
        try {
            return Optional.of(Duration.parse(duration));
        } catch (DateTimeParseException dateTimeParseException) {
            LOGGER.warn("Failed to parse duration for Custom Certificate {} : {}", name, duration);
            return Optional.empty();
        }
    }
}

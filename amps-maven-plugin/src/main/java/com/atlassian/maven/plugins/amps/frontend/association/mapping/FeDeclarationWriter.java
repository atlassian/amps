package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.FINAL_ASSOCIATION_MAPPING_FILE;
import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.FINAL_EXTERNAL_DEPENDENCY_DECLARATION_FILE;
import static com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper.ASSOCIATIONS_OUTPUT_DIR;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import org.apache.maven.plugin.MojoExecutionException;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclarationsJson;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

public class FeDeclarationWriter {

    private final DirectoryHelper directoryHelper;
    private final JsonParser jsonParser;

    public FeDeclarationWriter(DirectoryHelper directoryHelper, JsonParser jsonParser) {
        this.directoryHelper = directoryHelper;
        this.jsonParser = jsonParser;
    }

    public void writeManifestAssociation(Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations)
            throws MojoExecutionException {
        File outputDirectory = getOrCreateOutputDirectory();
        File outputFile = new File(outputDirectory, FINAL_ASSOCIATION_MAPPING_FILE);

        try {
            jsonParser.writeFile(outputFile, outputJsFileDeclarations);
        } catch (IOException e) {
            throw new MojoExecutionException("Couldn't write final association mapping.");
        }
    }

    public void writeDependencyDeclarations(List<FeDependencyDeclaration> dependencyDeclarations)
            throws MojoExecutionException {
        File outputDirectory = getOrCreateOutputDirectory();
        File outputFile = new File(outputDirectory, FINAL_EXTERNAL_DEPENDENCY_DECLARATION_FILE);

        FeDependencyDeclarationsJson jsonContent = new FeDependencyDeclarationsJson();
        jsonContent.setDeclarations(dependencyDeclarations.stream()
                .collect(Collectors.toMap(
                        FeDependencyDeclaration::getArtifactPath, FeDependencyDeclaration::getDeclaredJsFiles)));

        try {
            jsonParser.writeFile(outputFile, jsonContent);
        } catch (IOException e) {
            throw new MojoExecutionException("Couldn't write final external dependency declarations.");
        }
    }

    @Nonnull
    private File getOrCreateOutputDirectory() throws MojoExecutionException {
        String outputDirectoryPath = directoryHelper.getOutputFileAbsolutePath(ASSOCIATIONS_OUTPUT_DIR);
        final File outputDirectory = new File(outputDirectoryPath);
        if (outputDirectory.exists() || outputDirectory.mkdirs()) {
            return outputDirectory;
        }

        throw new MojoExecutionException(
                "Output directory for frontend manifest association mapping couldn't be created");
    }
}

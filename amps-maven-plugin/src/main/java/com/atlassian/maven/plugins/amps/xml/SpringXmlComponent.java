package com.atlassian.maven.plugins.amps.xml;

import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.toList;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Components are basically classes which need to be exposed in Spring IoC.
 *
 * @see SpringXmlModel
 */
public final class SpringXmlComponent {
    private final String id;
    private final String fullClassName;
    private final Map<String, String> serviceProperties;

    private SpringXmlComponent(
            final String id, final String fullClassName, final Map<String, String> serviceProperties) {
        this.id = id;
        this.fullClassName = fullClassName;
        this.serviceProperties = serviceProperties;
    }

    @JacksonXmlProperty
    String id() {
        return id;
    }

    @JacksonXmlProperty
    String fullClassName() {
        return fullClassName;
    }

    @JacksonXmlProperty(localName = "serviceProperty")
    @JacksonXmlElementWrapper(localName = "serviceProperties")
    List<SpringXmlServiceProperty> serviceProperties() {
        return serviceProperties.entrySet().stream()
                .map(entry -> SpringXmlServiceProperty.builder()
                        .key(entry.getKey())
                        .value(entry.getValue())
                        .build())
                .collect(toList());
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        SpringXmlComponent component = (SpringXmlComponent) other;

        if (!Objects.equals(component.id, id)) {
            return false;
        }

        if (!Objects.equals(component.fullClassName, fullClassName)) {
            return false;
        }

        return Objects.equals(component.serviceProperties, serviceProperties);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(id, fullClassName, serviceProperties);
    }

    /**
     * Creates a new builder for {@link SpringXmlComponent}.
     *
     * @return {@link Builder}.
     */
    public static Builder builder() {
        return new Builder();
    }

    /** A builder for {@link SpringXmlComponent}. */
    public static final class Builder {
        private String id = "";
        private String fullClassName = "";
        private Map<String, String> serviceProperties = new LinkedHashMap<>();

        private Builder() {}

        /**
         * Sets value of the {@link #id} property.
         *
         * @param id a value to be set, it must not be empty
         * @return {@link Builder} so chaining the methods is possible
         */
        public Builder id(final String id) {
            if (id == null) {
                throw new IllegalArgumentException("'id' must not be null");
            }

            if (StringUtils.isWhitespace(id)) {
                throw new IllegalArgumentException("'id' must not be blank or whitespace");
            }

            this.id = id;
            return this;
        }

        /**
         * Sets value of the {@link #fullClassName} property.
         *
         * @param fullClassName a value to be set, it must not be empty
         * @return {@link Builder} so chaining the methods is possible
         */
        public Builder fullClassName(final String fullClassName) {
            if (fullClassName == null) {
                throw new IllegalArgumentException("'fullClassName' must not be null");
            }

            if (StringUtils.isWhitespace(fullClassName)) {
                throw new IllegalArgumentException("'fullClassName' must not be blank or whitespace");
            }

            this.fullClassName = fullClassName;
            return this;
        }

        /**
         * Adds a new service property. When called multiple times with the same `key` the previous value is
         * overwritten.
         *
         * @param key a name of the property to be set, it must not be empty
         * @param value a value of the property
         * @return {@link Builder} so chaining the methods is possible
         */
        public Builder putServiceProperty(final String key, final String value) {
            if (key == null) {
                throw new IllegalArgumentException("'key' must not be null");
            }

            if (StringUtils.isWhitespace(key)) {
                throw new IllegalArgumentException("'key' must not be blank or whitespace");
            }

            if (value == null) {
                throw new IllegalArgumentException("'value' must not be null");
            }

            serviceProperties.put(key, value);
            return this;
        }

        /**
         * Completes the creation process and returns a new component.
         *
         * @return {@link SpringXmlComponent}
         */
        public SpringXmlComponent build() {
            if (StringUtils.isBlank(id)) {
                throw new IllegalStateException("'id' property is mandatory");
            }

            if (StringUtils.isBlank(fullClassName)) {
                throw new IllegalStateException("'fullClassName' property is mandatory");
            }

            return new SpringXmlComponent(id, fullClassName, unmodifiableMap(serviceProperties));
        }
    }
}

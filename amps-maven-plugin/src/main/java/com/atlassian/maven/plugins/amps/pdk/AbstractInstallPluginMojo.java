package com.atlassian.maven.plugins.amps.pdk;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;

import com.atlassian.maven.plugins.amps.AbstractProductAwareMojo;
import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.PdkParams;
import com.atlassian.maven.plugins.amps.product.ProductHandler;
import com.atlassian.maven.plugins.amps.upm.signing.tools.SignatureBuilder;

/** Base class for Mojos that install a plugin. */
public abstract class AbstractInstallPluginMojo extends AbstractProductAwareMojo {

    @Parameter(property = "atlassian.plugin.key")
    private String pluginKey;

    @Parameter(property = "project.groupId")
    private String groupId;

    @Parameter(property = "project.artifactId")
    private String artifactId;

    /** HTTP port for the servlet containers */
    @Parameter(property = "http.port")
    private int httpPort;

    /** Application context path */
    @Parameter(property = "context.path")
    private String contextPath;

    /** Username of user that will install the plugin */
    @Parameter(property = "username", defaultValue = "admin")
    private String username;

    /** Password of user that will install the plugin */
    @Parameter(property = "password", defaultValue = "admin")
    private String password;

    /** Application server */
    @Parameter(property = "server", defaultValue = "localhost")
    private String server;

    @Parameter(property = "signature")
    private String signatureProperty;

    @Parameter(property = "signatureFile")
    private String signatureFile;

    @Parameter(property = "privateKey")
    private String privateKey;

    @Parameter(property = "privateKeyFile")
    private String privateKeyFile;

    protected final void installPlugin(final boolean isTestPlugin) throws MojoExecutionException {
        ensurePluginKeyExists();
        final ProductHandler productHandler = getProductHandler(getProductId());
        String pluginFileName = getPluginFileName(isTestPlugin);
        String signature = getSignature();
        if (signature == null) {
            signature = forgeSignature(pluginFileName);
        }
        getMavenGoals()
                .installPlugin(new PdkParams.Builder()
                        .testPlugin(isTestPlugin)
                        .pluginFile(pluginFileName)
                        .pluginKey(pluginKey)
                        .server(server)
                        .port(getPort(server, productHandler))
                        .contextPath(getContextPath(productHandler))
                        .username(username)
                        .password(password)
                        .signature(signature)
                        .build());
    }

    private String getPluginFileName(boolean isTestPlugin) {
        MavenContext mavenContext = getMavenContext();
        StringBuilder sb = new StringBuilder(mavenContext.getBuildDirectory());
        sb.append("/").append(mavenContext.getProject().getBuild().getFinalName());
        if (isTestPlugin) {
            sb.append("-tests");
        }
        // This must match what pdkParams is doing when no pluginFile is provided
        sb.append(".jar");
        return sb.toString();
    }

    private String getSignature() throws MojoExecutionException {
        return getPropertyValue(signatureProperty, "signature", signatureFile, "signatureFile")
                .orElse(null);
    }

    private String forgeSignature(String file) throws MojoExecutionException {
        return getPropertyValue(privateKey, "privateKey", privateKeyFile, "privateKeyFile")
                .map(SignatureBuilder::withPrivateKey)
                .map(sb -> sb.forgeSignature(new File(file)))
                .orElse(null);
    }

    private void ensurePluginKeyExists() {
        if (pluginKey == null) {
            pluginKey = groupId + "." + artifactId;
        }
    }

    private int getPort(final String server, final ProductHandler handler) {
        if (httpPort != 0) {
            return httpPort;
        }
        return (server != null && server.startsWith("https://"))
                ? handler.getDefaultHttpsPort()
                : handler.getDefaultHttpPort();
    }

    private String getContextPath(final ProductHandler handler) {
        return contextPath == null ? "/" + handler.getId() : contextPath;
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.verification.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.MutablePair;

import de.schlichtherle.truezip.file.TFile;

public class ArtifactScanResults extends MutablePair<List<TFile>, Map<TFile, ModuleScanResults>> {
    public ArtifactScanResults() {
        super(new ArrayList<>(), new HashMap<>());
    }

    public List<TFile> getArchiveFiles() {
        return getLeft();
    }

    public Map<TFile, ModuleScanResults> getModules() {
        return getRight();
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.verification.model;

import java.util.ArrayList;
import java.util.List;

import de.schlichtherle.truezip.file.TFile;

public class ModuleScanResults {

    private final List<TFile> associationFiles = new ArrayList<>();
    private final List<TFile> nestedDeclarationFiles = new ArrayList<>();
    private final List<TFile> javascriptFiles = new ArrayList<>();

    public ModuleScanResults() {}

    public List<TFile> getAssociationFiles() {
        return associationFiles;
    }

    public List<TFile> getNestedDeclarationFiles() {
        return nestedDeclarationFiles;
    }

    public boolean hasNestedDeclarationFiles() {
        return !nestedDeclarationFiles.isEmpty();
    }

    public List<TFile> getJavascriptFiles() {
        return javascriptFiles;
    }
}

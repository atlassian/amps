package com.atlassian.maven.plugins.amps.product.common;

import javax.annotation.ParametersAreNonnullByDefault;

import org.dom4j.Document;

/**
 * Validates an XML document.
 *
 * @since 8.2
 */
@ParametersAreNonnullByDefault
public interface XMLDocumentValidator {

    /**
     * Validates the given XML document.
     *
     * @param document the document to be validated
     * @throws ValidationException if validation fails
     */
    void validate(Document document) throws ValidationException;
}

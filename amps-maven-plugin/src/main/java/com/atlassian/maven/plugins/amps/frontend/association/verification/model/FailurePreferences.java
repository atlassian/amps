package com.atlassian.maven.plugins.amps.frontend.association.verification.model;

public class FailurePreferences {

    private final boolean failOnUndeclaredFiles;
    private final boolean failOnExtraDeclarations;

    public FailurePreferences(boolean failOnUndeclaredFiles, boolean failOnExtraDeclarations) {
        this.failOnUndeclaredFiles = failOnUndeclaredFiles;
        this.failOnExtraDeclarations = failOnExtraDeclarations;
    }

    public boolean isFailOnUndeclaredFiles() {
        return failOnUndeclaredFiles;
    }

    public boolean isFailOnExtraDeclarations() {
        return failOnExtraDeclarations;
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper.WEB_RESOURCE_PLUGIN_CONFIGURATIONS_DIR;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

import org.apache.maven.plugin.MojoExecutionException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.ExternalFeManifestAssociationConfiguration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclarationParameter;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociation;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociationParameter;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.WebResourcePluginFeManifestAssociationConfiguration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

public class FeDeclarationReader {

    private static final String WEB_RESOURCE_PLUGIN_CONFIGURATIONS_SUFFIX = ".intermediary.json";

    private final DirectoryHelper directoryHelper;
    private final JsonParser jsonParser;

    public FeDeclarationReader(DirectoryHelper directoryHelper, JsonParser jsonParser) {
        this.directoryHelper = directoryHelper;
        this.jsonParser = jsonParser;
    }

    public List<FeManifestAssociation> getManifestAssociations(
            List<FeManifestAssociationParameter> manifestAssociationConfigurations) throws MojoExecutionException {
        List<FeManifestAssociation> fromConfiguration =
                getManifestAssociationsFromConfiguration(manifestAssociationConfigurations);
        List<FeManifestAssociation> fromWebResourcePlugin = getManifestAssociationsFromWebResourcePluginConfigurations(
                directoryHelper.getOutputFileAbsolutePath(WEB_RESOURCE_PLUGIN_CONFIGURATIONS_DIR));

        return Stream.of(fromConfiguration.stream(), fromWebResourcePlugin.stream())
                .flatMap(i -> i)
                .collect(Collectors.toList());
    }

    public List<FeDependencyDeclaration> getDependencyDeclarations(
            List<FeDependencyDeclarationParameter> dependencyDeclarationConfigurations) throws MojoExecutionException {
        List<FeDependencyDeclaration> dependencyDeclarations = new ArrayList<>();
        Set<String> externalConfigurations = new HashSet<>();

        if (dependencyDeclarationConfigurations != null) {
            for (FeDependencyDeclarationParameter dependencyDeclarationConfiguration :
                    dependencyDeclarationConfigurations) {
                if (dependencyDeclarationConfiguration.getNonAtlassianArtifactDeclaration() != null) {
                    externalConfigurations.add(dependencyDeclarationConfiguration.getNonAtlassianArtifactDeclaration());
                } else {
                    dependencyDeclarations.add(
                            createDependencyDeclarationFromConfiguration(dependencyDeclarationConfiguration));
                }
            }
        }

        for (String externalConfiguration : externalConfigurations) {
            List<FeDependencyDeclarationParameter> jsonDeclarations =
                    getDependencyDeclarationFromJsonConfiguration(externalConfiguration);
            dependencyDeclarations.addAll(jsonDeclarations.stream()
                    .map(this::createDependencyDeclarationFromConfiguration)
                    .collect(Collectors.toList()));
        }

        return dependencyDeclarations;
    }

    @VisibleForTesting
    @Nonnull
    protected List<FeManifestAssociation> getManifestAssociationsFromWebResourcePluginConfigurations(
            String webResourcePluginConfigurationsPath) throws MojoExecutionException {
        File webResourcePluginDirectory = new File(webResourcePluginConfigurationsPath);
        List<FeManifestAssociation> manifestAssociations = new ArrayList<>();

        if (webResourcePluginDirectory.exists() && webResourcePluginDirectory.isDirectory()) {
            File[] directoryFiles = webResourcePluginDirectory.listFiles();

            if (directoryFiles != null) {
                for (final File fileEntry : directoryFiles) {
                    if (!fileEntry.isDirectory()
                            && fileEntry.getName().endsWith(WEB_RESOURCE_PLUGIN_CONFIGURATIONS_SUFFIX)) {
                        manifestAssociations.add(
                                getManifestAssociationFromWebResourcePluginConfiguration(fileEntry.getAbsolutePath()));
                    }
                }
            }
        }

        return manifestAssociations;
    }

    private FeManifestAssociation getManifestAssociationFromWebResourcePluginConfiguration(
            String webResourcePluginConfigurationAbsPath) throws MojoExecutionException {
        File webResourcePluginConfigurationFile = new File(webResourcePluginConfigurationAbsPath);

        WebResourcePluginFeManifestAssociationConfiguration webResourcePluginConfiguration;
        try {
            webResourcePluginConfiguration = jsonParser.readFile(
                    webResourcePluginConfigurationFile, WebResourcePluginFeManifestAssociationConfiguration.class);
        } catch (IOException e) {
            throw new MojoExecutionException(String.format(
                    "Intermediary manifest association from web resource build plugin couldn't be parsed. Path: %s",
                    webResourcePluginConfigurationAbsPath));
        }

        return new FeManifestAssociation(
                webResourcePluginConfiguration.getPackageName(),
                webResourcePluginConfiguration.getOutputDirectoryFiles(),
                directoryHelper.getPathRelativeToSourceDirectory(webResourcePluginConfigurationFile.getAbsolutePath()));
    }

    @VisibleForTesting
    @Nonnull
    protected FeManifestAssociation getManifestAssociationsFromJsonConfiguration(String externalConfigurationPath)
            throws MojoExecutionException {
        String externalConfigurationAbsolutePath = directoryHelper.getSourceFileAbsolutePath(externalConfigurationPath);
        File externalConfigurationFile = new File(externalConfigurationAbsolutePath);

        ExternalFeManifestAssociationConfiguration externalConfiguration;
        try {
            externalConfiguration =
                    jsonParser.readFile(externalConfigurationFile, ExternalFeManifestAssociationConfiguration.class);
        } catch (IOException e) {
            throw new MojoExecutionException(String.format(
                    "External manifest association couldn't be parsed. Path: %s", externalConfigurationAbsolutePath));
        }

        return new FeManifestAssociation(
                externalConfiguration.getPackageName(),
                externalConfiguration
                        .getManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest()
                        .getOutputDirectoryFiles(),
                directoryHelper.getPathRelativeToSourceDirectory(externalConfigurationPath));
    }

    @VisibleForTesting
    @Nonnull
    protected List<FeDependencyDeclarationParameter> getDependencyDeclarationFromJsonConfiguration(
            String externalConfigurationPath) throws MojoExecutionException {
        String externalConfigurationAbsolutePath = directoryHelper.getSourceFileAbsolutePath(externalConfigurationPath);
        File externalConfigurationFile = new File(externalConfigurationAbsolutePath);

        try {
            return jsonParser.readFile(
                    externalConfigurationFile, new TypeReference<List<FeDependencyDeclarationParameter>>() {});
        } catch (IOException e) {
            throw new MojoExecutionException(String.format(
                    "External dependency declaration in json format couldn't be parsed. Path: %s",
                    externalConfigurationAbsolutePath));
        }
    }

    @Nonnull
    private List<FeManifestAssociation> getManifestAssociationsFromConfiguration(
            List<FeManifestAssociationParameter> manifestAssociationConfigurations) throws MojoExecutionException {
        List<FeManifestAssociation> manifestAssociations = new ArrayList<>();
        Set<String> externalConfigurations = new HashSet<>();

        if (manifestAssociationConfigurations != null) {
            for (FeManifestAssociationParameter associationConfiguration : manifestAssociationConfigurations) {
                if (associationConfiguration.getOutputDirectoryFilesDeclaration() != null) {
                    externalConfigurations.add(associationConfiguration.getOutputDirectoryFilesDeclaration());
                } else {
                    manifestAssociations.add(createManifestAssociationFromConfiguration(associationConfiguration));
                }
            }
        }

        for (String externalConfiguration : externalConfigurations) {
            FeManifestAssociation manifestAssociation =
                    getManifestAssociationsFromJsonConfiguration(externalConfiguration);
            manifestAssociations.add(manifestAssociation);
        }

        return manifestAssociations;
    }

    private FeManifestAssociation createManifestAssociationFromConfiguration(
            FeManifestAssociationParameter associationConfiguration) {
        return new FeManifestAssociation(
                associationConfiguration.getPackageName(),
                associationConfiguration.getFilesDeclaration().getOutputDirectoryFiles().stream()
                        .filter(path -> path != null && !path.isEmpty())
                        .map(String::trim)
                        .collect(Collectors.toList()),
                "pom.xml");
    }

    private FeDependencyDeclaration createDependencyDeclarationFromConfiguration(
            FeDependencyDeclarationParameter dependencyDeclarationConfiguration) {
        return new FeDependencyDeclaration(
                dependencyDeclarationConfiguration.getManuallyVerifiedNonAtlassianArtifactPath(),
                dependencyDeclarationConfiguration.getManuallyVerifiedEveryJsDoesntHaveExternalDependencies().stream()
                        .filter(path -> path != null && !path.isEmpty())
                        .map(String::trim)
                        .collect(Collectors.toList()));
    }
}

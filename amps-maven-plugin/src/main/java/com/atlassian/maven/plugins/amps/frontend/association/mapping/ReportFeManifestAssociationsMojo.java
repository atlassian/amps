package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static java.lang.Boolean.TRUE;
import static org.apache.maven.plugins.annotations.LifecyclePhase.PREPARE_PACKAGE;

import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.maven.plugins.amps.AbstractAmpsMojo;
import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeDependencyDeclarationParameter;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociation;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociationParameter;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociationsConfiguration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.JsonParser;

/**
 * Creates a mapping association between output js files and fe module manifest file provided in configuration or from
 * WRM plugin (webpack, vite, etc). Prepared association is output to
 * {@code META-INF/fe-manifest-associations/fe-manifest-association.json}.
 *
 * @since 8.15.0
 * @since 8.17.6 Accepts all `.intermediary.json` files
 */
@Mojo(name = "report-fe-manifest-associations", defaultPhase = PREPARE_PACKAGE)
public class ReportFeManifestAssociationsMojo extends AbstractAmpsMojo {

    /**
     * Mapping configuration providing manifest file and a list of output js files
     *
     * @since 8.15.0
     */
    @Parameter
    private List<FeManifestAssociationParameter> feManifestAssociations;

    @Parameter
    private FeManifestAssociationsConfiguration feManifestAssociationsConfiguration =
            new FeManifestAssociationsConfiguration();

    /**
     * Declarations of javascript files present in external dependencies
     *
     * @since 9.0.3
     */
    @Parameter
    private List<FeDependencyDeclarationParameter> feExternalDependenciesDeclarations;

    /**
     * When set to true the mojo will not execute
     *
     * @since 8.17.2
     */
    @Parameter(property = "skipReportFeManifestAssociations")
    private Boolean skipReportFeManifestAssociations;

    /**
     * The output directory for the frontend manifest associations file. Please define a value only in case you need to
     * create definitions in other location than Maven's project build output directory (default). targetDirectory: null
     * or "" = Maven's project build output directory will be used targetDirectory: "path" = path will be used as the
     * output directory
     *
     * @since 9.0.7
     */
    @VisibleForTesting
    @Parameter(property = "targetDirectory")
    private String feSbomTargetDirectory;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (TRUE.equals(skipReportFeManifestAssociations)) {
            getLog().info("Skipping `report-fe-manifest-associations` execution");
            return;
        }

        MavenContext mavenContext = getMavenContext();
        DirectoryHelper directoryHelper = new DirectoryHelper(mavenContext, feSbomTargetDirectory);
        JsonParser jsonParser = new JsonParser();

        FeDeclarationReader reader = new FeDeclarationReader(directoryHelper, jsonParser);
        FeManifestAssociationProcessor processor = new FeManifestAssociationProcessor(directoryHelper, jsonParser);
        FeDeclarationWriter writer = new FeDeclarationWriter(directoryHelper, jsonParser);

        processManifestAssociations(reader, processor, writer, mavenContext, directoryHelper);
        processDependencyDeclarations(reader, writer);
    }

    private void processDependencyDeclarations(FeDeclarationReader reader, FeDeclarationWriter writer)
            throws MojoExecutionException {
        List<FeDependencyDeclaration> dependencyDeclarations =
                reader.getDependencyDeclarations(feExternalDependenciesDeclarations);
        if (dependencyDeclarations.isEmpty()) {
            getLog().info("There were no frontend dependency declarations found. Skipping generating final mapping.");
            return;
        }

        writer.writeDependencyDeclarations(dependencyDeclarations);
    }

    private void processManifestAssociations(
            FeDeclarationReader reader,
            FeManifestAssociationProcessor processor,
            FeDeclarationWriter writer,
            MavenContext mavenContext,
            DirectoryHelper directoryHelper)
            throws MojoExecutionException {
        List<FeManifestAssociation> manifestAssociations = reader.getManifestAssociations(feManifestAssociations);
        if (manifestAssociations.isEmpty()) {
            getLog().info("There were no frontend manifest associations found. Skipping generating final mapping.");
            return;
        }

        Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations =
                processor.transformManifestAssociationsToFileDeclarations(manifestAssociations);

        String packaging = mavenContext.getProject().getPackaging();

        if (packaging.equals("war")) {
            getLog().warn(
                            "Cannot verify declared FE associations because the project specifies 'war' packaging. This is a limitation of the report-fe-manifest-associations mojo. To ensure correctness, verify fe-manifest-association.json exists in the output and declares all FE files.");
        } else {
            FeManifestAssociationArbiter arbiter =
                    new FeManifestAssociationArbiter(directoryHelper, feManifestAssociationsConfiguration, getLog());
            arbiter.verifyOutput(outputJsFileDeclarations);
        }

        writer.writeManifestAssociation(outputJsFileDeclarations);
    }
}

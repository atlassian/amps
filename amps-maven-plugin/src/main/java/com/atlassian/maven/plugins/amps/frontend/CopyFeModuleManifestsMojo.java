package com.atlassian.maven.plugins.amps.frontend;

import static com.atlassian.maven.plugins.amps.util.FileUtils.file;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.io.FileUtils.copyFile;
import static org.apache.maven.plugins.annotations.LifecyclePhase.PROCESS_RESOURCES;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.maven.plugins.amps.AbstractAmpsMojo;
import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.util.FileUtils;

/**
 * Copies frontend module manifest files into {@code META-INF/fe-module-manifests}.
 *
 * @since 8.13.0
 */
@Mojo(name = "copy-fe-module-manifests", defaultPhase = PROCESS_RESOURCES)
public class CopyFeModuleManifestsMojo extends AbstractAmpsMojo {

    public static final String MODULE_MANIFESTS_OUTPUT_DIR = "META-INF/fe-module-manifests";

    public static final List<String> SUPPORTED_FILE_NAMES = asList("package.json", "package-lock.json", "yarn.lock");

    /**
     * Paths provided in this parameter should be relative to project basedir directory or absolute
     *
     * @since 8.13.0 Paths should be relative and point to files within project basedir directory
     * @since 8.13.1 Paths can be absolute or relative, paths can point to files outside project basedir directory
     */
    @Parameter
    private List<String> includedFeModuleManifests;

    /**
     * When set to true the mojo will not execute
     *
     * @since 8.17.2
     */
    @Parameter(property = "skipCopyFeModuleManifests")
    private Boolean skipCopyFeModuleManifests;

    /**
     * The output directory for the frontend manifest files. Please define a value only in case you need to create
     * definitions in other location than Maven's project build output directory (default). targetDirectory: null or ""
     * = Maven's project build output directory will be used targetDirectory: "path" = path will be used as the output
     * directory
     *
     * @since 9.0.7
     */
    @VisibleForTesting
    @Parameter(property = "targetDirectory")
    private String feSbomTargetDirectory;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        if (TRUE.equals(skipCopyFeModuleManifests)) {
            getLog().info("Skipping `copy-fe-module-manifests` execution");
        } else if (includedFeModuleManifests == null || includedFeModuleManifests.isEmpty()) {
            getLog().info("There were no frontend module manifest files specified to include in the artifact");
        } else {
            getLog().info("The following frontend module manifest files are to be included in the artifact: "
                    + includedFeModuleManifests);

            Set<String> absoluteFilePaths = resolveToAbsolutePaths(includedFeModuleManifests);
            validateDeclaredFiles(absoluteFilePaths, getLog());

            File commonDirectory = FileUtils.findBaseDirectory(absoluteFilePaths);
            File outputDirectory = createOutputSubdirectory(getOutputDirectory());

            for (String absoluteFilePath : absoluteFilePaths) {
                File sourceFile = getSourceFile(absoluteFilePath);

                String commonDirRelativeFilePath = getCommonDirRelativePath(commonDirectory, absoluteFilePath);
                File outputFile = getOutputFile(commonDirRelativeFilePath, outputDirectory);

                copyWithOverwrite(sourceFile, outputFile, getLog());
            }
        }
    }

    @VisibleForTesting
    String getOutputDirectory() {

        if (!StringUtils.isEmpty(feSbomTargetDirectory)) {
            return feSbomTargetDirectory;
        }

        return getMavenContext().getProject().getBuild().getOutputDirectory();
    }

    @VisibleForTesting
    String getCommonDirRelativePath(File commonDirectory, String absoluteFilePath) throws MojoExecutionException {
        String commonDirectoryPath = commonDirectory.getAbsolutePath();

        if (!absoluteFilePath.equals(File.separator) && !commonDirectoryPath.endsWith(File.separator)) {
            commonDirectoryPath += File.separator;
        }

        String commonDirectoryPathLiteral = Pattern.quote(commonDirectoryPath);
        Pattern pathStartsWithCommonDirPattern = Pattern.compile(String.format("^%s.*$", commonDirectoryPathLiteral));
        boolean pathStartsWithCommonDir =
                pathStartsWithCommonDirPattern.matcher(absoluteFilePath).matches();

        if (!pathStartsWithCommonDir) {
            throw new MojoExecutionException(
                    "File '" + absoluteFilePath + "' is outside base directory '" + commonDirectoryPath);
        }

        return absoluteFilePath.replaceFirst(commonDirectoryPathLiteral, "");
    }

    private Set<String> resolveToAbsolutePaths(List<String> paths) {
        return paths.stream()
                .map(filepath -> {
                    Path path = Paths.get(filepath);
                    if (path.isAbsolute()) return path.normalize().toString();
                    return Paths.get(getMavenContext().getProject().getBasedir().getAbsolutePath())
                            .resolve(filepath)
                            .normalize()
                            .toString();
                })
                .collect(toSet());
    }

    @Nonnull
    @VisibleForTesting
    File createOutputSubdirectory(final String outputDirectory) throws MojoExecutionException {
        final File feModuleManifestsDir = file(outputDirectory, MODULE_MANIFESTS_OUTPUT_DIR);
        if (feModuleManifestsDir.exists() || feModuleManifestsDir.mkdirs()) {
            return feModuleManifestsDir;
        }

        throw new MojoExecutionException("Output directory for frontend module manifest files couldn't be created");
    }

    private File getSourceFile(String absoluteFilePath) {
        return new File(absoluteFilePath);
    }

    private File getOutputFile(String relativeFilePath, File outputBaseDirectory) {
        return new File(outputBaseDirectory, relativeFilePath);
    }

    @VisibleForTesting
    void copyWithOverwrite(final File sourceFile, final File outputFile, final Log log) throws MojoExecutionException {
        final String outputPath = outputFile.getAbsolutePath();
        if (outputFile.exists()) {
            log.warn(format("File <%s> already exists and is going to be overwritten.", outputPath));
        }

        try {
            copyFile(sourceFile, outputFile);
        } catch (IOException exception) {
            throw new MojoExecutionException(
                    format("Copying from <%s> to <%s> failed", sourceFile.getAbsolutePath(), outputPath), exception);
        }
    }

    /**
     * Does its best to give all the validation errors at once to make it easier on engineers
     *
     * @throws MojoExecutionException if validation fails
     */
    @VisibleForTesting
    void validateDeclaredFiles(Set<String> filepaths, Log logger) throws MojoExecutionException {
        final List<String> validationErrors = new ArrayList<>();

        for (String filepath : filepaths) {
            final File file = new File(filepath);

            validateFileExists(filepath, file).ifPresent(validationErrors::add);
            validateFilenameIsSupported(filepath, file).ifPresent(validationErrors::add);
        }

        if (!validationErrors.isEmpty()) {
            validationErrors.forEach(logger::error);
            throw new MojoExecutionException(
                    "Validation of includedFeModuleManifests failed, check the logs above " + "for more details");
        }
    }

    @Nonnull
    private static Optional<String> validateFileExists(String filepath, File file) {
        if (file.exists()) {
            return empty();
        }

        return Optional.of("File doesn't exist: " + filepath);
    }

    @Nonnull
    private static Optional<String> validateFilenameIsSupported(String filepath, File file) {
        if (SUPPORTED_FILE_NAMES.contains(file.getName())) {
            return empty();
        }

        return Optional.of(format("Only %s files are supported, not %s", SUPPORTED_FILE_NAMES, filepath));
    }

    @VisibleForTesting
    protected void setMavenContext(MavenContext mavenContext) {
        super.setMavenContext(mavenContext);
    }
}

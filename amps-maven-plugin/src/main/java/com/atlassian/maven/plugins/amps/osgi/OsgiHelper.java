package com.atlassian.maven.plugins.amps.osgi;

import java.util.Map;

import org.apache.maven.project.MavenProject;

/** */
public class OsgiHelper {
    public static boolean isAtlassianPlugin(MavenProject project) {
        if (project.getOrganization() != null && project.getOrganization().getName() != null) {
            return project.getOrganization().getName().contains("Atlassian");
        }
        return false;
    }

    /**
     * .obr artifact can be only generated if the instructions are provided
     *
     * @param instructions
     * @return boolean
     */
    public static boolean canCreateObrArtifact(Map instructions) {
        return instructions != null && !instructions.isEmpty();
    }
}

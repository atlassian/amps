package com.atlassian.maven.plugins.amps.product;

import java.util.List;
import javax.annotation.Nullable;

import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;

/**
 * Determines which plugins to provide to the application. Should take into account default plugins, plugin version
 * overrides, and plugins specified in the configuration.
 */
public interface PluginProvider {

    /**
     * @param product configuration of the product
     * @param selfInstalled project's artifact installed as a plugin
     * @return list of artifacts to provide to the application
     */
    List<ProductArtifact> provide(Product product, @Nullable ProductArtifact selfInstalled);
}

package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.List;

public class ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest {
    private List<String> outputDirectoryFiles;

    public List<String> getOutputDirectoryFiles() {
        return outputDirectoryFiles;
    }

    public void setOutputDirectoryFiles(List<String> outputDirectoryFiles) {
        this.outputDirectoryFiles = outputDirectoryFiles;
    }
}

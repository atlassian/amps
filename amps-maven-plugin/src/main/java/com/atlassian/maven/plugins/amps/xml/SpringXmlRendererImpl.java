package com.atlassian.maven.plugins.amps.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

final class SpringXmlRendererImpl implements SpringXmlRenderer {
    private final TransformerFactory transformerFactory;
    private final XmlMapper xmlMapper;

    SpringXmlRendererImpl(final TransformerFactory transformerFactory, final XmlMapper xmlMapper) {
        if (transformerFactory == null) {
            throw new IllegalArgumentException("'transformerFactory' must not be null");
        }

        if (xmlMapper == null) {
            throw new IllegalArgumentException("'xmlMapper' must not be null");
        }

        this.transformerFactory = transformerFactory;
        this.xmlMapper = xmlMapper;
    }

    @Override
    public void render(final SpringXmlModel model, final Writer writer) throws SpringXmlException {
        if (model == null) {
            throw new IllegalArgumentException("'model' must not be null");
        }

        if (writer == null) {
            throw new IllegalArgumentException("'writer' must not be null");
        }

        Transformer transformer = createTransformer();
        Source modelSource = serializeModel(model);
        writeTransformation(transformer, modelSource, writer);
    }

    private Transformer createTransformer() throws SpringXmlException {
        Source stylesheetSource = new StreamSource(getClass().getResourceAsStream("stylesheet.xsl"));

        try {
            return transformerFactory.newTransformer(stylesheetSource);
        } catch (TransformerConfigurationException e) {
            throw new SpringXmlException(
                    "Unable to process a template of the Spring XML document. Check the stylesheet", e);
        }
    }

    private Source serializeModel(final SpringXmlModel model) throws SpringXmlException {
        String modelXmlString;
        try {
            modelXmlString = xmlMapper.writeValueAsString(model);
        } catch (JsonProcessingException e) {
            throw new SpringXmlException("Spring XML model serialization has failed", e);
        }

        return new StreamSource(new StringReader(modelXmlString));
    }

    private void writeTransformation(final Transformer transformer, final Source modelSource, final Writer writer)
            throws SpringXmlException {
        // NOTE: XML declaration is added manually here due to https://bugs.openjdk.org/browse/JDK-7166588
        try {
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        } catch (IOException e) {
            throw new SpringXmlException("Writing XML declaration has failed", e);
        }

        try {
            transformer.transform(modelSource, new StreamResult(writer));
        } catch (TransformerException e) {
            throw new SpringXmlException("Application of the stylesheet to the model has failed", e);
        }
    }
}

package com.atlassian.maven.plugins.amps;

import java.util.Collection;

interface LegacyBannedDependenciesProvider {
    Collection<String> getDependencies();
}

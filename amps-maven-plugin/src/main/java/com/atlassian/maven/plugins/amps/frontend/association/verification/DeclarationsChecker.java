package com.atlassian.maven.plugins.amps.frontend.association.verification;

import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.model.FailurePreferences;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.NestedModuleDeclarationData;

public interface DeclarationsChecker {
    void setFailurePreferences(FailurePreferences failurePreferences);

    void verifyModule(TFile moduleRoot, Set<String> declarations, Set<String> files);

    void verifyExternalModule(TFile moduleRoot, NestedModuleDeclarationData nestedModuleData, Set<String> files);

    void finalizeChecks() throws MojoExecutionException;
}

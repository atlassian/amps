package com.atlassian.maven.plugins.amps.product;

import aQute.bnd.version.MavenVersion;

public final class JiraH2VersionChecker {

    private static final int FIRST_MAJOR_VERSION_WITHOUT_H2 = 10;

    /**
     * Since Jira 10, H2 is removed from distribution - it must be bundled for every version equal or greater than 10
     *
     * @param version of Jira that is being bundled
     * @return true if provided Jira version is equal or greater than 10, false otherwise
     */
    public static boolean shouldBundleH2(String version) {
        MavenVersion mavenVersion = MavenVersion.parseMavenString(version);
        int majorVersion = mavenVersion.getOSGiVersion().getMajor();
        return majorVersion >= FIRST_MAJOR_VERSION_WITHOUT_H2;
    }
}

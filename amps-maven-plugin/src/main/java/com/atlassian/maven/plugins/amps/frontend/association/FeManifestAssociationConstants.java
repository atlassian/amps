package com.atlassian.maven.plugins.amps.frontend.association;

import java.io.File;
import java.util.regex.Pattern;

public class FeManifestAssociationConstants {
    public static final String FINAL_ASSOCIATION_MAPPING_FILE = "fe-manifest-association.json";
    public static final String FINAL_EXTERNAL_DEPENDENCY_DECLARATION_FILE =
            "fe-external-dependencies-with-js-files.json";

    public static final Pattern JAVASCRIPT_FILE_REGEX = Pattern.compile(".*\\.[cem]?js$");

    public static final String METAINF_LIB_DIRECTORY_NAME = "META-INF" + File.separator + "lib";
}

package com.atlassian.maven.plugins.amps;

import java.util.Collection;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.twdata.maven.mojoexecutor.MojoExecutor.Element;

/**
 * Utility Class containing dependencies managed by platform
 *
 * @since 8.1
 */
interface BannedDependencies {
    /**
     * Returns the {@code <exclude>} elements to add to the configuration of the Enforcer plugin's "banned dependencies"
     * rule, for enforcing that no platform dependencies are in {@code compile} scope.
     *
     * @param allowedDependencies dependencies to be allowed, in the form {@code groupId:artifactId[:version][:type]}
     * @param platformVersion a version range for a platform. It is used to check dependencies which should not be used
     *     targetting a given platform
     * @param forceInternal the implementation is going to try detecting if the given plugin is an internal one or not
     *     based on groupId of the maven project; setting this flag causes the check to be skipped and force the plugin
     *     to be considered as an internal one
     * @param useLegacyDependencyList dependencies are not going to be discovered but a fallback list of disallowed
     *     dependencies is used instead
     * @return set of excluded elements without banning excludes
     * @throws MojoExecutionException
     */
    Set<Element> getBannedElements(
            final Collection<String> allowedDependencies,
            final String platformVersion,
            final boolean forceInternal,
            final boolean useLegacyDependencyList)
            throws PlatformDependencyProvisioningException;
}

package com.atlassian.maven.plugins.amps.frontend.association.verification.impl;

import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.FINAL_ASSOCIATION_MAPPING_FILE;
import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.FINAL_EXTERNAL_DEPENDENCY_DECLARATION_FILE;
import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.JAVASCRIPT_FILE_REGEX;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.MojoExecutionException;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.ArtifactScanner;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ArtifactScanResults;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ModuleScanResults;

public class ArtifactScannerImpl implements ArtifactScanner {
    public static final Set<String> SUPPORTED_ARCHIVE_EXTENSIONS =
            Stream.of("jar", "war", "obr", "tar.gz", "zip").collect(Collectors.toCollection(HashSet::new));

    private static final HashSet<String> MODULE_METADATA_DIRS =
            Stream.of("META-INF", "WEB-INF").collect(Collectors.toCollection(HashSet::new));

    public ArtifactScanResults scan(TFile root) throws MojoExecutionException {
        if (!root.exists()) {
            throw new MojoExecutionException(String.format("Provided artifact doesn't exist: %s", root.getPath()));
        }

        ArtifactScanResults results = new ArtifactScanResults();
        explore(root, results);

        return results;
    }

    private void explore(TFile root, ArtifactScanResults results) {
        TFile[] entries = root.listFiles();
        if (entries == null) return;

        boolean isJavaModule = checkIfModuleMetaExists(entries);
        if (isJavaModule) {
            exploreModule(root, root, results);
        } else {
            exploreRoot(root, results);
        }
    }

    private void exploreModule(TFile root, TFile moduleRoot, ArtifactScanResults results) {
        TFile[] entries = root.listFiles();
        if (entries == null) return;

        for (TFile entry : entries) {
            if (entry.isDirectory()) {
                if (isArchiveFile(entry)) {
                    results.getArchiveFiles().add(entry);
                } else {
                    exploreModule(entry, moduleRoot, results);
                }
            } else {
                if (isJsFile(entry)) {
                    ModuleScanResults moduleScanResults = getModuleScanResults(results, moduleRoot);
                    moduleScanResults.getJavascriptFiles().add(entry);
                } else if (isAssociationFile(entry)) {
                    ModuleScanResults moduleScanResults = getModuleScanResults(results, moduleRoot);
                    moduleScanResults.getAssociationFiles().add(entry);
                } else if (isExternalDependencyDeclarationFile(entry)) {
                    ModuleScanResults moduleScanResults = getModuleScanResults(results, moduleRoot);
                    moduleScanResults.getNestedDeclarationFiles().add(entry);
                }
            }
        }
    }

    private void exploreRoot(TFile root, ArtifactScanResults results) {
        TFile[] entries = root.listFiles();
        if (entries == null) return;

        for (TFile entry : entries) {
            if (entry.isDirectory()) {
                if (isArchiveFile(entry)) {
                    results.getArchiveFiles().add(entry);
                } else {
                    explore(entry, results);
                }
            } else {
                if (isJsFile(entry)) {
                    ModuleScanResults moduleScanResults = getModuleScanResults(results, root);
                    moduleScanResults.getJavascriptFiles().add(entry);
                }
            }
        }
    }

    private ModuleScanResults getModuleScanResults(ArtifactScanResults results, TFile moduleRoot) {
        Map<TFile, ModuleScanResults> modules = results.getModules();

        if (modules.containsKey(moduleRoot)) {
            return results.getModules().get(moduleRoot);
        }
        ModuleScanResults moduleScanResults = new ModuleScanResults();
        modules.put(moduleRoot, moduleScanResults);
        return moduleScanResults;
    }

    private boolean checkIfModuleMetaExists(TFile[] entries) {
        return Arrays.stream(entries)
                .anyMatch(entry -> entry.isDirectory() && MODULE_METADATA_DIRS.contains(entry.getName()));
    }

    private boolean isArchiveFile(TFile file) {
        return file.isArchive() && hasOneOfExtensions(file, SUPPORTED_ARCHIVE_EXTENSIONS);
    }

    private boolean isAssociationFile(TFile file) {
        return file.getName().equals(FINAL_ASSOCIATION_MAPPING_FILE);
    }

    private boolean isExternalDependencyDeclarationFile(TFile file) {
        return file.getName().equals(FINAL_EXTERNAL_DEPENDENCY_DECLARATION_FILE);
    }

    private boolean isJsFile(TFile file) {
        return JAVASCRIPT_FILE_REGEX.matcher(file.getName()).matches();
    }

    private boolean hasOneOfExtensions(TFile file, Set<String> extensions) {
        return extensions.contains(FilenameUtils.getExtension(file.getName()));
    }
}

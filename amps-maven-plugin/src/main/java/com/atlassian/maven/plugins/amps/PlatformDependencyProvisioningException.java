package com.atlassian.maven.plugins.amps;

class PlatformDependencyProvisioningException extends Exception {

    public PlatformDependencyProvisioningException(String message) {
        super(message);
    }

    public PlatformDependencyProvisioningException(String message, Throwable cause) {
        super(message, cause);
    }
}

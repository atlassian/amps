package com.atlassian.maven.plugins.amps.frontend.association.verification.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import de.schlichtherle.truezip.file.TFile;

public class ArtifactPathUtils {
    public static Set<String> getRelativePaths(Collection<TFile> files, TFile moduleRoot) {
        return files.stream().map(file -> getRelativePath(file, moduleRoot)).collect(Collectors.toSet());
    }

    public static String getRelativePath(TFile file, TFile moduleRoot) {
        String separatorNormalizedPath = normalizePathSeparators(file.getPath());
        Path normalizedPath = Paths.get(separatorNormalizedPath).normalize();
        return normalizePathSeparators(
                Paths.get(moduleRoot.getPath()).relativize(normalizedPath).toString());
    }

    public static Set<String> normalizePathSeparators(Set<String> paths) {
        return paths.stream().map(ArtifactPathUtils::normalizePathSeparators).collect(Collectors.toSet());
    }

    public static String normalizePathSeparators(String path) {
        // It's better to go to linux separators (supported on all existing platforms) than use
        // FilenameUtils.separatorsToSystem for consistency and ease of unit tests creation
        return path.replaceAll("\\\\", "/");
    }
}

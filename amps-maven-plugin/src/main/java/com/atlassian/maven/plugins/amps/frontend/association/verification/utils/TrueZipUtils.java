package com.atlassian.maven.plugins.amps.frontend.association.verification.utils;

import org.apache.maven.plugin.MojoExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.schlichtherle.truezip.file.TArchiveDetector;
import de.schlichtherle.truezip.file.TConfig;
import de.schlichtherle.truezip.file.TVFS;
import de.schlichtherle.truezip.fs.FsSyncException;
import de.schlichtherle.truezip.fs.FsSyncWarningException;
import de.schlichtherle.truezip.fs.archive.zip.JarDriver;
import de.schlichtherle.truezip.socket.sl.IOPoolLocator;

public class TrueZipUtils {

    public static void configureTrueZip() {
        TConfig.get()
                .setArchiveDetector(new TArchiveDetector(
                        TArchiveDetector.ALL, new Object[][] {{"obr", new JarDriver(IOPoolLocator.SINGLETON)}}));
    }

    public static void unmountTrueZip() throws MojoExecutionException {
        try {
            TVFS.umount();
        } catch (FsSyncWarningException e) {
            // Shows up when running in Java11 environments - ie. testing AMPS) but it looks like we can ignore it, from
            // the docs:
            // "An exception of this class implies that no or only insignificant parts of the data of the federated file
            // system has been lost"
            // And from FsSyncException:
            // "Unless this is an instance of the sub-class FsSyncWarningException, an exception of this class implies
            // that some or all of the data of the federated file system has been lost."
            LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).debug("TVFS.umount threw a warning:", e);
        } catch (FsSyncException e) {
            throw new MojoExecutionException(e);
        }
    }
}

package com.atlassian.maven.plugins.amps.frontend.association.mapping.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.maven.plugins.amps.MavenContext;

public class DirectoryHelper {

    public static final String ASSOCIATIONS_OUTPUT_DIR = "META-INF/fe-manifest-associations";
    public static final String WEB_RESOURCE_PLUGIN_CONFIGURATIONS_DIR = ASSOCIATIONS_OUTPUT_DIR;

    private final MavenContext mavenContext;
    private final String targetDirectoryOverride;

    /**
     * @param targetDirectoryOverride If Target directory is defined then it'll be used as the fe-associations metadata
     *     destination. If null or empty, then Maven's project build output folder will be used.
     */
    public DirectoryHelper(MavenContext mavenContext, String targetDirectoryOverride) {
        this.mavenContext = mavenContext;
        this.targetDirectoryOverride = targetDirectoryOverride;
    }

    public String getOutputFileAbsolutePath(String filepath) {
        return Paths.get(getOutputDirectoryPath(), filepath).toAbsolutePath().toString();
    }

    public String getOutputDirectoryPath() {
        if (!StringUtils.isEmpty(targetDirectoryOverride)) {
            return targetDirectoryOverride;
        }

        return mavenContext.getProject().getBuild().getOutputDirectory();
    }

    public String getSourceFileAbsolutePath(String filepath) {
        Path path = Paths.get(filepath);
        Path outputPath =
                path.isAbsolute() ? path : Paths.get(getSourceDirectoryPath()).resolve(filepath);

        return outputPath.normalize().toString();
    }

    public String getSourceDirectoryPath() {
        return mavenContext.getProject().getBasedir().getAbsolutePath();
    }

    public String getPathRelativeToSourceDirectory(String filepath) {
        Path absolutePath = Paths.get(filepath);
        Path sourceDirectoryPath = Paths.get(getSourceDirectoryPath());

        if (!absolutePath.startsWith(sourceDirectoryPath)) {
            throw new RuntimeException(
                    "getPathRelativeToSourceDirectory: input path should be under the source directory "
                            + sourceDirectoryPath + " but got " + filepath);
        }

        Path relativePath = sourceDirectoryPath.relativize(absolutePath);
        return relativePath.normalize().toString();
    }
}

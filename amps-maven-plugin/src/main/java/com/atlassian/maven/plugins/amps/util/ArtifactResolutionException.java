package com.atlassian.maven.plugins.amps.util;

import org.apache.maven.artifact.resolver.ArtifactResolutionResult;

/** Indicates that a specific artifact could not be resolved. */
public class ArtifactResolutionException extends RuntimeException {

    private static String getMessage(final ArtifactResolutionResult resolutionResult) {
        return "exceptions = " + resolutionResult.getExceptions() + "; missing artifacts = "
                + resolutionResult.getMissingArtifacts();
    }

    public ArtifactResolutionException(final ArtifactResolutionResult resolutionResult) {
        super(getMessage(resolutionResult));
    }
}

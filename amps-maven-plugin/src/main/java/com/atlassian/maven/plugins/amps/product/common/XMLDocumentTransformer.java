package com.atlassian.maven.plugins.amps.product.common;

import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.maven.plugin.MojoExecutionException;
import org.dom4j.Document;

/**
 * Makes changes to an XML document.
 *
 * @since 8.2
 */
@ParametersAreNonnullByDefault
public interface XMLDocumentTransformer {

    /**
     * Transforms the given XML document.
     *
     * @param document the document to transform
     * @return {@code true} if this transformer made any changes
     * @throws MojoExecutionException if transformation fails
     */
    boolean transform(Document document) throws MojoExecutionException;
}

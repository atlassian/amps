package com.atlassian.maven.plugins.amps.xml;

public final class SpringXmlException extends Exception {
    SpringXmlException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

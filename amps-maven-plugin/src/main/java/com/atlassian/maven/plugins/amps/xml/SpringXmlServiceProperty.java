package com.atlassian.maven.plugins.amps.xml;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

final class SpringXmlServiceProperty {
    private final String key;
    private final String value;

    private SpringXmlServiceProperty(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    @JacksonXmlProperty
    String key() {
        return key;
    }

    @JacksonXmlProperty
    String value() {
        return value;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        SpringXmlServiceProperty serviceProperty = (SpringXmlServiceProperty) other;

        if (!Objects.equals(serviceProperty.key, key)) {
            return false;
        }

        return Objects.equals(serviceProperty.value, value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    static Builder builder() {
        return new Builder();
    }

    static final class Builder {
        private String key = "";
        private String value = "";

        private Builder() {}

        Builder key(final String key) {
            if (key == null) {
                throw new IllegalArgumentException("'key' must not be null");
            }

            if (StringUtils.isWhitespace(key)) {
                throw new IllegalArgumentException("'key' must not be blank or whitespace");
            }

            this.key = key;

            return this;
        }

        Builder value(final String value) {
            if (value == null) {
                throw new IllegalArgumentException("'value' must not be null");
            }

            this.value = value;
            return this;
        }

        SpringXmlServiceProperty build() {
            if (StringUtils.isBlank(key)) {
                throw new IllegalStateException("'key' property is mandatory");
            }

            return new SpringXmlServiceProperty(key, value);
        }
    }
}

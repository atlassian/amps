package com.atlassian.maven.plugins.amps.product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;

public abstract class AbstractPluginProvider implements PluginProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPluginProvider.class);

    public final List<ProductArtifact> provide(final Product product, final ProductArtifact selfInstalled) {
        final NonOverridingArtifacts artifacts =
                new NonOverridingArtifacts(selfInstalled, product.getPluginArtifacts());

        if (product.getSalVersion() != null) {
            artifacts.addAll(getSalArtifacts(product.getSalVersion()));
        }

        if (product.getPdkVersion() != null) {
            artifacts.addAll(getPdkInstallArtifacts(product.getPdkVersion()));
        }

        if (product.getRestVersion() != null) {
            artifacts.addAll(getRestArtifacts(product.getRestVersion()));
        }

        if (product.isEnableWebConsole() && product.getWebConsoleVersion() != null) {
            artifacts.addAll(getWebConsoleArtifacts(product.getWebConsoleVersion()));
        }

        if (product.isEnableAchoo() && product.getAchooVersion() != null) {
            artifacts.addAll(getAchooArtifacts(product.getAchooVersion()));
        }

        if (product.isEnableDevToolbox() && product.getDevToolboxVersion() != null) {
            artifacts.addAll(getDevToolboxArtifacts(product.getDevToolboxVersion()));
        }

        if (product.isEnablePde() && product.getPdeVersion() != null) {
            artifacts.addAll(getPdeArtifacts(product.getPdeVersion()));
        }

        if (product.isEnableQuickReload() && product.getQuickReloadVersion() != null) {
            artifacts.addAll(getQuickReloadArtifacts(product.getQuickReloadVersion()));
        }

        if (product.isEnablePluginViewer() && product.getPluginViewerVersion() != null) {
            artifacts.addAll(getPluginViewerArtifacts(product.getPluginViewerVersion()));
        }

        return artifacts.getProvided();
    }

    protected abstract Collection<ProductArtifact> getSalArtifacts(String salVersion);

    protected Collection<ProductArtifact> getPdkInstallArtifacts(String pdkInstallVersion) {
        return Collections.singletonList(
                new ProductArtifact("com.atlassian.pdkinstall", "pdkinstall-plugin", pdkInstallVersion));
    }

    protected Collection<ProductArtifact> getWebConsoleArtifacts(String webConsoleVersion) {
        return Arrays.asList(
                new ProductArtifact("org.apache.felix", "org.apache.felix.webconsole", webConsoleVersion),
                new ProductArtifact("org.apache.felix", "org.osgi.compendium", "1.2.0"),
                new ProductArtifact("com.atlassian.labs.httpservice", "httpservice-bridge", "0.6.2"));
    }

    protected Collection<ProductArtifact> getAchooArtifacts(String version) {
        return Collections.singletonList(
                new ProductArtifact("com.atlassian.support", "achoo-database-console-plugin", version));
    }

    protected Collection<ProductArtifact> getQuickReloadArtifacts(String version) {
        return Collections.singletonList(new ProductArtifact("com.atlassian.labs.plugins", "quickreload", version));
    }

    protected Collection<ProductArtifact> getPluginViewerArtifacts(String version) {
        return Collections.singletonList(new ProductArtifact("com.atlassian", "plugins-viewer-plugin", version));
    }

    protected Collection<ProductArtifact> getDevToolboxArtifacts(String devToolboxVersion) {
        final List<ProductArtifact> artifacts = new ArrayList<>();
        artifacts.add(new ProductArtifact("com.atlassian.devrel", "developer-toolbox-plugin", devToolboxVersion));
        if (!devToolboxVersion.isEmpty() && devToolboxVersion.charAt(0) < '4') {
            artifacts.add(new ProductArtifact(
                    "com.atlassian.labs", "rest-api-browser", AmpsDefaults.DEFAULT_REST_API_BROWSER_VERSION));
        } else {
            artifacts.add(new ProductArtifact(
                    "com.atlassian.labs", "rest-api-browser", AmpsDefaults.JAKARTA_REST_API_BROWSER_VERSION));
        }
        return artifacts;
    }

    protected Collection<ProductArtifact> getPdeArtifacts(String pdeVersion) {
        return Collections.singletonList(
                new ProductArtifact("com.atlassian.plugins", "plugin-data-editor", pdeVersion));
    }

    protected Collection<ProductArtifact> getRestArtifacts(String restVersion) {
        return Collections.singletonList(
                new ProductArtifact("com.atlassian.plugins.rest", "atlassian-rest-module", restVersion));
    }

    private static class NonOverridingArtifacts {

        private final List<ProductArtifact> defined;
        private final List<ProductArtifact> provided;

        /**
         * Ensures that the list of provided plugins doesn't override initial list and self installed plugin
         *
         * @param selfInstalled project's artifact installed as a plugin
         * @param defined initial list of plugins that cannot be overriden
         */
        private NonOverridingArtifacts(
                @Nullable final ProductArtifact selfInstalled, final List<ProductArtifact> defined) {
            this.defined = new ArrayList<>(defined);
            if (selfInstalled != null) {
                this.defined.add(selfInstalled);
            }

            this.provided = new ArrayList<>(defined);
        }

        private void addIfNotOverriding(final ProductArtifact toAdd) {
            Optional<ProductArtifact> potentialOverride = defined.stream()
                    .filter(definedArtifact -> Objects.equals(definedArtifact.getArtifactId(), toAdd.getArtifactId()))
                    .findFirst();
            if (potentialOverride.isPresent()) {
                ProductArtifact overridenArtifact = potentialOverride.get();
                LOGGER.warn("Defined artifact '{}' would be overriden by '{}'. Ignored.", overridenArtifact, toAdd);
            } else {
                provided.add(toAdd);
            }
        }

        private void addAll(final Collection<ProductArtifact> productArtifacts) {
            for (ProductArtifact productArtifact : productArtifacts) {
                addIfNotOverriding(productArtifact);
            }
        }

        private List<ProductArtifact> getProvided() {
            return provided;
        }
    }
}

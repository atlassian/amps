package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.List;

public class FeManifestAssociation {

    private String packageName;
    private List<String> files;
    private String declarationOrigin;

    public FeManifestAssociation(String packageName, List<String> files, String declarationOrigin) {
        this.packageName = packageName;
        this.files = files;
        this.declarationOrigin = declarationOrigin;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getDeclarationOrigin() {
        return declarationOrigin;
    }

    public void setDeclarationOrigin(String declarationOrigin) {
        this.declarationOrigin = declarationOrigin;
    }
}

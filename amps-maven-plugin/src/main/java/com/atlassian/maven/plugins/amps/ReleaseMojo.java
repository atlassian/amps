package com.atlassian.maven.plugins.amps;

import static com.atlassian.maven.plugins.amps.analytics.event.impl.AnalyticsEventFactory.release;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/** Performs a Maven release of the current project. */
@Mojo(name = "release")
public class ReleaseMojo extends AbstractProductHandlerMojo {

    @Parameter(property = "maven.args", defaultValue = "")
    private String mavenArgs;

    @Override
    protected void doExecute() throws MojoExecutionException {
        trackFirstRunIfNeeded();
        sendAnalyticsEvent(
                release(getPluginInformation().getId(), getPluginInformation().getVersion()));
        getMavenGoals().release(mavenArgs);
    }
}

package com.atlassian.maven.plugins.amps.product.manager;

import static java.util.Objects.requireNonNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;

/**
 * The network configuration of the Cargo container.
 *
 * @since 8.3 was previously an inner class of {@code WebAppManagerImpl}
 */
class ContainerConfig {

    private final Container container;
    private final Product product;
    private final int nodeIndex;

    ContainerConfig(final Container container, final Product product, final int nodeIndex) {
        this.container = requireNonNull(container);
        this.product = requireNonNull(product);
        this.nodeIndex = nodeIndex;
    }

    public Container getContainer() {
        return container;
    }

    public Product getProduct() {
        return product;
    }

    /**
     * Returns the install directory for this container.
     *
     * @param buildDirectory the parent build directory
     * @return see description
     */
    public String getInstallDirectory(final String buildDirectory) {
        // e.g. target/container/tomcat8x/apache-tomcat-8.5.40
        final String baseInstallDirectory = container.getInstallDirectory(buildDirectory);
        if (nodeIndex == 0) {
            return baseInstallDirectory; // for backward compatibility with single-node operation
        }
        return baseInstallDirectory + "-" + nodeIndex;
    }

    /**
     * Returns the name of the log file for this container.
     *
     * @return see description
     */
    @Nullable public String getLogFile() {
        final String baseLogFile = product.getOutput();
        if (baseLogFile == null || nodeIndex == 0) {
            return baseLogFile; // for backward compatibility with single-node operation
        }
        return baseLogFile + "-" + nodeIndex;
    }

    /**
     * Returns the configuration directory for this container.
     *
     * @param buildDirectory the build directory
     * @return see description
     */
    @Nonnull
    public String getConfigDirectory(final String buildDirectory) {
        final String baseConfigDirectory = container.getConfigDirectory(buildDirectory, product.getInstanceId());
        if (nodeIndex == 0) {
            return baseConfigDirectory; // for backward compatibility with single-node operation
        }
        return baseConfigDirectory + "-" + nodeIndex;
    }

    /**
     * Indicates whether this is the first node of a cluster, or the only node of a non-clustered instance.
     *
     * @return see description
     * @since 8.3
     */
    public boolean isFirstNode() {
        return nodeIndex == 0;
    }

    /**
     * Returns the product node to which this container config relates.
     *
     * @return see description
     */
    @Nonnull
    public Node getNode() {
        return product.getNodes().get(nodeIndex);
    }
}

package com.atlassian.maven.plugins.amps.util.ant;

import java.io.File;
import java.util.Collections;
import java.util.Map;

import org.apache.maven.plugin.logging.Log;
import org.apache.tools.ant.taskdefs.Java;

public class JavaTaskFactory {
    private final AntUtils antUtils = new AntUtils();
    private final Log logger;

    public JavaTaskFactory(Log logger) {
        this.logger = logger;
    }

    public Java newJavaTask(Parameters params) {
        Java java = (Java) antUtils.createAntTask("java");
        java.setFork(true);

        // Override JVM if defined
        setJvm(params.jvm, java);

        // If the user has not specified any output file then the process's output will be logged
        // to the Ant logging subsystem which will in turn go to the Cargo's logging subsystem as
        // we're configuring Ant with our own custom build listener (see below).
        setOutput(params.output, java);

        // Add a build listener to the Ant project so that we can catch what the Java task logs
        addBuildListener(java);

        // Add system properties for the container JVM
        addSystemProperties(java, params.systemProperties);

        // Add JVM args if defined
        addJvmArgs(java, params.jvmArgs);

        return java;
    }

    private void setOutput(String output, Java java) {
        if (output != null) {
            File outputFile = new File(output);

            // Ensure that directories where the output file will go are created
            outputFile.getAbsoluteFile().getParentFile().mkdirs();

            java.setOutput(outputFile);
            java.setAppend(true);
        }
    }

    private void setJvm(String jvm, Java java) {
        if (jvm != null) {
            java.setJvm(jvm);
        }
    }

    private void addBuildListener(Java java) {
        boolean foundBuildListener = false;
        for (Object listenerObject : java.getProject().getBuildListeners()) {
            if (listenerObject instanceof AntBuildListener) {
                foundBuildListener = true;
                break;
            }
        }
        if (!foundBuildListener) {
            java.getProject().addBuildListener(new AntBuildListener(logger));
        }
    }

    /**
     * Add system properties to the Ant java command used to start the container.
     *
     * @param java the java command that will start the container
     */
    private void addSystemProperties(Java java, Map<String, String> systemProperties) {
        for (Map.Entry<String, String> prop : systemProperties.entrySet()) {
            java.addSysproperty(antUtils.createSysProperty(prop.getKey(), prop.getValue()));
        }
    }

    /**
     * Add the jvm arguments to the java command.
     *
     * @param java The java task
     */
    private void addJvmArgs(Java java, String jvmArgs) {
        if (jvmArgs != null) {
            java.createJvmarg().setLine(jvmArgs);
        }
    }

    public static final class Parameters {
        private final String output;
        private final String jvm;
        private final String jvmArgs;
        private final Map<String, String> systemProperties;

        private Parameters(String output, String jvm, String jvmArgs, Map<String, String> systemProperties) {
            this.output = output;
            this.jvm = jvm;
            this.jvmArgs = jvmArgs;
            this.systemProperties = systemProperties;
        }

        public static class Builder {
            private String output;
            private String jvm;
            private String jvmArgs;
            private Map<String, String> systemProperties = Collections.emptyMap();

            public Builder output(String output) {
                this.output = output;
                return this;
            }

            public Builder jvm(String jvm) {
                this.jvm = jvm;
                return this;
            }

            public Builder jvmArgs(String jvmArgs) {
                this.jvmArgs = jvmArgs;
                return this;
            }

            public Builder systemProperties(Map<String, String> systemProperties) {
                this.systemProperties = systemProperties;
                return this;
            }

            public Parameters build() {
                return new Parameters(output, jvm, jvmArgs, systemProperties);
            }
        }
    }
}

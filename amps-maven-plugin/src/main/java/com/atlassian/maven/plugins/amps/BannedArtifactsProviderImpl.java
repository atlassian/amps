package com.atlassian.maven.plugins.amps;

import static com.atlassian.maven.plugins.amps.util.ProjectUtils.hasAtlassianGroupId;
import static java.util.stream.Collectors.toSet;

import java.util.Set;
import java.util.stream.Stream;

import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.project.MavenProject;
import com.google.common.collect.ImmutableSet;

class BannedArtifactsProviderImpl implements BannedArtifactsProvider {
    private static final Set<String> DISALLOWED_PUBLIC_ARTIFACT_IDS =
            ImmutableSet.of("platform-public-api", "platform-deprecated-public-api");
    private static final Set<String> INTERNAL_ARTIFACT_IDS = ImmutableSet.of("platform-internal-api");

    @Component
    private MavenProject mavenProject;

    @Override
    public Set<String> getBannedArtifacts(final boolean forceInternal) {
        boolean internal = forceInternal || isInternalPlugin();

        // If it’s an internal plugin, then it should declare all platform-public-api and platform-internal-api
        // dependencies as provided.
        // If it is a third-party plugin, then only check for platform-public-api dependencies, since 3rd party plugins
        // don’t know about internal api.

        if (internal) {
            return union(DISALLOWED_PUBLIC_ARTIFACT_IDS, INTERNAL_ARTIFACT_IDS);
        }

        return DISALLOWED_PUBLIC_ARTIFACT_IDS;
    }

    private boolean isInternalPlugin() {
        return hasAtlassianGroupId(mavenProject.getArtifact().getGroupId());
    }

    private static Set<String> union(Set<String> first, Set<String> second) {
        return Stream.concat(first.stream(), second.stream()).collect(toSet());
    }
}

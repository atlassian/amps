package com.atlassian.maven.plugins.amps.frontend.association.mapping;

import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.JAVASCRIPT_FILE_REGEX;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import com.google.common.annotations.VisibleForTesting;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeManifestAssociationsConfiguration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.model.FeOutputJsFileDeclaration;
import com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper;

public class FeManifestAssociationArbiter {

    private final DirectoryHelper directoryHelper;

    private final FeManifestAssociationsConfiguration configuration;

    private final Log logger;

    public FeManifestAssociationArbiter(
            DirectoryHelper directoryHelper, FeManifestAssociationsConfiguration configuration, Log logger) {
        this.directoryHelper = directoryHelper;
        this.configuration = configuration;
        this.logger = logger;
    }

    public void verifyOutput(Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations)
            throws MojoExecutionException {
        verifyAllDeclaredFilesExistInOutput(outputJsFileDeclarations);
        verifyAllOutputFilesAreDeclared(outputJsFileDeclarations);
    }

    @VisibleForTesting
    protected void verifyAllDeclaredFilesExistInOutput(Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations)
            throws MojoExecutionException {
        for (FeOutputJsFileDeclaration fileDeclaration : outputJsFileDeclarations.values()) {
            String filepath = fileDeclaration.getFilepath();
            String fileAbsolutePath = directoryHelper.getOutputFileAbsolutePath(filepath);
            File file = new File(fileAbsolutePath);
            if (!file.exists()) {
                throw new MojoExecutionException(
                        String.format("Declared file doesn't exist in build directory: %s", filepath));
            }
        }
    }

    @VisibleForTesting
    protected void verifyAllOutputFilesAreDeclared(Map<String, FeOutputJsFileDeclaration> outputJsFileDeclarations)
            throws MojoExecutionException {
        String outputDirectoryPath = directoryHelper.getOutputDirectoryPath();
        File outputDirectory = new File(outputDirectoryPath);

        Collection<File> files = FileUtils.listFiles(
                outputDirectory, new RegexFileFilter(JAVASCRIPT_FILE_REGEX), DirectoryFileFilter.DIRECTORY);

        Set<String> filePaths = outputJsFileDeclarations.keySet();
        List<String> undeclaredFiles = new ArrayList<>();

        for (final File fileEntry : files) {
            String relativeToOutputDir =
                    outputDirectory.toURI().relativize(fileEntry.toURI()).getPath();
            if (!filePaths.contains(relativeToOutputDir)) {
                undeclaredFiles.add(relativeToOutputDir);
            }
        }

        if (!undeclaredFiles.isEmpty()) {
            String prettyPrintFilePaths = String.join("\n", undeclaredFiles);
            String message = String.format(
                    "Some files exist in output but have not been declared in any manifest association:\n%s",
                    prettyPrintFilePaths);
            if (Boolean.TRUE.equals(configuration.getIgnoreSomeFilesNotDeclared())) {
                logger.warn(message);
            } else {
                throw new MojoExecutionException(message);
            }
        }
    }
}

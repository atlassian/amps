package com.atlassian.maven.plugins.amps.frontend.association.verification.impl;

import static com.atlassian.maven.plugins.amps.frontend.association.verification.utils.ArtifactPathUtils.normalizePathSeparators;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.slf4j.Logger.ROOT_LOGGER_NAME;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.Collection;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;
import org.slf4j.Logger;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.DeclarationsChecker;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.FailurePreferences;
import com.atlassian.maven.plugins.amps.frontend.association.verification.model.NestedModuleDeclarationData;

public class DeclarationsCheckerImpl implements DeclarationsChecker {
    private final Logger logger = getLogger(ROOT_LOGGER_NAME);
    private FailurePreferences failurePreferences;

    private boolean undeclaredFilesFound = false;
    private boolean extraDeclarationsFound = false;

    public void setFailurePreferences(FailurePreferences failurePreferences) {
        this.failurePreferences = failurePreferences;
    }

    @Override
    public void verifyModule(TFile moduleRoot, Set<String> declarations, Set<String> files) {
        logger.info("Verifying directory: {}", moduleRoot.getPath());

        if (!declarations.equals(normalizePathSeparators(declarations))
                || !files.equals(normalizePathSeparators(files))) {
            throw new IllegalArgumentException("Paths should be normalized before verification:\ndeclarations: "
                    + declarations + "\nfiles: " + files);
        }

        checkDeclarationsExist(declarations);
        checkUndeclaredFiles(declarations, files);
        checkExtraDeclarations(declarations, files);
    }

    @Override
    public void verifyExternalModule(
            TFile moduleRoot, NestedModuleDeclarationData nestedModuleData, Set<String> files) {
        Set<String> declarations = nestedModuleData.getDeclaredFiles();

        verifyModule(moduleRoot, declarations, files);
    }

    private void checkDeclarationsExist(Set<String> declarations) {
        if (declarations.isEmpty()) {
            logger.warn("No declarations provided for the module!");
        }
    }

    private void checkExtraDeclarations(Set<String> declarations, Set<String> files) {
        Collection<String> extraDeclarations = filterOut(declarations, files);
        if (!extraDeclarations.isEmpty()) {
            extraDeclarationsFound = true;
            logExtraDeclarations(extraDeclarations);
        }
    }

    private void checkUndeclaredFiles(Set<String> declarations, Set<String> files) {
        Collection<String> undeclaredFiles = filterOut(files, declarations);

        if (!undeclaredFiles.isEmpty()) {
            undeclaredFilesFound = true;
            logUndeclaredFiles(undeclaredFiles);
        }
    }

    private Collection<String> filterOut(Set<String> originalSet, Set<String> elementsToRemove) {
        return originalSet.stream()
                .filter(file -> !elementsToRemove.contains(file))
                .collect(toList());
    }

    private void logExtraDeclarations(Collection<String> extraDeclarations) {
        String sortedUndeclaredFiles = extraDeclarations.stream().sorted().collect(joining(",\n\t"));
        String logMessage = format("Extra declarations detected: [\n\t%s\n]", sortedUndeclaredFiles);

        logWithPreferredLevel(logMessage, failurePreferences.isFailOnExtraDeclarations());
    }

    private void logUndeclaredFiles(Collection<String> undeclaredFiles) {
        String sortedUndeclaredFiles = undeclaredFiles.stream().sorted().collect(joining(",\n\t"));
        String logMessage = format(
                "Undeclared files detected! First check that they are not left over from "
                        + "switching branches (i.e. try running 'mvn clean' first). If they are supposed to be bundled, check "
                        + "them for third-party code (all 3rd-party code must be installed and bundled by using the "
                        + "package.json file so that our SBOMs are accurate) then declare the files in the pom.xml file. "
                        + "Learn more: https://hello.atlassian.net/wiki/spaces/DCCore/pages/3400121615\n\n"
                        + "The undeclared files were : [\n\t%s\n]",
                sortedUndeclaredFiles);

        logWithPreferredLevel(logMessage, failurePreferences.isFailOnUndeclaredFiles());
    }

    private void logWithPreferredLevel(String message, boolean willFail) {
        if (willFail) {
            logger.error(message);
        } else {
            logger.warn(message);
        }
    }

    @Override
    public void finalizeChecks() throws MojoExecutionException {
        finalizeUndeclaredFileChecks();
        finalizeExtraDeclarationChecks();
    }

    private void finalizeUndeclaredFileChecks() throws MojoExecutionException {
        if (failurePreferences.isFailOnUndeclaredFiles() && undeclaredFilesFound) {
            throw new MojoExecutionException(
                    "There were undeclared files found when verifying frontend manifest associations. See logs above for a complete list of problems.");
        }
    }

    private void finalizeExtraDeclarationChecks() throws MojoExecutionException {
        if (failurePreferences.isFailOnExtraDeclarations() && extraDeclarationsFound) {
            throw new MojoExecutionException(
                    "There were extra file declarations found when verifying frontend manifest associations. See logs above for a complete list of problems.");
        }
    }
}

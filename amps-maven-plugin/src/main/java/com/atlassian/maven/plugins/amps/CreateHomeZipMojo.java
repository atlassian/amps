package com.atlassian.maven.plugins.amps;

import static java.lang.String.format;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import com.atlassian.maven.plugins.amps.product.ProductHandler;

/**
 * Creates a zip file for each of the previous run's home directories, in the format to use as test-resources.
 *
 * @since 3.1
 */
@Mojo(name = "create-home-zip")
public class CreateHomeZipMojo extends AbstractProductHandlerMojo {

    private static final String BASE_ZIP_NAME = "generated-test-resources";
    private static final String DOT_ZIP = ".zip";

    /** Generated home directory zip file. */
    @Parameter(property = "homeZip")
    protected File homeZip;

    public void doExecute() throws MojoExecutionException, MojoFailureException {
        trackFirstRunIfNeeded();
        final Product product = getProduct(instanceId, getProductId());
        final ProductHandler productHandler = getProductHandler(product.getId());
        final List<File> snapshotDirs = productHandler.getSnapshotDirectories(product);
        final List<File> homeZips = getHomeZips(productHandler.getBaseDirectory(product), snapshotDirs.size());

        for (int i = 0; i < snapshotDirs.size(); i++) {
            final File snapshotDir = snapshotDirs.get(i);
            final File thisHomeZip = homeZips.get(i);
            productHandler.createHomeZip(snapshotDir, thisHomeZip, product);
            getLog().info("Home directory zip created successfully at " + thisHomeZip.getAbsolutePath());
        }

        // Make the home ZIPs artifacts of the project
        getMavenGoals().attachArtifacts(homeZips, "zip");
    }

    // Returns the yet-to-be-created files into which the snapshot directories will be zipped
    private List<File> getHomeZips(final File baseDirectory, final int nodeCount) {
        final List<File> homeZips = new ArrayList<>();
        for (int i = 0; i < nodeCount; i++) {
            final String filename = getHomeZipName(i, nodeCount);
            homeZips.add(new File(baseDirectory, filename));
        }
        return homeZips;
    }

    private String getHomeZipName(final int nodeIndex, final int nodeCount) {
        if (nodeCount == 1) {
            return homeZip == null ? BASE_ZIP_NAME + DOT_ZIP : homeZip.getName();
        }
        // Multi-node operation; ignore any custom name, it's too hard to differentiate it per node
        if (homeZip != null) {
            getLog().warn(format("Multi-node operation; Ignoring configured <homeZip> '%s'", homeZip));
        }
        // For backward compatibility with single-node operation, we don't suffix the filename of the first home ZIP
        if (nodeIndex == 0) {
            return BASE_ZIP_NAME + DOT_ZIP;
        }
        return BASE_ZIP_NAME + "-" + nodeIndex + DOT_ZIP;
    }

    /**
     * Returns the product to snapshot.
     *
     * @param instanceId the instance to snapshot (preferred solution to reference the product)
     * @param productId the product to snapshot if instanceId is null. It is not advisable to use this parameter, as it
     *     doesn't reference the instance in a unique manner.
     * @return a Product object
     * @throws MojoExecutionException if Mojo execution fails
     */
    private Product getProduct(final String instanceId, final String productId) throws MojoExecutionException {
        final Map<String, Product> contexts = getProductContexts();

        Product product = null;
        if (instanceId != null) {
            product = contexts.get(instanceId);
            if (product == null) {
                throw new MojoExecutionException(
                        "There is no instance with name " + instanceId + " defined in the pom.xml");
            }
        } else {
            for (Product candidate : contexts.values()) {
                if (candidate.getId().equals(productId)) {
                    product = candidate;
                    break;
                }
            }
            if (product == null) {
                throw new MojoExecutionException("There is no product with name " + productId
                        + " defined in the pom.xml. Please use -DinstanceId=... to set the instance to snapshot.");
            }
        }

        return product;
    }
}

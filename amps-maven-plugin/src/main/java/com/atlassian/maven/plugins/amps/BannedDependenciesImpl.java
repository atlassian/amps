package com.atlassian.maven.plugins.amps;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;

import java.util.Collection;
import java.util.Set;

import org.apache.maven.plugins.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.twdata.maven.mojoexecutor.MojoExecutor.Element;

/**
 * Utility Class containing dependencies managed by the platform.
 *
 * @since 8.1
 */
class BannedDependenciesImpl implements BannedDependencies {
    private static final Logger log = LoggerFactory.getLogger(BannedDependenciesImpl.class);
    private static final String PLATFORM_GROUP_ID = "com.atlassian.platform.dependencies";

    @Component
    private PlatformDependenciesProvider platformDependenciesProvider;

    @Component
    private LegacyBannedDependenciesProvider legacyBannedDependenciesProvider;

    @Component
    private BannedArtifactsProvider bannedArtifactsProvider;

    @Override
    public Set<Element> getBannedElements(
            final Collection<String> allowedDependencies,
            final String platformVersionRange,
            final boolean forceInternal,
            final boolean useLegacyDependencyList)
            throws PlatformDependencyProvisioningException {
        log.info(
                "Dependencies excluded from banning: [{}]",
                allowedDependencies.stream().collect(joining(", ")));
        log.info("Scanning using platform version range: '{}'", platformVersionRange);

        Set<String> bannedDependencies =
                resolveDependencies(forceInternal, platformVersionRange, useLegacyDependencyList).stream()
                        .distinct()
                        .filter(dependency -> !allowedDependencies.contains(dependency))
                        .collect(toSet());

        if (log.isDebugEnabled()) {
            log.debug(
                    "Banned dependencies:\n{}",
                    bannedDependencies.stream().sorted().collect(joining("\n")));
        }

        return bannedDependencies.stream()
                .map(BannedDependenciesImpl::inCompileScope)
                .map(BannedDependenciesImpl::asExcludeElement)
                .collect(toSet());
    }

    /**
     * Normalises the given dependency into a format suitable for the {@code excludes} parameter of the Enforcer
     * plugin's "Banned Dependencies" rule, with {@code compile} scope applied.
     *
     * @param dependency the dependency expression to normalise
     * @return an expression of the form {@code {groupId}:{artifactId}:{version}:{type}:compile}
     */
    private static String inCompileScope(final String dependency) {
        switch (countMatches(dependency, ":")) {
            case 1:
                return dependency.concat(":*:*:compile");
            case 2:
                return dependency.concat(":*:compile");
            case 3:
                return dependency.concat(":compile");
            default:
                throw new UnsupportedOperationException(format("Unsupported dependency format '%s'", dependency));
        }
    }

    private static Element asExcludeElement(String elementValue) {
        return element(name("exclude"), elementValue);
    }

    private Collection<String> resolveDependencies(
            final boolean forceInternal, final String platformVersionRange, final boolean useLegacyDependencyList)
            throws PlatformDependencyProvisioningException {

        if (useLegacyDependencyList) {
            log.debug("Using legacy banned dependencies list");
            return legacyBannedDependenciesProvider.getDependencies();
        }

        log.debug("Using banned dependencies list from platform POMs");
        Set<String> artifactIds = bannedArtifactsProvider.getBannedArtifacts(forceInternal);

        return platformDependenciesProvider.getDependencies(PLATFORM_GROUP_ID, artifactIds, platformVersionRange);
    }
}

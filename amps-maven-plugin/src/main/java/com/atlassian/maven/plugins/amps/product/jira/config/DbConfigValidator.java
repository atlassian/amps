package com.atlassian.maven.plugins.amps.product.jira.config;

import javax.annotation.ParametersAreNonnullByDefault;

import org.dom4j.Document;

import com.atlassian.maven.plugins.amps.product.common.ValidationException;
import com.atlassian.maven.plugins.amps.product.common.XMLDocumentValidator;

/**
 * Validates the Jira database configuration file.
 *
 * @since 8.2
 */
@ParametersAreNonnullByDefault
public class DbConfigValidator implements XMLDocumentValidator {

    private static final String ROOT_ENTITY = "jira-database-config";

    public void validate(final Document document) throws ValidationException {
        if (document.selectSingleNode("/" + ROOT_ENTITY) == null) {
            throw new ValidationException(
                    "Database configuration file is invalid - missing root entity '" + ROOT_ENTITY + "'");
        }
    }
}

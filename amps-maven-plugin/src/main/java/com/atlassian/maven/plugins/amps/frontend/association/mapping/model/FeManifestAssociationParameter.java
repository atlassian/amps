package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

public class FeManifestAssociationParameter {

    private String packageName;

    private ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest
            manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest;

    private String outputDirectoryFilesDeclaration;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest getFilesDeclaration() {
        return manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest;
    }

    public void setFilesDeclaration(ManuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest filesDeclaration) {
        this.manuallyVerifiedEveryFileHasItsDependenciesDeclaredInTheManifest = filesDeclaration;
    }

    public String getOutputDirectoryFilesDeclaration() {
        return outputDirectoryFilesDeclaration;
    }

    public void setOutputDirectoryFilesDeclaration(String outputDirectoryFilesDeclaration) {
        this.outputDirectoryFilesDeclaration = outputDirectoryFilesDeclaration;
    }
}

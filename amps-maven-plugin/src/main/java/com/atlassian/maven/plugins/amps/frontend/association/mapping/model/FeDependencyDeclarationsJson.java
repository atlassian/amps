package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class FeDependencyDeclarationsJson {

    private Map<String, List<String>> declarations;

    @JsonAnyGetter
    public Map<String, List<String>> getDeclarations() {
        return declarations;
    }

    @JsonAnySetter
    public void setDeclarations(Map<String, List<String>> declarations) {
        this.declarations = declarations;
    }
}

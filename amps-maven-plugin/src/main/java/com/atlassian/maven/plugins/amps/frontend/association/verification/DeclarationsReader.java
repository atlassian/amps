package com.atlassian.maven.plugins.amps.frontend.association.verification;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.maven.plugin.MojoExecutionException;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.model.NestedModuleDeclarationData;

public interface DeclarationsReader {
    Set<String> readRelativeDeclarations(Collection<TFile> associationFiles, TFile moduleRoot)
            throws MojoExecutionException;

    Map<TFile, NestedModuleDeclarationData> readNestedModuleDeclarationData(
            Collection<TFile> declarationFiles, TFile moduleRoot) throws MojoExecutionException;
}

package com.atlassian.maven.plugins.amps;

import java.util.Set;

interface BannedArtifactsProvider {
    /**
     * Returns a collection of platform {@code artifactId}s which are not allowed to be used in the plugin. Internal
     * Atlassian plugins are allowed to use some dependencies which are not allowed to be used by thrid-party plugins.
     * The provider tries to detect if the plugin is an internal one but it is also possible to force the plugin being
     * considered as internal by setting {@code forceInternal} flag. Authors of thrid-party plugins must not set the
     * flag since it may lead to dependency on components/libraries which can be removed at any time without notice.
     *
     * @param forceInternal forces the returned list to be valid for internal plugins only
     */
    Set<String> getBannedArtifacts(final boolean forceInternal);
}

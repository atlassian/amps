package com.atlassian.maven.plugins.amps.frontend.association.mapping.model;

import java.util.List;

public class FeDependencyDeclaration {

    private String artifactPath;

    private List<String> declaredJsFiles;

    public FeDependencyDeclaration(String artifactPath, List<String> declaredJsFiles) {
        this.artifactPath = artifactPath;
        this.declaredJsFiles = declaredJsFiles;
    }

    public String getArtifactPath() {
        return artifactPath;
    }

    public void setArtifactPath(String artifactPath) {
        this.artifactPath = artifactPath;
    }

    public List<String> getDeclaredJsFiles() {
        return declaredJsFiles;
    }

    public void setDeclaredJsFiles(List<String> declaredJsFiles) {
        this.declaredJsFiles = declaredJsFiles;
    }
}

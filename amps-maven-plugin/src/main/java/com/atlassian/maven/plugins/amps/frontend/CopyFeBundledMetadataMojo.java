package com.atlassian.maven.plugins.amps.frontend;

import static com.atlassian.maven.plugins.amps.frontend.CopyFeModuleManifestsMojo.MODULE_MANIFESTS_OUTPUT_DIR;
import static com.atlassian.maven.plugins.amps.frontend.CopyFeModuleManifestsMojo.SUPPORTED_FILE_NAMES;
import static com.atlassian.maven.plugins.amps.frontend.association.FeManifestAssociationConstants.FINAL_ASSOCIATION_MAPPING_FILE;
import static com.atlassian.maven.plugins.amps.frontend.association.mapping.utils.DirectoryHelper.ASSOCIATIONS_OUTPUT_DIR;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.AbstractAmpsMojo;
import com.atlassian.maven.plugins.amps.CopyBundledDependenciesMojo;

/**
 * Extracts and copies frontend metadata like frontend module manifests and frontend manifest associations from bundled
 * dependencies into {@code META-INF/fe-bundled-metadata}.
 *
 * @since 8.16.0
 */
@Mojo(name = "copy-fe-bundled-metadata", requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class CopyFeBundledMetadataMojo extends AbstractAmpsMojo {
    public static final String BUNDLED_METADATA_OUTPUT_DIR = "META-INF/fe-bundled-metadata";

    /** When set to true bundled dependencies' metadata will not be copied over to current build output. */
    @Parameter(property = "excludeFeBundledMetadata", defaultValue = "false")
    private Boolean excludeFeBundledMetadata;

    /**
     * This parameter controls whether dependencies are extracted or just copied in {@link CopyBundledDependenciesMojo
     * CopyBundledDependenciesMojo}. Frontend metadata should only be extracted when bundled dependencies are extracted.
     *
     * @since 8.17.1
     */
    @Parameter(property = "extractDependencies", defaultValue = "true")
    private Boolean extractDependencies;

    public void execute() throws MojoExecutionException, MojoFailureException {
        if (!excludeFeBundledMetadata && extractDependencies) {
            getLog().info("Copying bundled frontend metadata");
            copyBundledMetadata();
        } else {
            getLog().info("Skipped copying bundled frontend metadata");
        }
    }

    private void copyBundledMetadata() throws MojoExecutionException {
        List<String> includedPatterns = Stream.of(getFeManifestsPatterns(), getFeAssociationsPatterns())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        String outputDirectory = String.format("${project.build.outputDirectory}/%s", BUNDLED_METADATA_OUTPUT_DIR);

        getMavenGoals().extractBundledFeModuleManifests(includedPatterns, outputDirectory);
    }

    private List<String> getFeManifestsPatterns() {
        return SUPPORTED_FILE_NAMES.stream()
                .map(fileName -> String.format("%s/**/%s", MODULE_MANIFESTS_OUTPUT_DIR, fileName))
                .collect(Collectors.toList());
    }

    private List<String> getFeAssociationsPatterns() {
        return Collections.singletonList(
                String.format("%s/**/%s", ASSOCIATIONS_OUTPUT_DIR, FINAL_ASSOCIATION_MAPPING_FILE));
    }
}

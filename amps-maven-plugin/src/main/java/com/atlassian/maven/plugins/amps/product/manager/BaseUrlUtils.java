package com.atlassian.maven.plugins.amps.product.manager;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.prependIfMissing;

import java.util.regex.Pattern;
import javax.annotation.Nullable;

/**
 * Utility methods relating to product base URLs.
 *
 * @since 8.3
 */
public final class BaseUrlUtils {

    /**
     * Returns the product's base URL, based on the given inputs.
     *
     * @param server the TCP/IP host on which the product is running; should not be blank
     * @param actualWebPort the product's actual web port
     * @param contextPath the product's servlet context path
     * @return the base URL
     */
    public static String getBaseUrl(final String server, final int actualWebPort, @Nullable final String contextPath) {
        return formatServer(server) + formatPort(server, actualWebPort) + formatContextPath(contextPath);
    }

    private static String formatServer(String server) {
        if (isBlank(server)) {
            throw new IllegalArgumentException(format("Invalid server name '%s'", server));
        }
        String schemeRegex = "^https?://";
        if (!Pattern.compile(schemeRegex).matcher(server).find()) {
            server = "http://" + server;
        }
        return server;
    }

    private static String formatPort(final String server, final int port) {
        int defaultPort = 80;
        if (server.startsWith("https://")) {
            defaultPort = 443;
        }
        return port != defaultPort ? ":" + port : "";
    }

    private static String formatContextPath(final String contextPath) {
        if (isBlank(contextPath)) {
            return "";
        }
        return prependIfMissing(contextPath, "/");
    }

    private BaseUrlUtils() {}
}

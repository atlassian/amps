package com.atlassian.maven.plugins.amps.frontend.association.verification;

import org.apache.maven.plugin.MojoExecutionException;

import de.schlichtherle.truezip.file.TFile;

import com.atlassian.maven.plugins.amps.frontend.association.verification.model.ArtifactScanResults;

public interface ArtifactScanner {
    ArtifactScanResults scan(TFile root) throws MojoExecutionException;
}

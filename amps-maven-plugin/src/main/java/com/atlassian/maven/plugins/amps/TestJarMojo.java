package com.atlassian.maven.plugins.amps;

import static com.atlassian.maven.plugins.amps.util.FileUtils.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import com.atlassian.maven.plugins.amps.util.ClassUtils;
import com.atlassian.maven.plugins.amps.util.WiredTestInfo;
import com.atlassian.maven.plugins.amps.xml.SpringXmlComponent;
import com.atlassian.maven.plugins.amps.xml.SpringXmlException;
import com.atlassian.maven.plugins.amps.xml.SpringXmlModel;
import com.atlassian.maven.plugins.amps.xml.SpringXmlRenderer;
import com.atlassian.maven.plugins.amps.xml.SpringXmlRendererFactory;
import com.atlassian.plugins.codegen.util.ClassnameUtil;

/**
 * Jars the tests into an OSGi bundle. Only builds the jar if the parent class' {@code #buildTestPlugin} flag is set or
 * it detects an atlassian-plugin.xml file in the target/test-classes directory.
 *
 * <p>Note, this test jar will not have its resources filtered or a manifest generated for it. If no manifest is
 * present, a dummy manifest that does a dynamic package import on everything will be used.
 *
 * @since 3.3
 */
@Mojo(name = "test-jar", requiresDependencyResolution = ResolutionScope.TEST)
public class TestJarMojo extends AbstractAmpsMojo {
    /** The final name for the test plugin, without the "-tests" suffix. */
    @Parameter(property = "project.build.finalName")
    private String finalName;

    public void execute() throws MojoExecutionException, MojoFailureException {
        MavenProject prj = getMavenContext().getProject();

        File testClassesDir = file(prj.getBuild().getTestOutputDirectory());

        if (shouldBuildTestPlugin()) {
            File mf = file(testClassesDir, "META-INF", "MANIFEST.MF");

            if (!mf.exists()) {
                try {
                    String symbolicName = prj.getGroupId() + "." + prj.getArtifactId() + "-tests";
                    FileUtils.writeStringToFile(
                            mf,
                            "Manifest-Version: 1.0\n" + "Bundle-SymbolicName: "
                                    + symbolicName + "\n" + "Bundle-Version: 1.0\n"
                                    + "Bundle-Name: "
                                    + finalName + "-tests\n" + "DynamicImport-Package: *\n"
                                    + "Atlassian-Plugin-Key: "
                                    + symbolicName + "\n",
                            StandardCharsets.UTF_8);
                } catch (IOException e) {
                    throw new MojoFailureException("Unable to write manifest");
                }
            }

            File pluginXml = file(testClassesDir, "atlassian-plugin.xml");
            File itPackageDir = file(testClassesDir, "it");

            if (pluginXml.exists() && itPackageDir.exists()) {
                Collection<File> classFiles = FileUtils.listFiles(itPackageDir, new String[] {"class"}, true);

                try {
                    SpringXmlModel.Builder modelBuilder = SpringXmlModel.builder();

                    for (File classFile : classFiles) {
                        String className = ClassUtils.getClassnameFromFile(
                                classFile, prj.getBuild().getTestOutputDirectory());
                        WiredTestInfo wiredInfo = ClassUtils.getWiredTestInfo(classFile);
                        if (wiredInfo.isWiredTest()) {
                            getLog().info("found Test: " + className
                                    + ", adding to test-classes/META-INF/spring/wired-test-components.xml...");

                            String simpleClassname = StringUtils.substringAfterLast(className, ".");
                            SpringXmlComponent component = SpringXmlComponent.builder()
                                    .putServiceProperty("inProductTest", "true")
                                    .fullClassName(className)
                                    .id(ClassnameUtil.camelCaseToDashed(simpleClassname)
                                            .toLowerCase())
                                    .build();

                            modelBuilder.addComponent(component);
                        }
                    }

                    File springTestFile = file(testClassesDir, "META-INF", "spring", "wired-test-components.xml");
                    Path springDirectoryPath = springTestFile.getParentFile().toPath();
                    Files.createDirectories(springDirectoryPath);

                    Writer writer = new FileWriter(springTestFile);
                    SpringXmlRendererFactory springXmlBuilderFactory = new SpringXmlRendererFactory();
                    SpringXmlRenderer springXmlRenderer = springXmlBuilderFactory.create();
                    springXmlRenderer.render(modelBuilder.build(), writer);
                } catch (SpringXmlException e) {
                    throw new MojoExecutionException(
                            "unable to generate Spring XML configuration for test classes folder", e);
                } catch (MalformedURLException e) {
                    throw new MojoExecutionException("unable to convert test classes folder to URL", e);
                } catch (IOException e) {
                    throw new MojoExecutionException("unable to modify plugin.xml", e);
                }
            }

            getMavenGoals().jarTests(finalName);
        }
    }
}

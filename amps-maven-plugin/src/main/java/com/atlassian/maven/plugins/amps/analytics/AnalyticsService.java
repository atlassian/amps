package com.atlassian.maven.plugins.amps.analytics;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;

/**
 * The API used within AMPS for sending analytics events.
 *
 * @since 8.2.4 extracted as an interface from {@code GoogleAmpsTracker} and refactored
 */
@ParametersAreNonnullByDefault
public interface AnalyticsService {

    /**
     * Sends the given analytics event.
     *
     * @param event the event to send
     */
    void send(AnalyticsEvent event);
}

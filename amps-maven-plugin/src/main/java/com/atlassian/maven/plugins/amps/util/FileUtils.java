package com.atlassian.maven.plugins.amps.util;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.io.FileUtils.copyFile;
import static org.apache.commons.io.FileUtils.deleteQuietly;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class FileUtils {

    private FileUtils() {}

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    @Nonnull
    public static File file(String parent, String... kids) {
        return file(new File(parent), kids);
    }

    @Nonnull
    public static File file(@Nonnull final File parent, final String... kids) {
        File cur = parent;
        for (String kid : kids) {
            cur = new File(cur, kid);
        }
        return cur;
    }

    /**
     * Compares the filename with the artifact name using pattern
     *
     * @param fileName name of the file
     * @param artifactId artifactId
     * @return true if the file name matches the pattern
     */
    public static boolean doesFileNameMatchArtifact(final String fileName, final String artifactId) {
        // this is not perfect, but it sure beats fileName.contains(artifactId)
        Pattern pattern =
                Pattern.compile("^([a-z][a-z0-9]*(\\.[a-z0-9]+)*_)?(" + Pattern.quote(artifactId) + ")-\\d.*$");
        Matcher matcher = pattern.matcher(fileName);
        return matcher.find();
    }

    public static void deleteDir(File dir) {
        if (dir.exists()) {
            deleteQuietly(dir);
        }
    }

    public static String fixWindowsSlashes(final String path) {
        return path.replaceAll("\\\\", "/");
    }

    public static void cleanDirectory(File directory, FileFilter filter) throws IOException {
        if (!directory.exists()) {
            String message = directory + " does not exist";
            throw new IllegalArgumentException(message);
        }

        if (!directory.isDirectory()) {
            String message = directory + " is not a directory";
            throw new IllegalArgumentException(message);
        }

        File[] files = directory.listFiles(filter);
        if (files == null) { // null if security restricted
            throw new IOException("Failed to list contents of " + directory);
        }

        IOException exception = null;
        for (File file : files) {
            try {
                org.apache.commons.io.FileUtils.forceDelete(file);
            } catch (IOException ioe) {
                exception = ioe;
            }
        }

        if (null != exception) {
            throw exception;
        }
    }

    /**
     * Copy all files and directories from one folder to another, preserving structure. This is an unfortunate duplicate
     * of other libraries, which do not preserve the executable status of files, most likely due to Java version
     * restrictions.
     *
     * <p>If you do <i>not</i> have this requirement please use commons-io instead:
     * {@link org.apache.commons.io.FileUtils#copyDirectory(java.io.File, java.io.File)}.
     *
     * @param source source directory from which to copy all files/directories from
     * @param destination destination directory to copy all files/directories to
     * @param preserveFileAttrs indicate whether date and exec should be preserved
     * @throws IOException if the destination files could not be created
     */
    public static void copyDirectory(File source, File destination, boolean preserveFileAttrs) throws IOException {
        if (!destination.mkdirs() && !destination.isDirectory()) {
            throw new IOException("Destination '" + destination + "' directory cannot be created");
        }
        File[] srcFiles = source.listFiles();
        // Who decided that listFiles could return null?!?
        srcFiles = srcFiles != null ? srcFiles : new File[0];
        for (File srcFile : srcFiles) {
            File dstFile = new File(destination, srcFile.getName());
            if (srcFile.isDirectory()) {
                copyDirectory(srcFile, dstFile, preserveFileAttrs);
            } else {
                copyFile(srcFile, dstFile, preserveFileAttrs);
                if (preserveFileAttrs && srcFile.canExecute()) {
                    makeExecutable(dstFile);
                }
            }
        }
    }

    /**
     * Makes the given file executable, if not already. Logs a warning if the attempt fails.
     *
     * @param file the file to make executable; ignored if null, missing, or a directory
     * @since 8.1.2
     */
    public static void makeExecutable(final File file) {
        if (file != null && file.isFile() && !file.canExecute() && !file.setExecutable(true)) {
            LOGGER.warn("Couldn't make {} executable", file);
        }
    }

    /**
     * Reads the given file to a String.
     *
     * @param name the name of the file to read
     * @param loadingClass the class loading the file
     * @param encoding the encoding to use
     * @return the contents of the file as a String
     * @see Class#getResourceAsStream(java.lang.String)
     */
    @ParametersAreNonnullByDefault
    public static String readFileToString(final String name, final Class<?> loadingClass, final Charset encoding) {
        final InputStream fileStream = loadingClass.getResourceAsStream(name);
        requireNonNull(fileStream, format("Could not find '%s' on classpath of %s", name, loadingClass.getName()));
        try {
            return IOUtils.toString(fileStream, encoding);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Reads the given file to a String.
     *
     * @param file the file to read.
     * @return the contents of the file as a String. If any exception occurred while reading the file to string, this
     *     method returns an empty optional.
     */
    @Nonnull
    public static Optional<String> readFileToString(@Nonnull final File file, @Nonnull final Charset encoding) {
        try {
            return Optional.ofNullable(org.apache.commons.io.FileUtils.readFileToString(file, encoding));
        } catch (Exception e) {
            LOGGER.debug("Failed to read file '" + file + "'", e);
            return Optional.empty();
        }
    }

    /**
     * Sets the last modified time of the given file to the given value.
     *
     * @param file the file to update
     * @param time the new last modified time
     * @since 8.1.2
     */
    public static void setLastModified(final File file, final long time) {
        if (file != null && file.exists() && !file.setLastModified(time)) {
            LOGGER.warn("Could not set last modified time of {} to {}", file, time);
        }
    }

    /**
     * Creates the given directory if it doesn't already exist. Identical to calling {@link File#mkdir()}, but without
     * the annoying "unused return value" warning.
     *
     * @param file the directory to create
     * @see File#mkdir()
     * @since 8.3
     */
    public static void makeDirectory(final File file) {
        final boolean made = file.mkdir();
        LOGGER.debug("{} made = {}", file, made);
    }

    /**
     * Creates the given directories if they don't already exist. Identical to calling {@link File#mkdirs()}, but
     * without the annoying "unused return value" warning.
     *
     * @param file the directories to create
     * @see File#mkdirs()
     * @since 8.3
     */
    public static void makeDirectories(final File file) {
        final boolean made = file.mkdirs();
        LOGGER.debug("{} made = {}", file, made);
    }

    /**
     * Finds a common parent directory for all the provided files
     *
     * @param absoluteFilePaths a list of absolute file paths
     * @since 8.13.1
     */
    public static File findBaseDirectory(Set<String> absoluteFilePaths) throws IllegalArgumentException {
        if (absoluteFilePaths.isEmpty())
            throw new IllegalArgumentException("Provided list of paths should not be empty");

        String[][] pathComponents = absoluteFilePaths.stream()
                .map(path -> path.split(Pattern.quote(File.separator)))
                .toArray(String[][]::new);

        List<String> baseDirPathComponents = new ArrayList<>();
        String[] referencePathComponents = pathComponents[0];

        boolean foundBaseDir = false;
        int componentIndex = 0;
        int numberOfDirectoryComponents = referencePathComponents.length - 1;
        while (componentIndex < numberOfDirectoryComponents) {
            File referencePathComponent = new File(referencePathComponents[componentIndex]);
            boolean allPathsHaveComponent = true;

            for (int i = 1; i < pathComponents.length && allPathsHaveComponent; i++) {
                String[] pathAsComponents = pathComponents[i];
                if (componentIndex < pathAsComponents.length) {
                    File pathComponent = new File(pathAsComponents[componentIndex]);
                    if (!referencePathComponent.equals(pathComponent)) {
                        allPathsHaveComponent = false;
                    }
                } else {
                    allPathsHaveComponent = false;
                }
            }

            if (allPathsHaveComponent) {
                baseDirPathComponents.add(referencePathComponent.toString());
                foundBaseDir = true;
            } else {
                break;
            }

            componentIndex++;
        }

        if (!foundBaseDir)
            throw new RuntimeException("Base directory couldn't be found for provided paths: " + absoluteFilePaths);

        String baseDirPath = String.join(File.separator, baseDirPathComponents);
        return new File(baseDirPath);
    }
}

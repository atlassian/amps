<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes"/>
  <xsl:output omit-xml-declaration="yes"/>
  <xsl:template match="/model">
    <beans
      xmlns:beans="http://www.springframework.org/schema/beans"
      xmlns:osgi="http://www.eclipse.org/gemini/blueprint/schema/blueprint"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/context http://www.springframework.org/schema/beans/spring-beans.xsd http://www.eclipse.org/gemini/blueprint/schema/blueprint http://www.eclipse.org/gemini/blueprint/schema/blueprint/gemini-blueprint.xsd">
      <xsl:for-each select="components/component">
        <beans:bean id="{id}" autowire="default" class="{fullClassName}" />
        <osgi:service id="{id}_osgi-service" ref="{id}">
          <osgi:interfaces>
            <beans:value><xsl:value-of select="fullClassName" /></beans:value>
          </osgi:interfaces>
          <xsl:if test="count(serviceProperties/serviceProperty) > 0">
            <osgi:service-properties>
              <xsl:for-each select="serviceProperties/serviceProperty">
                <beans:entry key="{key}" value="{value}"/>
              </xsl:for-each>
            </osgi:service-properties>
          </xsl:if>
        </osgi:service>
      </xsl:for-each>
    </beans>
  </xsl:template>
</xsl:stylesheet>

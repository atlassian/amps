# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [9.2.6]

### Changed

- [CONFSRVDEV-37593](https://jira.atlassian.com/browse/CONFSRVDEV-37593): Update Confluence plugin archetype

## [9.2.4]

### Changed

- [AMPS-1708](https://ecosystem.atlassian.net/browse/AMPS-1708): Bump Crowd version to 6.2.2

## [9.2.3]

### Changed

- [AMPS-1706](https://ecosystem.atlassian.net/browse/AMPS-1706) : bump [Atlassian PDK](https://bitbucket.org/atlassian/atlassian-pdk-maven-plugin) to 2.3.6  
- [AMPS-1707](https://ecosystem.atlassian.net/browse/AMPS-1707) : make UPM Signature Check configuration available to test code in remote mojos.

## [9.2.2]

### Changed

- [DCPL-2275](https://ecosystem.atlassian.net/browse/DCPL-2275) : added disable webconsole 

## [9.2.1]

### Changed

- [DCPL-2272](https://ecosystem.atlassian.net/browse/DCPL-2272): using migrated to Jakarta Rest API Browser for devToolbox >= 4.0.0

## [9.2.0]
- [AMPS-1705](https://ecosystem.atlassian.net/browse/AMPS-1705): Make AMPS create UPM plugin signature check feature configuration

### Changed

- [AMPS-1701](https://ecosystem.atlassian.net/browse/AMPS-1701): Bump Crowd version to 6.2.0

## [9.1.11]

### Changed

- [AMPS-1700](https://ecosystem.atlassian.net/browse/AMPS-1700): Bump Crowd version to 6.1.3

## [9.1.9]

### Changed

- [DCA11Y-1346](https://hello.jira.atlassian.cloud/browse/DCA11Y-1346): Upgrade QuickReload to be forward compatible without LESS transformer in products


## [9.1.2]

### Fixed

- [DCA11Y-1182](https://hello.jira.atlassian.cloud/browse/DCA11Y-1182): Provide a descriptive log message for undeclared files

## [9.1.1]

### Fixed

- [DCA11Y-1089](https://hello.jira.atlassian.cloud/browse/DCA11Y-1089): `verify-fe-manifest-associations` checks now `.jar` build artifact instead of `.obr` for  `atlassian-plugin` module without instructions configuration

## [9.1.0]

### Added

- [DCA11Y-1089](https://hello.jira.atlassian.cloud/browse/DCA11Y-1089): `verify-fe-manifest-associations` has been added to `atlassian-plugin` build lifecycle. It will only execute for Atlassian plugins that have a group id that starts with one of the prefixes: "com.atlassian", "io.atlassian", "net.java.dev.activeobjects"
- [DCA11Y-1089](https://hello.jira.atlassian.cloud/browse/DCA11Y-1089): Newly created plugins will now also have package.json file and sample report configuration for plugin javascript files

### Fixed

- [DCA11Y-1207](https://hello.jira.atlassian.cloud/browse/DCA11Y-1207): Upgraded commons-lang3 to 3.17.0, needed to resolve the issue with TrueZip which relies on commons-lang3 >=3.13.0

## [9.0.7]

### Fixed

- [DEVBOX-59](https://ecosystem.atlassian.net/browse/DEVBOX-59): Bump Dev Toolbox to 3.1.1 to fix failing to start in Jira (all Jira versions)

## [9.0.6]

### Changed

- [DCA11Y-1169](https://hello.jira.atlassian.cloud/browse/DCA11Y-1169): Added default phase for `copy-fe-module-manifests`, `report-fe-manifest-associations` and `verify-fe-manifest-associations` goals.
- [DCA11Y-1123](https://hello.jira.atlassian.cloud/browse/DCA11Y-1123): `verifyFeManifestAssociationsInputEntrypoint` property of `verify-fe-manifest-associations` will use default value based on packaging if value not provided

## [9.0.2]
### Changed

- [AMPS-1691](https://ecosystem.atlassian.net/browse/AMPS-1691): Upgraded Crowd version to 6.0.1

## [9.0.1]
### Added

- [KRAK-6570](https://bulldog.internal.atlassian.com/browse/KRAK-6570): Fix libArtifacts replacing in Confluence
- [AMPS-1689](https://ecosystem.atlassian.net/browse/AMPS-1689): Updated integration tests of Crowd to work with JDK 17

## [9.0.0]

### Added

- [AMPS-1686](https://ecosystem.atlassian.net/browse/AMPS-1686): Add ability to specify jvm when runnning a product

### Changed

- [AMPS-1685](https://ecosystem.atlassian.net/browse/AMPS-1685): Upgraded Crowd version to 6.0.0
- [AMPS-1687](https://ecosystem.atlassian.net/browse/AMPS-1687): Manifest for test source is generated based on the test-classes rather than classes directory

## [8.17.7]

### Added

- Added maven mojo `verify-fe-manifest-associations`
  that verifies that all javascript files have a manifest association in a given artifact to product specific maven plugins.

## [8.17.6]

### Fixed

- [DCA11Y-1132](https://hello.jira.atlassian.cloud/browse/DCA11Y-1132): Process all intermediary files in ending in `.intermediary.json` by `fe-report-manifest-associations`
-

## [8.17.5]

### Fixed

- [ECOHELP-43701](https://ecosystem.atlassian.net/jira/servicedesk/projects/ECOHELP/queues/issue/ECOHELP-43701): Upgrade developer Toolbox to 3.0.8

## [8.17.4]

### Fixed

- [ECOHELP-43701](https://ecosystem.atlassian.net/jira/servicedesk/projects/ECOHELP/queues/issue/ECOHELP-43701): Update developer tooling to work with Plat 7 - Developer Toolbox 3.0.7

## [8.17.2]

### Added

- [AMPS-1683](https://ecosystem.atlassian.net/browse/AMPS-1683): Added `skipCopyFeModuleManifests` and
  `skipReportFeManifestAssociations` to allow skipping goals from command line even if configured in pom.xml

## [8.17.1]

### Fixed

- [AMPS-1682](https://ecosystem.atlassian.net/browse/AMPS-1682): Run `copy-fe-bundled-metadata` extraction only when
  `copy-bundled-dependencies` does.

## [8.17.0]

### Added

- [AMPS-1679](https://ecosystem.atlassian.net/browse/AMPS-1679): Added new maven mojo `verify-fe-manifest-associations`
  that verifies that all javascript files have a manifest association in a given artifact.

## [8.16.2]

### Fixed

- [AMPS-1681](https://ecosystem.atlassian.net/browse/AMPS-1681): the mojo `report-fe-manifest-associations`
  now doesn't fail when a module uses the `war` packaging; though in that case it cannot automatically verify
  declared associations

## [8.16.1]

### Fixed

- [AMPS-1680](https://ecosystem.atlassian.net/browse/AMPS-1680): Property plugin.resource.directories is no longer set when running integration-test goal

## [8.16.0]

### Added

- [AMPS-1676](https://ecosystem.atlassian.net/browse/AMPS-1676): Added new maven mojo `copy-fe-bundled-metadata` that extracts
  and copies frontend manifests from `META-INF/fe-module-manifests` and manifest associations from
  `META-INF/fe-manifest-associations` and places them in `META-INF/fe-bundled-metadata`

### Fixed

- [AMPS-1676](https://ecosystem.atlassian.net/browse/AMPS-1676): Excluded frontend manifests and associations from being
  copied by `copy-bundled-dependencies` goal to avoid overrides. Those files are instead handled by `copy-fe-bundled-metadata`

## [8.15.4]

### Added

- [AMPS-1675] Added `legacyValidateBannedDependencies` to provide more time adapting platform driven `validate-banned-dependencies` behaviour

## [8.15.2]

### Fixed

- [AMPS-1673](https://ecosystem.atlassian.net/browse/AMPS-1673): the mojo `report-fe-manifest-associations`
  now accepts entries about all file types (instead of only JavaScript files) in the inputs.

## [8.15.1]

### Fixed

- [AMPS-1672](https://ecosystem.atlassian.net/browse/AMPS-1672): the mojo `report-fe-manifest-associations`
  now outputs paths in the `declarations` values relative to the project base dir in all cases

## [8.15.0]

### Added

- [AMPS-1671](https://ecosystem.atlassian.net/browse/AMPS-1671): Added new maven mojo `report-fe-manifest-associations` that will be used to report all output js files have their dependencies declared in package.json

## [8.14.2]

### Fixed

- [DCPL-1104](https://ecosystem.atlassian.net/browse/DCPL-1104): Update developer tooling to work with Plat 7 milestones - Quick Reload (QR) to 5.0.3 Developer Toolbox 3.0.5 and Plugin Data Editor (PDE) to 1.5.2

## [8.14.1]

### Fixed

- [AMPS-1664](https://ecosystem.atlassian.net/browse/AMPS-1664): Updated JVM arguments for Refapp in preparation for Platform 7

## [8.14.0]

### Changed

- [AMPS-1659](https://ecosystem.atlassian.net/browse/AMPS-1659): Updated versions of some maven plugins used by default - the default version of `maven-dependency-plugin` no longer offers `useJvmChmod` option for `unpack` goal
- [AMPS-1660](https://ecosystem.atlassian.net/browse/AMPS-1660): Updated Tomcat container for RefApp to version 9.x
- [AMPS-1661](https://ecosystem.atlassian.net/browse/AMPS-1661): Integration test run with `testGroups` including nonexistent group id fails instead of reporting only warning
- [AMPS-1662](https://ecosystem.atlassian.net/browse/AMPS-1662): `validate-banned-dependencies` goal is usign platform artifacts to resolve list of disallowed dependencies

## [8.13.6]

### Added

- [AMPS-1656](https://ecosystem.atlassian.net/browse/AMPS-1656): Include H2 dependency if Jira 10.0.0 or newer is
  bundled

## [8.13.5]

Released by mistake. Please do not use this version, use 8.13.4 or 8.13.6 instead.

## [8.13.4]

### Added

- [AMPS-1658](https://ecosystem.atlassian.net/browse/AMPS-1658): Updated required SSL version from TLS to TLS 1.2

## [8.13.3]

### Fixed
- [AMPS-1655](https://ecosystem.atlassian.net/browse/AMPS-1655): `copy-fe-module-manifests` now correctly finds common base directory of provided frontend
  module manifest files also on Windows.

## [8.13.2]

Released by mistake with changes that will be included in 8.14. Please do not use this version, use 8.13.3 instead.

## [8.13.1]

### Fixed
- [AMPS-1655](https://ecosystem.atlassian.net/browse/AMPS-1655): Frontend manifest files paths can be now outside base
  directory or specified as absolute paths. `copy-fe-module-manifests` goal correctly handles existing output directories now so that incremental builds don't break.

## [8.13.0]

### Added
- [DCA11Y-783](https://hello.jira.atlassian.cloud/browse/DCA11Y-783): Add maven mojo to include package.json
  and lock files in the final artifact

## [8.12.1]

### Added
- [AMPS-1649](https://ecosystem.atlassian.net/browse/AMPS-1649): Bump default Crowd version to 5.2.2 in AMPS
- [AMPS-1650](https://ecosystem.atlassian.net/browse/AMPS-1650): Updated vulnerable dependencies

## [8.12.0]

### Added
- [AMPS-1646](https://ecosystem.atlassian.net/browse/AMPS-1646): Provide H2 database when starting Bamboo

## [8.11.6]
### Added
- [KRAK-6570](https://bulldog.internal.atlassian.com/browse/KRAK-6570): Fix libArtifacts replacing in Confluence

## [8.11.5]

### Added
- [AMPS-1647](https://ecosystem.atlassian.net/browse/AMPS-1647): Bump default Crowd version to 5.2.1 in AMPS


## [8.11.2]

### Added
- [AMPS-1641](https://ecosystem.atlassian.net/browse/AMPS-1641): Bump default Crowd version to 5.1.5 in AMPS

## [8.11.1]
### Fixed

- [RAB-38](https://ecosystem.atlassian.net/browse/RAB-38): Fix REST API browser in FeCru
- [RAB-39](https://ecosystem.atlassian.net/browse/RAB-39): Fix missing spinner
- [RAB-41](https://ecosystem.atlassian.net/browse/RAB-41): Add admin web-item for REST API browser in Crowd
- [RAB-42](https://ecosystem.atlassian.net/browse/RAB-42): Make it obvious when the list of APIs is empty

### Changed

- [AMPS-1635](https://ecosystem.atlassian.net/browse/AMPS-1635): Bump default Crowd version to 5.1.4 in AMPS

## [8.11.0]

### Added
- [SPLAT-710](https://bulldog.internal.atlassian.com/browse/SPLAT-710): Add support for Postgres 15

## [8.10.3]

### Fixed
- [AMPS-1628](https://ecosystem.atlassian.net/browse/AMPS-1628): Fix running Bitbucket with external LDAP on Java 17

## [8.10.2]

### Fixed
- [AMPS-1629](https://ecosystem.atlassian.net/browse/AMPS-1629): Fix default port for HTTPS

## [8.10.1]

### Changed

- [AMPS-1621](https://ecosystem.atlassian.net/browse/AMPS-1621) Plugin Viewer, Plugin Data Editor, and Achoo DB console
  are consistently enabled by default. To change this, set `enablePluginViewer`, `enablePde`, `enableAchoo` etc.
  respectively. [Learn more from the configuration reference guide](https://developer.atlassian.com/server/framework/atlassian-sdk/amps-build-configuration-reference/)
- [BSP-4657](https://bulldog.internal.atlassian.com/browse/BSP-4657) Achoo DB console is now separate from Quick Reload
  and is set with `enableAchoo`. The version can be set with `achooVersion`
- [BSP-4653](https://bulldog.internal.atlassian.com/browse/BSP-4653) Achoo DB console now requires system administrator
  privileges to prevent accidental misuse.
- [AMPS-1623](https://ecosystem.atlassian.net/browse/AMPS-1623) Update Developer Toolbox to 3.0.2

### Fixed

- Quick Reload upgraded to 5.0.0 which works
  on [Apple silicon Macs](https://bitbucket.org/atlassianlabs/quickreload/pull-requests/142)
- [BSP-4654](https://bulldog.internal.atlassian.com/browse/BSP-4654) Achoo DB console now works again when using H2 and
  a product with a modern version
  of [SAL](https://developer.atlassian.com/server/framework/atlassian-sdk/shared-access-layer/) (the error message
  was `com.atlassian.sal.core.rdbms.WrappedConnection cannot be cast to org.h2.jdbc.JdbcConnection`)

## [8.9.2]

### Added

- [AMPS-1609] AMPS deploys older versions of certain plugins instead of working copy
- [AMPS-1622] Bump default Crowd version to 5.1.2 in AMPS
- [AMPS-1625] Allow control of google closure warnings for all diagnostic groups

### Changed

- [AMPS-1623](https://ecosystem.atlassian.net/browse/AMPS-1623) Update Developer Toolbox to 3.0.2

### Fixed

- [BSP-4723](https://bulldog.internal.atlassian.com/browse/BSP-4723) Add packages to be open for reflection so Jira can start on Java 17

## [8.9.1]

### Fixed

- [AMPS-1617] Add `--add-opens java.base/sun.nio.ch` to `BitbucketProductHandler` to allow spinning Bitbucket up on Java
  17

## [8.9.0]

### Added

- [AMPS-1605] Allow control of Closure warnings

## [8.8.1]

### Changed

- [AMPS-1613] Upgrade default Quick Reload version to 3.1.1

## [8.8.0]

### Added

- [AMPS-1611] Bump default Crowd version to 5.0.2

## [8.7.0]

### Added

- Added log4j 2 config file replacement feature.

## [8.6.0] - TBD
## [8.5.0] - TBD
## [8.4.0] - TBD

## [8.3.0]

### Added

- New i18n files postprocessing, this reduces load on the application server and solves a long-standing issue of hitting a `StackOverflowError` in some environments. You may need to update your plugin if you run into issues, [learn how](https://community.developer.atlassian.com/t/dc-server-only-mitigating-potential-i18n-breakage-in-your-plugins/56824). If you do need to turn it off, add this to your configuration: `<processI18nUsage>false</processI18nUsage>` to disable it.

### Changed

- Maven 3.6.3 or later is required

## [8.2.0] - TBD

### Changed

- Upgraded to Jackson 2. This means that Jackson 1 will no longer be provided to the classpath of generate-rest-docs. This should not cause issues, but to be on the safe side failure to generate REST documentation will not fail the Maven build. Jackson 1.x is required to be on the classpath if at least one rest module is defined in the plugin XML and if the jacksonModules option is passed to the goal.

## [8.1.1] - 2020-06-16

### Fixed

- [AMPS-1504] Fixed error running remote tests (replaced deprecated Apache Wink with Apache HttpComponents' HttpClient)
- [AMPS-1524] Fixed spurious warnings about missing `common/lib/activation.jar`
- [AMPS-1530] Fixed the Integration Test console and the `remote-test` goal
- [BSP-1271] Fixed four resource leaks found by SonarQube

### Security

- Upgraded `commons-compress` to 1.20 (CVE-2019-1240)
- Upgraded `mysql-connector` to 8.0.16 (CVE-2018-3258, CVE-2019-2692)

## [8.1.0] - 2020-03-10

### Added

- Changelog
- All code and strategies for handling resource minification can be found in the `com.atlassian.maven.plugins.amps.minifier` package.
- `Minifier`: New interface for file minification strategies.
- `Sources`: New class that encapsulates multiple input files - e.g., raw source & source maps - for resource minification process.
- `AbstractProductHandler`: Added possibility to override product context with properties defined in product POM
- `AbstractWebappProductHandler`: Added overrides for webapp container artifact and `containerId` with properties defined in product POM. Properties' names:
    - `amps.product.specific.cargo.container` for the Cargo container id
    - `amps.product.specific.container` for the container artifact
- `IntegrationTestMojo`: Added `testGroup.instanceIds` property, a comma separated list of all product instances in the current testgroup.
  This can be used as an entrypoint to discover each instances complete configuration through the properties.
- System property `atlassian.allow.insecure.url.parameter.login` as default for products.

### Fixed

- [AMPS-1514] `-Dno.webapp` now correctly sets the HTTP port.

### Changed

- Google Closure Compiler is the default minifier for JS files.
- `ResourcesMinifier`: Rewritten to employ strategy pattern for selecting minification strategies.
- `IntegrationTestMojo`: Fixed the name of the product version property
- `MavenGoals`: Updated the `maven-javadoc-plugin` to fix REST doc generation for plain Maven builds

### Deprecated

- All usage of YUI Compressor is deprecated and will be removed in AMPS v9.

## [8.0.0]

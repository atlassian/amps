package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.cli.IdeaMojo;

@Mojo(name = "idea", requiresProject = false)
public class FeCruIdeaMojo extends IdeaMojo {}

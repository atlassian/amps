package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.ReleaseMojo;
import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;

@Mojo(name = "release")
public class FeCruReleaseMojo extends ReleaseMojo {
    @Override
    protected String getDefaultProductId() throws MojoExecutionException {
        return ProductHandlerFactory.FECRU;
    }
}

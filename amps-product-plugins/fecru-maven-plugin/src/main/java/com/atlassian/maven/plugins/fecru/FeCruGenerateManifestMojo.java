package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateManifestMojo;

@Mojo(name = "generate-manifest")
public class FeCruGenerateManifestMojo extends GenerateManifestMojo {}

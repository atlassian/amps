package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.PrepareDatabaseMojo;

@Mojo(name = "prepare-database", requiresDependencyResolution = ResolutionScope.TEST)
public class FeCruPrepareDatabaseMojo extends PrepareDatabaseMojo {}

package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateObrArtifactMojo;

@Mojo(name = "generate-obr-artifact")
public class FeCruGenerateObrArtifactMojo extends GenerateObrArtifactMojo {}

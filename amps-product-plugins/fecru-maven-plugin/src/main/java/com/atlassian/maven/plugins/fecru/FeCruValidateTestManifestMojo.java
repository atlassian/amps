package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateTestManifestMojo;

@Mojo(name = "validate-test-manifest")
public class FeCruValidateTestManifestMojo extends ValidateTestManifestMojo {}

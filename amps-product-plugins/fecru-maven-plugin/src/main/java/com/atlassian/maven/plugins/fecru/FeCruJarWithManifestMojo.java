package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

@Mojo(name = "jar")
public class FeCruJarWithManifestMojo extends JarWithManifestMojo {}

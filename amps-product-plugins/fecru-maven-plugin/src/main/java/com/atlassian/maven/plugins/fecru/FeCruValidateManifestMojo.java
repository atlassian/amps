package com.atlassian.maven.plugins.fecru;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateManifestMojo;

@Mojo(name = "validate-manifest")
public class FeCruValidateManifestMojo extends ValidateManifestMojo {}

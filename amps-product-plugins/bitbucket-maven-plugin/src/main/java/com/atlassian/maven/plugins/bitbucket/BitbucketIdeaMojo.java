package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.cli.IdeaMojo;

/** @since 6.1.0 */
@Mojo(name = "idea", requiresProject = false)
public class BitbucketIdeaMojo extends IdeaMojo {}

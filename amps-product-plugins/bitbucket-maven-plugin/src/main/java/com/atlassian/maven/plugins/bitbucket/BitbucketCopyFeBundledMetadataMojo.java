package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.frontend.CopyFeBundledMetadataMojo;

@Mojo(name = "copy-fe-bundled-metadata", requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class BitbucketCopyFeBundledMetadataMojo extends CopyFeBundledMetadataMojo {}

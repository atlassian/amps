package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateManifestMojo;

/** @since 6.1.0 */
@Mojo(name = "validate-manifest")
public class BitbucketValidateManifestMojo extends ValidateManifestMojo {}

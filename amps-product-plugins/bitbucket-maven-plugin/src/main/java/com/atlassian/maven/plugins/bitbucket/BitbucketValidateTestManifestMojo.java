package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateTestManifestMojo;

/** @since 6.1.0 */
@Mojo(name = "validate-test-manifest")
public class BitbucketValidateTestManifestMojo extends ValidateTestManifestMojo {}

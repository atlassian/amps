package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.PrepareDatabaseMojo;

/** @since 6.1.0 */
@Mojo(name = "prepare-database", requiresDependencyResolution = ResolutionScope.TEST)
public class BitbucketPrepareDatabaseMojo extends PrepareDatabaseMojo {}

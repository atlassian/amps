package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateTestManifestMojo;

/** @since 6.1.0 */
@Mojo(name = "generate-test-manifest")
public class BitbucketGenerateTestManifestMojo extends GenerateTestManifestMojo {}

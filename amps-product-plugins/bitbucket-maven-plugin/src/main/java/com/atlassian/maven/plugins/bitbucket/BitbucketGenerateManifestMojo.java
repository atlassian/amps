package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateManifestMojo;

/** @since 6.1.0 */
@Mojo(name = "generate-manifest")
public class BitbucketGenerateManifestMojo extends GenerateManifestMojo {}

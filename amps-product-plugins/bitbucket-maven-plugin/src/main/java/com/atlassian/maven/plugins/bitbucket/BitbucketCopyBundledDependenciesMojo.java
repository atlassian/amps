package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.CopyBundledDependenciesMojo;

/** @since 6.1.0 */
@Mojo(name = "copy-bundled-dependencies", requiresDependencyResolution = ResolutionScope.TEST)
public class BitbucketCopyBundledDependenciesMojo extends CopyBundledDependenciesMojo {}

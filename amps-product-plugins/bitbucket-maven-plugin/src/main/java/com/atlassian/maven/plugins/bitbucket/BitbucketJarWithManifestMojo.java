package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

/** @since 6.1.0 */
@Mojo(name = "jar")
public class BitbucketJarWithManifestMojo extends JarWithManifestMojo {}

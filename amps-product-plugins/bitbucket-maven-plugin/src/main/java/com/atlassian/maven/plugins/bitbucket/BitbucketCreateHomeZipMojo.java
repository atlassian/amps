package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.CreateHomeZipMojo;

/** @since 6.1.0 */
@Mojo(name = "create-home-zip")
public class BitbucketCreateHomeZipMojo extends CreateHomeZipMojo {}

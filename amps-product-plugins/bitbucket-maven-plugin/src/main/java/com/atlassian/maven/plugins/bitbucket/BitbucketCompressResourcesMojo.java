package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.CompressResourcesMojo;

/** @since 6.1.0 */
@Mojo(name = "compress-resources")
public class BitbucketCompressResourcesMojo extends CompressResourcesMojo {}

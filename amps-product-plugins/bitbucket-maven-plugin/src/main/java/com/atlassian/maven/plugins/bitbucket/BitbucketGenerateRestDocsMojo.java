package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.GenerateRestDocsMojo;

/** @since 6.1.0 */
@Mojo(name = "generate-rest-docs", requiresDependencyResolution = ResolutionScope.TEST)
public class BitbucketGenerateRestDocsMojo extends GenerateRestDocsMojo {}

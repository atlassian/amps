package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.ValidateBannedDependenciesMojo;

@Mojo(name = "validate-banned-dependencies")
public class BitbucketValidateBannedDependenciesMojo extends ValidateBannedDependenciesMojo {}

package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.FilterPluginDescriptorMojo;

/** @since 6.1.0 */
@Mojo(name = "filter-plugin-descriptor")
public class BitbucketFilterPluginDescriptorMojo extends FilterPluginDescriptorMojo {}

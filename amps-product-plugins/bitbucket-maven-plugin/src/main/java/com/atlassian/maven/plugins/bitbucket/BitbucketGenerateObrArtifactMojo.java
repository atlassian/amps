package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateObrArtifactMojo;

/** @since 6.1.0 */
@Mojo(name = "generate-obr-artifact")
public class BitbucketGenerateObrArtifactMojo extends GenerateObrArtifactMojo {}

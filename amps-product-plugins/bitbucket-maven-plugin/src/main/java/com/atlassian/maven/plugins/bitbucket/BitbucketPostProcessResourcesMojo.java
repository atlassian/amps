package com.atlassian.maven.plugins.bitbucket;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;

@Mojo(name = "post-process-resources")
public class BitbucketPostProcessResourcesMojo extends PostProcessResourcesMojo {}

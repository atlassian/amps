package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateManifestMojo;

@Mojo(name = "generate-manifest")
public class CrowdGenerateManifestMojo extends GenerateManifestMojo {}

package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateManifestMojo;

@Mojo(name = "validate-manifest")
public class CrowdValidateManifestMojo extends ValidateManifestMojo {}

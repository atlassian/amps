package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

@Mojo(name = "jar")
public class CrowdJarWithManifestMojo extends JarWithManifestMojo {}

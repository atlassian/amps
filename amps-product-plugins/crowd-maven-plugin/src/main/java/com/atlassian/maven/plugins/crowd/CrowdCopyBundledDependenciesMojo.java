package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.CopyBundledDependenciesMojo;

@Mojo(name = "copy-bundled-dependencies", requiresDependencyResolution = ResolutionScope.TEST)
public class CrowdCopyBundledDependenciesMojo extends CopyBundledDependenciesMojo {}

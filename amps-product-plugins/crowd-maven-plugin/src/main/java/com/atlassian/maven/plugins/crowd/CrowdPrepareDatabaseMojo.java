package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.PrepareDatabaseMojo;

@Mojo(name = "prepare-database", requiresDependencyResolution = ResolutionScope.TEST)
public class CrowdPrepareDatabaseMojo extends PrepareDatabaseMojo {}

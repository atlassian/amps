package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.TestJarMojo;

@Mojo(name = "test-jar", requiresDependencyResolution = ResolutionScope.TEST)
public class CrowdTestJarMojo extends TestJarMojo {}

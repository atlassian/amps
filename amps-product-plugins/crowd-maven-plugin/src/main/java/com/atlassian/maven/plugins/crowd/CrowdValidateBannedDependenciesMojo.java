package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.ValidateBannedDependenciesMojo;

@Mojo(name = "validate-banned-dependencies")
public class CrowdValidateBannedDependenciesMojo extends ValidateBannedDependenciesMojo {}

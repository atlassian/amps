package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.FilterTestPluginDescriptorMojo;

@Mojo(name = "filter-test-plugin-descriptor")
public class CrowdFilterTestPluginDescriptorMojo extends FilterTestPluginDescriptorMojo {}

package com.atlassian.maven.plugins.crowd;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.FilterPluginDescriptorMojo;

@Mojo(name = "filter-plugin-descriptor")
public class CrowdFilterPluginDescriptorMojo extends FilterPluginDescriptorMojo {}

package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.FilterTestPluginDescriptorMojo;

@Mojo(name = "filter-test-plugin-descriptor")
public class BambooFilterTestPluginDescriptorMojo extends FilterTestPluginDescriptorMojo {}

package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.PrepareDatabaseMojo;

@Mojo(name = "prepare-database", requiresDependencyResolution = ResolutionScope.TEST)
public class BambooPrepareDatabaseMojo extends PrepareDatabaseMojo {}

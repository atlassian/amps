package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.FilterPluginDescriptorMojo;

@Mojo(name = "filter-plugin-descriptor")
public class BambooFilterPluginDescriptorMojo extends FilterPluginDescriptorMojo {}

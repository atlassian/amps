package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.CopyBundledDependenciesMojo;

@Mojo(name = "copy-bundled-dependencies", requiresDependencyResolution = ResolutionScope.TEST)
public class BambooCopyBundledDependenciesMojo extends CopyBundledDependenciesMojo {}

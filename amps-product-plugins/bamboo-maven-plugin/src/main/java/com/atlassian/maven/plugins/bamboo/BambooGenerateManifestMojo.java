package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateManifestMojo;

@Mojo(name = "generate-manifest")
public class BambooGenerateManifestMojo extends GenerateManifestMojo {}

package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.UnitTestMojo;

@Mojo(name = "unit-test", requiresDependencyResolution = ResolutionScope.TEST)
public class BambooUnitTestMojo extends UnitTestMojo {}

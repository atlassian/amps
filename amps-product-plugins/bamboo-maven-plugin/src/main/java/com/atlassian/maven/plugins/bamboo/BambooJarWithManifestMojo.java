package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

@Mojo(name = "jar")
public class BambooJarWithManifestMojo extends JarWithManifestMojo {}

package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.cli.IdeaMojo;

@Mojo(name = "idea", requiresProject = false)
public class BambooIdeaMojo extends IdeaMojo {}

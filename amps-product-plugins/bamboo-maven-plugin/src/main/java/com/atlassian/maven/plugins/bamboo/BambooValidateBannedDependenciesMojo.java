package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.ValidateBannedDependenciesMojo;

@Mojo(name = "validate-banned-dependencies")
public class BambooValidateBannedDependenciesMojo extends ValidateBannedDependenciesMojo {}

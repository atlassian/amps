package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateTestManifestMojo;

@Mojo(name = "validate-test-manifest")
public class BambooValidateTestManifestMojo extends ValidateTestManifestMojo {}

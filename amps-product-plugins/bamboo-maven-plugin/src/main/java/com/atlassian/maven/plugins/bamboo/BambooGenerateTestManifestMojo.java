package com.atlassian.maven.plugins.bamboo;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateTestManifestMojo;

@Mojo(name = "generate-test-manifest")
public class BambooGenerateTestManifestMojo extends GenerateTestManifestMojo {}

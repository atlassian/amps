package com.atlassian.maven.plugins.jira;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;

public class JiraIntegrationTestMojoTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MavenProject mavenProject;

    private final JiraIntegrationTestMojo jiraIntegrationTestMojo = new JiraIntegrationTestMojo() {
        @Override
        protected MavenContext getMavenContext() {
            return mavenContext;
        }
    };

    @Before
    public void setUp() {
        when(mavenContext.getProject()).thenReturn(mavenProject);
    }

    @Test
    public void getProductFunctionalTestProperties_whenUsingHttps_shouldUseHttpsPort() {
        // Set up
        final Product product = mock(Product.class);
        final Node node = mock(Node.class);
        when(product.getNodes()).thenReturn(singletonList(node));
        when(product.getWebPortForNode(0)).thenReturn(42);

        // Invoke
        final Map<String, String> testProperties = jiraIntegrationTestMojo.getProductFunctionalTestProperties(product);

        // Check
        assertThat(testProperties, hasEntry("jira.port", "42"));
    }

    @Test
    public void getProductFunctionalTestProperties_whenMultiNode_shouldReturnPerNodeProperties() {
        // Set up
        final Product product = mock(Product.class);
        final Node node0 = mock(Node.class);
        final Node node1 = mock(Node.class);
        when(product.getNodes()).thenReturn(asList(node0, node1));
        when(product.getWebPortForNode(0)).thenReturn(40);
        when(product.getWebPortForNode(1)).thenReturn(41);

        // Invoke
        final Map<String, String> testProperties = jiraIntegrationTestMojo.getProductFunctionalTestProperties(product);

        // Check
        assertThat(testProperties, hasEntry("jira.port", "40"));
        assertThat(testProperties, hasEntry("jira.port.0", "40"));
        assertThat(testProperties, hasEntry("jira.port.1", "41"));
    }
}

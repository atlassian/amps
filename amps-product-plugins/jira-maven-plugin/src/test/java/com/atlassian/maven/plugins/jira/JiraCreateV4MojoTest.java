package com.atlassian.maven.plugins.jira;

import static org.codehaus.plexus.util.ReflectionUtils.getFieldByNameIncludingSuperclasses;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Field;

import org.junit.Test;

import com.atlassian.maven.plugins.amps.PluginInformation;

public class JiraCreateV4MojoTest {

    @Test
    public void getPluginInformation_shouldReturnCorrectInformation() throws Exception {
        // Arrange
        final JiraCreateV4Mojo mojo = new JiraCreateV4Mojo();
        final Field pluginVersionField = getFieldByNameIncludingSuperclasses("pluginVersion", JiraCreateV4Mojo.class);
        pluginVersionField.setAccessible(true);
        final String pluginVersion = "thePluginVersion";
        pluginVersionField.set(mojo, pluginVersion);

        // Act
        final PluginInformation pluginInformation = mojo.getPluginInformation();

        // Assert
        assertThat(pluginInformation.getGroupId(), is("com.atlassian.maven.plugins"));
        assertThat(pluginInformation.getArtifactId(), is("maven-jira4-plugin"));
        assertThat(pluginInformation.getVersion(), is(pluginVersion));
        assertThat(pluginInformation.getProductId(), is("jira4"));
    }
}

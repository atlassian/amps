package com.atlassian.maven.plugins.jira;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateManifestMojo;

@Mojo(name = "validate-manifest")
public class JiraValidateManifestMojo extends ValidateManifestMojo {}

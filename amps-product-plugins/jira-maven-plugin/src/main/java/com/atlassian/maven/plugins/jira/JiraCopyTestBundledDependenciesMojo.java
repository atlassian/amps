package com.atlassian.maven.plugins.jira;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.CopyTestBundledDependenciesMojo;

@Mojo(name = "copy-test-bundled-dependencies", requiresDependencyResolution = ResolutionScope.TEST)
public class JiraCopyTestBundledDependenciesMojo extends CopyTestBundledDependenciesMojo {}

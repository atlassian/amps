package com.atlassian.maven.plugins.jira;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;

@Mojo(name = "post-process-resources")
public class JiraPostProcessResourcesMojo extends PostProcessResourcesMojo {}

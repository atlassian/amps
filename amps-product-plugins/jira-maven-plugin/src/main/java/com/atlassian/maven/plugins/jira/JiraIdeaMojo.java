package com.atlassian.maven.plugins.jira;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.cli.IdeaMojo;

@Mojo(name = "idea", requiresProject = false)
public class JiraIdeaMojo extends IdeaMojo {}

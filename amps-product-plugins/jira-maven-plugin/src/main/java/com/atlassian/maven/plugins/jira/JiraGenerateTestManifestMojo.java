package com.atlassian.maven.plugins.jira;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateTestManifestMojo;

@Mojo(name = "generate-test-manifest")
public class JiraGenerateTestManifestMojo extends GenerateTestManifestMojo {}

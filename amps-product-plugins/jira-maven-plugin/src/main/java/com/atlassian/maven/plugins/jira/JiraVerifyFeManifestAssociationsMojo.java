package com.atlassian.maven.plugins.jira;

import static org.apache.maven.plugins.annotations.LifecyclePhase.VERIFY;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.ReportFeManifestAssociationsMojo;
import com.atlassian.maven.plugins.amps.frontend.association.verification.VerifyFeManifestAssociationsMojo;
import com.atlassian.maven.plugins.amps.frontend.association.verification.impl.ArtifactScannerImpl;

/**
 * Verifies if all javascript files in a given artifact have corresponding manifest association created by
 * {@link ReportFeManifestAssociationsMojo}. It accepts all standard archive types:
 * {@link ArtifactScannerImpl#SUPPORTED_ARCHIVE_EXTENSIONS}. And also works with directories.
 *
 * @since 8.17.7
 * @since 9.0.6 Default phase set to `VERIFY`
 */
@Mojo(name = "verify-fe-manifest-associations", defaultPhase = VERIFY, requiresProject = false)
public class JiraVerifyFeManifestAssociationsMojo extends VerifyFeManifestAssociationsMojo {}

package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.PrepareDatabaseMojo;

@Mojo(name = "prepare-database", requiresDependencyResolution = ResolutionScope.TEST)
public class RefappPrepareDatabaseMojo extends PrepareDatabaseMojo {}

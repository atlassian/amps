package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateObrArtifactMojo;

@Mojo(name = "generate-obr-artifact")
public class RefappGenerateObrArtifactMojo extends GenerateObrArtifactMojo {}

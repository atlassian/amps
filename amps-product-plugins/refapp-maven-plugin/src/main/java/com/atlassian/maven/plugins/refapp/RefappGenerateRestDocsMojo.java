package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.GenerateRestDocsMojo;

/** @since 3.6.1 */
@Mojo(name = "generate-rest-docs", requiresDependencyResolution = ResolutionScope.TEST)
public class RefappGenerateRestDocsMojo extends GenerateRestDocsMojo {}

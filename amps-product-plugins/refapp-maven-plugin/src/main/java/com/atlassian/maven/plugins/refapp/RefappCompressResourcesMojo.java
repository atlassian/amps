package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.CompressResourcesMojo;

@Mojo(name = "compress-resources")
public class RefappCompressResourcesMojo extends CompressResourcesMojo {}

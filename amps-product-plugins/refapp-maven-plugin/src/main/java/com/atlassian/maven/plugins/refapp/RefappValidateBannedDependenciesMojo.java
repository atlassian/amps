package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.ValidateBannedDependenciesMojo;

@Mojo(name = "validate-banned-dependencies")
public class RefappValidateBannedDependenciesMojo extends ValidateBannedDependenciesMojo {}

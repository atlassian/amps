package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.cli.IdeaMojo;

@Mojo(name = "idea", requiresProject = false)
public class RefappIdeaMojo extends IdeaMojo {}

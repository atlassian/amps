package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateTestManifestMojo;

@Mojo(name = "validate-test-manifest")
public class RefappValidateTestManifestMojo extends ValidateTestManifestMojo {}

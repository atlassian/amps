package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

@Mojo(name = "jar")
public class RefappJarWithManifestMojo extends JarWithManifestMojo {}

package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.pdk.InstallMojo;
import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;

@Mojo(name = "install")
public class RefappInstallMojo extends InstallMojo {
    @Override
    protected String getDefaultProductId() throws MojoExecutionException {
        return ProductHandlerFactory.REFAPP;
    }
}

package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;

@Mojo(name = "post-process-resources")
public class RefappPostProcessResourcesMojo extends PostProcessResourcesMojo {}

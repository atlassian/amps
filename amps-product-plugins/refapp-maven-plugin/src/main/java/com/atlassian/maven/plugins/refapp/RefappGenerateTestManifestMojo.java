package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateTestManifestMojo;

@Mojo(name = "generate-test-manifest")
public class RefappGenerateTestManifestMojo extends GenerateTestManifestMojo {}

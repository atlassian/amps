package com.atlassian.maven.plugins.refapp;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.UnitTestMojo;

@Mojo(name = "unit-test", requiresDependencyResolution = ResolutionScope.TEST)
public class RefappUnitTestMojo extends UnitTestMojo {}

package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

@Mojo(name = "jar")
public class ConfluenceJarWithManifestMojo extends JarWithManifestMojo {}

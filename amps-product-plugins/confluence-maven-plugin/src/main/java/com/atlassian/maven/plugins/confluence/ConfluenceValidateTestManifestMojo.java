package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.ValidateTestManifestMojo;

@Mojo(name = "validate-test-manifest")
public class ConfluenceValidateTestManifestMojo extends ValidateTestManifestMojo {}

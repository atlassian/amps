package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateManifestMojo;

@Mojo(name = "generate-manifest")
public class ConfluenceGenerateManifestMojo extends GenerateManifestMojo {}

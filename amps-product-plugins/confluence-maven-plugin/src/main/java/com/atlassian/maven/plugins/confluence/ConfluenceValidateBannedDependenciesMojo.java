package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.ValidateBannedDependenciesMojo;

@Mojo(name = "validate-banned-dependencies")
public class ConfluenceValidateBannedDependenciesMojo extends ValidateBannedDependenciesMojo {}

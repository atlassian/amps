package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.atlassian.maven.plugins.amps.IntegrationTestMojo;
import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;

@Mojo(name = "integration-test", requiresDependencyResolution = ResolutionScope.TEST)
public class ConfluenceIntegrationTestMojo extends IntegrationTestMojo {
    @Override
    protected String getDefaultProductId() throws MojoExecutionException {
        return ProductHandlerFactory.CONFLUENCE;
    }
}

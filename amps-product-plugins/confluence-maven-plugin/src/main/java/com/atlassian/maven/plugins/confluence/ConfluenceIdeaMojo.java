package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.cli.IdeaMojo;

@Mojo(name = "idea", requiresProject = false)
public class ConfluenceIdeaMojo extends IdeaMojo {}

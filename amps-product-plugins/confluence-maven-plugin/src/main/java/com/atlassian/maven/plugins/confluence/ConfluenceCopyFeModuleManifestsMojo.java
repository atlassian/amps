package com.atlassian.maven.plugins.confluence;

import static org.apache.maven.plugins.annotations.LifecyclePhase.PROCESS_RESOURCES;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.frontend.CopyFeModuleManifestsMojo;

/**
 * Copies frontend module manifest files into {@code META-INF/fe-module-manifests}.
 *
 * @since 8.13.0
 * @since 9.0.6 Default phase set to `PROCESS_RESOURCES`
 */
@Mojo(name = "copy-fe-module-manifests", defaultPhase = PROCESS_RESOURCES)
public class ConfluenceCopyFeModuleManifestsMojo extends CopyFeModuleManifestsMojo {}

package com.atlassian.maven.plugins.confluence;

import static org.apache.maven.plugins.annotations.LifecyclePhase.PREPARE_PACKAGE;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.frontend.association.mapping.ReportFeManifestAssociationsMojo;

/**
 * Creates a mapping association between output js files and fe module manifest file provided in configuration or from
 * WRM plugin (webpack, vite, etc). Prepared association is output to
 * {@code META-INF/fe-manifest-associations/fe-manifest-association.json}.
 *
 * @since 8.15.0
 * @since 8.17.6 Accepts all `.intermediary.json` files
 * @since 9.0.6 Default phase set to `PREPARE_PACKAGE`
 */
@Mojo(name = "report-fe-manifest-associations", defaultPhase = PREPARE_PACKAGE)
public class ConfluenceReportFeManifestAssociationsMojo extends ReportFeManifestAssociationsMojo {}

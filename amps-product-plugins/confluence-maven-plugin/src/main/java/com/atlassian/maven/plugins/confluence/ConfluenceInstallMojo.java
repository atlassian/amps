package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.pdk.InstallMojo;
import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;

@Mojo(name = "install")
public class ConfluenceInstallMojo extends InstallMojo {
    @Override
    protected String getDefaultProductId() throws MojoExecutionException {
        return ProductHandlerFactory.CONFLUENCE;
    }
}

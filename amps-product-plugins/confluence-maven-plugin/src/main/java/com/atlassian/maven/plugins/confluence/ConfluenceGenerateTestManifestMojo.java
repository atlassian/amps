package com.atlassian.maven.plugins.confluence;

import org.apache.maven.plugins.annotations.Mojo;

import com.atlassian.maven.plugins.amps.osgi.GenerateTestManifestMojo;

@Mojo(name = "generate-test-manifest")
public class ConfluenceGenerateTestManifestMojo extends GenerateTestManifestMojo {}

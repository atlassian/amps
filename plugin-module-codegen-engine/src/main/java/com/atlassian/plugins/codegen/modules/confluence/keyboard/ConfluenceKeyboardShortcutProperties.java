package com.atlassian.plugins.codegen.modules.confluence.keyboard;

import java.util.List;

import com.google.common.collect.Lists;

import com.atlassian.plugins.codegen.modules.common.keyboard.AbstractKeyboardShortcutProperties;

public class ConfluenceKeyboardShortcutProperties extends AbstractKeyboardShortcutProperties {

    private static final List<String> CONFLUENCE_CONTEXTS = Lists.newArrayList("viewcontent", "viewinfo");

    public ConfluenceKeyboardShortcutProperties(String moduleName) {
        super(moduleName);
    }

    @Override
    protected List<String> getAdditionalContexts() {
        return CONFLUENCE_CONTEXTS;
    }
}

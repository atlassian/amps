package com.atlassian.plugins.codegen.modules;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.io.File;

import org.junit.Test;

public class PluginModuleLocationTest {

    @Test
    public void getDefaultPluginKey_shouldReturnGroupIdPlusArtifactId() {
        // Arrange
        final File sourceDir = mock(File.class);
        final String groupId = "theGroupId";
        final String artifactId = "theArtifactId";
        final PluginModuleLocation pluginModuleLocation = new PluginModuleLocation.Builder(sourceDir)
                .groupAndArtifactId(groupId, artifactId)
                .build();

        // Act
        final String pluginKey = pluginModuleLocation.getDefaultPluginKey();

        // Assert
        assertThat(pluginKey, is(groupId + "." + artifactId));
    }
}

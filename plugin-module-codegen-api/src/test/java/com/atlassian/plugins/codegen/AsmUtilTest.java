package com.atlassian.plugins.codegen;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class AsmUtilTest {

    @Test
    public void asmOpCode_shouldBeGreaterThanZero() {
        assertThat(AsmUtil.asmOpCode(), greaterThan(0));
    }
}

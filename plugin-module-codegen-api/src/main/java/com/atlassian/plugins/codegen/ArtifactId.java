package com.atlassian.plugins.codegen;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;

import java.util.Optional;

/** Simple wrapper for a Maven artifact ID with optional group ID. */
public final class ArtifactId {
    /**
     * Factory method for no {@code groupId}.
     *
     * @param artifactId the Maven artifact id
     * @return a new instance
     */
    public static ArtifactId artifactId(final String artifactId) {
        return new ArtifactId(empty(), artifactId);
    }

    /**
     * Factory method for both a {@code groupId} and an {@code artifactId}.
     *
     * @param groupId the Maven group id
     * @param artifactId the Maven artifact id
     * @return a new instance
     */
    public static ArtifactId artifactId(String groupId, String artifactId) {
        return new ArtifactId(Optional.of(groupId), artifactId);
    }

    /**
     * Factory method for an optional {@code groupId}.
     *
     * @param groupId the Maven group id, if any
     * @param artifactId the Maven artifact id
     * @return a new instance
     */
    public static ArtifactId artifactId(Optional<String> groupId, String artifactId) {
        return new ArtifactId(groupId, artifactId);
    }

    private final Optional<String> groupId;
    private final String artifactIdStr;

    private ArtifactId(final Optional<String> groupId, final String artifactId) {
        this.groupId = requireNonNull(groupId, "groupId");
        this.artifactIdStr = requireNonNull(artifactId, "artifactId");
    }

    public Optional<String> getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactIdStr;
    }

    public String getCombinedId() {
        final String prefix = groupId.map(g -> g + ":").orElse("");
        return prefix + artifactIdStr;
    }

    @Override
    public String toString() {
        return getCombinedId();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ArtifactId) {
            ArtifactId c = (ArtifactId) other;
            return groupId.equals(c.groupId) && artifactIdStr.equals(c.artifactIdStr);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getCombinedId().hashCode();
    }
}

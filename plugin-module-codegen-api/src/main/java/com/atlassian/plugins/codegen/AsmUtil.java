package com.atlassian.plugins.codegen;

import static org.objectweb.asm.Opcodes.ASM9;

/**
 * Utility methods relating to <a href="https://asm.ow2.io">asm</a>.
 *
 * @since 8.2.2
 */
public final class AsmUtil {

    /**
     * Returns the JVM opcode that ASM should use.
     *
     * @return a valid opcode
     */
    public static int asmOpCode() {
        return ASM9;
    }

    private AsmUtil() {}
}

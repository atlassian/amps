package com.atlassian.maven.plugins.amps.upm.signing.tools;

public enum AtlassianCertificate {
    MARKETPLACE_STAGING_ROOT(
            "atl_root_dev.pem",
            format("MIIBzjCCAYCgAwIBAgIUcdrM45sJOTHrr+6+pJCPVlhuZA4wBQYDK2VwMFsxCzAJ\n"
                    + "BgNVBAYTAkFVMQwwCgYDVQQIDANOU1cxDzANBgNVBAcMBlN5ZG5leTEtMCsGA1UE\n"
                    + "AwwkQXRsYXNzaWFuIEFTIFJvb3QgQ0EgREVWIE5PX1BST0RfVVNFMB4XDTI0MDgy\n"
                    + "MTA3NTAxM1oXDTI5MDgyMTA3NTAxM1owWzELMAkGA1UEBhMCQVUxDDAKBgNVBAgM\n"
                    + "A05TVzEPMA0GA1UEBwwGU3lkbmV5MS0wKwYDVQQDDCRBdGxhc3NpYW4gQVMgUm9v\n"
                    + "dCBDQSBERVYgTk9fUFJPRF9VU0UwKjAFBgMrZXADIQDzgOHHlhFSUCVdcClpjeRV\n"
                    + "kk4kQdInbjT27dXxmvzthKNWMFQwHQYDVR0OBBYEFOdf2y5SEJ0k3D22x5XuXPEe\n"
                    + "ySwBMB8GA1UdIwQYMBaAFOdf2y5SEJ0k3D22x5XuXPEeySwBMBIGA1UdEwEB/wQI\n"
                    + "MAYBAf8CAQEwBQYDK2VwA0EA1t7/meNg+do0SbD8knmPmH547ck1dYMaJex48Y7+\n"
                    + "ipJx2We6vMc6zFESw2r5BYsR7zc2762YyzVjpvkVMLrqBQ==\n")),
    MARKETPLACE_STAGING_INTERMEDIATE(
            "atl_intermediate_dev.pem",
            format("MIIB1jCCAYigAwIBAgIUGbYjRrIDwTIC/l8Oy7rIChPGxiswBQYDK2VwMFsxCzAJ\n"
                    + "BgNVBAYTAkFVMQwwCgYDVQQIDANOU1cxDzANBgNVBAcMBlN5ZG5leTEtMCsGA1UE\n"
                    + "AwwkQXRsYXNzaWFuIEFTIFJvb3QgQ0EgREVWIE5PX1BST0RfVVNFMB4XDTI0MDgy\n"
                    + "MTA3NTE1OFoXDTI1MDgyMTA3NTE1OFowYzELMAkGA1UEBhMCQVUxDDAKBgNVBAgM\n"
                    + "A05TVzEPMA0GA1UEBwwGU3lkbmV5MTUwMwYDVQQDDCxBdGxhc3NpYW4gQVMgSW50\n"
                    + "ZXJtZWRpYXRlIENBIERFViBOT19QUk9EX1VTRTAqMAUGAytlcAMhAIn2HkyfIoj9\n"
                    + "MnKAg4e9oc5VGObyP7VuoKHhHcsUwAXWo1YwVDAdBgNVHQ4EFgQUtNdUFB1fZkkD\n"
                    + "d2hmCbSjEIJpccMwHwYDVR0jBBgwFoAU51/bLlIQnSTcPbbHle5c8R7JLAEwEgYD\n"
                    + "VR0TAQH/BAgwBgEB/wIBADAFBgMrZXADQQC+ZrltqsK70pD5W98FK/GMhePW3E0/\n"
                    + "EDMT0iV8RthzeNI2BFPBzOI+kDGbajSqG2eLvH6gTbwxg5Y+uPfgH54K\n")),
    MARKETPLACE_ROOT_V1(
            "atlassian_mpac_intermediate_ca_v1.crt",
            format("MIIBiDCCATqgAwIBAgIUYRpJO0DR/qqKlSw6SOAl3C5iPcswBQYDK2VwMDgxEjAQ\n"
                    + "BgNVBAoMCUF0bGFzc2lhbjEiMCAGA1UEAwwZTWFya2V0cGxhY2UgQVMgUm9vdCBD\n"
                    + "QSB2MTAeFw0yNDEyMzEwMDAwMDBaFw0zNDAxMDEwMDAwMDBaMDgxEjAQBgNVBAoM\n"
                    + "CUF0bGFzc2lhbjEiMCAGA1UEAwwZTWFya2V0cGxhY2UgQVMgUm9vdCBDQSB2MTAq\n"
                    + "MAUGAytlcAMhAK3EQa3ZukwKrUcMmp210dN+x6pwF6qzVHiNi/EB1jUTo1YwVDAd\n"
                    + "BgNVHQ4EFgQU7hrebw3cARlhhkaxF8mN03PbMOowHwYDVR0jBBgwFoAU7hrebw3c\n"
                    + "ARlhhkaxF8mN03PbMOowEgYDVR0TAQH/BAgwBgEB/wIBATAFBgMrZXADQQAx0Uvv\n"
                    + "0seDT1ncRodzIOFG5n9PdNCc51zGtNkQL4IUidgHRiA7/yAg9hCYkM9AFOUMJHLJ\n"
                    + "MCjxSPSro6vKDtgP\n")),
    MARKETPLACE_INTERMEDIATE_V1(
            "atlassian_mpac_root_ca_v1.crt",
            format("MIIBkDCCAUKgAwIBAgIUeN3VUUZtMDpOMj4/53HYAEg7fe8wBQYDK2VwMDgxEjAQ\n"
                    + "BgNVBAoMCUF0bGFzc2lhbjEiMCAGA1UEAwwZTWFya2V0cGxhY2UgQVMgUm9vdCBD\n"
                    + "QSB2MTAeFw0yNDEyMzEwMDAwMDBaFw0yNzAxMTAwMDAwMDBaMEAxEjAQBgNVBAoM\n"
                    + "CUF0bGFzc2lhbjEqMCgGA1UEAwwhTWFya2V0cGxhY2UgQVMgSW50ZXJtZWRpYXRl\n"
                    + "IENBIHYxMCowBQYDK2VwAyEAJMOA3thx/gEukg1W/Mvi0buKyWW9VUZk5CR1n3W9\n"
                    + "hT6jVjBUMB0GA1UdDgQWBBS9amfvKQ0bdj8sD9pzs6+jwTkYfDAfBgNVHSMEGDAW\n"
                    + "gBTuGt5vDdwBGWGGRrEXyY3Tc9sw6jASBgNVHRMBAf8ECDAGAQH/AgEAMAUGAytl\n"
                    + "cANBABqdcd+nGTO+2spHj77Gvn6O5qdDSniwKegNNNsH6HKCNlvZVEomEagf0jmT\n"
                    + "7/D9KijNXJTgdkhDIyQaW2H3eA0=\n"));

    private final String filename;
    private final String certificate;

    private static String format(String certificate) {
        StringBuilder sb = new StringBuilder("-----BEGIN CERTIFICATE-----\n");
        sb.append(certificate).append("-----END CERTIFICATE-----\n");
        return sb.toString();
    }

    AtlassianCertificate(String filename, String certificate) {
        this.filename = filename;
        this.certificate = certificate;
    }

    public String getFileName() {
        return filename;
    }

    public String getCertificate() {
        return certificate;
    }
}

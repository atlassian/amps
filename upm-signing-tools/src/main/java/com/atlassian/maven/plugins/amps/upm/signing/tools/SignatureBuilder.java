package com.atlassian.maven.plugins.amps.upm.signing.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.util.Base64;
import java.util.function.BiConsumer;

import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** This class can be used to sign artifacts given a Private key / certificate pair. */
public class SignatureBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignatureBuilder.class);

    private final Ed25519Signer signer;
    private final PrivateKeyAndCert privateKeyAndCert;

    public static SignatureBuilder withPrivateKey(String privateKey) {
        return new SignatureBuilder(new PrivateKeyAndCert(privateKey, null));
    }

    public SignatureBuilder(PrivateKeyAndCert privateKeyAndCert) {
        this.privateKeyAndCert = privateKeyAndCert;
        this.signer = privateKeyAndCert == null
                ? null
                : privateKeyAndCert.buildSigner().orElse(null);
    }

    private <T> String forgeSignature(T data, BiConsumer<T, Ed25519Signer> writer) {
        if (signer == null) {
            LOGGER.warn("Null signer");
            return null;
        }
        try {
            signer.reset();
            writer.accept(data, signer);
            byte[] signatureBytes = signer.generateSignature();
            return Base64.getEncoder().encodeToString(signatureBytes);
        } catch (Exception e) {
            LOGGER.warn("Failed to sign plugin", e);
            return null;
        }
    }

    public String forgeSignature(File file) {
        try (InputStream in = Files.newInputStream(file.toPath())) {
            return forgeSignature(in);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String forgeSignature(byte[] dataBytes) {
        return forgeSignature(dataBytes, (data, s) -> s.update(data, 0, data.length));
    }

    public String forgeSignature(InputStream inputStream) {
        return forgeSignature(inputStream, (in, s) -> {
            try {
                byte[] data = new byte[1024];
                int len = in.read(data);
                while (len != -1) {
                    s.update(data, 0, len);
                    len = in.read(data);
                }
            } catch (IOException io) {
                throw new UncheckedIOException(io);
            }
        });
    }

    public static Builder builder(String issuer) {
        return new Builder(issuer);
    }

    public PrivateKeyAndCert getPrivateKeyAndCert() {
        return privateKeyAndCert;
    }

    public static class Builder {
        private ZonedDateTime from;
        private ZonedDateTime to;
        private final String issuer;

        private Builder(String issuer) {
            this.issuer = issuer;
        }

        public Builder from(ZonedDateTime from) {
            this.from = from;
            return this;
        }

        public Builder to(ZonedDateTime to) {
            this.to = to;
            return this;
        }

        public SignatureBuilder build() {
            ZonedDateTime start = from == null ? ZonedDateTime.now() : from;
            ZonedDateTime end = to == null ? start.plusDays(1) : to;
            PrivateKeyAndCert privateKeyAndCert =
                    PrivateKeyAndCert.generateSelfSignedX509Certificate(start, end, issuer);
            return new SignatureBuilder(privateKeyAndCert);
        }
    }
}

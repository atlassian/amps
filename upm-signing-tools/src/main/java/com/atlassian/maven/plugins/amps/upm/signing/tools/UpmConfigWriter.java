package com.atlassian.maven.plugins.amps.upm.signing.tools;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.Set;
import java.util.function.BooleanSupplier;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This utility class builds UPM plugin signature check configuration with the following elements : - the upm.properties
 * configuration file, - the truststore, - an additional keyStore, to keep track of private keys associated with custom
 * certificates. They can be used later to sign plugins for tests.
 */
public class UpmConfigWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmConfigWriter.class);

    public static final String TRUSTSTORE_FOLDER = "truststore";
    public static final String PK_FOLDER = "pks";
    public static final String CONFIG_FILENAME = "upm.properties";

    public static final String CERT_PEM = "-cert.pem";
    public static final String PK_PEM = "-pk.pem";

    private final Path configPath;
    private final Path truststore;
    private final Path privateKeysStore;

    public UpmConfigWriter(Path configPath) {
        this.configPath = configPath;
        truststore = configPath.resolve(TRUSTSTORE_FOLDER);
        privateKeysStore = configPath.resolve(PK_FOLDER);
        File f = configPath.toFile();
        if (f.exists() && !f.isDirectory()) {
            throw new IllegalArgumentException("UPM Configuration Location exists and is not a directory");
        }
    }

    public void deleteConfiguration() {
        if (configPath.toFile().exists()) {
            makeConfigWritable();
            try (Stream<Path> paths = Files.walk(configPath, 3)) {
                paths.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            } catch (IOException ioe) {
                throw new UncheckedIOException(ioe);
            }
        }
    }

    public boolean createConfig(boolean deleteIfExists, Set<AtlassianCertificate> marketplaceCertificates) {
        boolean configExists = configPath.toFile().exists();

        if (configExists && deleteIfExists) {
            makeConfigWritable();
            deleteConfiguration();
            configExists = false;
        }
        if (!configExists) {
            configPath.toFile().mkdirs();
        }
        if (!Files.exists(truststore)) {
            truststore.toFile().mkdirs();
        }
        if (!Files.exists(privateKeysStore)) {
            privateKeysStore.toFile().mkdirs();
        }
        Path configFile = configPath.resolve(CONFIG_FILENAME);
        makeConfigWritable();
        if (!Files.exists(configFile)) {
            try {
                Files.write(
                        configFile,
                        "atlassian.upm.signature.check.enabled=true\nsecuritycheck.strict=false"
                                .getBytes(StandardCharsets.UTF_8));
            } catch (IOException ioe) {
                throw new IllegalStateException("Cannot write upm.properties file", ioe);
            }
        }
        if (marketplaceCertificates != null) {
            marketplaceCertificates.forEach(mc -> writeCertificate(mc.getFileName(), mc.getCertificate()));
        }
        makeConfigReadonly();
        return !configExists;
    }

    /**
     * Can be used to add custom certificates to the truststore
     *
     * @param filename the name of the file to write to.
     * @param certificate the certificate.
     */
    public void writeCertificate(String filename, String certificate) {
        Path target = truststore.resolve(filename);
        if (!Files.exists(target)) {
            try {
                Files.write(
                        target,
                        certificate.getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException ioe) {
                throw new UncheckedIOException("Failed to write certificate to " + target, ioe);
            }
        }
    }

    private boolean doWithConfigWriteAccess(BooleanSupplier task) {
        try {
            makeConfigWritable();
            return task.getAsBoolean();
        } catch (Exception ioe) {
            LOGGER.warn("Failed to write to truststore", ioe);
            return false;
        } finally {
            makeConfigReadonly();
        }
    }

    public PrivateKeyAndCert getPrivateKeyAndCertificate(String name) {
        Path certificatePath = getCertificatePath(name + CERT_PEM);
        Path privateKeyPath = getPrivateKeyPath(name + PK_PEM);
        return PrivateKeyAndCert.load(certificatePath, privateKeyPath);
    }

    public boolean writePrivateKeyAndCertificate(String name, PrivateKeyAndCert privateKeyAndCert) {
        if (privateKeyAndCert != null) {
            return doWithConfigWriteAccess(
                    () -> write(getCertificatePath(name + CERT_PEM), privateKeyAndCert.getCertificate())
                            && write(getPrivateKeyPath(name + PK_PEM), privateKeyAndCert.getPrivateKey()));
        } else {
            LOGGER.error("Private Key and certificate are null!");
            return false;
        }
    }

    public Path getConfigPath() {
        return configPath;
    }

    public Path getTruststorePath() {
        return truststore;
    }

    public Path getKeystorePath() {
        return privateKeysStore;
    }

    private Path getCertificatePath(String fileName) {
        return truststore.resolve(fileName);
    }

    private Path getPrivateKeyPath(String fileName) {
        return privateKeysStore.resolve(fileName);
    }

    private boolean write(Path target, String value) {
        if (value == null) {
            return true;
        }
        try {
            Files.deleteIfExists(target);
            Path file = Files.createFile(target);
            Files.write(file, value.getBytes(StandardCharsets.UTF_8));
            return true;
        } catch (IOException ioe) {
            LOGGER.error("Error writing to {}", target, ioe);
            return false;
        }
    }

    private void changeWritePermissions(boolean writable) {
        String type = writable ? "writable" : "readonly";
        if (!Files.exists(configPath)) {
            return;
        }
        try (Stream<Path> paths = Files.walk(configPath, 3)) {
            if (paths.map(Path::toFile).allMatch(f -> f.setWritable(writable))) {
                LOGGER.debug("Configuration permissions changed to {} : {}", type, configPath);
            } else {
                LOGGER.warn("Failed ot change configuration permissions to {} : {}", type, configPath);
            }
        } catch (IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    public void makeConfigWritable() {
        changeWritePermissions(true);
    }

    private void makeConfigReadonly() {
        changeWritePermissions(false);
    }
}

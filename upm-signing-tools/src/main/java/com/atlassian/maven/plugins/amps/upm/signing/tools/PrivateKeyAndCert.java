package com.atlassian.maven.plugins.amps.upm.signing.tools;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.openssl.PKCS8Generator;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.bc.BcContentSignerBuilder;
import org.bouncycastle.operator.bc.BcEdECContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Private key and certificate pair used to sign and check custom plugins. The pair can be used to build a
 * SignatureBuilder to sign plugins. The certificate can be written to the Truststore using UpmConfigWriter.
 */
public class PrivateKeyAndCert {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrivateKeyAndCert.class);

    private final String privateKey;
    private final String certificate;

    private static final Random RANDOM = new SecureRandom();

    public PrivateKeyAndCert(String privateKey, String certificate) {
        this.privateKey = privateKey;
        this.certificate = certificate;
    }

    public static PrivateKeyAndCert load(Path certificatePath, Path privateKeyPath) {
        boolean hasCertificate = isReadable(certificatePath);
        boolean hasPrivateKey = isReadable(privateKeyPath);
        if (!hasCertificate) {
            LOGGER.debug("Certificate file {} is absent or unreadable", certificatePath);
        }
        if (!hasPrivateKey) {
            LOGGER.debug("Private key file {} is absent or unreadable", privateKeyPath);
        }

        if (!hasCertificate && !hasPrivateKey) {
            LOGGER.warn(
                    "PrivateKeyAndCert failed to load : {} and {} are both absent or unreadable",
                    certificatePath,
                    privateKeyPath);
            return null;
        }
        try {
            String cert = readString(certificatePath);
            String pk = readString(privateKeyPath);
            return new PrivateKeyAndCert(pk, cert);
        } catch (Exception e) {
            LOGGER.warn(
                    "Exception loading PrivateKeyAndCert from {}, {} : {}",
                    certificatePath,
                    privateKeyPath,
                    e.getMessage());
            return null;
        }
    }

    private static boolean isReadable(Path path) {
        return path != null && Files.isRegularFile(path) && Files.isReadable(path);
    }

    private static String readString(Path path) throws IOException {
        return isReadable(path) ? new String(Files.readAllBytes(path), StandardCharsets.UTF_8) : null;
    }

    public String getCertificate() {
        return certificate;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public Optional<Ed25519Signer> buildSigner() {
        if (privateKey == null) {
            LOGGER.warn("Failed to create Signer : key is null");
            return Optional.empty();
        }
        StringReader reader = new StringReader(privateKey);
        try (PemReader pemReader = new PemReader(reader)) {
            PemObject pemObject = pemReader.readPemObject();
            byte[] privateKeyBytes = pemObject.getContent();
            Ed25519PrivateKeyParameters pKey =
                    (Ed25519PrivateKeyParameters) PrivateKeyFactory.createKey(privateKeyBytes);
            Ed25519Signer signer = new Ed25519Signer();
            signer.init(true, pKey);
            return Optional.of(signer);
        } catch (IOException ioe) {
            LOGGER.warn("Failed to create Signer", ioe);
            return Optional.empty();
        }
    }

    /** Generate a self signed X509 certificate with Bouncy Castle. */
    public static PrivateKeyAndCert generateSelfSignedX509Certificate(
            ZonedDateTime from, ZonedDateTime to, String issuer) {
        try {
            // Using SecureRandom for test purposes only, enforce strong randomness source for production
            // See: https://docs.oracle.com/javase/8/docs/technotes/guides/security/SunProviders.html#SecureRandomImp
            Ed25519PrivateKeyParameters privateKeyParams = new Ed25519PrivateKeyParameters(new SecureRandom());

            // Not relying on PrivateKeyInfoFactory#createPrivateKeyInfo() here as it adds extra attributes which makes
            // the generated keys incompatible with openssl (tested with openssl 3.3.2).
            // See:
            // https://github.com/bcgit/bc-java/blob/main/core/src/main/java/org/bouncycastle/crypto/util/PrivateKeyInfoFactory.java#L203
            PrivateKeyInfo privateKeyInfo = new PrivateKeyInfo(
                    new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed25519),
                    new DEROctetString(privateKeyParams.getEncoded()));

            String privateKey = writePrivateKey(privateKeyInfo);

            Ed25519PublicKeyParameters publicKeyParams = privateKeyParams.generatePublicKey();
            SubjectPublicKeyInfo publicKeyInfo =
                    SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(publicKeyParams);

            X500Name dnName = new X500Name("CN=" + issuer);
            X500Name subject = new X500Name("CN=" + issuer);

            BigInteger serial = BigInteger.valueOf(RANDOM.nextLong());
            X509v3CertificateBuilder certGen = new X509v3CertificateBuilder(
                    dnName, serial, Date.from(from.toInstant()), Date.from(to.toInstant()), subject, publicKeyInfo);

            JcaX509ExtensionUtils extensionUtils = new JcaX509ExtensionUtils();
            AuthorityKeyIdentifier authorityKeyIdentifier = extensionUtils.createAuthorityKeyIdentifier(publicKeyInfo);

            certGen.addExtension(Extension.basicConstraints, true, new BasicConstraints(true));
            certGen.addExtension(
                    Extension.subjectKeyIdentifier, false, new SubjectKeyIdentifier(publicKeyInfo.getEncoded()));
            certGen.addExtension(Extension.authorityKeyIdentifier, false, authorityKeyIdentifier);
            certGen.addExtension(
                    Extension.keyUsage, true, new KeyUsage(KeyUsage.digitalSignature | KeyUsage.dataEncipherment));
            BcContentSignerBuilder signerBuilder =
                    new BcEdECContentSignerBuilder(new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed25519));
            ContentSigner sigGen = signerBuilder.build(privateKeyParams);
            X509CertificateHolder certificateHolder = certGen.build(sigGen);
            return new PrivateKeyAndCert(
                    privateKey,
                    convertX509CertToPem(new JcaX509CertificateConverter().getCertificate(certificateHolder)));
        } catch (Exception e) {
            throw new IllegalStateException("Error creating certificate and private key", e);
        }
    }

    private static String writePrivateKey(PrivateKeyInfo privateKeyInfo) throws IOException {
        StringWriter writer = new StringWriter();
        try (PemWriter privateWriter = new PemWriter(writer)) {
            privateWriter.writeObject(new PKCS8Generator(privateKeyInfo, null).generate());
        }
        return writer.toString();
    }

    private static String convertX509CertToPem(X509Certificate certificate) throws CertificateEncodingException {
        return "-----BEGIN CERTIFICATE-----\n"
                + org.bouncycastle.util.encoders.Base64.toBase64String(certificate.getEncoded())
                + "\n-----END CERTIFICATE-----";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrivateKeyAndCert)) return false;
        PrivateKeyAndCert that = (PrivateKeyAndCert) o;
        return Objects.equals(getPrivateKey(), that.getPrivateKey())
                && Objects.equals(getCertificate(), that.getCertificate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrivateKey(), getCertificate());
    }
}

package com.atlassian.maven.plugins.amps.upm.signing.tools;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PrivateKeyAndCertTest {

    static Path certificate_does_not_exist;
    static Path privateKey_does_not_exist;
    static Path certificate_exists;
    static Path privateKey_exists;

    @BeforeAll
    static void prepare() throws URISyntaxException, IOException {
        Path target = Paths.get(Objects.requireNonNull(UpmConfigWriterTest.class.getResource("/"))
                        .toURI())
                .getParent();
        certificate_does_not_exist = target.resolve("idonotexist-cert.pem");
        privateKey_does_not_exist = target.resolve("idonotexist-pk.pem");
        certificate_exists = target.resolve("exists-cert.pem");
        privateKey_exists = target.resolve("exists-pk.pem");

        PrivateKeyAndCert privateKeyAndCert = PrivateKeyAndCert.generateSelfSignedX509Certificate(
                ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), "ISSUER");
        Files.write(certificate_exists, privateKeyAndCert.getCertificate().getBytes(StandardCharsets.UTF_8));
        Files.write(privateKey_exists, privateKeyAndCert.getPrivateKey().getBytes(StandardCharsets.UTF_8));
    }

    @AfterAll
    static void tearDown() throws IOException {
        Files.deleteIfExists(certificate_exists);
        Files.deleteIfExists(privateKey_exists);
    }

    public static Stream<Arguments> shouldNotLoadIfBothFilesDoNotExist() {
        return Stream.of(
                Arguments.of(certificate_does_not_exist, privateKey_does_not_exist),
                Arguments.of(certificate_does_not_exist, null),
                Arguments.of(null, privateKey_does_not_exist),
                Arguments.of(null, null));
    }

    @ParameterizedTest
    @MethodSource
    void shouldNotLoadIfBothFilesDoNotExist(Path certificate, Path privateKey) {
        Assertions.assertThat(PrivateKeyAndCert.load(certificate, privateKey)).isNull();
    }

    public static Stream<Arguments> shouldLoadPrivateKeyAndOrCertificateFiles() {
        return Stream.of(
                Arguments.of(certificate_does_not_exist, privateKey_exists, false),
                Arguments.of(null, privateKey_exists, false),
                Arguments.of(certificate_exists, privateKey_does_not_exist, true),
                Arguments.of(certificate_exists, null, true),
                Arguments.of(certificate_exists, privateKey_exists, false));
    }

    @ParameterizedTest
    @MethodSource
    void shouldLoadPrivateKeyAndOrCertificateFiles(Path certificate, Path privateKey, boolean empty) {
        PrivateKeyAndCert loaded = PrivateKeyAndCert.load(certificate, privateKey);
        Assertions.assertThat(loaded).isNotNull();
        Optional<Ed25519Signer> signer = loaded.buildSigner();
        if (empty) {
            Assertions.assertThat(signer).isEmpty();
        } else {
            Assertions.assertThat(signer).isNotEmpty();
        }
    }
}

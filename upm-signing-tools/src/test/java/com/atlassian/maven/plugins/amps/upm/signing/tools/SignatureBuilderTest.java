package com.atlassian.maven.plugins.amps.upm.signing.tools;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SignatureBuilderTest {

    private final String data =
            "To be, or not to be : that is the question :\n" + "Whether 'tis nobler in the mind to suffer\n"
                    + "The slings and arrows of outrageous fortune,\n"
                    + "Or to take arms against a sea of troubles,\n"
                    + "And by opposing end them ? To die : to sleep ;\n"
                    + "No more ; and by a sleep to say we end\n"
                    + "The heart-ache and the thousand natural shocks\n"
                    + "That flesh is heir to, 'tis a consummation\n"
                    + "Devoutly to be wish'd. To die, to sleep ;\n"
                    + "To sleep : perchance to dream : ay, there's the rub ;\n"
                    + "For in that sleep of death what dreams may come\n"
                    + "When we have shuffled off this mortal coil,\n"
                    + "Must give us pause : there's the respect\n"
                    + "That makes calamity of so long life;\n"
                    + "For who would bear the whips and scorns of time,\n"
                    + "The oppressor's wrong, the proud man's contumely,\n"
                    + "The pangs of despised love, the law's delay,\n"
                    + "The insolence of office and the spurns\n"
                    + "That patient merit of the unworthy takes,\n"
                    + "When he himself might his quietus make\n"
                    + "With a bare bodkin ? who would fardels bear,\n"
                    + "To grunt and sweat under a weary life,\n"
                    + "But that the dread of something after death,\n"
                    + "The undiscover'd country from whose bourn\n"
                    + "No traveller returns, puzzles the will\n"
                    + "And makes us rather bear those ills we have\n"
                    + "Than fly to others that we know not of ?\n"
                    + "Thus conscience does make cowards of us all ;\n"
                    + "And thus the native hue of resolution\n"
                    + "Is sicklied o'er with the pale cast of thought,\n"
                    + "And enterprises of great pith and moment\n"
                    + "With this regard their currents turn awry,\n"
                    + "And lose the name of action";

    final String privateKey = "-----BEGIN PRIVATE KEY-----\n"
            + "MC4CAQAwBQYDK2VwBCIEIHGcwoP6SYxnBMejlwY5O8UyNoW504u2LerEvAoNRQvt\n"
            + "-----END PRIVATE KEY-----";

    final String signature = "2EQu/Tetsbvzsjh6hXt75BdmKZD+vm4HJaf5j6TbRoM9RlwRr9+QqY3z+o8bNvd7UVroTwTXg3HVOj3+cwxPAg==";

    @Test
    void shouldForgeCorrectSignature() {
        SignatureBuilder builder = new SignatureBuilder(new PrivateKeyAndCert(privateKey, null));
        Assertions.assertThat(builder.forgeSignature(data.getBytes(StandardCharsets.UTF_8)))
                .isEqualTo(signature);
    }

    @Test
    void shouldBuildSignatureBuilder() {
        SignatureBuilder builder = SignatureBuilder.builder("Issuer")
                .from(ZonedDateTime.now())
                .to(ZonedDateTime.now().plusDays(1))
                .build();

        Assertions.assertThat(builder).isNotNull();
        Assertions.assertThat(builder.getPrivateKeyAndCert()).isNotNull();
    }

    @Test
    void shouldSignData() throws URISyntaxException, IOException {
        SignatureBuilder builder = SignatureBuilder.builder("Issuer")
                .from(ZonedDateTime.now())
                .to(ZonedDateTime.now().plusDays(1))
                .build();

        String signature = builder.forgeSignature(data.getBytes(StandardCharsets.UTF_8));
        Assertions.assertThat(signature).isNotEmpty();

        Path file = Paths.get(SignatureBuilderTest.class
                        .getClassLoader()
                        .getResource("")
                        .toURI())
                .getParent()
                .resolve("data.txt");

        Files.write(file, data.getBytes(StandardCharsets.UTF_8));

        String sig2 = builder.forgeSignature(file.toFile());
        Assertions.assertThat(sig2).isEqualTo(signature);
    }

    @Test
    void shouldBuildSignatureBuilderWithNullCertificate() {
        SignatureBuilder builder = new SignatureBuilder(null);
        Assertions.assertThat(builder).isNotNull();
        Assertions.assertThat(builder.getPrivateKeyAndCert()).isNull();
        Assertions.assertThat(builder.forgeSignature(data.getBytes(StandardCharsets.UTF_8)))
                .isNull();
    }
}

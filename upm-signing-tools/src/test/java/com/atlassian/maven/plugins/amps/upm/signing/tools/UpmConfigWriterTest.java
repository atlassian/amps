package com.atlassian.maven.plugins.amps.upm.signing.tools;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class UpmConfigWriterTest {

    private static Path parent;

    @BeforeAll
    static void prepare() throws URISyntaxException {
        parent = Paths.get(Objects.requireNonNull(UpmConfigWriterTest.class.getResource("/"))
                .toURI());
    }

    @Test
    void shouldFailToCreateConfiguration() throws IOException {
        try {
            Files.createFile(parent.resolve("test"));
            Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
                    .isThrownBy(() -> new UpmConfigWriter(parent.resolve("test")));
        } finally {
            Files.deleteIfExists(parent.resolve("test"));
        }
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void shouldCreateConfigurationIfOverwriteOrNonExisting(boolean overwrite) throws IOException {
        UpmConfigWriter writer = new UpmConfigWriter(parent.resolve("test"));

        try {
            Path config = Files.createDirectory(writer.getConfigPath());
            Path dummy = Files.createFile(writer.getConfigPath().resolve("dummy.file"));
            Files.write(
                    config.resolve(UpmConfigWriter.CONFIG_FILENAME),
                    "preexisting.file".getBytes(StandardCharsets.UTF_8));

            boolean written = writer.createConfig(overwrite, EnumSet.allOf(AtlassianCertificate.class));
            Assertions.assertThat(written).isEqualTo(overwrite);
            Assertions.assertThat(config).exists().isDirectory();
            Assertions.assertThat(config.resolve(UpmConfigWriter.TRUSTSTORE_FOLDER))
                    .isDirectory();
            Assertions.assertThat(config.resolve(UpmConfigWriter.PK_FOLDER)).isDirectory();
            Assertions.assertThat(config.resolve(UpmConfigWriter.CONFIG_FILENAME))
                    .isRegularFile();

            if (overwrite) {
                Assertions.assertThat(dummy).doesNotExist();
                Assertions.assertThat(config.resolve(UpmConfigWriter.CONFIG_FILENAME))
                        .content()
                        .contains("atlassian.upm.signature.check.enabled=true");
            } else {
                Assertions.assertThat(dummy).exists();
                Assertions.assertThat(config.resolve(UpmConfigWriter.CONFIG_FILENAME))
                        .content()
                        .contains("preexisting.file");
            }
        } finally {
            writer.deleteConfiguration();
        }
    }

    @Test
    void shouldCopyCertificates() throws IOException {
        UpmConfigWriter writer = new UpmConfigWriter(parent.resolve("test"));
        writer.createConfig(true, EnumSet.allOf(AtlassianCertificate.class));
        Path truststore = writer.getTruststorePath();
        Assertions.assertThat(truststore).exists().isDirectory();
        try (Stream<Path> certificates = Files.walk(truststore, 1)) {

            List<String> names = certificates
                    .filter(Files::isRegularFile)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toList());
            Assertions.assertThat(names)
                    .containsExactlyInAnyOrder(
                            "atl_intermediate_dev.pem",
                            "atl_root_dev.pem",
                            "atlassian_mpac_intermediate_ca_v1.crt",
                            "atlassian_mpac_root_ca_v1.crt");
        } finally {
            writer.deleteConfiguration();
        }
    }

    @Test
    void shouldWritePrivateKeyAndCertificate() {
        UpmConfigWriter writer = new UpmConfigWriter(parent.resolve("test"));
        try {
            PrivateKeyAndCert privateKeyAndCert = PrivateKeyAndCert.generateSelfSignedX509Certificate(
                    ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), "DUMMY");
            PrivateKeyAndCert onlyCertificate = new PrivateKeyAndCert(null, privateKeyAndCert.getCertificate());
            PrivateKeyAndCert onlyPrivateKey = new PrivateKeyAndCert(privateKeyAndCert.getPrivateKey(), null);

            writer.createConfig(true, EnumSet.allOf(AtlassianCertificate.class));
            Assertions.assertThat(writer.writePrivateKeyAndCertificate("TEST", privateKeyAndCert))
                    .isTrue();
            Assertions.assertThat(writer.writePrivateKeyAndCertificate("ONLY_CERTIFICATE", onlyCertificate))
                    .isTrue();
            Assertions.assertThat(writer.writePrivateKeyAndCertificate("ONLY_PRIVATEKEY", onlyPrivateKey))
                    .isTrue();

            Path truststore = writer.getTruststorePath();
            Assertions.assertThat(truststore.resolve("TEST" + UpmConfigWriter.CERT_PEM))
                    .isRegularFile();
            Assertions.assertThat(truststore.resolve("ONLY_CERTIFICATE" + UpmConfigWriter.CERT_PEM))
                    .isRegularFile();
            Assertions.assertThat(truststore.resolve("ONLY_PRIVATEKEY" + UpmConfigWriter.PK_PEM))
                    .doesNotExist();

            Path privateKeyStore = writer.getConfigPath().resolve(UpmConfigWriter.PK_FOLDER);
            Assertions.assertThat(privateKeyStore.resolve("TEST" + UpmConfigWriter.PK_PEM))
                    .isRegularFile();
            Assertions.assertThat(privateKeyStore.resolve("ONLY_CERTIFICATE" + UpmConfigWriter.PK_PEM))
                    .doesNotExist();
            Assertions.assertThat(privateKeyStore.resolve("ONLY_PRIVATEKEY" + UpmConfigWriter.PK_PEM))
                    .isRegularFile();
        } finally {
            writer.deleteConfiguration();
        }
    }

    @Test
    void shouldLoadPrivateKeyAndCertificate() {
        UpmConfigWriter writer = new UpmConfigWriter(parent.resolve("test"));
        try {
            PrivateKeyAndCert privateKeyAndCert = PrivateKeyAndCert.generateSelfSignedX509Certificate(
                    ZonedDateTime.now(), ZonedDateTime.now().plusDays(1), "DUMMY");
            PrivateKeyAndCert onlyCertificate = new PrivateKeyAndCert(null, privateKeyAndCert.getCertificate());
            PrivateKeyAndCert onlyPrivateKey = new PrivateKeyAndCert(privateKeyAndCert.getPrivateKey(), null);

            writer.createConfig(true, EnumSet.allOf(AtlassianCertificate.class));
            Assertions.assertThat(writer.writePrivateKeyAndCertificate("TEST", privateKeyAndCert))
                    .isTrue();
            Assertions.assertThat(writer.writePrivateKeyAndCertificate("ONLY_CERTIFICATE", onlyCertificate))
                    .isTrue();
            Assertions.assertThat(writer.writePrivateKeyAndCertificate("ONLY_PRIVATEKEY", onlyPrivateKey))
                    .isTrue();

            Assertions.assertThat(writer.getPrivateKeyAndCertificate("TEST")).isEqualTo(privateKeyAndCert);
            Assertions.assertThat(writer.getPrivateKeyAndCertificate("ONLY_CERTIFICATE"))
                    .isEqualTo(onlyCertificate);
            Assertions.assertThat(writer.getPrivateKeyAndCertificate("ONLY_PRIVATEKEY"))
                    .isEqualTo(onlyPrivateKey);
        } finally {
            writer.deleteConfiguration();
        }
    }
}
